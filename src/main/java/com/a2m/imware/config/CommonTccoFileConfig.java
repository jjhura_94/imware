package com.a2m.imware.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Configuration
@PropertySource("classpath:largefileupload.properties")
public class CommonTccoFileConfig {
    @Value("${path.default.uploaddir}")
    private String pathDefaultUploaddir;

    public String getPathDefaultUploaddir() {
        return pathDefaultUploaddir;
    }

    public void setPathDefaultUploaddir(String pathDefaultUploaddir) {
        this.pathDefaultUploaddir = pathDefaultUploaddir;
    }
    
    @Bean(name = "multipartResolver")
	public CommonsMultipartResolver  multipartResolver() {
		return new SmartMultipartResolver();
	}
}
