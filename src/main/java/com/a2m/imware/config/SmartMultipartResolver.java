package com.a2m.imware.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;

public class SmartMultipartResolver extends CommonsMultipartResolver {
	@Override
	public boolean isMultipart(HttpServletRequest request) {
		if ("1".equals(request.getHeader("Upload-check"))) {
			return super.isMultipart(request);
		}
		return false;
		
	}

}
