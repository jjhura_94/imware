package com.a2m.imware.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.a2m.imware.interceptor.MenuPermissionInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Value("${dir.upload.pdf}")
	private String pdfUploadDir;
	
	// NERO upload folder
    @Value("${path.default.uploaddir}")
    private String pathDefaultUploaddir;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(new MenuPermissionInterceptor());
	}
	
	@Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        registry.viewResolver(resolver);
    }
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){ 
		registry.addResourceHandler("/resources/**","/**")
        .addResourceLocations("/resources/", "file:" + pathDefaultUploaddir + "/", "file:" + pdfUploadDir + "/","/", "classpath:/images");
    }
	
}