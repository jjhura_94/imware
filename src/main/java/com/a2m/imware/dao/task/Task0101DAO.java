package com.a2m.imware.dao.task;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.task.TndmTaskAttach;
import com.a2m.imware.model.task.TndmTaskChecklist;
import com.a2m.imware.model.task.TndmTaskComment;
import com.a2m.imware.model.task.TndmTaskCommentAttach;
import com.a2m.imware.model.task.TndmTaskMgt;
import com.a2m.imware.model.task.TndmTaskRecipient;
import com.a2m.imware.model.task.TndmTaskReferrer;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface Task0101DAO {
    // TaskMgt

    List<TndmTaskMgt> searchTndmTaskMgt(Map<Object, Object> arg) throws SQLException;

    Integer countTndmTaskMgt(Map<Object, Object> arg) throws SQLException;

    TndmTaskMgt getTndmTaskMgt(Long id) throws SQLException;

    int deleteTndmTaskMgt(Long id) throws SQLException;

    int insertTndmTaskMgt(TndmTaskMgt arg) throws SQLException;

    int updateTndmTaskMgt(TndmTaskMgt arg) throws SQLException;

    // Task Attach
    List<TndmTaskAttach> searchTndmTaskAttach(Map<Object, Object> arg) throws SQLException;

    int countAttachment(Long taskId) throws SQLException;

    TndmTaskAttach getTndmTaskAttach(Long id) throws SQLException;

    int deleteTndmTaskAttach(String seq) throws SQLException;

    int insertTndmTaskAttach(TndmTaskAttach arg) throws SQLException;

    int updateTndmTaskAttach(TndmTaskAttach arg) throws SQLException;

    // Task referrer
    List<TndmTaskReferrer> searchTndmTaskReferrer(Map<Object, Object> arg) throws SQLException;

    TndmTaskReferrer getTndmTaskReferrer(Long id) throws SQLException;

    int deleteTndmTaskReferrer(Long id) throws SQLException;

    int insertTndmTaskReferrer(TndmTaskReferrer arg) throws SQLException;

    int updateTndmTaskReferrer(TndmTaskReferrer arg) throws SQLException;

    // Task recipient
    List<TndmTaskRecipient> searchTndmTaskRecipient(Map<Object, Object> arg) throws SQLException;

    TndmTaskRecipient getTndmTaskRecipient(Long id) throws SQLException;

    int deleteTndmTaskRecipient(Long id) throws SQLException;

    int insertTndmTaskRecipient(TndmTaskRecipient arg) throws SQLException;

    int updateTndmTaskRecipient(TndmTaskRecipient arg) throws SQLException;

    // Task Comment 
    List<TndmTaskComment> searchTndmTaskComment(Map<Object, Object> arg) throws SQLException;

    TndmTaskComment getTndmTaskComment(Long id) throws SQLException;

    int deleteTndmTaskComment(Long id) throws SQLException;

    int insertTndmTaskComment(TndmTaskComment arg) throws SQLException;

    int updateTndmTaskComment(TndmTaskComment arg) throws SQLException;

    // Delete
    int deleteTndmTaskAttachByTaskId(Long taskId) throws SQLException;

    int deleteTndmTaskReferrerByTaskId(Long taskId) throws SQLException;

    int deleteTndmTaskCommentByTaskId(Long taskId) throws SQLException;

    int deleteTndmTaskRecipientByTaskId(Long taskId) throws SQLException;

    int deleteTndmTaskCommentAttachByCommentId(Long commentId) throws SQLException;

    int deleteTndmTaskCommentByUpComId(Long id) throws SQLException;

    // Comment Attach
    List<TndmTaskCommentAttach> searchTndmTaskCommentAttach(Map<Object, Object> arg) throws SQLException;

    int deleteTndmTaskCommentAttach(Long id) throws SQLException;

    int insertTndmTaskCommentAttach(TndmTaskCommentAttach arg) throws SQLException;
    
    
    List searchTndmTaskChecklist(Map arg) throws SQLException;

    int updateFavourite(@Param("ids") List<Long> ids, @Param("favourite") boolean favourite);

    Map getTndmTaskChecklist(Long id) throws SQLException;

    int deleteTndmTaskChecklist(Long id) throws SQLException;
 
    int insertTndmTaskChecklist(TndmTaskChecklist arg) throws SQLException;

    int updateTndmTaskChecklist(TndmTaskChecklist arg) throws SQLException;
    
    int countTaskNew(Map<Object, Object> arg) throws SQLException;


}
