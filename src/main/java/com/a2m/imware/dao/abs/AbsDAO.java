package com.a2m.imware.dao.abs;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface AbsDAO {

    List<Map<Object, Object>> list(Map arg) throws SQLException;

    Map<Object, Object> findById(String userId) throws SQLException;

    int insert(Map<Object, Object> data) throws SQLException;

    int updateByUserId(Map<Object, Object> data) throws SQLException;

}
