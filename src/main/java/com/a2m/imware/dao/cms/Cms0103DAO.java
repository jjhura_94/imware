
package com.a2m.imware.dao.cms;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Cms0103DAO {

	List<Map<Object, Object>> searchTslgLoginState(Map<Object, Object> arg) throws SQLException;

	int count(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchTslgLoginState2(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchTslgLoginState3(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTslgLoginState(Map<Object, Object> id) throws SQLException;

	int deleteTslgLoginState(Map<Object, Object> id) throws SQLException;

	int insertTslgLoginState(Map<Object, Object> arg) throws SQLException;

	int updateSessInfo(Map<Object, Object> arg) throws SQLException;
}
