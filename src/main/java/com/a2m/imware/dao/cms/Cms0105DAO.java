package com.a2m.imware.dao.cms;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Cms0105DAO {

    List<Map<Object, Object>> search(Map<Object, Object> arg) throws SQLException;

    int count(Map<Object, Object> arg) throws SQLException;

    Map<Object, Object> findById(Long id) throws SQLException;

    List<Map<Object, Object>> getDateVacation(Map<Object, Object> arg) throws SQLException;

    int delete(Map<Object, Object> arg) throws SQLException;

    int update(Map<Object, Object> arg) throws SQLException;

    int insert(Map<Object, Object> arg) throws SQLException;

    List<Map<Object, Object>> getListSendMessage() throws SQLException;

}
