package com.a2m.imware.dao.cms;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Cms010402DAO {

	List<Map<Object, Object>> searchTcdsEquipBooking(Map<Object, Object> arg) throws SQLException;

	int count(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsEquipBooking(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsEquipBooking(Map<Object, Object> arg) throws SQLException;

	int insertTcdsEquipBooking(Map<Object, Object> arg) throws SQLException;

	int updateTcdsEquipBooking(Map<Object, Object> arg) throws SQLException;
}
