package com.a2m.imware.dao.cms;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Cms010401DAO {

	List<Map<Object, Object>> searchTcdsRoomBooking(Map<Object, Object> arg) throws SQLException;
	
	int count(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsRoomBooking(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsRoomBooking(Map<Object, Object> arg) throws SQLException;

	int insertTcdsRoomBooking(Map<Object, Object> arg) throws SQLException;

	int updateTcdsRoomBooking(Map<Object, Object> arg) throws SQLException;
}
