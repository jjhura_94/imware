
package com.a2m.imware.dao.cms;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Cms0102DAO {

	List<Map<Object, Object>> searchTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException;

	int insertTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException;

	int updateTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException;

	int insertTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException;

	int updateTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchTcdsEmpVacationRequest2(Map<Object, Object> arg) throws SQLException;

	int countVacationRequest(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchVRForUsageHistory(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchVAForApproval2(Map<Object, Object> arg) throws SQLException;

	int countVRForUsageHistory(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException;

	int insertTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException;

	int updateTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException;

	String getDeptHead(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTheLastVR(Map<Object, Object> arg) throws SQLException;

	String getDeptCodeByUserUid(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> searchVAForApproval(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getVIById(Map<Object, Object> arg) throws SQLException;

	int countDeptHead(Map<Object, Object> arg) throws SQLException;

	int countUsedDays(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getListDeptHead(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAllDeptCodeByUserUid1(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAllDeptCodeByUserUid2(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAllUserUidByDeptCode(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAllDeptHeadByDeptCode(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAllDeptCodeByUpDeptCode(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getAllInfoByUserUid(Map<Object, Object> arg) throws SQLException;
	
	Map<Object, Object> getVRForUsageHistory(Map<Object, Object> arg) throws SQLException;

}
