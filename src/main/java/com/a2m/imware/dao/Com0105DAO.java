package com.a2m.imware.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Com0105DAO {

    List<Map<Object, Object>> search(Map<Object, Object> filter) throws Exception;

    Map<Object, Object> findById(Long id) throws Exception;

    int insert(Map<Object, Object> data) throws Exception;

    int update(Map<Object, Object> data) throws Exception;

    int count(Map<Object, Object> filter) throws Exception;

    List<Map<Object, Object>> findAttachment(Long noticeId) throws Exception;

    int insertAttachment(Map<Object, Object> data) throws Exception;

    List<Map<Object, Object>> findCommentLevel1(Long suggestId) throws Exception;

    List<Map<Object, Object>> findCommentLevel2(Map<Object, Object> data) throws Exception;

    int insertComment(Map<Object, Object> data) throws Exception;

    int updateComment(Map<Object, Object> data) throws Exception;

    void deleteComment(Long id);

}
