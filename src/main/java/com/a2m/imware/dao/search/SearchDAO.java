package com.a2m.imware.dao.search;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SearchDAO {
	List<Map<Object, Object>> getListElectronicPaymentWithConditions(Map<Object, Object> args) throws Exception;
	
	int getCountElectronicPaymentWithConditions(Map<Object, Object> args) throws Exception;
	
	List<Map<Object, Object>> getListBusinessManagementWithConditions(Map<Object, Object> args) throws Exception;
	
	int getCountBusinessManagementWithConditions(Map<Object, Object> args) throws Exception;
	
	List<Map<Object, Object>> getListCommunitiesWithConditions(Map<Object, Object> args) throws Exception;
	
	int getCountCommunitiesWithConditions(Map<Object, Object> args) throws Exception;
	
	List<Map<Object, Object>> getListMyPageWithConditions(Map<Object, Object> args) throws Exception;
	
	int getCountMyPageWithConditions(Map<Object, Object> args) throws Exception;
	
}
