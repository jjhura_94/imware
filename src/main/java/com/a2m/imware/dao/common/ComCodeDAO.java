
package com.a2m.imware.dao.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ComCodeDAO {
    List searchTccoStd(Map arg) throws SQLException;

    Map getTccoStd(String id) throws SQLException;

    int deleteTccoStd(String id) throws SQLException;
 
    int insertTccoStd(Map arg) throws SQLException;

    int updateTccoStd(Map arg) throws SQLException;

    List getTccoStds(String upCommCd);
}
