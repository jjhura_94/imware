
package com.a2m.imware.dao.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.apv.TndmReferrerFavorite;
import com.a2m.imware.model.apv.TndmReferrerFavoriteDetail;

import java.util.List;

@Mapper
public interface ReferrerDAO {
	List<TndmReferrerFavorite> searchTndmReferrerFavorite(Map arg) throws SQLException;

    Map getTndmReferrerFavorite(Long id) throws SQLException;

    int deleteTndmReferrerFavorite(Long id) throws SQLException;
 
    int insertTndmReferrerFavorite(TndmReferrerFavorite arg) throws SQLException;

    int updateTndmReferrerFavorite(TndmReferrerFavorite arg) throws SQLException;
    
    
    
    List searchTndmReferrerFavoriteDetail(Map arg) throws SQLException;

    Map getTndmReferrerFavoriteDetail(Long id) throws SQLException;

    int deleteTndmReferrerFavoriteDetail(Long id) throws SQLException;
 
    int insertTndmReferrerFavoriteDetail(TndmReferrerFavoriteDetail arg) throws SQLException;

    int updateTndmReferrerFavoriteDetail(TndmReferrerFavoriteDetail arg) throws SQLException;
}
