
package com.a2m.imware.dao.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.common.TndmComment;


@Mapper
public interface TndmCommentDAO {
	
    List searchTndmComment(Map arg) throws SQLException;

    Map getTndmComment(Long id) throws SQLException;

    int deleteTndmComment(Long id) throws SQLException;
 
    int insertTndmComment(TndmComment arg) throws SQLException;

    int updateTndmComment(TndmComment arg) throws SQLException;
}
