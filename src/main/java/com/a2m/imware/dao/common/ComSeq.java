package com.a2m.imware.dao.common;

import java.sql.SQLException;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ComSeq {
	
	Object setSeq(String seqName) throws SQLException;
	
	String getSeq() throws SQLException;
}
