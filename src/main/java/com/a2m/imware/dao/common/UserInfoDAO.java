package com.a2m.imware.dao.common;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.util.ImwareException;
import java.util.List;

@Mapper
public interface UserInfoDAO {

    Map<Object, Object> getUserInfo(String userId) throws ImwareException;

    Map<Object, Object> getUserById(Long empNo) throws ImwareException;

    String getUserIdFromToken(String token) throws ImwareException;

    List getUserIds() throws ImwareException;

    List getEmpNo(List<String> userIds);

    void saveLoginInfo(Map<Object, Object> loginInfo) throws ImwareException;

    void saveLogoutInfo(Map<Object, Object> logoutInfo) throws ImwareException;

    void updateFirstLogin(String userUid) throws Exception;
}
