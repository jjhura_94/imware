package com.a2m.imware.dao.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TsstDateWorkingDAO {
	
	List<Map<Object, Object>> searchTsstDateWorking(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTsstDateWorking(Map<Object, Object> arg) throws SQLException;

	int deleteTsstDateWorking(Map<Object, Object> arg) throws SQLException;

	int insertTsstDateWorking(Map<Object, Object> arg) throws SQLException;

	int updateTsstDateWorking(Map<Object, Object> arg) throws SQLException;
}
