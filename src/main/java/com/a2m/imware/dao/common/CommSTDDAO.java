package com.a2m.imware.dao.common;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.util.ImwareException;

@Mapper
public interface CommSTDDAO {
	
	List<Map<Object, Object>> getListComm(Map<Object, Object> agr) throws ImwareException;
}
