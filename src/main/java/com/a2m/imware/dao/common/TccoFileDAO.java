package com.a2m.imware.dao.common;

import java.util.List;
import java.util.Map;

import com.a2m.imware.model.TccoFile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TccoFileDAO {

	Map<Object, Object> findBySequence(String fileSequence) throws Exception;

	List<Map<Object, Object>> findBySequences(List<String> fileSequences) throws Exception;

	int insert(Map<Object, Object> data) throws Exception;

	int delete(List<String> fileSequences) throws Exception;
	
	// for testing...
	List<Map<Object, Object>> findTcdsEmpAttaches(String refId);
	
	int insertToTcdsEmpAttach(Map<Object, Object> data) throws Exception;
	
	int deleteFromTcdsEmpAttach(List<String> attachIdList) throws Exception;
	// end test

	int insertTccoFile(TccoFile tccoFile) throws Exception;

	int updateTccoFile(TccoFile tccoFile) throws Exception;
	
}
