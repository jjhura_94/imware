/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.dao.common;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @author Admin
 */
@Mapper
public interface CommonDeptDAO {

    Map<Object, Object> getDeptStructByDeptCode(String deptCode);

    List<Map<Object, Object>> getChirldrentDepartment(String deptCode, String structId);

    List getUserByDeptCodes(List<String> deptCodes);

    List getUidByDeptCodes(List<String> deptCodes);

    Map<String, String> getByDeptCodes(List<String> deptCodes);

}
