package com.a2m.imware.dao.apv;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import com.a2m.imware.model.apv.TndmApvlLineFavorite;
import com.a2m.imware.model.apv.TndmApvlLineFavoriteDetail;

@Mapper
@Service
public interface ApvLineProcessingDAO {
	
	List<TndmApvlLineFavorite> searchTndmApvlLineFavorite(Map<Object, Object> arg) throws SQLException;
	
	int deleteTndmApvlLineFavorite(Long id) throws SQLException;
	
	int insertTndmApvlLineFavorite(TndmApvlLineFavorite arg) throws SQLException;

	int updateTndmApvlLineFavorite(TndmApvlLineFavorite arg) throws SQLException;
	
	List<TndmApvlLineFavoriteDetail> searchTndmApvlLineFavoriteDetail(Map<Object, Object> arg) throws SQLException;
	
	int deleteTndmApvlLineFavoriteDetail(Long id) throws SQLException;
	
	int insertTndmApvlLineFavoriteDetail(TndmApvlLineFavoriteDetail arg) throws SQLException;

	int updateTndmApvlLineFavoriteDetail(TndmApvlLineFavoriteDetail arg) throws SQLException;
	
}
