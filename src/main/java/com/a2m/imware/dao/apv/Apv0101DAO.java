package com.a2m.imware.dao.apv;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.apv.TndmApvlHist;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.apv.TndmDrftBusTrip;
import com.a2m.imware.model.apv.TndmDrftBusTripDetail;
import com.a2m.imware.model.apv.TndmDrftReference;
import com.a2m.imware.model.apv.TndmDrftReferer;
import com.a2m.imware.model.request.DrftRequest;
import com.a2m.imware.model.request.TndmDocMstRequest;
import com.a2m.imware.model.request.TndmDrftAttachRequest;
import com.a2m.imware.model.request.TndmDrftMeetingMinuteRequest;
import com.a2m.imware.model.request.TndmDrftRequest;
import com.a2m.imware.model.request.TndmDrftSpendingRequest;
import com.a2m.imware.model.response.TndmDrftAttachResponse;
import com.a2m.imware.model.response.TndmDrftResponse;

@Mapper
public interface Apv0101DAO {

	List<TndmDrftResponse> searchTndmDrft(Map arg) throws SQLException;

	TndmDrftResponse getTndmDrft(String id) throws SQLException;

	int deleteTndmDrft(String id) throws SQLException;

	int insertTndmDrft(TndmDrftRequest arg) throws SQLException;

	int updateTndmDrft(TndmDrftRequest arg) throws SQLException;

	int count(Map arg) throws SQLException;

	int deleteTndmDocMst(String id) throws SQLException;

	int insertTndmDocMst(TndmDocMstRequest arg) throws SQLException;

	int updateTndmDocMst(TndmDocMstRequest arg) throws SQLException;

	TndmDocMstRequest getTndmDocMst(String arg) throws SQLException;

	int deleteTndmDrftMeetingMinute(String id) throws SQLException;

	int insertTndmDrftMeetingMinute(TndmDrftMeetingMinuteRequest arg) throws SQLException;

	int updateTndmDrftMeetingMinute(TndmDrftMeetingMinuteRequest arg) throws SQLException;

	int insertTndmDrftBusTrip(TndmDrftBusTrip arg) throws SQLException;

	int updateTndmDrftBusTrip(TndmDrftBusTrip arg) throws SQLException;

	int deleteTndmDrftBusTrip(String id) throws SQLException;

	int deleteTndmDrftSpending(String id) throws SQLException;

	int insertTndmDrftSpending(TndmDrftSpendingRequest arg) throws SQLException;

	int updateTndmDrftSpending(TndmDrftSpendingRequest arg) throws SQLException;

	List<TndmDrftAttachResponse> searchTndmDrftAttach(Map arg) throws SQLException;

	int deleteTndmDrftAttach(String drftDocNo) throws SQLException;

	int insertTndmDrftAttach(TndmDrftAttachRequest arg) throws SQLException;

	int updateTndmDrftAttach(TndmDrftAttachRequest arg) throws SQLException;

	int deleteTndmDrftAttachBySeq(String atchFleSeq) throws SQLException;

	DrftRequest getDrftRequest(String id) throws SQLException;

	List<TndmDrftApvlLine> searchTndmDrftApvlLine(Map arg) throws SQLException;

	TndmDrftApvlLine getTndmDrftApvlLine(Long id) throws SQLException;

	int deleteTndmDrftApvlLine(String drftDocNo) throws SQLException;

	int insertTndmDrftApvlLine(TndmDrftApvlLine arg) throws SQLException;

	int updateTndmDrftApvlLine(TndmDrftApvlLine arg) throws SQLException;

	List<TndmDrftReference> searchTndmDrftReference(Map arg) throws SQLException;

	int deleteTndmDrftReference(String drftDocNo) throws SQLException;

	int insertTndmDrftReference(TndmDrftReference arg) throws SQLException;

	int updateTndmDrftReference(TndmDrftReference arg) throws SQLException;

	List<TndmDrftReferer> searchTndmDrftReferer(Map arg) throws SQLException;

	int deleteTndmDrftReferer(String drftDocNo) throws SQLException;

	int insertTndmDrftReferer(TndmDrftReferer arg) throws SQLException;

	int updateTndmDrftReferer(TndmDrftReferer arg) throws SQLException;

	List searchTndmDrftWaitingApv(Map arg) throws SQLException;

	int countTndmDrftWaitingApv(Map arg) throws SQLException;

	// Apvl Hist
	List<TndmApvlHist> searchTndmApvlHist(Map<Object, Object> arg) throws SQLException;

	TndmApvlHist getTndmApvlHist(Long id) throws SQLException;

	int deleteTndmApvlHist(Map<Object, Object> arg) throws SQLException;

	int insertTndmApvlHist(TndmApvlHist arg) throws SQLException;

	int updateTndmApvlHist(TndmApvlHist arg) throws SQLException;

	String getMaxTagDoc(Map<Object, Object> arg) throws SQLException;

	// Approve document

	List<TndmDrftApvlLine> getDraftApvlLineList(Map<Object, Object> arg) throws SQLException;

	List<Map<Object, Object>> getAbsentById(Map<Object, Object> arg) throws SQLException;

	int changeApprovalType(Map<Object, Object> arg) throws SQLException;

	int insertDelegateApproval(Map<Object, Object> arg) throws SQLException;

	int insertApvlHist(Map<Object, Object> arg) throws SQLException;

	int updateDrftApvlLine(Map<Object, Object> arg) throws SQLException;

	int updateSancYnInDrftApvlLine(Map<Object, Object> arg) throws SQLException;

	int updateDraftStatus(Map<Object, Object> arg) throws SQLException;

	int updateDocMstForApproval(Map<Object, Object> arg) throws SQLException;

	int updateSancOrderNo(Map<Object, Object> arg) throws SQLException;

	List searchTndmFormMgt(Map arg) throws SQLException;

	Map getTndmTaskMgt(Long id) throws SQLException;

	void deleteDrftDoc(Map arg) throws SQLException;

    List<TndmDrftBusTripDetail> getTndmDrftBusTripDetail(Long id) throws SQLException;

	int deleteTndmDrftBusTripDetail(Long id) throws SQLException;

	int insertTndmDrftBusTripDetail(TndmDrftBusTripDetail arg) throws SQLException;

	int updateTndmDrftBusTripDetail(TndmDrftBusTripDetail arg) throws SQLException;

	Map<Object, Object> getTndmDrftCMS0102(Map<Object, Object> arg) throws SQLException;

}
