package com.a2m.imware.dao.apv;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.apv.TndmDocReceived;
import com.a2m.imware.model.apv.TndmDocReceivedAttach;

@Mapper
public interface Apv0107DAO {
	List searchTndmDocReceived(Map arg) throws SQLException;
	
	Integer countTndmDocReceived(Map arg) throws SQLException;
	
	TndmDocReceived getTndmDocReceived(Long id) throws SQLException;

    int deleteTndmDocReceived(String id) throws SQLException;
 
    int insertTndmDocReceived(TndmDocReceived arg) throws SQLException;

    int updateTndmDocReceived(TndmDocReceived arg) throws SQLException;
    
    List searchTndmDocReceivedAttach(Map arg) throws SQLException;

    Map getTndmDocReceivedAttach(String id) throws SQLException;

    int deleteTndmDocReceivedAttach(String id) throws SQLException;
 
    int insertTndmDocReceivedAttach(TndmDocReceivedAttach arg) throws SQLException;

    int updateTndmDocReceivedAttach(TndmDocReceivedAttach arg) throws SQLException;
}
