
package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpMstRequest;
import com.a2m.imware.model.response.TcdsEmpMstResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
public interface Sys0105DAO {
	List search(Map arg) throws SQLException;

	int insert(List<TcdsEmpMstRequest> tcdsEmpMstRequests) throws SQLException;

	int update(List<TcdsEmpMstRequest> tcdsEmpMstRequests) throws SQLException;

	int count(Map arg) throws SQLException;

	TcdsEmpMstResponse get(Long empNo);
	
	int deleteAllByEmpNos(List<Long> empNos) throws SQLException;

	int insert(TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException;

	int update(TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException;

	void delete(Long empNo);

	List<Map<Object, Object>> searchForVacation(Map<Object, Object> arg) throws SQLException;
	
	Map<Object, Object> getForVacation(Long empNo);
}
