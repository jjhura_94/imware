package com.a2m.imware.dao.sys;

import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
public interface CareerDAO {
    List getAllCareerByEmpNo(String EMP_NO) throws SQLException;

    void deleteAllByCareerIds(List<Long> CAREER_IDS);

    int insert(Map arg) throws SQLException;

    int update(Map arg) throws SQLException;
}
