package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpSwRequest;
import com.a2m.imware.model.response.TcdsEmpSwResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface Sys010506DAO {
    List getAllSwByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllBySwIds(List<Long> swIds);

    int insert(TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException;

    int update(TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException;
    
    TcdsEmpSwResponse getById(Long id) throws SQLException;

}
