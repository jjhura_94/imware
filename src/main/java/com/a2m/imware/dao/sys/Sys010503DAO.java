package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpAwardRequest;
import com.a2m.imware.model.response.TcdsEmpAwardResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface Sys010503DAO {
    List getAllAwardByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllByAwardIds(List<Long> awardIds);

    int insert(TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException;

    int update(TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException;
    
    TcdsEmpAwardResponse getById(Long id) throws SQLException;
}
