package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpEquipRequest;
import com.a2m.imware.model.response.TcdsEmpEquipResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface Sys010505DAO {

    List getAllEquipmentByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEquipmentIds(List<Long> equipIds);

    int insert(TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException;

    int update(TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException;

    TcdsEmpEquipResponse getById(Long id) throws SQLException;

}
