package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.request.TsstMenuRequest;

@Mapper
public interface Sys0101DAO {
	List getMenuByUser(Map arg) throws SQLException;
	
	List getAllMenu() throws SQLException;
	
	List searchTsstMenu(Map arg) throws SQLException;
	
	int insertTsstMenu(TsstMenuRequest arg) throws SQLException;
	
	String getMaxMenuId(String menuId) throws SQLException;
	
	Integer getMaxOrdNo() throws SQLException;
	
	int updateTsstMenu(TsstMenuRequest arg) throws SQLException;
	
	int activeOrBlockMenu(Map arg) throws SQLException;
	
	int deleteTsstMenu(String meuId) throws SQLException;
	
	int deleteTsstRoleMenu(String meuId) throws SQLException;
	
}
