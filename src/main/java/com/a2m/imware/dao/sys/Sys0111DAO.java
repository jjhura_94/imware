package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Sys0111DAO {

	List<Map<Object, Object>> searchTcdsEquipmentMgt(Map<Object, Object> arg) throws SQLException;
	
	int count(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsEquipmentMgt(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsEquipmentMgt(Map<Object, Object> arg) throws SQLException;

	int insertTcdsEquipmentMgt(Map<Object, Object> arg) throws SQLException;

	int updateTcdsEquipmentMgt(Map<Object, Object> arg) throws SQLException;
}
