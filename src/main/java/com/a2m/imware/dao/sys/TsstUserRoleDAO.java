package com.a2m.imware.dao.sys;

import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;

@Mapper
public interface TsstUserRoleDAO {
    void deleteAllByUserUid(String userUid) throws SQLException;
}
