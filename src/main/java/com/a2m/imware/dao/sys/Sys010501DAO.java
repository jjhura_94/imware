package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpEduRequest;
import com.a2m.imware.model.response.TcdsEmpEduResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface Sys010501DAO {
    List getAllEduByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEduIds(List<Long> eduIds);

    int insert(TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException;

    int update(TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException;
    
    TcdsEmpEduResponse getEduById(Long id) throws SQLException;
}
