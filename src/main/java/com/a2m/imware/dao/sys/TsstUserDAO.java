package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TsstUserRequest;
import com.a2m.imware.model.response.TsstUserResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface TsstUserDAO {
    TsstUserResponse findFirstTsstUserByEmpNo(Long empNo);

    List<TsstUserResponse> findAllTsstUserByEmpNo(Long empNo);

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    int insert(TsstUserRequest tsstUserRequest);

    int update(TsstUserRequest tsstUserRequest);

    Long countAllByUserIdAndUserUidNot(String userId, String userUidNot);

    Long countAllByUserIdAndPwd(String userId, String pwd);
    
    TsstUserResponse getByUserId(String userId);
}
