package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Sys010202DAO {

	List<Map<Object, Object>> getAllEdu(Map<Object, Object> arg) throws SQLException;

	int count(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getEduInfo(Map<Object, Object> arg) throws SQLException;

	int addEdu(Map<Object, Object> arg) throws SQLException;

	int updateEdu(Map<Object, Object> arg) throws SQLException;

	int deleteEdu(Map<Object, Object> arg) throws SQLException;
}