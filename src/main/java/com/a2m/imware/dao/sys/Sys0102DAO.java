package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Sys0102DAO {
	List getUserInfo(Map arg) throws SQLException;
	List searchUser(Map arg) throws SQLException;
	int addUser(Map arg) throws SQLException;
	int updateUser(Map arg) throws SQLException;
	int activeOrBlockUser(Map arg) throws SQLException;
}