package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpCertRequest;
import com.a2m.imware.model.response.TcdsEmpCertResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Mapper
public interface Sys010504DAO {
    List getAllCertificateByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllByCertificateIds(List<Long> certIds);
    
    int insert(TcdsEmpCertRequest tcdsEmpCertRequest) throws SQLException;

    int update(TcdsEmpCertRequest tcdsEmpCertRequest) throws SQLException;
    
    TcdsEmpCertResponse getById(Long id) throws SQLException;

}
