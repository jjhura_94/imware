
package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.TsstUserRole;
import com.a2m.imware.model.request.TsstRoleMenuRequest;
import com.a2m.imware.model.request.TsstRoleRequest;
import com.a2m.imware.model.response.TsstRoleMenuResponse;

import java.util.List;

@Mapper
public interface Sys0103DAO {
    List searchTsstRole(Map arg) throws SQLException;

    Map getTsstRole(String id) throws SQLException;

    int deleteTsstRole(String id) throws SQLException;
 
    int insertTsstRole(TsstRoleRequest arg) throws SQLException;

    int updateTsstRole(TsstRoleRequest arg) throws SQLException;
    
    String getMaxRoleId() throws SQLException;
    
    int deleteTsstRoles(String ids) throws SQLException;
    
    List searchTsstUserRole(Map arg) throws SQLException;
    
    int updateTsstUserRole(TsstUserRole userRole) throws SQLException;
    
    int insertTsstUserRole(TsstUserRole userRole) throws SQLException;
    
    int deleteTsstUserRole(TsstUserRole userRole) throws SQLException;
    
    List searchTsstRoleMenu(Map arg) throws SQLException;
    
    TsstRoleMenuResponse getTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException;
    
    int updateTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException;
    
    int insertTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException;
}
