package com.a2m.imware.dao.sys;

import com.a2m.imware.model.request.TcdsEmpCareerRequest;
import com.a2m.imware.model.response.TcdsEmpCareerResponse;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.List;

@Mapper
public interface Sys010502DAO {
    List getAllCareerByEmpNo(Long empNo) throws SQLException;

    void deleteAllByEmpNo(Long empNo) throws SQLException;

    void deleteAllByCareerIds(List<Long> careerIds);

    int insert(TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException;

    int update(TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException;
    
    TcdsEmpCareerResponse getById(Long id) throws SQLException;
}
