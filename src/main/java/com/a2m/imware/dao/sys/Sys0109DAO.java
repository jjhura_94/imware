package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.sys.TcdsDutyMgt;

@Mapper
public interface Sys0109DAO {
    List<Object> searchTcdsDutyMgt(Map<Object, Object> arg) throws SQLException;
    
    Integer countTcdsDutyMgt(Map<Object, Object> arg) throws SQLException;

    TcdsDutyMgt getTcdsDutyMgt(String id) throws SQLException;

    int deleteTcdsDutyMgt(String id) throws SQLException;
 
    int insertTcdsDutyMgt(TcdsDutyMgt arg) throws SQLException;

    int updateTcdsDutyMgt(TcdsDutyMgt arg) throws SQLException;
    
    String getMaxId() throws SQLException;
    
    Long countEmpByDutyCode(Map<Object, Object> arg) throws SQLException;
}
