
package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Sys0106DAO {
    List searchTsstRole(Map arg) throws SQLException;

    Map getTsstRole(String id) throws SQLException;

    int deleteTsstRole(String id) throws SQLException;
 
    int insertTsstRole(Map arg) throws SQLException;

    int updateTsstRole(Map arg) throws SQLException;
}
