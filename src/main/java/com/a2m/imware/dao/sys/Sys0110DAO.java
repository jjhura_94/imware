package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Sys0110DAO {
	List<Map<Object, Object>> searchTcdsMeetingRoomMgt(Map<Object, Object> arg) throws SQLException;
	
	int count(Map<Object, Object> arg) throws SQLException;

	Map<Object, Object> getTcdsMeetingRoomMgt(Map<Object, Object> arg) throws SQLException;

	int deleteTcdsMeetingRoomMgt(Map<Object, Object> arg) throws SQLException;

	int insertTcdsMeetingRoomMgt(Map<Object, Object> arg) throws SQLException;

	int updateTcdsMeetingRoomMgt(Map<Object, Object> arg) throws SQLException;
}
