package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.sys.TndmFormMgt;

@Mapper
public interface Sys0112DAO {
    List<TndmFormMgt> searchTndmFormMgt(Map<Object, Object> arg) throws SQLException;
    
    int getCount(Map<Object, Object> arg) throws SQLException;

    TndmFormMgt getTndmFormMgt(Long id) throws SQLException;

    int deleteTndmFormMgt(Long id) throws SQLException;
 
    int insertTndmFormMgt(TndmFormMgt arg) throws SQLException;

    int updateTndmFormMgt(TndmFormMgt arg) throws SQLException;
}
