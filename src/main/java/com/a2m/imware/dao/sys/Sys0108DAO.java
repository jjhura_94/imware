package com.a2m.imware.dao.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.a2m.imware.model.sys.TcdsDeptStruct;
import com.a2m.imware.model.sys.TcdsStructMgt;

@Mapper
public interface Sys0108DAO {
	List<TcdsStructMgt> searchTcdsStructMgt(Map<Object, Object> arg) throws SQLException;

	TcdsStructMgt getTcdsStructMgt(String id) throws SQLException;

	int deleteTcdsStructMgt(String id) throws SQLException;

	int insertTcdsStructMgt(TcdsStructMgt arg) throws SQLException;

	int updateTcdsStructMgt(TcdsStructMgt arg) throws SQLException;
	
	Long countStructMgt(Map<Object, Object> arg) throws SQLException;

	String getStructMaxId() throws SQLException;

	List<TcdsDeptStruct> searchTcdsDeptStruct(Map<Object, Object> arg) throws SQLException;
	
	Long countTcdsDeptStruct(Map<Object, Object> arg) throws SQLException;
	
	Long countEmpByDeptCode(Map<Object, Object> arg) throws SQLException;

	TcdsDeptStruct getTcdsOneDeptStructName(Map<Object, Object> arg) throws SQLException;

	List<TcdsDeptStruct> getTcdsDeptStruct(Map<Object, Object> id) throws SQLException;

	int deleteTcdsDeptStruct(Map<Object, Object> id) throws SQLException;

	int insertTcdsDeptStruct(TcdsDeptStruct arg) throws SQLException;

	int updateTcdsDeptStruct(TcdsDeptStruct arg) throws SQLException;

	int deleteTcdsDept(Map<Object, Object> id) throws SQLException;

	int insertTcdsDept(String id) throws SQLException;

	String getDeptMaxId() throws SQLException;

	List getDeptStruct(Map arg) throws SQLException;
	
	Map<Object, Object> getTcdsOneDeptStructName2(Map<Object, Object> arg) throws SQLException;

}
