package com.a2m.imware.wschat;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.a2m.imware.wschat.common.ObjectUtil;
import com.a2m.imware.wschat.manager.HttpSessionOutMgn;
import com.a2m.imware.wschat.manager.MessageModelCmdMng;
import com.a2m.imware.wschat.manager.MessageSender;
import com.a2m.imware.wschat.manager.SessionModelStateMng;
import com.a2m.imware.wschat.model.MessageModel;
import com.a2m.imware.wschat.util.ParameterUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class EchoHandler extends TextWebSocketHandler {

	private Logger logger = LoggerFactory.getLogger(EchoHandler.class);

	@Autowired
	MessageModelCmdMng mcm;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		logger.info("***** ECHO HANDLERS CONNECTION ESTABLISHED  *****");
		super.afterConnectionEstablished(session);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		// handle ping - pong
		if ("__ping__".equals(message.getPayload())) {
			session.sendMessage(new TextMessage("__pong__"));
			return;
		}

		logger.info("TEXT MSG - SESSION ID: " + session.getId());
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, Object> mapRecieve = mapper.readValue(message.getPayload(),
					new TypeReference<Map<String, Object>>() {
					});
			String userUid = ObjectUtil.getUSER_UID_OR_PERSON_ID(session);

			MessageModel mm = new MessageModel();

			logger.info("List out session: " + HttpSessionOutMgn.getInstance().toString());

			ParameterUtil.validParam(mapRecieve);
			mm.setCATEGORY((String) mapRecieve.get("CATEGORY"));
			mm.setTYPE((String) mapRecieve.get("TYPE"));
			mm.setPAYLOAD((Map<Object, Object>) mapRecieve.get("PAYLOAD"));

			if (userUid == null) {
				userUid = String.valueOf(mm.getPAYLOAD().get("USERNAME"));
			}
			mm.setUSER_UID(userUid);

			logger.info("__________________________________________________^");
			logger.info("CATE_TYPE: " + mm.getCATEGORY() + "_" + mm.getTYPE());
			// Save to database
			try {
				// MessageModelCmdMng mcm = new MessageModelCmdMng();
				mcm.setMessageModel(mm);
				mcm.setBusinessCommand(session, mm);
			} catch (Exception e) {
				e.printStackTrace();
				mm.setRESULT("ERROR");
				mm.setMSG(e.getMessage());
				MessageSender.sendMessageModel(mm.getUSER_UID(), mm);
				logger.info("ERROR: CATE_TYPE: " + mm.getCATEGORY() + "_" + mm.getTYPE() + " USER_UID: "
						+ mm.getUSER_UID() + " USER_UID_REC: " + mm.getUSER_REC_UID());
				logger.info("__________________________________________________V");
			}

			// Send message
			// MessageSender.sendMessageModel(userUid, mm);
			logger.info("OK: CATE_TYPE: " + mm.getCATEGORY() + "_" + mm.getTYPE() + " USER_UID: " + mm.getUSER_UID()
					+ " USER_UID_REC: " + mm.getUSER_REC_UID());
			logger.info("__________________________________________________V");
			// logger.info("PAYLOAD: " + mm.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		logger.info("onClose");
		logger.info("USER_UID: " + session.getAttributes().get("USER_UID"));
		logger.info("SessionId: " + session.getId() + " has connected yet." + "Hostname(IP): "
				+ session.getRemoteAddress().getHostName());
		SessionModelStateMng.getInstance().destroySession(session);
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		logger.info("handleTransportError " + session.getId());
	}
}
