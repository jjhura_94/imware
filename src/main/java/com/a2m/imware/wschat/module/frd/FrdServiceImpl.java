package com.a2m.imware.wschat.module.frd;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FrdServiceImpl {
	
	@Autowired
	private FrdDAO  dao;
	
	public void insertAutoMakeFrd(Map<Object, Object> invt) throws Exception {
		dao.insertAutoMakeFrd(invt);
	}
}
