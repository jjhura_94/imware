package com.a2m.imware.wschat.module.user;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.service.common.CommonDeptService;

@Service
public class ChatUserServiceImpl {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private static ChatUserServiceImpl service;
	
	@Autowired
	private ChatUserDAO dao;
	
	@Autowired
    private CommonDeptService commonDeptService;

	public ChatUserDAO getDao() {
		return dao;
	}

	public void setDao(ChatUserDAO dao) {
		this.dao = dao;
	}

	public Map<Object, Object> getUserDetail(Map<Object, Object> params) throws Exception {
		return dao.getUser(params);
	}

	public Map<Object, Object> getUserProfile(Map<Object, Object> params) throws Exception {
		return dao.getUserDetail(params);
	}

	public static ChatUserServiceImpl getInstance() {
		if (service == null) {
			service = new ChatUserServiceImpl();
			// service.setDao(new ChatUserDAO());
		}
		return service;
	}

	public void update(Map<Object, Object> p_room) throws Exception {
		dao.update(p_room);
	}

	public List<Map<Object, Object>> getUserFriendList(Map<Object, Object> param) throws Exception {
		return dao.getUserList(param);
	}

	public List<Map<Object, Object>> getContactList02(Map<Object, Object> map) throws Exception {
		return dao.getContactList02(map);
	}

	public List<Map<Object, Object>> getListCheckChatInContact(Map<Object, Object> para12) throws Exception {
		return dao.getListCheckChatInContact(para12);
	}

	public Map<Object, Object> getChatUserByKey(Map<Object, Object> p_chatuser1) throws Exception {
		return dao.getChatUserByKey(p_chatuser1);
	}

	public void updateChatUser_RoomOpenCheck_UserCheck(Map<Object, Object> para3) throws Exception {
		dao.updateChatUser_RoomOpenCheck_UserCheck(para3);
	}

	public void insertCHAT_USER(Map<Object, Object> chat_user) throws Exception {
		dao.insertCHAT_USER(chat_user);
	}

	public void insertListCHAT_USER(List<Map<Object, Object>> userList) throws Exception {
		dao.insertCHAT_USER_batch(userList);
	}

	public Map<Object, Object> getUserByKey(Map<Object, Object> p_userRd) throws Exception {
		return dao.getUser(p_userRd);
	}

	public void updateUser_Nm_Eng(Map<Object, Object> user_nm) throws Exception {
		dao.updateUser_Nm_Eng(user_nm);
	}

	public void deleteChatUser(Map<Object, Object> user_nm) throws Exception {
		dao.deleteChatUser(user_nm);
	}

	public void deleteUserByRoomUid(String ROOM_UID) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", ROOM_UID);
		dao.deleteUserByRoomUid(data);
	}

	public List<String> getListUid(Map<Object, Object> p_remove) throws Exception {
		return dao.getListUid(p_remove);
	}

	public List<Map<Object, Object>> getAllUser(Map<Object, Object> param) throws Exception {
		return dao.getAllUser(param);
	}

	public Object sendInvite(Map<Object, Object> param) throws Exception {
		// String userUidInvite = (String)param.get("USER_UID_1");
		// String userUidAccept = (String)param.get("USER_UID_2");
		// notificationService.sendNotificationInvite(false, userUidInvite,
		// userUidAccept);
		return dao.insertFRIEND_INVITE(param);

	}

	public Object cancelRequest(Map<Object, Object> param) throws Exception {
		return dao.deleteInvite(param);
	}

	public Object addFavorite(Map<Object, Object> param) throws Exception {
		return dao.updateToFavorite(param);
	}

	public Object unFavorite(Map<Object, Object> param) throws Exception {
		return dao.updateUnfavorite(param);
	}

	public Object acceptFriend(Map<Object, Object> param) throws Exception {
		Map<Object, Object> rs = dao.getInvite(param);
		param.putAll(rs);
		int rs1 = (Integer) dao.insertFriend1(param);
		int rs2 = (Integer) dao.insertFriend2(param);
		int rs3 = (Integer) dao.updateINVITE(param);
		// String userUidAccept = (String) param.get("USER_UID");
		// String userUidInvite = (String) param.get("USER_UID_2");
		// notificationService.sendNotificationAcceptFriend(false, userUidAccept,
		// userUidInvite);
		return rs1 * rs2 * rs3;
	}

	public int deleteFriend(Map<Object, Object> param) throws Exception {
		int rs1 = (int) dao.deleteFriend(param);
		int rs2 = (int) dao.deleteInvite2(param);
		int rs3 = (int) dao.updateChatUserDate(param);
		return rs1 * rs2 * rs3;
	}

	public Map<Object, Object> getOneDept(Map<Object, Object> imap) throws Exception {
		return dao.getOneDept(imap);
	}

	public Object checkUserInCurrentStruc(Map<Object, Object> param) throws Exception {
		return dao.checkUserInCrtStru(param);
	}

	public Object checkIsFriend(Map<Object, Object> p_user) throws Exception {
		return dao.checkIsFriend(p_user);
	}

	public void addFriend(Map<Object, Object> p_user) throws Exception {
		p_user.put("INVT_UID", 0);
		dao.insertFriend1(p_user);
		dao.insertFriend2(p_user);
	}

	public Map<Object, Object> getUserByUserId(String userId) throws Exception {
		return dao.getUserByUserId(userId);
	}
	
	public List<String> getAbsentUserUIDList(String deptCode, String userUid) throws Exception {
		List<String> childrenOfUser = commonDeptService.getChildrenOfUser(deptCode, userUid);
		Map<Object, Object> data = new HashMap<>();
		final Date date = new Date();
		data.put("FROM_DATE", sdf.format(date) + " 00:00:00");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		data.put("TO_DATE", sdf.format(c.getTime()) + " 00:00:00");
		if (childrenOfUser != null && !childrenOfUser.isEmpty()) {
			data.put("UID_LIST", childrenOfUser);
		}
		
		return dao.getAbsentUserUIDList(data);
	}
	
}