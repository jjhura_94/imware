package com.a2m.imware.wschat.module.userconfig;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserConfigDAO {
	
	Map<Object, Object> getConfigByKey(Map<Object, Object> data) throws Exception;
	
	int insertConfig(Map<Object, Object> data) throws Exception;
	
	int updateConfig(Map<Object, Object> data) throws Exception;
	
}
