package com.a2m.imware.wschat.module.chatroom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.module.user.ChatUserDAO;

@Service
public class ChatroomServiceImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(ChatroomServiceImpl.class);

	@Autowired
	private ChatroomDAO dao;
	
	@Autowired
	private ChatUserDAO chatUserDao;

	public List<Map<Object, Object>> getChatroomList(Map<Object, Object> parameter) throws Exception {
		return dao.getChatroomList(parameter);
	}

	public List<Map<Object, Object>> getChatroomListPaging(Map<Object, Object> parameter, int startIndex,
			int recordPerPage) throws Exception {
		parameter.put("start", startIndex);
		parameter.put("limit", recordPerPage);
		return dao.getChatroomList(parameter);
	}

	public void deleteChatroom(String roomUid) throws Exception {
		Map<Object, Object> parameter = new HashMap<>();
		parameter.put("ROOM_UID", roomUid);
		dao.deleteChattom(parameter);
	}

	public List<Map<Object, Object>> getChatroomUserList(Map<Object, Object> parameter) throws Exception {
		return dao.getChatroomList(parameter);
	}

	public void deleteChatUser(Map<Object, Object> parameter) throws Exception {
		dao.deteteChatUser(parameter);
	}

	public void updateRoom(Map<Object, Object> para) throws Exception {
		dao.updateRoom(para);
	}

	public List<Map<Object, Object>> getListMemberByRoomUid02(String roomUid) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", roomUid);
		return dao.getListMemberByRoomUid02(data);
	}

	public List<Map<Object, Object>> getListMemberByRoomUid03(String roomUid) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", roomUid);
		return dao.getListMemberByRoomUid03(data);
	}

	public void updateRoomName(Map<Object, Object> p_room) throws Exception {
		dao.updateRoomName(p_room);
	}

	public Map<Object, Object> getRoomByKey(Map<Object, Object> parameter) throws Exception {
		return dao.getRoomByKey(parameter);
	}

	public Map<Object, Object> getRoomTypeByKey(Map<Object, Object> parameter) throws Exception {
		Map<Object, Object> user2 = dao.getUserFriendInRoomContact(parameter);
		if (user2 != null) {
			Object user_uid_2 = user2.get("USER_UID");
			parameter.put("USER_UID_2", user_uid_2);
		}

		return dao.getRoomTypeByKey(parameter);
	}

	public List<String> getListMemberByRoomUid(Map<Object, Object> param) throws Exception {
		return dao.getListMemberByRoomUid(param);
	}

	public List<Map<Object, Object>> getListInfoMemberByRoomUid(Map<Object, Object> param) throws Exception {
		return dao.getListInfoMemberByRoomUid(param);
	}

	public void updateRoomOpenStatus(Map<Object, Object> parap) throws Exception {
		dao.updateChatUser_RoomOpenCheck_UserCheck(parap);
	}

	public boolean insertCHAT_ROOM(Map<Object, Object> room) throws Exception {
		return dao.insertCHAT_ROOM(room) > 0;
	}

	public List<Map<Object, Object>> getListChatUserByRoomUid(Map<Object, Object> object) throws Exception {
		return dao.getListChatUserByRoomUid(object);
	}

	public Object updateDateModified(Map<Object, Object> param) throws Exception {
		return dao.updateRoomNotify(param);
	}

	public Object updateRoomOpenCheck(Map<Object, Object> param) throws Exception {
		return dao.updateRoomOpenCheck(param);
	}

	public Object updateRoomNotification(Map<Object, Object> param) throws Exception {
		return dao.updateRoomNotification(param);
	}

	public Object getInContactRoom(Map<Object, Object> p_user) throws Exception {
		return dao.getContactChatroom(p_user);
	}
	
	public void createSystemRoom(String userUid) {
		try {
			if (Utils.isNullOrEmpty(userUid)) {
				logger.error("userUid is null or empty!");
				return;
			}
			
			// get user system
			Map<Object, Object> sysUser = chatUserDao.getUserByUserId("system");
			if (sysUser == null || sysUser.isEmpty()) {
				logger.error("Cannot find System user! Please check system account in your DB.");
				return;
			}
			
			final String roomUid = "SYS-" + userUid;
			// check room existed
			Map<Object, Object> existedRoom = dao.getRoomByKey(Collections.singletonMap("ROOM_UID", roomUid));
			if (existedRoom != null && !existedRoom.isEmpty()) {
				logger.info("This user already has a room with System account.");
				return;
			}
			
			final String roomName = "System";
			final String sysUserUid = sysUser.get("USER_UID").toString();
			Map<Object, Object> data = new HashMap<>();
			data.put("KEY_ROOM", UUID.randomUUID().toString());
			data.put("ROOM_UID", roomUid);
			data.put("REG_DATE", new Date());
			data.put("REG_USER_UID", sysUserUid);
			data.put("ROOM_NM", roomName);
			data.put("ROOM_DEFAULT_NAME", roomName);
			data.put("ROOM_TYPE", "IN_CHATROOM");
			
			int affectedRows = dao.insertCHAT_ROOM(data);
			if (affectedRows > 0) {
				// Create CHAT_USER
				List<Map<Object, Object>> userList = new ArrayList<>();
				for (String iUserUid : Arrays.asList(userUid, sysUser.get("USER_UID").toString())) {
					Map<Object, Object> chatUser = new HashMap<>();
					chatUser.put("USER_UID", iUserUid);
					chatUser.put("ROOM_UID", data.get("ROOM_UID"));
					chatUser.put("REG_DATE", new Date());
					chatUser.put("ROOM_NM", roomName);
					chatUser.put("USER_INVT_UID", sysUserUid);
					chatUser.put("USER_CHECK", "N");
					chatUser.put("ROOM_OPEN_CHECK", "N");
					if (iUserUid.equals(sysUserUid)) {
						chatUser.put("ROOM_OPEN_CHECK", "Y");
						chatUser.put("USER_CHECK", "Y");
					}

					if (!userList.contains(chatUser)) {
						userList.add(chatUser);
					}
				}
				
				chatUserDao.insertCHAT_USER_batch(userList);
				
				logger.info("Create System room success to userUid=" + userUid);
			} else
				logger.error("Create System room not success to userUid=" + userUid);
		} catch (Exception e) {
			logger.error("Error occurred when trying to create System room: ", e);
			e.printStackTrace();
		}
	}

}