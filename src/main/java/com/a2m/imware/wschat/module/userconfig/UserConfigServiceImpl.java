package com.a2m.imware.wschat.module.userconfig;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserConfigServiceImpl {

	@Autowired
	private UserConfigDAO dao;

	public void updateConfig(Map<Object, Object> param) throws Exception {
		dao.updateConfig(param);
	}

	public int insertConfig(Map<Object, Object> param) throws Exception {
		return dao.insertConfig(param);
	}

	public Map<Object, Object> getConfigByKey(Map<Object, Object> parameter) throws Exception {
		return dao.getConfigByKey(parameter);
	}
}
