package com.a2m.imware.wschat.module.chatroom;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChatroomDAO {

	List<Map<Object, Object>> getListMemberByRoomUid02(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListMemberByRoomUid03(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListChatUserByRoomUid(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getChatroomList(Map<Object, Object> data) throws Exception;
	
	int updateRoomNotify(Map<Object, Object> data) throws Exception;
	
	int updateRoomOpenCheck(Map<Object, Object> data) throws Exception;
	
	int updateRoom(Map<Object, Object> data) throws Exception;
	
	int updateChatUser_RoomOpenCheck_UserCheck(Map<Object, Object> data) throws Exception;
	
	int deleteChattom(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getRoomTypeByKey(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getRoomByKey(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getUserFriendInRoomContact(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getChatroomUser(Map<Object, Object> data) throws Exception;
	
	int insertChatroomUser(Map<Object, Object> data) throws Exception;
	
	int updateChatroomUser(Map<Object, Object> data) throws Exception;
	
	int updateRoomName(Map<Object, Object> data) throws Exception;
	
	int deleteChatroomUser(Map<Object, Object> data) throws Exception;
	
	int deteteChatUser(Map<Object, Object> data) throws Exception;
	
	List<String> getListMemberByRoomUid(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListInfoMemberByRoomUid(Map<Object, Object> data) throws Exception;
	
	int insertCHAT_ROOM(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getContactChatroom(Map<Object, Object> data) throws Exception;
	
	int updateRoomNotification(Map<Object, Object> data) throws Exception;
	
}