package com.a2m.imware.wschat.module.user;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ChatUserDAO {

	int update(Map<Object, Object> data) throws Exception;
	
	int updateUser_Nm_Eng(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getOneDept(Map<Object, Object> data) throws Exception;
	
	int updateChatUser_RoomOpenCheck_UserCheck(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getChatUserByKey(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListCheckChatInContact(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getContactList02(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getUserDetail(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getUser(Map<Object, Object> data) throws Exception;
	
	Object checkUserInCrtStru(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getUserList(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getAllUser(Map<Object, Object> data) throws Exception;
	
	int deleteFriend(Map<Object, Object> data) throws Exception;
	
	int updateChatUserDate(Map<Object, Object> data) throws Exception;
	
	int updateToFavorite(Map<Object, Object> data) throws Exception;
	
	int updateUnfavorite(Map<Object, Object> data) throws Exception;
	
	int deleteInvite(Map<Object, Object> data) throws Exception;
	
	int deleteInvite2(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getInvite(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> checkIsFriend(Map<Object, Object> data) throws Exception;
	
	int insertFriend1(Map<Object, Object> data) throws Exception;
	
	int insertFriend2(Map<Object, Object> data) throws Exception;
	
	int updateINVITE(Map<Object, Object> data) throws Exception;
	
	List<String> getListUid(Map<Object, Object> data) throws Exception;
	
	int insertFRIEND_INVITE(Map<Object, Object> data) throws Exception;
	
	int insertCHAT_USER(Map<Object, Object> data) throws Exception;
	
	int insertCHAT_USER_batch(List<Map<Object, Object>> data) throws Exception;
	
	int deleteChatUser(Map<Object, Object> data) throws Exception;
	
	int deleteUserByRoomUid(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getUserByUserId(String userId) throws Exception;
	
	List<String> getAbsentUserUIDList(Map<Object, Object> data) throws Exception;
	
}