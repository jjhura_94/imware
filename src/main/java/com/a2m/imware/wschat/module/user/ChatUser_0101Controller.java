package com.a2m.imware.wschat.module.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.a2m.imware.wschat.manager.SessionModelStateMng;
import com.a2m.imware.wschat.module.chatroom.ChatroomServiceImpl;
import com.a2m.imware.wschat.util.ParameterUtil;

@Controller("ChatUser_0101Controller")
@RequestMapping("/chatuser/chatuser_0101")
public class ChatUser_0101Controller {

	@Autowired
	private ChatroomServiceImpl roomService;
	
	@Autowired
	ChatUserServiceImpl userService;

	@RequestMapping("/getListFrd.ajax")
	public ModelAndView getListFrd(ModelAndView mav, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map user_sess = (Map) SessionModelStateMng.getInstance().getSessionModelMap();
		List<Map<Object, Object>> listContact = new ArrayList<>();
		Map parameter = ParameterUtil.getParameterMapWithOutSession(request);
		Object roomUid = parameter.get("ROOM_UID");
		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", parameter.get("USER_UID"));
		listContact = userService.getUserFriendList(param);
		
		if(!Strings.isEmpty((String)roomUid)) {
			List<Map<Object, Object>> listMembers = roomService.getListMemberByRoomUid03((String) roomUid);
			if (listContact != null && listMembers != null) {
				ListIterator<Map<Object, Object>> litstIter = listContact.listIterator();
				while (litstIter.hasNext()) {
					Map el = litstIter.next();
					boolean flag = false;
					for (Map imember : listMembers) {
						if (imember.get("USER_UID").equals(el.get("USER_UID")))
						{
							flag = true;
							continue;
						}
					}
					if (flag)
						litstIter.remove();
					else
					{
						//Set online status
						if(user_sess!= null && user_sess.containsKey(el.get("USER_UID"))) {
							//online
							el.put("ONLINE_YN", "Y");
						}else 
							el.put("ONLINE_YN", "N");
						litstIter.set(el);
					}
				}
			}
		}else {
			for( Map imap : listContact) {
				//Set online status
				if(user_sess!= null && user_sess.containsKey(imap.get("USER_UID"))) {
					//online
					imap.put("ONLINE_YN", "Y");
				}else
					imap.put("ONLINE_YN", "N");
			}
		}
		mav.setViewName("jsonView");
		mav.addObject("DATA", listContact);
		return mav;
	}
	
	@RequestMapping("/getListMember.ajax")
	public ModelAndView getListMember(ModelAndView mav, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map parameter = ParameterUtil.getParameterMapWithOutSession(request);
		String room_uid = (String)parameter.get("ROOM_UID");
		List<Map<Object, Object>> listMembers = roomService.getListMemberByRoomUid02(room_uid);
		Map user_sess = (Map) SessionModelStateMng.getInstance().getSessionModelMap();
		for( Map imap: listMembers) {
			//Set online status
			if(user_sess!= null && user_sess.containsKey(imap.get("USER_UID"))) {
				//online
				imap.put("ONLINE_YN", "Y");
			}else
				imap.put("ONLINE_YN", "N");
		}
		mav.setViewName("jsonView");
		mav.addObject("DATA", listMembers);
		return mav;
	}
	
}
