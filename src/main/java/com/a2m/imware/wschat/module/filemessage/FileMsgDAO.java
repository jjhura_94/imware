package com.a2m.imware.wschat.module.filemessage;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileMsgDAO {

	int insertFileMsg(Map<Object, Object> data) throws Exception;
	
	int updateFileMsg(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getMsgFileMap(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListFileByRoomId(Map<Object, Object> data) throws Exception;
	
	int deleteFileByRoomUid(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> findByMsgUidList(List<String> msgUidList) throws Exception;
	
}