package com.a2m.imware.wschat.module.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatMsgServiceImpl {

	@Autowired
	private ChatMsgDAO dao;

	public void insertMsg(Map<Object, Object> param) throws Exception {
		dao.insertMsg01(param);
	}

	public List<Map<Object, Object>> getListMsgByRoomUid(Map<Object, Object> param) throws Exception {
		return dao.getListMsgByRoomUid(param);
	}

	public String gen_chatuid_seq() throws Exception {
		return (String) dao.gen_chatuid_seq();
	}
	
	public Map<Object, Object> getMsgInfo(Map<Object, Object> data) throws Exception {
		return dao.getMsgInfo(data);
	}
	
	public int updateMsgInfo(Map<Object, Object> data) throws Exception {
		return dao.updateMsgInfo(data);
	}

	public void insertMsgConf(Map<Object, Object> param) throws Exception {
		dao.insertMsgConf(param);
	}

	public void insertMsgConfirm(Map<Object, Object> param) throws Exception {
		dao.insertMsgConfirm(param);
	}

	public void insertMsgConfirmBatch(Map<Object, Object> param) throws Exception {
		dao.insertMsgConfirmBatch(param);
	}

	public Map<Object, Object> getMsgConfirmByKey(Map<Object, Object> param) throws Exception {
		return dao.getMsgConfirmByKey(param);
	}

	public void updateMsgConfirm(Map<Object, Object> param) throws Exception {
		dao.updateMsgConfirm(param);
	}

	public List<Map<Object, Object>> getListMsgConfirmByMsgUid(Map<Object, Object> param) throws Exception {
		return dao.getListMsgConfirmByMsgUid(param);
	}

	public List<Map<Object, Object>> getListMsgByRoomUid(Map<Object, Object> param, int startIndex, int recordPerPage) throws Exception {
		param.put("start", startIndex);
		param.put("limit", recordPerPage);
		return dao.getListMsgByRoomUidAndUserUID(param);
	}

	public List<Map<Object, Object>> getListMsgByRoomUidAndUserUIDAsConfirm(Map<Object, Object> param) throws Exception {
		return dao.getListMsgByRoomUidAndUserUIDAsConfirm(param);
	}

	public void deleteMsgByRoomUid(String ROOM_UID) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", ROOM_UID);
		dao.deleteMsgByRoomUid(data);
	}

	public void deleteConfirmByRoomUid(String ROOM_UID) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", ROOM_UID);
		dao.deleteConfirmByRoomUid(data);
	}

	public int chatSearchCount(Map<Object, Object> filter) throws Exception {
		Long count = dao.chatSearchCount(filter);
		return count == null ? 0 : count.intValue();
	}

	public List<Map<Object, Object>> chatSearch(Map<Object, Object> filter) throws Exception {
		List<Map<Object, Object>> result = dao.chatSearch(filter);
		return result == null ? new ArrayList<>(0) : result;
	}

	public List<Map<Object, Object>> chatSearchAround(Map<Object, Object> filter) throws Exception {
		List<Map<Object, Object>> result = dao.chatSearchAround(filter);
		return result == null ? new ArrayList<>(0) : result;
	}

}