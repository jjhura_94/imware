package com.a2m.imware.wschat.module.frd;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FrdDAO {
	
	int insertAutoMakeFrd(Map<Object, Object> data) throws Exception;
	
}
