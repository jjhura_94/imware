package com.a2m.imware.wschat.module.message;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChatMsgDAO {

	int insertMsgConfirm(Map<Object, Object> data) throws Exception;
	
	int insertMsgConfirmBatch(Map<Object, Object> data) throws Exception;
	
	int updateMsgConfirm(Map<Object, Object> data) throws Exception;
	
	Map<Object, Object> getMsgConfirmByKey(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListMsgConfirmByMsgUid(Map<Object, Object> data) throws Exception;
	
	int deleteMsgByRoomUid(Map<Object, Object> data) throws Exception;
	
	int deleteConfirmByRoomUid(Map<Object, Object> data) throws Exception;
	
	int insertMsg01(Map<Object, Object> data) throws Exception;
	
	Object gen_chatuid_seq() throws Exception;
	
	Map<Object, Object> getMsgInfo(Map<Object, Object> data) throws Exception;
	
	int updateMsgInfo(Map<Object, Object> data) throws Exception;
	
	int insertMsgConf(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListMsgByRoomUid(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListMsgByRoomUidAndUserUID(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getMsgReadById(Map<Object, Object> data) throws Exception;
	
	List<Map<Object, Object>> getListMsgByRoomUidAndUserUIDAsConfirm(Map<Object, Object> data) throws Exception;
	
	Long chatSearchCount(Map<Object, Object> filter) throws Exception;
	
	List<Map<Object, Object>> chatSearch(Map<Object, Object> filter) throws Exception;
	
	List<Map<Object, Object>> chatSearchAround(Map<Object, Object> filter) throws Exception;
	
	List<Map<Object, Object>> getListMsgRoomBooking(Map<Object, Object> filter) throws SQLException;
	
}