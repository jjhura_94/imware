package com.a2m.imware.wschat.module.filemessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileMsgServiceImpl {

	@Autowired
	private FileMsgDAO dao;

	public void insertFileMsg(Map<Object, Object> param) throws Exception {
		dao.insertFileMsg(param);
	}

	public Map<Object, Object> getMsgFileMap(Map<Object, Object> parameter) throws Exception {
		return dao.getMsgFileMap(parameter);
	}

	public List<Map<Object, Object>> getListFileByRoomId(Map<Object, Object> param) throws Exception {
		return dao.getListFileByRoomId(param);
	}

	public void deleteFileByRoomUid(String ROOM_UID) throws Exception {
		Map<Object, Object> data = new HashMap<>();
		data.put("ROOM_UID", ROOM_UID);
		dao.deleteFileByRoomUid(data);
	}
	
	public List<Map<Object, Object>> findByMsgUidList(List<String> msgUidList) throws Exception {
		return dao.findByMsgUidList(msgUidList);
	}
}