package com.a2m.imware.wschat;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Component
public class HandshakeInterceptor extends HttpSessionHandshakeInterceptor {


		private static final Log logger = LogFactory
		        .getLog(HandshakeInterceptor.class);
		
		@Override
		public boolean beforeHandshake(ServerHttpRequest request,
		        ServerHttpResponse response, WebSocketHandler wsHandler,
		        Map<String, Object> attributes) throws Exception {
			logger.info("Before Handshake");
			// LeDT 12Nov2018: by pass this interceptor for external connect
			// after handshake, client must send LOGIN_IN to authenticate
			
			/*if (request instanceof ServletServerHttpRequest) {
				ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
				HttpSession session = servletRequest.getServletRequest().getSession(false);
				if (session != null) {
					Map user = (Map)session.getAttribute("SESS_USER");
					attributes.put("USER_UID", user.get("USER_UID"));//temp
					attributes.put("USER_ID", user.get("USER_ID"));
					String type_lg = (String)user.get("TYPE_LOGIN");
					if(type_lg == null) type_lg = "";
					attributes.put("TYPE_LOGIN", type_lg);  
				}else
					return false;
			}*/
			
			if (request instanceof ServletServerHttpRequest) {
				ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
				HttpSession session = servletRequest.getServletRequest().getSession(false);
				if (session != null) {
					attributes.put("httpSession", session);
				}

			}
			return true;
		}
		
		@Override
		public void afterHandshake(ServerHttpRequest request,
		        ServerHttpResponse response, WebSocketHandler wsHandler,
		        Exception ex) {
		    logger.info("After Handshake");
		    super.afterHandshake(request, response, wsHandler, ex);
		}
}
