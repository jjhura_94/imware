package com.a2m.imware.wschat.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import kr.a2mvn.largefileupload.common.CastUtil;

/**
 * 공통 Utility 클래스.
 * @FileName  : CommonUtil.java
 * @Project     : mis_java
 * @최초작성일 : 2014. 9. 26. 
 * @프로그램설명 : 공통적인 데이터 변경등의 기능을 수행하는 단위 컴포넌트
 */
/**
 * @author keim
 *
 */
/**
 * 기능명
 * 
 * @작성일 : 2015. 6. 22.
 * @작성자 : keim
 * @프로그램설명 :
 * @진행상태: TO-DO, DEBUG, TEST, COMPLETE
 */
public final class CommonUtil {
	protected static Logger logger = LogManager.getLogger(CommonUtil.class);

	/**
	 * 도메인
	 */
	public static String getDomain(HttpServletRequest req) {
		StringBuffer sb = new StringBuffer();
		sb.append(req.getScheme());
		sb.append("://");
		sb.append(req.getServerName());
		sb.append(":");
		sb.append(req.getServerPort());
		sb.append(req.getContextPath());
		return sb.toString();
	}

	/**
	 * Map에 대한 데이타 가져오기
	 * 
	 * @param map          - 대상Map
	 * @param key          - 데이터를 가져올 Key
	 * @param defaultValue - 데이터가 없는 경우 기본값
	 * @return String
	 */
	public static String getMapValue(Map map, String key, String defaultValue) {
		if (map == null)
			return "";
		Object value = map.get(key);
		if (value == null)
			return defaultValue;
		if (StringUtils.isEmpty("" + value))
			return defaultValue;
		return "" + value;
	}

	/**
	 * NUll 체크 로직
	 * 
	 * @작성일 : 2014. 9. 26.
	 * @작성자 : keim
	 * @프로그램설명 :
	 * @진행상태설명: TEST - LOGIC TEST 이후 수정 필
	 */
	public static boolean isNull(Object obj) {
		if (obj == null) {
			return true;
		}
		return false;
	}

	/**
	 * 타입상관없이 null 체크
	 * 
	 * @작성일 : 2015. 5. 8.
	 * @작성자 : keim
	 * @프로그램설명 :
	 * @진행상태: TO-DO, DEBUG, TEST, COMPLETE
	 */
	public static boolean isNotEmpty(Object obj) {
		String tmp = CastUtil.castToString(obj);
		if (tmp != null && !tmp.equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * byte 체크
	 * 
	 * @작성일 : 2015. 4. 27.
	 * @작성자 : keim
	 * @프로그램설명 : 바이트 체크
	 * @진행상태설명: TO-DO :
	 */
	public static String rightBytes(String strValue, int iByte) {
		byte[] result = null;

		if (strValue == null) {
			return null;
		} else if (strValue.equals("") || iByte < 0) {
			return new String("");
		} else {
			byte[] source = strValue.getBytes();

			if (source.length < iByte) {
				return new String(strValue);
			} else {
				result = new byte[iByte];
				System.arraycopy(source, source.length - iByte, result, 0, iByte);
				return new String(result);
			}
		}
	}

	/**
	 * 지정한 문자열로 지정한 길이만큼 반복해서 채운다.
	 * 
	 * @작성일 : 2015. 4. 27.
	 * @작성자 : keim
	 * @프로그램설명 : fill("#", 5) return : "#####" fill("abc", 7) return : "abcabca"
	 *         fill("abc", 2) return : "ab"
	 * @진행상태설명: TO-DO :
	 */
	public static String fill(String strPattern, int iLen) {
		StringBuffer sb = new StringBuffer();

		if (strPattern == null) {
			return null;
		} else if (strPattern.equals("") || iLen < 0) {
			return "";
		} else {
			for (int i = 0; i < iLen; i = i + strPattern.length()) {
				sb.append(strPattern);
			}
		}

		return sb.substring(0, iLen);
	}

	public static String lpad(String padChar, String data, int length) {
		String str = "";

		for (int i = 0; i < length - data.length(); i++) {
			str += padChar;
		}
		str += data;

		return str;
	}

	/**
	 * 인스턴스 타입 리턴
	 * 
	 * @작성일 : 2014. 9. 26.
	 * @작성자 : keim
	 * @프로그램설명 : pameter Object 에 대하여 클래스 유형을
	 * @진행상태설명: TO-DO : 미정의된 클래스에 대하여 reflection으로 클래스를 찾아 타입 리턴
	 */
	public static Class getInstanceType(Object obj) {
		Object cls = new Object();
		if (obj != null) {
			if (obj instanceof String) {
				cls = new String();
			} else if (obj instanceof Integer) {
				cls = new Integer(0);
			} else if (obj instanceof Double) {
				cls = new Double(0);
			} else if (obj instanceof Float) {
				cls = new Float(0);
			} else if (obj instanceof HashMap) {
				cls = new HashMap();
			} else if (obj instanceof ArrayList) {
				cls = new ArrayList();
			} else if (obj instanceof BigDecimal) {
				cls = new BigDecimal(0);
			} else if (obj instanceof Date) {
				cls = new Date();
			} else {
				cls = obj;
//	    		Reflection refex = new Reflection();
			}
		}
		return cls.getClass();
	}

	/**
	 * 구동되는 상대 경로 추출
	 * 
	 * @작성일 : 2014. 10. 24.
	 * @작성자 : keim
	 * @프로그램설명 :
	 * @진행상태설명: TEST 리눅스 환경 window 환경 테스트 필요
	 */
	public static String getDirectoryPath(String proName) {
		Class cls = CommonUtil.class;
//    	String proName = "mis_java";

		String path = cls.getClassLoader().getResource("").getPath();
//    	String path = "/web/mis/WEB-INF/class";
		String directory = "";
//    	ResourceUtil.getMessage
//    	ResourceBundle bundle= ResourceBundle.getBundle("kr.or.innopolis.mis.config.properties.system",Locale.KOREAN);
		Map messageMap = ResourceUtil.getMessageMap("system.dir.upload");

//    	if(proName.equals("excelDownload")){
//    		directory = bundle.getString("dir.download.excel");
//    	}
		// Object obj = (String) messageMap.get(proName);
		Object obj = messageMap.get(proName);

		Map map = new HashMap();

		if (obj instanceof Map) {
			map = (Map) obj;
			directory = (String) map.get("MESSAGE");
		} else {
			directory = (String) messageMap.get(proName);
		}

//		String proName = "mis_java";
		if (SystemUtils.IS_OS_WINDOWS) {

			if (path.indexOf("WEB-INF") > 0) {
				path = path.substring(0, path.indexOf("WEB-INF"));
				if (path.indexOf("/") == 0) {
					path = path.substring(1);
				}
			}

		} else {
			if (directory.contains(":")) {
				directory = directory.split(":")[1];
			}

			// 1. /usr1/으로 시작할 경우 개발서버
			// 2. /usr2/으로 시작할 경우 운영서버
			if (path.indexOf("/usr2") == 0) {
				path = "/usr2";
			} else {
				if (proName.equals("dev_noticeImg"))
					path = "/usr1/tomcat-7.0.64/webapps";
				else
					path = "/usr1/tomcat-7.0.64/usr1";
			}

		}

		logger.error("path" + path);

//    	path = path.substring(0,path.lastIndexOf("/",path.lastIndexOf("/",path.lastIndexOf("/")-1)-1));
		path += directory;
		// return path;
		return directory;
	}

	// anhpv 20190328
	public static String getDirectoryPath2(String type) {
		String directory = "";
		Map mapRootPath = ResourceUtil.getMessageMap("system.dir.upload.rootPath");
		String rootPath = (String) mapRootPath.get("MESSAGE");
		directory = rootPath + type;
		return directory;
	}

	public static String getContextPath2(String type) {
		String directory = "";
		Map mapContextPath = ResourceUtil.getMessageMap("system.dir.upload.contextPath");
		String contextPath = (String) mapContextPath.get("MESSAGE");
		directory = contextPath + type;
		return directory;
	}

	public static String getContextUploadMessage() {
		return getContextPath2("message");
	}

	public static String getDirectoryDownloadExcel() {
		return getDirectoryPath2("excelDownload");
	}

	public static String getDirectoryUploadDefault() {
		return getDirectoryPath2("default");
	}

	public static String getDirectoryUploadMessage() {
		return getDirectoryPath2("message");
	}

	public static String getDirectoryUploadExcel() {
		return getDirectoryPath2("excelUpload");
	}

	public static String getDirectoryDownloadHomeTax() {
		return getDirectoryPath2("hometaxDownload");
	}

	public static String getDirectoryUploadEmpImg() {

//    	Class cls = CommonUtil.class;
//    	String path = cls.getClassLoader().getResource("").getPath();
//    	String directory = "";
//    	ResourceBundle bundle= ResourceBundle.getBundle("kr.or.innopolis.mis.config.properties.system",Locale.KOREAN);
//    	
//    	
//    	directory = bundle.getString("dir.upload.img");
//    	logger.error(path);
//    	path = path.substring(0,path.indexOf("WEB-INF"));
//    	path += directory;

		return getDirectoryPath2("empimg");
	}

	public static void printMap(Map map) {
		for (Map.Entry<String, Object> entry : ((Map<String, Object>) map).entrySet()) {
//    		System.out.println(entry.getKey()+" : "+ entry.getValue());
		}
	}

	static Random rand = new Random();

	public static String generateUUID() {
		String str = rand.nextInt(1000) + "" + Calendar.getInstance().getTimeInMillis();
		return str;
	}

}