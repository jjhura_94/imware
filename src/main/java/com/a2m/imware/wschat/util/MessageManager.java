package com.a2m.imware.wschat.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class MessageManager {
	private volatile static MessageManager instance;

	private Map<String, Map> messageMap;

	public MessageManager() {
		messageMap = new HashMap();

		ResourceBundle bundle = ResourceUtil.getResourceBundle("config.message.config");
		Enumeration<String> keys = bundle.getKeys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();

			Map keyMap = ResourceUtil.convertMsgSource(bundle.getString(key));
			messageMap.put(key, keyMap);
		}
	}

	public static synchronized MessageManager getInstance() {
		if (instance == null) {// 있는지 체크 없으면
			instance = new MessageManager(); // 생성한뒤
		}
		return instance;// 생성자를 넘긴다.
	}

	public Map getMessageMap(String str) {
		Map map = messageMap.get(str);

		return map;
	}

	public Map<String, Map> getMessageMap() {
		return messageMap;
	}

}
