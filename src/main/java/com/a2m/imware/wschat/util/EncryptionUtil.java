package com.a2m.imware.wschat.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.DigestException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.io.BaseEncoding;

public class EncryptionUtil {

	public static final String ALGORITHM = "RSA";

	public static byte[] encryptRSA(PublicKey key, byte[] inputData) throws Exception {

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.PUBLIC_KEY, key);

		byte[] encryptedBytes = cipher.doFinal(inputData);

		return encryptedBytes;
	}

	public static byte[] decryptRSA(PrivateKey key, byte[] inputData) throws Exception {

		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.PRIVATE_KEY, key);

		byte[] decryptedBytes = cipher.doFinal(inputData);

		return decryptedBytes;
	}

	public static KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {

		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);

		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

		// 512 is keysize
		// fixed keysize
		keyGen.initialize(2048, random);

		KeyPair generateKeyPair = keyGen.generateKeyPair();
		return generateKeyPair;
	}

	public static PublicKey publicKeyFromString(String str) throws InvalidKeySpecException, NoSuchAlgorithmException {

//    	KeyStore ks = KeyStore.getInstance("pkcs12");
//    	ks.load(;
//    	Enumeration<String> enumeration = ks.aliases();
//    	while (enumeration.hasMoreElements()) {
//    	  String alias = enumeration.nextElement();
//    	  Certificate certificate = (Certificate) ks.getCertificate(alias);
//    	  RSAPublicKey pub = (RSAPublicKey) certificate.getPublicKey();
//    	  System.out.println(pub.getModulus().toString(16));
//    	  System.out.println(pub.getPublicExponent().toString(16));
//    	}
//		PublicKey publicKey = null;

		// String guid = "YxRfXk827kPgkmMUX15PNg==";
//		byte[] decoded = Base64.getDecoder().decode(str);
//		
//		String hexString = Hex.encodeHexString(decoded);
		BigInteger modulus = new BigInteger(str, 16);
		BigInteger publicExponent = new BigInteger("03", 16);

		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
		// publicKey = KeyFactory.getInstance("RSA").generatePublic(new
		// X509EncodedKeySpec(b));
		// var N = my.b64to16(string.split("|")[0]);
		// var E = "03";
		// var rsa = new RSAKey();
		// rsa.setPublic(N, E);
		return publicKey;
	}

	public static PublicKey publicKeyFromString(String str, String exp) throws InvalidKeySpecException, NoSuchAlgorithmException {
		if (exp == null) {
			return publicKeyFromString(str);
		}
		BigInteger modulus = new BigInteger(str, 16);
		BigInteger publicExponent = new BigInteger(exp, 16);

		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
		return publicKey;
	}

	/**
	 * Generates a key and an initialization vector (IV) with the given salt and
	 * password.
	 * <p>
	 * This method is equivalent to OpenSSL's EVP_BytesToKey function (see
	 * https://github.com/openssl/openssl/blob/master/crypto/evp/evp_key.c). By
	 * default, OpenSSL uses a single iteration, MD5 as the algorithm and UTF-8
	 * encoded password data.
	 * </p>
	 * 
	 * @param keyLength  the length of the generated key (in bytes)
	 * @param ivLength   the length of the generated IV (in bytes)
	 * @param iterations the number of digestion rounds
	 * @param salt       the salt data (8 bytes of data or <code>null</code>)
	 * @param password   the password data (optional)
	 * @param md         the message digest algorithm to use
	 * @return an two-element array with the generated key and IV
	 */
	public static byte[][] GenerateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password, MessageDigest md) {

		int digestLength = md.getDigestLength();
		int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
		byte[] generatedData = new byte[requiredLength];
		int generatedLength = 0;

		try {
			md.reset();

			// Repeat process until sufficient data has been generated
			while (generatedLength < keyLength + ivLength) {

				// Digest data (last digest if available, password data, salt if available)
				if (generatedLength > 0)
					md.update(generatedData, generatedLength - digestLength, digestLength);
				md.update(password);
				if (salt != null)
					md.update(salt, 0, 8);
				md.digest(generatedData, generatedLength, digestLength);

				// additional rounds
				for (int i = 1; i < iterations; i++) {
					md.update(generatedData, generatedLength, digestLength);
					md.digest(generatedData, generatedLength, digestLength);
				}

				generatedLength += digestLength;
			}

			// Copy key and IV into separate byte arrays
			byte[][] result = new byte[2][];
			result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
			if (ivLength > 0)
				result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

			return result;

		} catch (DigestException e) {
			throw new RuntimeException(e);

		} finally {
			// Clean out temporary data
			Arrays.fill(generatedData, (byte) 0);
		}
	}

	/**
	 * @return a new pseudorandom salt of the specified length
	 */
	private static byte[] generateSalt(int length) {
		Random r = new SecureRandom();
		byte[] salt = new byte[length];
		r.nextBytes(salt);
		return salt;
	}

	private static byte[] EvpKDF(byte[] password, int keySize, int ivSize, byte[] salt, byte[] resultKey, byte[] resultIv) throws NoSuchAlgorithmException {
		return EvpKDF(password, keySize, ivSize, salt, 1, "MD5", resultKey, resultIv);
	}

	private static byte[] EvpKDF(byte[] password, int keySize, int ivSize, byte[] salt, int iterations, String hashAlgorithm, byte[] resultKey, byte[] resultIv) throws NoSuchAlgorithmException {
		keySize = keySize / 32;
		ivSize = ivSize / 32;
		int targetKeySize = keySize + ivSize;
		byte[] derivedBytes = new byte[targetKeySize * 4];
		int numberOfDerivedWords = 0;
		byte[] block = null;
		MessageDigest hasher = MessageDigest.getInstance(hashAlgorithm);
		while (numberOfDerivedWords < targetKeySize) {
			if (block != null) {
				hasher.update(block);
			}
			hasher.update(password);
			block = hasher.digest(salt);
			hasher.reset();

			// Iterations
			for (int i = 1; i < iterations; i++) {
				block = hasher.digest(block);
				hasher.reset();
			}

			System.arraycopy(block, 0, derivedBytes, numberOfDerivedWords * 4, Math.min(block.length, (targetKeySize - numberOfDerivedWords) * 4));

			numberOfDerivedWords += block.length / 4;
		}

		System.arraycopy(derivedBytes, 0, resultKey, 0, keySize * 4);
		System.arraycopy(derivedBytes, keySize * 4, resultIv, 0, ivSize * 4);

		return derivedBytes; // key + iv
	}

	public static String encryptAES(String text, String secret) {
		try {
			final int keySize = 256;
			final int ivSize = 128;

			// Create empty key and iv
			byte[] key = new byte[keySize / 8];
			byte[] iv = new byte[ivSize / 8];

			// Create random salt
			byte[] saltBytes = generateSalt(8);

			// Derive key and iv from passphrase and salt
			EvpKDF(secret.getBytes("UTF-8"), keySize, ivSize, saltBytes, key, iv);

			// Actual encrypt
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv));
			byte[] cipherBytes = cipher.doFinal(text.getBytes("UTF-8"));

			/**
			 * Create CryptoJS-like encrypted string from encrypted data This is how
			 * CryptoJS do: 1. Create new byte array to hold ecrypted string (b) 2.
			 * Concatenate 8 bytes to b 3. Concatenate salt to b 4. Concatenate encrypted
			 * data to b 5. Encode b using Base64
			 */
			byte[] sBytes = "Salted__".getBytes("UTF-8");
			byte[] b = new byte[sBytes.length + saltBytes.length + cipherBytes.length];
			System.arraycopy(sBytes, 0, b, 0, sBytes.length);
			System.arraycopy(saltBytes, 0, b, sBytes.length, saltBytes.length);
			System.arraycopy(cipherBytes, 0, b, sBytes.length + saltBytes.length, cipherBytes.length);

//            byte[] base64b = Base64.getEncoder().encode(b);
			byte[] base64b = BaseEncoding.base64().encode(b).getBytes();

			return new String(base64b);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String encryptAES2(String strToEncrypt, String myKey) {
		try {
			IvParameterSpec iv = new IvParameterSpec(myKey.substring(0, 16).getBytes());
			String secretKey = myKey.substring(0, 32);
			SecretKeySpec skeySpec = new SecretKeySpec(secretKey.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			return byteArrayToHexString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String decryptAES(String cipherText, String secret) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		/*
		 * String ret = "";
		 * 
		 * byte[] cipherData = Base64.getDecoder().decode(cipherText); byte[] saltData =
		 * Arrays.copyOfRange(cipherData, 8, 16);
		 * 
		 * MessageDigest md5 = MessageDigest.getInstance("MD5"); final byte[][] keyAndIV
		 * = GenerateKeyAndIV(32, 16, 1, saltData,
		 * secret.getBytes(StandardCharsets.UTF_8), md5); SecretKeySpec key = new
		 * SecretKeySpec(keyAndIV[0], "AES"); IvParameterSpec iv = new
		 * IvParameterSpec(keyAndIV[1]);
		 * 
		 * byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
		 * Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
		 * aesCBC.init(Cipher.DECRYPT_MODE, key, iv); byte[] decryptedData =
		 * aesCBC.doFinal(encrypted); String decryptedText = new String(decryptedData,
		 * StandardCharsets.UTF_8);
		 * 
		 * return decryptedText;
		 */
		try {
			IvParameterSpec iv = new IvParameterSpec(secret.substring(0, 16).getBytes());
			String secretKey = secret.substring(0, 32);
			SecretKeySpec skeySpec = new SecretKeySpec(secretKey.getBytes(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(hexStringToByteArray(cipherText));

			return new String(original);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}
	
	public static String byteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars).toLowerCase();
    }

	public static String encryptDES(String text, String stringKey) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		// key String to SecretKey desKey
		DESKeySpec dks = new DESKeySpec(stringKey.getBytes());
		SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
		SecretKey desKey = skf.generateSecret(dks);
		Cipher cipher = Cipher.getInstance("DES"); // DES/ECB/PKCS5Padding for SunJCE
		cipher.init(Cipher.ENCRYPT_MODE, desKey);

		byte[] unencryptedByteArray = text.getBytes("UTF8");

		// Encrypt
		byte[] encryptedBytes = cipher.doFinal(unencryptedByteArray);

		// Encode bytes to base64 to get a string
		byte[] encodedBytes = // com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64.encodeBase64(encryptedBytes);
				Base64.getEncoder().encode(encryptedBytes);

		return new String(encodedBytes);
	}

	public static void main(String[] args) {
		String text = "Ngoc create a chatroom. 👻 👻";
		String secret = "20180531100733_userUid02_15277360532520000";
		// String cypherText = "U2FsdGVkX18/O36SDLF3j5+6Zesyg4YLW30OKrj+4OE=";
		try {
			/*
			 * String newTxt = encryptAES(text, secret); System.out.println(newTxt); String
			 * str = decryptAES(cypherText, secret); System.out.println(str);
			 */
			String str = encryptDES(text, secret);
			System.out.println(str);
			
			str = encryptAES2(text, "12345678912345671234567891234567");
			System.out.println(str);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
