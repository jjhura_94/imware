package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class ECBCMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("STATUS")
	private String status;

	@MapKey("PERMALINK")
	private String permalink;

	@MapKey("USER_UID")
	private String userUid;

	private ECBCMessage() {
	}
	
	public static ECBCBranches build() {
		return new Builder();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public static interface ECBCBranches {
		/**
		 * Employment certification request.
		 */
		ECBCStatusRequest employmentCertification();

		/**
		 * Business card request.
		 */
		ECBCStatusRequest businessCard();
	}

	public static interface ECBCStatusRequest {
		/**
		 * You have been requested.
		 */
		ECBCUserUID requested();

		/**
		 * Your request has been handled.
		 */
		ECBCPermalink completed();
	}

	public static interface ECBCPermalink {
		/**
		 * HTTP link to your request handled or file to download.
		 * 
		 * @param link
		 */
		ECBCUserUID permalink(String link);
	}

	public static interface ECBCUserUID {
		/**
		 * User UID of user receive this message.
		 * 
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}

	public static class Builder implements ECBCBranches, ECBCStatusRequest, ECBCPermalink, ECBCUserUID {

		private String type;
		private String status;
		private String permalink;
		private String[] userUid;

		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public ECBCUserUID permalink(String link) {
			this.permalink = link;
			return this;
		}

		@Override
		public ECBCUserUID requested() {
			this.status = ECBCStatusType.REQUESTED.name();
			return this;
		}

		@Override
		public ECBCPermalink completed() {
			this.status = ECBCStatusType.COMPLETED.name();
			return this;
		}

		@Override
		public ECBCStatusRequest employmentCertification() {
			this.type = ExtraMessageType.EMPLOYMENT_CERTIFICATION.name();
			return this;
		}

		@Override
		public ECBCStatusRequest businessCard() {
			this.type = ExtraMessageType.BUSINESS_CARD.name();
			return this;
		}

		public ECBCMessage build() {
			ECBCMessage message = new ECBCMessage();
			message.setType(this.type);
			message.setStatus(this.status);
			message.setPermalink(this.permalink);
			message.setUserUid(this.userUid);

			return message;
		}

	}

	private static enum ECBCStatusType {
		REQUESTED, COMPLETED
	}

}
