package com.a2m.imware.wschat.model;

public enum EnumMessageDef {
	LOGIN_IN(EnumMessageCategory.LOGIN, EnumMessageType.IN, "LOGIN_IN"),
	SECURITY_GET_KEY(EnumMessageCategory.SECURITY, EnumMessageType.GET_KEY, "SECURITY_GET_KEY"),
	CONTACT_LIST(EnumMessageCategory.CONTACT, EnumMessageType.LIST, "CONTACT_LIST"),
	CONTACT_LISTFORM(EnumMessageCategory.CONTACT, EnumMessageType.LISTFORM, "CONTACT_LISTFORM"),
	CONTACT_LISTFORM2(EnumMessageCategory.CONTACT, EnumMessageType.LISTFORM2, "CONTACT_LISTFORM2"),
	ROOM_CREATE(EnumMessageCategory.ROOM, EnumMessageType.CREATE, "ROOM_CREATE"),
	ROOM_CREATE_CONTACT(EnumMessageCategory.ROOM, EnumMessageType.CREATE_CONTACT, "ROOM_CREATE_CONTACT"),
	ROOM_CREATE_CHATROOM(EnumMessageCategory.ROOM, EnumMessageType.CREATE_CHATROOM, "ROOM_CREATE_CHATROOM"),
	ROOM_MODIFY(EnumMessageCategory.ROOM, EnumMessageType.MODIFY, "ROOM_MODIFY"),
	ROOM_CHECK(EnumMessageCategory.ROOM, EnumMessageType.CHECK, "ROOM_CHECK"),
	// check trang thai mo room moi
	ROOM_VIEW(EnumMessageCategory.ROOM, EnumMessageType.VIEW, "ROOM_VIEW"),
	ROOM_LIST(EnumMessageCategory.ROOM, EnumMessageType.LIST, "ROOM_LIST"),
	ROOM_OUT(EnumMessageCategory.ROOM, EnumMessageType.OUT, "ROOM_OUT"),
	ROOM_INVITE(EnumMessageCategory.ROOM, EnumMessageType.INVITE, "ROOM_INVITE"),
	CHAT_SEND(EnumMessageCategory.CHAT, EnumMessageType.SEND, "CHAT_SEND"),
	CHAT_RECEIVE(EnumMessageCategory.CHAT, EnumMessageType.RECEIVE, "CHAT_RECEIVE"),
	CHAT_LIST(EnumMessageCategory.CHAT, EnumMessageType.LIST, "CHAT_LIST"),
	CHAT_CONFIRM(EnumMessageCategory.CHAT, EnumMessageType.CONFIRM, "CHAT_CONFIRM"),
	CHAT_CHECK(EnumMessageCategory.CHAT, EnumMessageType.CHECK, "CHAT_CHECK"),
	// check trang thai da doc
	SECURITY_SEND_KEY(EnumMessageCategory.SECURITY, EnumMessageType.SEND_KEY, "SECURITY_SEND_KEY"),
	FILE_VIEW(EnumMessageCategory.FILE, EnumMessageType.VIEW, "FILE_VIEW"),
	FILE_UPLOAD(EnumMessageCategory.FILE, EnumMessageType.UPLOAD, "FILE_UPLOAD"),
	FILE_DOWNLOAD(EnumMessageCategory.FILE, EnumMessageType.DOWNLOAD, "FILE_DOWNLOAD"),
	MEMBER_LIST(EnumMessageCategory.MEMBER, EnumMessageType.LIST, "MEMBER_LIST"),
	MEMBER_VIEW(EnumMessageCategory.MEMBER, EnumMessageType.VIEW, "MEMBER_VIEW"),
	MEMBER_MODIFY(EnumMessageCategory.MEMBER, EnumMessageType.MODIFY, "MEMBER_MODIFY"),
	MEMBER_REFRESH(EnumMessageCategory.MEMBER, EnumMessageType.REFRESH, "MEMBER_REFRESH"),
	SETTING_VIEW(EnumMessageCategory.SETTING, EnumMessageType.VIEW, "SETTING_VIEW"),
	SETTING_MODIFY(EnumMessageCategory.SETTING, EnumMessageType.MODIFY, "SETTING_MODIFY"),
	FRIEND_INVITE(EnumMessageCategory.FRIEND, EnumMessageType.INVITE, "FRIEND_INVITE"),
	FRIEND_ACCEPT(EnumMessageCategory.FRIEND, EnumMessageType.ACCEPT, "FRIEND_ACCEPT"),
	FRIEND_CANCEL(EnumMessageCategory.FRIEND, EnumMessageType.CANCEL, "FRIEND_CANCEL"),
	FRIEND_ADD_FAVORITE(EnumMessageCategory.FRIEND, EnumMessageType.ADD_FAVORITE, "FRIEND_ADD_FAVORITE"),
	FRIEND_DELETE_FAVORITE(EnumMessageCategory.FRIEND, EnumMessageType.DELETE_FAVORITE, "FRIEND_DELETE_FAVORITE"),
	FRIEND_DELETE(EnumMessageCategory.FRIEND, EnumMessageType.DELETE, "FRIEND_DELETE"),
	MEMBER_ONLINE(EnumMessageCategory.MEMBER, EnumMessageType.ONLINE, "MEMBER_ONLINE"),
	NOTIFICATION_MODIFY(EnumMessageCategory.NOTIFICATION, EnumMessageType.MODIFY, "NOTIFICATION_MODIFY"),
	CHAT_SEARCH(EnumMessageCategory.CHAT, EnumMessageType.SEARCH, "CHAT_SEARCH"),
	CHAT_CALL(EnumMessageCategory.CHAT, EnumMessageType.CALL, "CHAT_CALL"),
	CHAT_EXTRA_MESSAGE(EnumMessageCategory.CHAT, EnumMessageType.EXTRA_MESSAGE, "CHAT_EXTRA_MESSAGE");

	private EnumMessageCategory category;
	private EnumMessageType type;
	private String commandName;

	EnumMessageDef(EnumMessageCategory category, EnumMessageType type, String commandName) {
		this.category = category;
		this.type = type;
		this.commandName = commandName;
	}

	/**
	 * @작성일 : 2017. 9.18.
	 * @작성자 : 김진학
	 * @프로그램설명 :
	 * @진행상태: TO-DO
	 */
	public enum EnumMessageCategory {
		LOGIN("LOGIN"), ROOM("ROOM"), CHAT("CHAT"), SECURITY("SECURITY"), FILE("FILE"), MEMBER("MEMBER"),
		SETTING("SETTING"), CONTACT("CONTACT"), FRIEND("FRIEND"), NOTIFICATION("NOTIFICATION");

		private String category;

		EnumMessageCategory(String category) {
			this.category = category;
		}

		public boolean equalsName(String otherName) {
			return (otherName != null) && category.equals(otherName);
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String toString() {
			return category;
		}

	}

	public static EnumMessageDef getEnumMessageTypeByTypeCategory(String type, String category) {
		EnumMessageDef result = null;
		EnumMessageCategory cat = EnumMessageCategory.valueOf(category);
		EnumMessageType types = EnumMessageType.valueOf(type);

		for (EnumMessageDef rt : EnumMessageDef.values()) {
			if (rt.getCategory().equals(cat)) {
				if (rt.getType().equals(types)) {
					result = rt;
				}
			}
		}
		return result;
	}

	public boolean equalsName(String otherName) {
		return (otherName != null) && toString().equals(otherName);
	}

	public String toString() {
		return category.toString() + "_" + type;
	}

	public EnumMessageCategory getCategory() {
		return category;
	}

	public void setCategory(EnumMessageCategory category) {
		this.category = category;
	}

	public EnumMessageType getType() {
		return type;
	}

	public void setType(EnumMessageType type) {
		this.type = type;
	}

	public String getCommandName() {
		return commandName;
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public enum EnumMessageType {
		IN("IN"), LIST("LIST"), LISTFORM("LISTFORM"), LISTFORM2("LISTFORM"), CREATE("CREATE"),
		CREATE_CONTACT("CREATE_CONTACT"), CREATE_CHATROOM("CREATE_CHATROOM"), VIEW("VIEW"), OUT("OUT"),
		INVITE("INVITE"), ACCEPT("ACCEPT"), CANCEL("CANCEL"), SEND("SEND"), RECEIVE("RECEIVE"), CONFIRM("CONFIRM"),
		SEND_KEY("SEND_KEY"), UPLOAD("UPLOAD"), DOWNLOAD("DOWNLOAD"), MODIFY("MODIFY"), REFRESH("REFRESH"),
		CHECK("CHECK"), DELETE("DELETE"), DELETE_FAVORITE("DELETE_FAVORITE"), ADD_FAVORITE("ADD_FAVORITE"),
		ONLINE("ONLINE"), GET_KEY("GET_KEY"), SEARCH("SEARCH"), CALL("CALL"), EXTRA_MESSAGE("EXTRA_MESSAGE");

		private String type;

		EnumMessageType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

	}

}
