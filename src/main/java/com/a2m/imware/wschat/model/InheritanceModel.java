package com.a2m.imware.wschat.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;

@SuppressWarnings("all")
public class InheritanceModel implements Serializable {

	public DefaultModel getMapToModel(Map map, DefaultModel model) throws Exception {

		Object obj;
		for (String key : ((Map<String, Object>) map).keySet()) {
			try {
				obj = (Object) map.get(key);
				if (obj instanceof BigDecimal) {
					BigDecimal bd = (BigDecimal) (obj);
					String dbls = bd.toString();
					map.put(key, dbls);
				}
				if (obj != null) {
					Method method = model.getClass().getDeclaredMethod("set" + key, obj.getClass());
					method.invoke(model, map.get(key));
				}

			} catch (SecurityException e) {
				// TODO Auto-generated catch block
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
			} catch (InvocationTargetException e) {
				Exception ex = (Exception) e.getCause();
				throw ex;
			}
		}
		return model;
	}

	protected Map getMapfromModel() {
		Field[] fields = this.getClass().getDeclaredFields();
		Map<String, Object> resultMap = new HashMap<>();
		try {
			for (int i = 0; i <= fields.length - 1; i++) {
				fields[i].setAccessible(true);
				if (fields[i].get(this) != null)
					resultMap.put(fields[i].getName(), fields[i].get(this));

			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return resultMap;

	}

	protected String getUSER_UID_OR_PERSON_ID(WebSocketSession session) {
		return "";
	}

}
