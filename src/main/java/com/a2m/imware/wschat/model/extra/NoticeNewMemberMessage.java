package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class NoticeNewMemberMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("MEMBER_USER_UID")
	private String memberUserUid;

	@MapKey("USER_UID")
	private String userUid;

	private NoticeNewMemberMessage() {
		this.type = ExtraMessageType.NOTICE_NEW_MEMBER.name();
	}

	public static NMMMessage builder() {
		return new Builder();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMemberUserUid() {
		return memberUserUid;
	}

	public void setMemberUserUid(String userUid) {
		this.memberUserUid = userUid;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public static interface NMMMessage {
		/**
		 * User UID of new member.
		 * 
		 * @param memberUserUid
		 */
		NMMUserUID memberUserUID(String memberUserUid);
	}

	public static interface NMMUserUID {
		/**
		 * User UID of user receive this message.
		 * 
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}

	public static class Builder implements NMMMessage, NMMUserUID {

		private String memberUserUid;
		private String[] userUid;

		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public NMMUserUID memberUserUID(String memberUserUid) {
			this.memberUserUid = memberUserUid;
			return this;
		}

		public NoticeNewMemberMessage build() {
			NoticeNewMemberMessage message = new NoticeNewMemberMessage();
			message.setMemberUserUid(this.memberUserUid);
			message.setUserUid(this.userUid);
			return message;
		}

	}

}
