package com.a2m.imware.wschat.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("all")
public class MessageModel extends InheritanceModel {

	public String USER_UID;// user who sends a request. First user send message from client to server
	public String USER_REC_UID;// user who receives a request. Then user send message from server to client
	public String CATEGORY;
	public String TYPE;
	public String TYPE_DTL;
	public String RESULT;
	public String MSG;
	public Map<Object, Object> PAYLOAD;// get data received from client
	public List<Map<Object, Object>> DATALIST; // set data to send to client

	@JsonIgnore
	public MessageModel() {
		// TODO Auto-generated constructor stub
		PAYLOAD = new HashMap<>();
		DATALIST = new ArrayList<>();
		USER_REC_UID = "";
		RESULT = "OK";
	}

	@JsonIgnore
	public String getMSG() {
		return MSG;
	}

	@JsonIgnore
	public void setMSG(String mSG) {
		MSG = mSG;
	}

	@JsonIgnore
	public String getTYPE_DTL() {
		return TYPE_DTL;
	}

	@JsonIgnore
	public void setTYPE_DTL(String tYPE_DTL) {
		TYPE_DTL = tYPE_DTL;
	}

	@JsonIgnore
	public String getRESULT() {
		return RESULT;
	}

	@JsonIgnore
	public void setRESULT(String rESULT) {
		RESULT = rESULT;
	}

	@JsonIgnore
	public String getUSER_REC_UID() {
		return USER_REC_UID;
	}

	@JsonIgnore
	public void setUSER_REC_UID(String uSER_REC_UID) {
		USER_REC_UID = uSER_REC_UID;
	}

	@JsonIgnore
	public String getUSER_UID() {
		return USER_UID;
	}

	@JsonIgnore
	public void setUSER_UID(String uSER_UID) {
		USER_UID = uSER_UID;
	}

	@JsonIgnore
	public String getCATEGORY() {
		return CATEGORY;
	}

	@JsonIgnore
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}

	@JsonIgnore
	public String getTYPE() {
		return TYPE;
	}

	@JsonIgnore
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	@JsonIgnore
	public Map<Object, Object> getPAYLOAD() {
		return PAYLOAD;
	}

	@JsonIgnore
	public void setPAYLOAD(Map<Object, Object> pAYLOAD) {
		PAYLOAD = pAYLOAD;
	}

	@JsonIgnore
	public List<Map<Object, Object>> getDATALIST() {
		return DATALIST;
	}

	@JsonIgnore
	public void setDATALIST(List<Map<Object, Object>> dATALIST) {
		DATALIST = dATALIST;
	}

	@JsonIgnore
	public void setDef(EnumMessageDef emd) {

		setCATEGORY(emd.getCategory().getCategory());
		setTYPE(emd.getType().getType());
	}

	@JsonIgnore
	public void put(String key, Object value) {
		PAYLOAD.put(key, value);
	}

	@JsonIgnore
	public Object get(String key) {
		return PAYLOAD.get(key);
	}

}
