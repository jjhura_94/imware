package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class ScheduleMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;
	
	@MapKey("SCHEDULE_TYPE")
	private String scheduleType;
	
	@MapKey("MEETING_ROOM_NAME")
	private String meetingRoomName;
	
	@MapKey("BUSINESS_TRIP_NAME")
	private String businessTripName;
	
	@MapKey("EVENT_NAME")
	private String eventName;
	
	@MapKey("USER_UID")
	private String userUid;
	
	private ScheduleMessage() {
		this.type = ExtraMessageType.SCHEDULE.name();
	}

	public static SCHBranches builder() {
		return new Builder();
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getMeetingRoomName() {
		return meetingRoomName;
	}

	public void setMeetingRoomName(String meetingRoomName) {
		this.meetingRoomName = meetingRoomName;
	}

	public String getBusinessTripName() {
		return businessTripName;
	}

	public void setBusinessTripName(String businessTripName) {
		this.businessTripName = businessTripName;
	}
	
	public String getEventName() {
		return eventName;
	}
	
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}
	
	public static interface SCHBranches {
		/**
		 * Meeting room.
		 */
		SCHMeetingRoom meetingRoom();
		
		/**
		 * Business trip.
		 */
		SCHBusinessTrip businessTrip();
		
		/**
		 * Event.
		 */
		SCHEvent event();
	}
	
	public static interface SCHMeetingRoom {
		/**
		 * The room name you picked.
		 * @param roomName
		 */
		SCHUserUID roomName(String roomName);
	}
	
	public static interface SCHBusinessTrip {
		/**
		 * The business trip name or trip title.
		 * @param tripName
		 */
		SCHUserUID tripName(String tripName);
	}
	
	public static interface SCHEvent {
		/**
		 * The name of the event.
		 * @param eventName
		 */
		SCHUserUID eventName(String eventName);
	}
	
	public static interface SCHUserUID {
		/**
		 * User UID of user receive this message.
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}
	
	public static class Builder implements SCHBranches, SCHMeetingRoom, SCHBusinessTrip, SCHUserUID, SCHEvent {

		private String scheduleType;
		private String meetingRoomname;
		private String businessTripName;
		private String eventName;
		private String[] userUid;
		
		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public SCHUserUID tripName(String tripName) {
			this.businessTripName = tripName;
			return this;
		}

		@Override
		public SCHUserUID roomName(String roomName) {
			this.meetingRoomname = roomName;
			return this;
		}
		
		@Override
		public SCHUserUID eventName(String eventName) {
			this.eventName = eventName;
			return this;
		}

		@Override
		public SCHMeetingRoom meetingRoom() {
			this.scheduleType = ScheduleType.MEETING.name();
			return this;
		}

		@Override
		public SCHBusinessTrip businessTrip() {
			this.scheduleType = ScheduleType.BUSINESS.name();
			return this;
		}
		
		@Override
		public SCHEvent event() {
			this.scheduleType = ScheduleType.EVENT.name();
			return this;
		}
		
		public ScheduleMessage build() {
			ScheduleMessage message = new ScheduleMessage();
			message.setScheduleType(this.scheduleType);
			message.setMeetingRoomName(this.meetingRoomname);
			message.setBusinessTripName(this.businessTripName);
			message.setEventName(this.eventName);
			message.setUserUid(this.userUid);
			
			return message;
		}

	}
	
	private static enum ScheduleType {
		BUSINESS, MEETING, EVENT
	}
	
}
