package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TaskRequestMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("SEND_USER_UID")
	private String sendUserUid;

	@MapKey("RECEIVE_USER_UID")
	private String receiveUserUid;
	
	@MapKey("TASK_TITLE")
	private String taskTitle;
	
	@MapKey("PERMALINK")
	private String permalink;
	
	@MapKey("USER_UID")
	private String userUid;
	
	private TaskRequestMessage() {
		this.type = ExtraMessageType.TASK_REQUEST.name();
	}
	
	public static TRSendUserUID builder() {
		return new Builder();
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSendUserUid() {
		return sendUserUid;
	}

	public void setSendUserUid(String sendUserUid) {
		this.sendUserUid = sendUserUid;
	}

	public String getReceiveUserUid() {
		return receiveUserUid;
	}

	public void setReceiveUserUid(String receiveUserUid) {
		this.receiveUserUid = receiveUserUid;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public static interface TRSendUserUID {
		/**
		 * User UID of user send this task request.
		 * @param userUid
		 */
		TRReceiveUserUID sendUserUID(String userUid);
	}
	
	public static interface TRReceiveUserUID {
		/**
		 * User UID of user receive this task request.
		 * @param userUid
		 */
		TRTaskTitle receiveUserUID(String userUid);
	}
	
	public static interface TRTaskTitle {
		/**
		 * Task request title.
		 * @param title
		 */
		TRPermalink taskTitle(String title);
	}
	
	public static interface TRPermalink {
		/**
		 * HTTP link to the task request.
		 * @param link
		 */
		TRUserUID permalink(String link);
	}
	
	public static interface TRUserUID {
		/**
		 * User UID of user receive this message.
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}
	
	public static class Builder implements TRSendUserUID, TRReceiveUserUID, TRTaskTitle, TRPermalink, TRUserUID {
		
		private String sendUserUid;
		private String receiveUserUid;
		private String taskTitle;
		private String permalink;
		private String[] userUid;
		
		public TaskRequestMessage build() {
			TaskRequestMessage message = new TaskRequestMessage();
			message.setSendUserUid(this.sendUserUid);
			message.setReceiveUserUid(this.receiveUserUid);
			message.setTaskTitle(this.taskTitle);
			message.setPermalink(this.permalink);
			message.setUserUid(this.userUid);
			return message;
		}

		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public TRUserUID permalink(String link) {
			this.permalink = link;
			return this;
		}

		@Override
		public TRPermalink taskTitle(String title) {
			this.taskTitle = title;
			return this;
		}

		@Override
		public TRReceiveUserUID sendUserUID(String userUid) {
			this.sendUserUid = userUid;
			return this;
		}

		@Override
		public TRTaskTitle receiveUserUID(String userUid) {
			this.receiveUserUid = userUid;
			return this;
		}
		
	}

}
