package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class VacationConfirmationMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("ACCEPTED")
	private boolean accepted;

	@MapKey("USER_UID")
	private String userUid;
	
	private VacationConfirmationMessage() {
		this.type = ExtraMessageType.VACATION_CONFIRMATION.name();
	}
	
	public static VCAccepted builder() {
		return new Builder();
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public static interface VCAccepted {
		/**
		 * Whether your vacation confirmation request is accepted or rejected.
		 * @param accepted is true if accepted, false if rejected.
		 */
		VCUserUID accepted(boolean accepted);
	}
	
	public static interface VCUserUID {
		/**
		 * User UID of user receive this message.
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}
	
	public static class Builder implements VCAccepted, VCUserUID {
		
		private boolean accepted;
		private String[] userUid;
		
		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}
		@Override
		public VCUserUID accepted(boolean accepted) {
			this.accepted = accepted;
			return this;
		}
		
		public VacationConfirmationMessage build() {
			VacationConfirmationMessage message = new VacationConfirmationMessage();
			message.setAccepted(this.accepted);
			message.setUserUid(this.userUid);
			return message;
		}
		
	}

}
