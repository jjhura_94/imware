package com.a2m.imware.wschat.model;

public enum EnumRoomType {
	IN_CHATROOM("IN_CHATROOM"), IN_CONTACT("IN_CONTACT");

	private String roomType;

	EnumRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getValue() {
		return this.roomType;
	}
}
