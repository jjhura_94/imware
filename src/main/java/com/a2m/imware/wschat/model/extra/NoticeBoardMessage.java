package com.a2m.imware.wschat.model.extra;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class NoticeBoardMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("TITLE")
	private String title;

	@MapKey("DEPT_NAME")
	private String deptName;

	@MapKey("WRITER_NAME")
	private String writerName;

	@MapKey("WRITER_POSITION")
	private String writerPosition;

	@MapKey("DOWNLOAD_LIST")
	private List<Map<Object, Object>> downloadList;
	
	@MapKey("PERMALINK")
	private String permalink;

	@MapKey("USER_UID")
	private String userUid;

	private NoticeBoardMessage() {
		this.type = ExtraMessageType.NOTICE_BOARD.name();
	}
	
	public static NBTitle builder() {
		return new Builder();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getWriterPosition() {
		return writerPosition;
	}

	public void setWriterPosition(String writerPosition) {
		this.writerPosition = writerPosition;
	}

	public List<Map<Object, Object>> getDownloadList() {
		return downloadList;
	}

	public void setDownloadList(List<Map<Object, Object>> downloadList) {
		this.downloadList = downloadList;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public static interface NBTitle {
		/**
		 * Notice board title.
		 * @param title
		 */
		NBDeptName title(String title);
	}
	
	public static interface NBDeptName {
		/**
		 * Writer department name.
		 * @param deptName
		 */
		NBWriterName writerDeptName(String deptName);
	}
	
	public static interface NBWriterName {
		/**
		 * Writer name.
		 * @param name
		 */
		NBWriterPosition writerName(String name);
	}
	
	public static interface NBWriterPosition {
		/**
		 * Writer position.
		 * @param position
		 */
		NBUserUID writerPosition(String position);
	}

	public static interface NBUserUID {
		/**
		 * User UID of user receive this message.
		 * 
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}

	public static class Builder implements NBTitle, NBDeptName, NBWriterName, NBWriterPosition, NBUserUID {
		
		private String title;
		private String deptName;
		private String name;
		private String position;
		private List<DownloadLink> downloadList;
		private String permalink;
		private String[] userUid;
		
		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public NBUserUID writerPosition(String position) {
			this.position = position;
			return this;
		}

		@Override
		public NBWriterPosition writerName(String name) {
			this.name = name;
			return this;
		}

		@Override
		public NBWriterName writerDeptName(String deptName) {
			this.deptName = deptName;
			return this;
		}

		@Override
		public NBDeptName title(String title) {
			this.title = title;
			return this;
		}

		public Builder permalink(String link) {
			this.permalink = link;
			return this;
		}
		
		public Builder downloadList(List<DownloadLink> downloadList) {
			this.downloadList = downloadList;
			return this;
		}
		
		public NoticeBoardMessage build() {
			NoticeBoardMessage message = new NoticeBoardMessage();
			message.setTitle(this.title);
			message.setDeptName(this.deptName);
			message.setWriterName(this.name);
			message.setWriterPosition(this.position);
			if (this.downloadList != null) {
				message.setDownloadList(this.downloadList.stream().map(link -> link.toMap()).collect(Collectors.toList()));
			}
			message.setPermalink(this.permalink);
			message.setUserUid(this.userUid);
			
			return message;
		}

	}

	public static class DownloadLink extends MapAndObjectConversion {

		@MapKey("LINK_TITLE")
		private String linkTitle;

		@MapKey("LINK")
		private String link;

		public DownloadLink() {
			super();
		}

		public DownloadLink(String linkTitle, String link) {
			super();
			this.linkTitle = linkTitle;
			this.link = link;
		}

		public String getLinkTitle() {
			return linkTitle;
		}

		public void setLinkTitle(String linkTitle) {
			this.linkTitle = linkTitle;
		}

		public String getLink() {
			return link;
		}

		public void setLink(String link) {
			this.link = link;
		}

	}

}
