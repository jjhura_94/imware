package com.a2m.imware.wschat.model;

import java.lang.reflect.Field;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.adapter.standard.StandardWebSocketSession;

import com.a2m.imware.wschat.common.ObjectUtil;

@SuppressWarnings("all")
public class SessionModel extends InheritanceModel {

	public enum TYPE {
		MOBILE, WEB, PC
	}

	private PublicKey _public_key;
	private String _encrypted_personal_id;
	private String personal_id;

	private WebSocketSession webSession;
	private WebSocketSession pcSession;
	private WebSocketSession mobileSession;

	private PrivateKey _private_key;

	public SessionModel(WebSocketSession session) {
		String id = ObjectUtil.getUSER_UID_OR_PERSON_ID(session);
		setPersonal_id(id);
		setSessions(session);

	}

	public void setSessions(WebSocketSession session) {
		String type_login = (String) session.getAttributes().get("TYPE_LOGIN");

		if ("PC".equals(type_login)) {
			setPcSession((StandardWebSocketSession) session);
		} else if ("MOBILE".equals(type_login)) {
			setMobileSession((StandardWebSocketSession) session);
		} else {
			setWebSession((StandardWebSocketSession) session);
		}

	}

	public TYPE getType() {
		if (webSession != null) {
			return TYPE.WEB;
		} else if (mobileSession != null) {
			return TYPE.MOBILE;
		} else if (pcSession != null) {
			return TYPE.PC;
		}
		return null;
	}

	public PublicKey get_public_key() {
		return _public_key;
	}

	public void set_public_key(PublicKey _public_key) {
		this._public_key = _public_key;
	}

	private String get_encrypted_personal_id() {
		return _encrypted_personal_id;
	}

	private void set_encrypted_personal_id(String _encrypted_personal_id) {
		this._encrypted_personal_id = _encrypted_personal_id;
	}

	public String getPersonal_id() {
		return personal_id;
	}

	public void setPersonal_id(String personal_id) {
		this.personal_id = personal_id;
	}

	public boolean validate_PERSONAL_ID(String _PERSONAL_ID) {
		System.out.println(this.personal_id);
		System.out.println(_PERSONAL_ID);
		if (this.personal_id != null) {
			return this.personal_id.equals(_PERSONAL_ID);
		} else {
			return false;
		}
	}

	public void printAll() {
		Class cls = this.getClass();
		Field[] fields = cls.getDeclaredFields();

		try {
			System.out.println("================================================");
			for (Field fd : fields) {
				System.out.println(fd.getName() + ": " + fd.get(this));
			}
			System.out.println("==================================================================");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	public List<WebSocketSession> getSessions() {
		List<WebSocketSession> list = new ArrayList<WebSocketSession>();

		if (getWebSession() != null)
			list.add(getWebSession());
		if (getPcSession() != null)
			list.add(getPcSession());
		if (getMobileSession() != null)
			list.add(getMobileSession());

		return list;
	}

	public WebSocketSession getWebSession() {
		return webSession;
	}

	public void setWebSession(WebSocketSession webSession) {
		this.webSession = webSession;
	}

	public WebSocketSession getPcSession() {
		return pcSession;
	}

	public void setPcSession(WebSocketSession pcSession) {
		this.pcSession = pcSession;
	}

	public WebSocketSession getMobileSession() {
		return mobileSession;
	}

	public void setMobileSession(WebSocketSession mobileSession) {
		this.mobileSession = mobileSession;
	}

	private PrivateKey get_private_key() {
		return _private_key;
	}

	private void set_private_key(PrivateKey _private_key) {
		this._private_key = _private_key;
	}

	public boolean containsSession(WebSocketSession session) {
		boolean bool = false;

		if ((getWebSession() != null && getWebSession().equals(session))
				|| (getPcSession() != null && getPcSession().equals(session))
				|| (getMobileSession() != null && getMobileSession().equals(session))) {
			bool = true;
		}
		return bool;
	}

}
