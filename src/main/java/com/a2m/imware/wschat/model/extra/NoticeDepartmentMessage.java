package com.a2m.imware.wschat.model.extra;

import org.apache.commons.lang3.StringUtils;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class NoticeDepartmentMessage extends MapAndObjectConversion {

	@MapKey("TYPE")
	private String type;

	@MapKey("TITLE")
	private String title;

	@MapKey("PERMALINK")
	private String permalink;

	@MapKey("USER_UID")
	private String userUid;

	private NoticeDepartmentMessage() {
		this.type = ExtraMessageType.NOTICE_DEPARTMENT.name();
	}

	public static NDTitle builder() {
		return new Builder();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String... userUid) {
		this.userUid = StringUtils.join(userUid, ",");
	}

	public static interface NDTitle {
		/**
		 * Notice title.
		 * 
		 * @param title
		 */
		NDUserUID title(String title);
	}

	public static interface NDUserUID {
		/**
		 * User UID of user receive this message.
		 * 
		 * @param userUid
		 */
		Builder userUID(String... userUid);
	}

	public static class Builder implements NDTitle, NDUserUID {

		private String title;
		private String[] userUid;
		private String permalink;

		@Override
		public Builder userUID(String... userUid) {
			this.userUid = userUid;
			return this;
		}

		@Override
		public NDUserUID title(String title) {
			this.title = title;
			return this;
		}

		public Builder permalink(String link) {
			this.permalink = link;
			return this;
		}

		public NoticeDepartmentMessage build() {
			NoticeDepartmentMessage message = new NoticeDepartmentMessage();
			message.setTitle(this.title);
			message.setUserUid(this.userUid);
			message.setPermalink(this.permalink);

			return message;
		}

	}

}
