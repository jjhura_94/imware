package com.a2m.imware.wschat.model.extra;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.manager.MessageModelCmdMng;
import com.a2m.imware.wschat.model.MessageModel;

@Repository
public class ExtraMessageUtil {

	@Autowired
	private MessageModelCmdMng cmd;

	/**
	 * Send a vacation confirmation message to messenger. Use this function when
	 * accepted/rejected vacation.
	 * 
	 * @param message
	 */
	public void vacationConfirmation(VacationConfirmationMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a task request message to messenger. Use this function when register a
	 * task.
	 * 
	 * @param message
	 */
	public void taskRequest(TaskRequestMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a schedule message to messenger. Use this function when remain 30min
	 * before start a meeting room or 1h before start a business trip.
	 * 
	 * @param message
	 */
	public void schedule(ScheduleMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a employment certification message to messenger.
	 * 
	 * @param message
	 */
	public void employmentCertification(ECBCMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a business card request/handled message to messenger. Use this function
	 * when send to make a business card request or handled a business card request.
	 * 
	 * @param message
	 */
	public void businessCard(ECBCMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a notice board message to messenger. Use this function when post new
	 * notice.
	 * 
	 * @param message
	 */
	public void noticeBoard(NoticeBoardMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a department notice message to messenger. Use this function when post a
	 * department notice.
	 * 
	 * @param message
	 */
	public void noticeDepartment(NoticeDepartmentMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send a notice new member message to messenger. Use this function when a new
	 * member join with us.
	 * 
	 * @param message
	 */
	public void noticeNewMember(NoticeNewMemberMessage message) {
		if (message == null || message.getUserUid() == null || message.getUserUid().isEmpty())
			return;

		this.sendMessage(message.toMap());
	}

	/**
	 * Send messages asynchronous.
	 * 
	 * @param payload
	 */
	private void sendMessage(Map<Object, Object> payload) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				MessageModel messageModel = new MessageModel();
				final String userUid = payload.get("USER_UID") == null ? null : payload.get("USER_UID").toString();
				if (Utils.isNullOrEmpty(userUid))
					return;
				if (userUid.contains(",")) {
					Arrays.asList(userUid.split(",")).stream().forEach(uid -> {
						payload.put("USER_UID", uid.trim());
						sendSingleMessage(payload, messageModel);
					});
				} else {
					sendSingleMessage(payload, messageModel);
				}
			}
		}).start();
	}

	private void sendSingleMessage(Map<Object, Object> payload, MessageModel messageModel) {
		messageModel.setPAYLOAD(payload);
		try {
			cmd.CHAT_EXTRA_MESSAGE(null, messageModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
