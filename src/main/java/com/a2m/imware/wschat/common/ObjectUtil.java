package com.a2m.imware.wschat.common;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;

public class ObjectUtil {

	public static Map<String, Object> getMap(Object obj) throws IllegalAccessException {
		Field[] fields = obj.getClass().getDeclaredFields();
		Map<String, Object> resultMap = new HashMap<>();
		for (int i = 0; i <= fields.length - 1; i++) {
			fields[i].setAccessible(true);
			if (fields[i].get(obj) != null)
				resultMap.put(fields[i].getName(), fields[i].get(obj));
		}
		return resultMap;
	}

	public static Object setMap(Map<?, ?> map, Object objClass)
			throws InvocationTargetException, IllegalAccessException {
		String keyAttribute = null;
		String setMethodString = "set";
		String methodString = null;
		Method[] methods = objClass.getClass().getDeclaredMethods();

		for (Object o : map.keySet()) {
			keyAttribute = (String) o;
			methodString = setMethodString + keyAttribute.substring(0, 1).toUpperCase() + keyAttribute.substring(1);

			for (int i = 0; i <= methods.length - 1; i++) {
				if (methodString.equals(methods[i].getName())) {
					methods[i].invoke(objClass, map.get(keyAttribute));
				}
			}
		}
		return objClass;
	}

	// Author: ngocdb
	// Get USER_UID is PERSON_ID, PERSON_ID is USER_UID
	// One PERSION_ID has websocket session, pc socket seeion, mobile socket session
	public static String getUSER_UID_OR_PERSON_ID(WebSocketSession session) {
		// USER_UID was saved when login okay
		String user_uid = (String) session.getAttributes().get("USER_UID");
		return user_uid;
	}

}
