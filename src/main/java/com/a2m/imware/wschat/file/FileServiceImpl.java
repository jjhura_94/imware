package com.a2m.imware.wschat.file;

import static org.bytedeco.javacpp.helper.opencv_imgcodecs.cvSaveImage;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.a2m.imware.wschat.util.ImageUtil;

@Service
public class FileServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

	public Map<Object, Object> makeThumbnail(Map<Object, Object> parameter) throws Exception {
		Map<Object, Object> result = new HashMap<>();
		makeThumbnail(parameter, result);

		return result;
	}

	private void makeThumbnail(Map<Object, Object> parameter, Map<Object, Object> result) throws Exception {
		try {
			String filepath = parameter.get("path").toString();
			File fileUpload = new File(filepath);

			String path = filepath.split(fileUpload.getName())[0];
			int posDot = fileUpload.getName().toString().lastIndexOf(".");
			String ext = "";
			if (posDot > 0) {
				ext = fileUpload.getName().substring(posDot + 1);
			}

			File file = new File(path + "/thum");
			file.setExecutable(true, true);
			file.setReadable(true);
			file.setWritable(true, true);
			if (!file.isDirectory()) {
				file.mkdirs();
			}

			List<String> images = Arrays.asList("jpg", "bmp", "png");

			if (images.contains(ext)) {
				BufferedImage in = ImageIO.read(new File(filepath));
				ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in,
						file.getAbsolutePath() + File.separator + fileUpload.getName());
			} else {
				// video
				OpenCVFrameConverter.ToIplImage converterToIplImage = new OpenCVFrameConverter.ToIplImage();

				FFmpegFrameGrabber g = new FFmpegFrameGrabber(filepath);
				g.setFormat(ext == "" ? "mp4" : ext);
				g.start();

				for (int i = 0; i < 60; i++) {
					if (g.grab().image != null) {
						IplImage image = converterToIplImage.convert(g.grab());
						String thumbPath = path + "thumb/" + fileUpload.getName().split("[.]")[0] + ".png";
						cvSaveImage(thumbPath, image);

					}
				}

				g.stop();
				g.close();

				BufferedImage in2 = ImageIO
						.read(new File(path + "thumb/" + fileUpload.getName().split("[.]")[0] + ".png"));
				ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in2,
						path + "thumb/" + fileUpload.getName().split("[.]")[0] + ".png");

			}

		} catch (IOException e) {
			e.printStackTrace();
			logger.info("io exception in file save about video thumbnail");
			result.put("error", "02");
		}
	}

}