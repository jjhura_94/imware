package com.a2m.imware.wschat.file;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.a2m.imware.wschat.model.EnumRoomType;
import com.a2m.imware.wschat.module.chatroom.ChatroomServiceImpl;
import com.a2m.imware.wschat.module.filemessage.FileMsgServiceImpl;
import com.a2m.imware.wschat.util.ImageUtil;
import com.a2m.imware.wschat.util.ParameterUtil;

@Controller
@RequestMapping("/wschat/common/file/")
public class FileController {

	private static Logger logger = LoggerFactory.getLogger(FileController.class);
	private static final String UPLOAD_ROOT_PATH_NAME = "images/wc";

	@Autowired
	private FileServiceImpl fileService;

	@Autowired
	private FileMsgServiceImpl fileMsgService;

	@Autowired
	private ChatroomServiceImpl roomService;

	@Autowired
	private Environment env;

	@SuppressWarnings("unchecked")
	@RequestMapping("/DwnlMsgFileNew.ajax")
	public void downloadMsgFileNew(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<Object, Object> parameter = ParameterUtil.getParameterMap(request);
		Map<Object, Object> fileMap = fileMsgService.getMsgFileMap(parameter);

		String sysFileName = "" + fileMap.get("FILE_MODI_NM");
		String orgFileName = "" + fileMap.get("FILE_ORI_NM");
		String filePath = "" + fileMap.get("FILE_PATH");
		String ext = "" + fileMap.get("FILE_EXTN").toString().substring(1);

		// 파일명 깨짐대응 문자열 인코딩 처리
		orgFileName = java.net.URLEncoder.encode(orgFileName, "UTF-8").replaceAll("\\+", "%20");
		List<String> images = Arrays.asList("jpg", "bmp", "png");

		final String uploadDir = env.getProperty("wschat.file.uploadDir");
		if (parameter.get("ACTION_TYPE") != null && parameter.get("ACTION_TYPE").equals("THUMB")
				&& fileMap.get("FILE_TYPE") != null ? fileMap.get("FILE_TYPE").toString().contains("video") : false) {
			String videofile = uploadDir + File.separator + "thumb" + File.separator + sysFileName.split(".")[0]
					+ ".png";
			filePath = videofile;
			if (parameter.get("type") != null && parameter.get("type").equals("slide")) {
				filePath = uploadDir + File.separator + "thumb" + File.separator + sysFileName;
			}
		}

		if (parameter.get("ACTION_TYPE") != null && parameter.get("ACTION_TYPE").equals("THUMB")
				&& fileMap.get("FILE_TYPE") != null
				&& (fileMap.get("FILE_TYPE").toString().contains("image") || images.contains(ext))) {
			filePath = uploadDir + File.separator + "thumb" + File.separator + sysFileName;
		}

		if (!filePath.contains(uploadDir))
			filePath = uploadDir + File.separator + filePath;
		File file = new File(filePath);

		if (file.isFile()) {
			if ("pdf".equalsIgnoreCase(ext)) {
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "inline; filename=\"" + orgFileName + "\"");
			} else if ("mp4".equalsIgnoreCase(ext)) {
				if (parameter.get("ACTION_TYPE") != null && parameter.get("ACTION_TYPE").equals("THUMB")) {
					response.setHeader("Content-Disposition",
							"inline; filename=\"" + sysFileName.split("[.]")[0] + ".png" + "\"");
				} else if (parameter.get("ACTION_TYPE") != null && parameter.get("ACTION_TYPE").equals("DOWNLOAD")) {
					response.setHeader("Content-Disposition", "attachment; filename=\"" + orgFileName + "\"");
				} else {
					response.setContentType("video/mp4");
					response.setHeader("Content-Disposition", "inline; filename=\"" + orgFileName + "\"");
				}

			} else if ("mp3".equalsIgnoreCase(ext)) {
				if (parameter.get("ACTION_TYPE") != null && parameter.get("ACTION_TYPE").equals("DOWNLOAD")) {
					response.setHeader("Content-Disposition", "attachment; filename=\"" + orgFileName + "\"");
				} else {
					response.setContentType("audio/mp3");
					response.setHeader("Content-Disposition", "inline; filename=\"" + orgFileName + "\"");
				}
			} else {
				response.setHeader("Content-Disposition", "attachment; filename=\"" + orgFileName + "\"");
			}
			response.setHeader("Accept-Ranges", "bytes");
			long contentLength = file.length();
			response.setHeader("Content-Length", "" + contentLength);

			byte b[] = new byte[8192];
			BufferedInputStream input = null;
			BufferedOutputStream output = null;

			try {
				input = new BufferedInputStream(new FileInputStream(file));
				output = new BufferedOutputStream(response.getOutputStream());

				int read = 0;

				if (contentLength > 0) {
					while (contentLength > 0) {
						if (contentLength < 8192) {
							b = new byte[(int) contentLength];
						}

						read = input.read(b);

						if (read == -1) {
							break;
						}

						output.write(b, 0, read);
						contentLength = contentLength - read;
					}
				}
			} catch (Exception e) {
				logger.info("예외가 발생하였습니다.");
			} finally {
				if (output != null) {
					output.flush();
					output.close();
				}

				if (input != null) {
					input.close();
				}
			}
		}
	}

	@RequestMapping("/getBase64.ajax")
	@ResponseBody
	public Object getBase64(HttpServletRequest request, HttpServletResponse response) {
		String newFilePath = "";
		File originalFile = new File(newFilePath);
		String encodedBase64 = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
			byte[] bytes = new byte[(int) originalFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));

			String fileNameOnly = newFilePath.split("/")[newFilePath.split("/").length - 1];
			String path = newFilePath.substring(0, newFilePath.indexOf(fileNameOnly));

			File uploadRootDirTN = new File(path + "/thumb");
			uploadRootDirTN.setExecutable(true, true);
			uploadRootDirTN.setReadable(true);
			uploadRootDirTN.setWritable(true, true);
			// Create directory if it not exists.
			if (!uploadRootDirTN.exists()) {
				uploadRootDirTN.mkdirs();
			}
			BufferedImage in = ImageIO.read(originalFile);
			ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in,
					uploadRootDirTN.getAbsolutePath() + File.separator + fileNameOnly);
			fileInputStreamReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return encodedBase64;
	}

	@RequestMapping("/uploadFiles.ajax")
	@ResponseBody
	public Object saveFile(@RequestParam("FILE") MultipartFile imageFile) throws Exception {
		String newName = doUpload3(imageFile);

		return newName;
	}

	private String doUpload3(MultipartFile uploadfile) throws Exception {
		String fileName = null;
		String newFileName = null;
		File uploadRootDir = null;
		final String rootPath = env.getProperty("wschat.file.uploadDir");

		if (uploadfile instanceof MultipartFile) {
			MultipartFile multipartFile = (MultipartFile) uploadfile;

			uploadRootDir = new File(rootPath);
			uploadRootDir.setExecutable(true, true);
			uploadRootDir.setReadable(true);
			uploadRootDir.setWritable(true, true);
			// Create directory if it not exists.
			if (!uploadRootDir.exists()) {
				uploadRootDir.mkdirs();
			}

			fileName = multipartFile.getOriginalFilename();

			if (fileName != null && fileName.length() > 0) {
				try {
					// Create file in Server.
					int ext_pos = fileName.lastIndexOf(".");
					String ext = fileName.substring(ext_pos).toLowerCase();
					newFileName = "PROF_" + (new Date()).getTime() + ext;

					File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + newFileName);

					// Ouput stream file in Server.
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(multipartFile.getBytes());
					stream.close();
					BufferedImage in = ImageIO.read(serverFile);
					String[] a = { "PNG", "JPG", "BMP" };
					String[] b = { "MP4", "MOV", "WMV" };

					if (Arrays.asList(a).contains(ext.substring(1).toUpperCase())) {
						File uploadRootDirTN = null;
						uploadRootDirTN = new File(rootPath + "/thumb");
						uploadRootDirTN.setExecutable(true, true);
						uploadRootDirTN.setReadable(true);
						uploadRootDirTN.setWritable(true, true);
						// Create directory if it not exists.
						if (!uploadRootDirTN.exists()) {
							uploadRootDirTN.mkdirs();
						}
						ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in,
								uploadRootDirTN.getAbsolutePath() + File.separator + newFileName);
					} else if (Arrays.asList(b).contains(ext.substring(1).toUpperCase())) {
						/*
						 * File uploadRootDirTN = null; uploadRootDirTN = new File(rootPath + "/thumb");
						 * uploadRootDirTN.setExecutable(true, true); uploadRootDirTN.setReadable(true);
						 * uploadRootDirTN.setWritable(true, true); // Create directory if it not
						 * exists. if (!uploadRootDirTN.exists()) { uploadRootDirTN.mkdirs(); }
						 * OpenCVFrameConverter.ToIplImage converterToIplImage = new
						 * OpenCVFrameConverter.ToIplImage();
						 * 
						 * FFmpegFrameGrabber g = new FFmpegFrameGrabber(serverFile);
						 * g.setFormat(ext.substring(1).toUpperCase() == "" ? "mp4" :
						 * ext.substring(1).toUpperCase()); g.start();
						 * 
						 * for (int i = 0; i < 60; i++) { if (g.grab().image != null) { IplImage image =
						 * converterToIplImage.convert(g.grab()); String thumbPath = rootPath +
						 * "/thumb/" + serverFile.getName().split("[.]")[0] + ".png";
						 * cvSaveImage(thumbPath, image); } }
						 * 
						 * g.stop(); g.close();
						 * 
						 * BufferedImage in2 = ImageIO .read(new File(rootPath + "/thumb/" +
						 * serverFile.getName().split("[.]")[0] + ".png"));
						 * ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in2, rootPath + "/thumb/" +
						 * serverFile.getName().split("[.]")[0] + ".png");
						 */
					}
				} catch (Exception e) {
					throw e;
				}
			}
		}

		return newFileName;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/getListFile.ajax")
	@ResponseBody
	public Object getListFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<Object, Object> parameter = ParameterUtil.getParameterMapWithOutSession(request);
		if (Strings.isEmpty((String) parameter.get("ORDER_BY"))) {
			parameter.put("ORDER_BY", "DESC");
		}

		try {
			List<Map<Object, Object>> list = fileMsgService.getListFileByRoomId(parameter);
			if (list == null) {
				list = new ArrayList<>(0);
			}

			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	// REST API Upload file
	// save Image profile
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveImgProfile2.ajax", method = RequestMethod.POST)
	@ResponseBody
	public Object saveImgProfile2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<Object, Object> parameter = ParameterUtil.getParameterMap(request);
		Map<String, Object> form = new HashMap<>();
		String imgData = parameter.get("IMG_FILE").toString();
		String fileName = parameter.get("FILE_NAME").toString();
		String base64Image = imgData.split(",")[1];
		BufferedImage image = null;
		byte[] imageByte;
		try {
			imageByte = Base64.decodeBase64(base64Image);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		image = ImageUtil.cropImageToSquare(image);
		File outputfile = new File(fileName);
		ImageIO.write(image, "png", outputfile);

		String imgName = doUpload2(outputfile, fileName);
		form.put("USER_IMG", "image" + "/" + imgName);
		// form.put("USER_UID", session.get("USER_UID"));
		// String res = adm0101svc.save02(form);
		// TODO: need to save user avatar...

		return "OK";
	}

	public static byte[] convertToImg(String base64) throws IOException {
		return Base64.decodeBase64(base64);
	}

	public static void writeByteToImageFile(byte[] imgBytes, String imgFileName) throws IOException {
		File imgFile = new File(imgFileName);
		BufferedImage img = ImageIO.read(new ByteArrayInputStream(imgBytes));
		ImageIO.write(img, "png", imgFile);
	}

	@RequestMapping("/saveImgProfile.ajax")
	@ResponseBody
	public Object saveImgProfile(@RequestParam("FILE") MultipartFile uploadfile,
			@RequestParam("USER_UID") String USER_UID) throws Exception {
		if (uploadfile == null || uploadfile.isEmpty() || Strings.isEmpty(USER_UID))
			return "ERROR";

		Map<String, Object> form = new HashMap<String, Object>();
		// Upload image
		String imgName = doUpload(uploadfile);
		form.put("USER_IMG", UPLOAD_ROOT_PATH_NAME + "/" + imgName);
		form.put("USER_UID", USER_UID);
		// String res = adm0101svc.save02(form);
		// TODO: need to save user avatar...

		return "OK";
	}

	// save Image group avatar
	@RequestMapping("/saveImgRoom.ajax")
	@ResponseBody
	public Object saveImgRoom(@RequestParam("FILE2") MultipartFile uploadfile,
			@RequestParam("USER_UID2") String USER_UID, @RequestParam("ROOM_UID2") String ROOM_UID) throws Exception {
		if (uploadfile == null || uploadfile.isEmpty() || Strings.isEmpty(USER_UID) || Strings.isEmpty(ROOM_UID))
			return "ERROR";

		Map<Object, Object> form = new HashMap<>();
		form.put("USER_UID", USER_UID);
		form.put("ROOM_UID", ROOM_UID);
		Map<Object, Object> fRoom = roomService.getRoomByKey(form);
		if (fRoom != null && !fRoom.isEmpty()) {
			if (EnumRoomType.IN_CHATROOM.getValue().equals(fRoom.get("ROOM_TYPE"))) {
				// Upload image
				String imgName = doUpload(uploadfile);
				form.put("ROOM_IMG", ConstantsFile.UPLOAD_CONTEXT_IMAGE + "/" + imgName);
				// String res = adm0101svc.save03(form);
				// TODO: need to save room image (room avatar)
			}
		}

		return "OK";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/makeThumbnail.ajax")
	@ResponseBody
	public Object makeThumbnail(HttpServletRequest request, HttpServletResponse response) {
		Map<Object, Object> result = new HashMap<>();
		try {
			Map<Object, Object> parameter = ParameterUtil.getParameterMapWithOutSession(request);
			result = fileService.makeThumbnail(parameter);
			result.put("error", "00");
		} catch (Exception e) {
			logger.info("make thumbnail error");
			result.put("error", "04");
		}

		return "";
	}

	// upload Image and crop to square
	private String doUpload(MultipartFile uploadfile) throws Exception {
		String imgName = null;
		String newFileName = null;
		String rootPath = env.getProperty("wschat.file.uploadDir");

		if (true) {
			if (uploadfile instanceof MultipartFile) {
				MultipartFile imgFile = (MultipartFile) uploadfile;

				// Check file is an image.
				// String[] strd = { "IMG", "" };
				if (!imgFile.getContentType().startsWith("image/")) {
					throw new Exception("An image is required!");
				}

				File uploadRootDir = new File(rootPath + UPLOAD_ROOT_PATH_NAME);
				uploadRootDir.setExecutable(true, true);
				uploadRootDir.setReadable(true);
				uploadRootDir.setWritable(true, true);
				// Create directory if it not exists.
				if (!uploadRootDir.exists()) {
					uploadRootDir.mkdirs();
				}

				imgName = imgFile.getOriginalFilename();

				if (imgName != null && imgName.length() > 0) {
					try {
						// Create file in Server.
						int ext_pos = imgName.lastIndexOf(".");
						String ext = imgName.substring(ext_pos).toLowerCase();
						newFileName = "PROF_" + (new Date()).getTime() + ext;

						File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + newFileName);

						// Ouput stream file in Server.
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
						stream.write(imgFile.getBytes());
						stream.close();
						BufferedImage in = ImageIO.read(serverFile);
						in = ImageUtil.cropImageToSquare(in);
						ImageIO.write(in, ext.substring(1), serverFile);

						File uploadRootDirTN = new File(rootPath + UPLOAD_ROOT_PATH_NAME + "/thumb");
						uploadRootDirTN.setExecutable(true, true);
						uploadRootDirTN.setReadable(true);
						uploadRootDirTN.setWritable(true, true);
						// Create directory if it not exists.
						if (!uploadRootDirTN.exists()) {
							uploadRootDirTN.mkdirs();
						}

						ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in,
								uploadRootDirTN.getAbsolutePath() + File.separator + newFileName);

					} catch (Exception e) {
						throw e;
					}
				}
			}
		}

		return newFileName;
	}

	private String doUpload2(File file, String fileName) throws Exception {
		String rootPath = env.getProperty("wschat.file.uploadDir");

		int ext_pos = fileName.lastIndexOf(".");
		String ext = fileName.substring(ext_pos).toLowerCase();
		String newFileName = "PROF_" + (new Date()).getTime() + ext;

		try {
			File uploadRootDir = new File(rootPath + "image");
			uploadRootDir.setExecutable(true, true);
			uploadRootDir.setReadable(true);
			uploadRootDir.setWritable(true, true);
			// Create directory if it not exists.
			if (!uploadRootDir.exists()) {
				uploadRootDir.mkdirs();
			}

			// Create file in Server.
			File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + newFileName);

			BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(serverFile));

			byte[] buff = new byte[32 * 1024];
			int len;
			while ((len = in.read(buff)) > 0)
				out.write(buff, 0, len);
			in.close();
			out.close();

			File uploadRootDirTN = new File(rootPath + UPLOAD_ROOT_PATH_NAME + "/thumb");
			uploadRootDirTN.setExecutable(true, true);
			uploadRootDirTN.setReadable(true);
			uploadRootDirTN.setWritable(true, true);
			// Create directory if it not exists.
			if (!uploadRootDirTN.exists()) {
				uploadRootDirTN.mkdirs();
			}
			BufferedImage buffImage = ImageIO.read(serverFile);

			ImageUtil.resizeFromBufferedImageByMaxEdgeSize(buffImage,
					uploadRootDirTN.getAbsolutePath() + File.separator + newFileName);
		} catch (Exception e) {
			throw e;
		}
		return newFileName;
	}

	@RequestMapping("/downloadZip")
	public void downloadZip(@RequestParam List<String> msgUidList, HttpServletResponse response) {
		if (msgUidList == null || msgUidList.isEmpty())
			return;

		try {
			List<Map<Object, Object>> fileList = fileMsgService.findByMsgUidList(msgUidList);
			if (fileList == null || fileList.isEmpty())
				return;

			List<File> filesToZip = new ArrayList<File>();
			final String baseDir = env.getProperty("wschat.file.uploadDir") + File.separator;
			File tempDir = Files.createTempDirectory("WSCHAT").toFile();
			for (Map<Object, Object> fileMap : fileList) {
				if (fileMap == null || fileMap.isEmpty() || !fileMap.containsKey("FILE_MODI_NM")
						|| fileMap.get("FILE_MODI_NM") == null)
					continue;
				File file = new File(baseDir + fileMap.get("FILE_MODI_NM").toString());
				if (!file.exists())
					continue;

				File destFile = new File(
						tempDir.getAbsolutePath() + File.separator + fileMap.get("FILE_ORI_NM").toString());
				File copiedFile = Files.copy(file.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
						.toFile();
				if (!copiedFile.exists())
					continue;
				filesToZip.add(copiedFile);
			}

			if (!filesToZip.isEmpty()) {
				File fileToDownload = zipFiles(filesToZip);
				if (fileToDownload != null) {
					FileInputStream fis = new FileInputStream(fileToDownload);
					response.setContentType("application/octet-stream");
					response.setHeader("Content-Disposition", "attachment; filename=" + fileToDownload.getName());
					response.setContentLength((int) fileToDownload.length());
					IOUtils.copy(fis, response.getOutputStream());
					response.flushBuffer();
					fis.close();
					FileUtils.forceDelete(fileToDownload);
				}
			}
			FileUtils.deleteDirectory(tempDir);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private File zipFiles(List<File> filesToZip) {
		File tempFile = null;
		try {
			tempFile = File.createTempFile("WSCHAT-", ".zip");
			FileOutputStream fos = new FileOutputStream(tempFile);
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			for (File fileToZip : filesToZip) {
				FileInputStream fis = new FileInputStream(fileToZip);
				ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
				zipOut.putNextEntry(zipEntry);

				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
				fis.close();
			}

			zipOut.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return tempFile;
	}

}
