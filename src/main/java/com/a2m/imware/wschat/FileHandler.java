package com.a2m.imware.wschat;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import com.a2m.imware.wschat.common.ObjectUtil;
import com.a2m.imware.wschat.manager.FileSessionStateMng;
import com.a2m.imware.wschat.manager.MessageSender;
import com.a2m.imware.wschat.model.MessageModel;
import com.a2m.imware.wschat.util.CommonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class FileHandler extends BinaryWebSocketHandler {
	private Logger logger = LoggerFactory.getLogger(FileHandler.class);

	/**
	 * 서버에 연결한 사용자들을 저장하는 리스트
	 */

	/**
	 * 접속과 관련된 Event Method
	 *
	 * @param WebSocketSession 접속한 사용자
	 */
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

		super.afterConnectionEstablished(session);

		logger.info("------------File WS------------");
		logger.info("USER_UID: " + session.getAttributes().get("USER_UID"));
		logger.info("SessionId: " + session.getId() + " has connected.");

		FileSessionStateMng.getInstance().setSessionModel(session);

	}

	/**
	 * 두 가지 이벤트를 처리
	 *
	 * 1. Send : 클라이언트가 서버에게 메시지를 보냄 2. Emit : 서버에 연결되어 있는 클라이언트에게 메시지를 보냄
	 *
	 * @param WebSocketSession 메시지를 보낸 클라이언트
	 * @param TextMessage      메시지의 내용
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */

	/**
	 * 클라이언트가 서버와 연결을 끊었을때 실행되는 메소드
	 *
	 * @param WebSocketSession 연결을 끊은 클라이언트
	 * @param CloseStatus      연결 상태(확인 필요함)
	 */
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		super.afterConnectionClosed(session, status);
		logger.info("------------File WS------------");
		logger.info("USER_UID: " + session.getAttributes().get("USER_UID"));
		logger.info("SessionId: " + session.getId() + " closed.");
//		finishUpload(session.getAttributes().get("USER_UID").toString() + "_"+session.getId());
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		logger.info("handleTransportError " + session.getId());
		logger.error(exception.getMessage());
		exception.printStackTrace();
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		// System.out.println("Text message");
		// Check http session logout

		// File file = new File("E:/tmp.txt");
		try {

			String payload = ((TextMessage) message).getPayload();
			ObjectMapper mapper = new ObjectMapper();
			String sess_key = ObjectUtil.getUSER_UID_OR_PERSON_ID(session) + "_" + session.getId();
			MessageModel mm = new MessageModel();
			mm.setUSER_UID(sess_key);
//        	boolean isTimeOut = !HttpSessionOutMgn.getInstance().contains(sess_key);
//    		if(isTimeOut) {
//    			//AUTH_LOGOUT
//    			mm.setCATEGORY("AUTH");
//    			mm.setTYPE("EXPIRE"); //AUTH_EXPIRE   	 LOGOUT		
//    			HttpSessionOutMgn.getInstance().remove(sess_key);
//    			MessageSender.sendMessageModel(mm.getUSER_UID(), mm);
//    			SessionModelStateMng.getInstance().destroySession(session);
//    			return;
//    		}

			if (payload.startsWith("STOR:")) {
				System.out.println("Text message: " + sess_key);

				Map<String, Object> mapRecieve = mapper.readValue(payload.substring(payload.indexOf("STOR:") + 6),
						new TypeReference<Map<String, Object>>() {
						});
				mapRecieve.put("sess_key", sess_key);
				initializeUpload(mapRecieve);
			} else if (payload.startsWith("DATA:")) {
				// First decode the data received as a base64 string.
				// message.getd
				System.out.println("Data receive: " + sess_key);
				Map<String, Object> mapRecieve = mapper.readValue(
						payload.substring(payload.indexOf("DATA:") + "DATA:".length()).trim(),
						new TypeReference<Map<String, Object>>() {
						});
				mapRecieve.put("sess_key", sess_key);
				addData(mapRecieve);
			}

			/*
			 * FileChannel channel = new FileOutputStream(file, false).getChannel(); //
			 * Flips this buffer. The limit is set to the current position and then // the
			 * position is set to zero. If the mark is defined then it is discarded.
			 * //buf.flip(); ByteBuffer buf = ((BinaryMessage) message).getPayload(); //
			 * Writes a sequence of bytes to this channel from the given buffer.
			 * channel.write(buf);
			 * 
			 * // close the channel channel.close();
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * byte readdata = (byte) -999; while(readdata!=-1) {
		 * readdata=message.getPayload(); try { fos.write(readdata); } catch
		 * (IOException e) { e.printStackTrace(); } }
		 */

	}

	Map<String, FileUpload> upload = new HashMap<String, FileUpload>();

	/**
	 * Initializes the upload for a given client
	 * 
	 * @param $user  : the client that is uploading a file
	 * @param $infos : the informations of the file
	 */
	public void initializeUpload(Map infos) {
		// Add the upload to the list
		FileUpload fileUpload = new FileUpload(((String) infos.get("filename")), (int) (infos.get("size")));
		// String user_uid = (String) infos.get("fileid");
		fileUpload.setIsVideo(infos.get("fileType").toString());
		String sess_key = (String) infos.get("sess_key");
		upload.put(sess_key, fileUpload);
		// Sends a response to the client
		MessageModel mm = new MessageModel();
		Map param = new HashMap();
		param.put("type", "STOR");
		param.put("message", "Upload initialized. Wait for data");
		param.put("filepath", fileUpload.getContextpath());
//		param.put("rootPath", fileUpload.getFilepath());
		param.put("savename", fileUpload.getFilename());
		param.put("ext", fileUpload.getExt());
		param.put("code", "200");
		param.put("sess_key", sess_key);
		mm.setPAYLOAD(param);
		// MessageSender.sendMessageModel(user_uid, mm);

		sendMessage(sess_key, mm);
	}

	private void sendMessage(String user_uid, MessageModel mm) {
		FileSessionStateMng mng = FileSessionStateMng.getInstance();
		WebSocketSession session = mng.getSessionModelByKey(user_uid).getWebSession();
		try {
			session.sendMessage(MessageSender.convertToTextMessage(mm));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Adds data to the file for a given client
	 * 
	 * @param $user : the client
	 * @param $data : the data to add to the file
	 */
	public void addData(Map infos) {
		/*
		 * if( isset($this->uploads[$user->getId()]) ) {
		 * $this->uploads[$user->getId()]->addData($data);
		 * 
		 * // Sends a response to the client $user->sendString(json_encode(array( 'type'
		 * => 'DATA', 'code' => 200, 'bytesRead' => mb_strlen($data) ))); }
		 */
		// String user_uid = ((String) infos.get("fileid"));
		String sess_key = (String) infos.get("sess_key");
		byte[] data = DatatypeConverter.parseBase64Binary(((String) infos.get("data")));
		if (upload.get(sess_key) != null) {
			upload.get(sess_key).addData(data);

			MessageModel mm = new MessageModel();
			Map param = new HashMap();
			param.put("type", "DATA");
			param.put("code", "200");
			// param.put("sess_key", sess_key);
			param.put("bytesRead", data.length);
			mm.setPAYLOAD(param);
			// MessageSender.sendMessageModel(user_uid, mm);

			sendMessage(sess_key, mm);
		}

	}

	/**
	 * Finishes the upload of a given client
	 * 
	 * @param $user : the client
	 */
	public void finishUpload(String user_uid) {
		FileUpload fileUpload = upload.get(user_uid);
		if (fileUpload != null) {

			if (fileUpload.getIsVideo()) {
				try {
//					SeekableByteChannel bc = (SeekableByteChannel) NIOUtils.readableFileChannel(fileUpload.getFilepath());
					Picture pic = this.getPicture(fileUpload, 60);
					BufferedImage img = AWTUtil.toBufferedImage(pic);
					// /(\w+\.\w+)(?=\?|$)/gm
					File video = new File(fileUpload.getFilepath());
					String path = video.getPath().split(video.getName())[0];

					File file = new File(path + "/thum");

					if (!file.isDirectory()) {
						file.mkdirs();
					}

					File videoThumb = new File(path + "/thum/" + video.getName().split("[.]")[0] + ".png");
					if (!videoThumb.exists()) {
						videoThumb.createNewFile();
					}

					ImageIO.write(img, "png", videoThumb);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					logger.info("file not found");
				} catch (IOException e) {
					e.printStackTrace();
					logger.info("io exception in file save about video thumbnail");
				} catch (JCodecException e) {
					e.printStackTrace();
					logger.info("jcodec exception");
				}
			}

			fileUpload.close();
			fileUpload = null;
			upload.remove(user_uid);
		}
	}

	private Picture getPicture(FileUpload fileUpload, int frame) throws IOException, JCodecException {
		Picture picture = FrameGrab.getFrameFromFile(new File(fileUpload.getFilepath()), frame);
		if (picture == null)
			this.getPicture(fileUpload, frame / 2);

		return picture;
	}

	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Binary message");
		super.handleBinaryMessage(session, message);
	}

	/**
	 * Class that represents the upload of a file
	 */
	class FileUpload {
		// The filename of the uploaded file
		private String filename;

		private String originFilename;

		private String filepath;

		private String contextpath;

		// The size of the uploaded file
		private long size;

		private String ext;

		// The ressource to the file
		private FileOutputStream fos = null;

		private boolean isVideo = false;

		/**
		 * Constructs a new File Upload
		 * 
		 * @param $filename : the name of the file
		 * @param $size     : the size of the file
		 * @throws FileNotFoundException
		 */
		public FileUpload(String orgfilename, long size) {
			this.originFilename = orgfilename;

			// 저장 파일명을 타임스탬프+확장자 형식으로 변경
			if (orgfilename != null && !orgfilename.equals("")) {

				int ext_pos = orgfilename.lastIndexOf(".");
				ext = orgfilename.substring(ext_pos).toLowerCase();

				filename = "MSG_" + (new Date()).getTime() + ext;
			}

			this.size = size;
			// this.filepath = "E:/"+ filename;
			String directory = CommonUtil.getDirectoryUploadMessage();
			this.contextpath = CommonUtil.getContextUploadMessage() + "/" + filename;
			File file = new File(directory);
			file.setExecutable(true, true);
			file.setReadable(true);
			file.setWritable(true, true);

			if (!file.isDirectory()) {
				file.mkdirs();
			}
			this.filepath = directory + "/" + filename;
			try {
				fos = new FileOutputStream(new File(filepath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public String getExt() {
			return ext;
		}

		public String getFilepath() {
			return filepath;
		}

		public String getOriginFilename() {
			return originFilename;
		}

		public String getFilename() {
			return filename;
		}

		public boolean getIsVideo() {
			return isVideo;
		}

		public void setIsVideo(String isVideo) {
			if (isVideo.contains("video"))
				this.isVideo = true;
			else
				this.isVideo = false;
		}

		public String getContextpath() {
			return contextpath;
		}

		public void setContextpath(String contextpath) {
			this.contextpath = contextpath;
		}

		/**
		 * Adds data to the file
		 * 
		 * @param $data : the data to add to the file
		 * @throws IOException
		 */
		public void addData(Object data) {
			try {
				fos.write((byte[]) data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/**
		 * Close the file
		 * 
		 * @throws IOException
		 */
		public void close() {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
