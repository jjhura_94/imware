package com.a2m.imware.wschat.manager;

import java.util.ArrayList;
import java.util.List;

public class HttpSessionOutMgn {
	private HttpSessionOutMgn() {
	};

	private volatile static HttpSessionOutMgn instance;
	private List<String> v = new ArrayList<String>();

	public void add(String value_user_uid) {
		v.add(value_user_uid);
	}

	public void remove(String value_user_uid) {
		v.remove(value_user_uid);
	}

	public void removeAll() {
		v = new ArrayList<String>();
	}

	public String toString() {
		return v.toString();
	}

	public boolean contains(String value_user_uid) {
		return v.contains(value_user_uid);
	}

	public static HttpSessionOutMgn getInstance() {
		if (instance == null) {// 있는지 체크 없으면
			synchronized (HttpSessionOutMgn.class) {
				if (instance == null) {
					instance = new HttpSessionOutMgn(); // 생성한뒤
				}
			}
		}
		return instance;
	}

}
