package com.a2m.imware.wschat.manager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.adapter.standard.StandardWebSocketSession;

import com.a2m.imware.wschat.model.SessionModel;

public class FileSessionStateMng {

	private volatile static FileSessionStateMng ssm;

	private Map<String, SessionModel> sessionModelMap = new HashMap<String, SessionModel>();

	public static FileSessionStateMng getInstance() {
		if (ssm == null) {// 있는지 체크 없으면
			synchronized (FileSessionStateMng.class) {
				if (ssm == null) {
					ssm = new FileSessionStateMng(); // 생성한뒤
				}
			}
		}
		return ssm;// 생성자를 넘긴다.
	}

	public void setSessionModel(String key, SessionModel sm) {
		sessionModelMap.put(key, sm);
	}

	public void setSessionModel(WebSocketSession session) {

		SessionModel sm = new SessionModel(session);
		String key = getSessionKey(session);
		sessionModelMap.put(key, sm);
	}

	public static String getSessionKey(WebSocketSession session) {
		String key = session.getAttributes().get("USER_UID") + "_" + session.getId();
		return key;
	}

	public void destroySessionModel(SessionModel sm) {

		if (sessionModelMap.containsValue(sm)) {
			destroySession(getKeyBySessionModel(sm));
		} else {
			System.out.println("session이 Null 입니다.");
		}
	}

	public void destroySession(WebSocketSession session) {
		destroySession(getKeyBySession(session));

	}

	public void destroySession(String UID_SessionId) {
		if (UID_SessionId != null && !UID_SessionId.equals(""))
			sessionModelMap.remove(UID_SessionId);
	}

	public String getKeyBySessionModel(SessionModel sm) {
		String key = null;
		for (String o : sessionModelMap.keySet()) {
			if (sessionModelMap.get(o).equals(sm)) {
				key = o;
				break;
			}
		}
		return key;
	}

	public String getKeyBySession(WebSocketSession session) {
		String key = null;
		for (Map.Entry<String, SessionModel> entry : sessionModelMap.entrySet()) {
			if (entry.getValue().containsSession(session)) {
				key = entry.getKey();
				break;
			}
		}
		System.out.print("key:" + key);
		return key;
	}

	public SessionModel getSessionModelBySession(StandardWebSocketSession session) {

		return sessionModelMap.get(getKeyBySession(session));
	}

	public SessionModel getSessionModelByKey(String UID) {
		SessionModel sm = null;
		sm = (SessionModel) sessionModelMap.get(UID);

		return sm;
	}

	public List<SessionModel> getSessionModelListByUidList(List<String> uidList) {
		List<SessionModel> smlist = new ArrayList<SessionModel>();

		for (String key : uidList) {
			smlist.add(getSessionModelByKey(key));
		}

		return smlist;
	}

	public Map<String, SessionModel> getSessionModelMap() {
		return sessionModelMap;
	}

	public void printAll() {
		Class<? extends FileSessionStateMng> cls = this.getClass();
		Field[] fields = cls.getDeclaredFields();

		try {
			for (Field fd : fields) {
				System.out.println(fd.getName() + ": " + fd.get(this));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void printSessionModelList() {
	}

}
