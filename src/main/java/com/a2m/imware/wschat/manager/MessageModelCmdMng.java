package com.a2m.imware.wschat.manager;

import java.io.File;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.EnumMessageDef;
import com.a2m.imware.wschat.model.EnumRoomType;
import com.a2m.imware.wschat.model.MessageModel;
import com.a2m.imware.wschat.model.SessionModel;
import com.a2m.imware.wschat.module.chatroom.ChatroomServiceImpl;
import com.a2m.imware.wschat.module.filemessage.FileMsgServiceImpl;
import com.a2m.imware.wschat.module.message.ChatMsgServiceImpl;
import com.a2m.imware.wschat.module.user.ChatUserServiceImpl;
import com.a2m.imware.wschat.module.userconfig.UserConfigServiceImpl;
import com.a2m.imware.wschat.util.EncryptionUtil;
import com.a2m.imware.wschat.util.ResourceUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
public class MessageModelCmdMng {

	private static Map<String, String> keyMap = new ConcurrentHashMap<>();

	@Autowired
	private ChatUserServiceImpl userService;

	@Autowired
	private ChatroomServiceImpl roomService;

	@Autowired
	private ChatMsgServiceImpl msgService;

	@Autowired
	private FileMsgServiceImpl fileMsgService;

	@Autowired
	private UserConfigServiceImpl confService;

	// @Autowired
	// private FrdServiceImpl frdService;

	// @Autowired
	// private ServletContext context;

	@Autowired
	private UserInfoService userInfoDAO;

	protected Logger logger = LogManager.getLogger(MessageModelCmdMng.class);

	public MessageModelCmdMng() {
	}

	public void setBusinessCommand(WebSocketSession session, MessageModel model) throws Exception {
		String type = model.getTYPE();
		String category = model.getCATEGORY();
		EnumMessageDef enums = EnumMessageDef.getEnumMessageTypeByTypeCategory(type, category);
		String command = enums.getCommandName();
		if (EnumMessageDef.LOGIN_IN.getCommandName().equals(command)) {
			this.LOGIN_IN(session, model);
		} else if (EnumMessageDef.MEMBER_ONLINE.getCommandName().equals(command)) {
			this.MEMBER_ONLINE(session, model);
		} else if (EnumMessageDef.CHAT_SEARCH.getCommandName().equals(command)) {
			this.CHAT_SEARCH(session, model);
		} else if (EnumMessageDef.CHAT_CALL.getCommandName().equals(command)) {
			this.CHAT_CALL(session, model);
		} else if (EnumMessageDef.ROOM_CREATE.getCommandName().equals(command)) {
			this.ROOM_CREATE(session, model);
		} else if (EnumMessageDef.ROOM_CREATE_CONTACT.getCommandName().equals(command)) {
			this.ROOM_CREATE_CONTACT(session, model);
		} else if (EnumMessageDef.ROOM_CREATE_CHATROOM.getCommandName().equals(command)) {
			this.ROOM_CREATE_CHATROOM(session, model); // ignored => unused
		} else if (EnumMessageDef.CONTACT_LIST.getCommandName().equals(command)) {
			this.CONTACT_LIST(session, model);
		} else if (EnumMessageDef.ROOM_MODIFY.getCommandName().equals(command)) {
			this.ROOM_MODIFY(session, model); // ignored => unused
		} else if (EnumMessageDef.ROOM_OUT.getCommandName().equals(command)) {
			this.ROOM_OUT(session, model); // ignored => unused
		} else if (EnumMessageDef.ROOM_LIST.getCommandName().equals(command)) {
			this.ROOM_LIST(session, model);
		} else if (EnumMessageDef.ROOM_VIEW.getCommandName().equals(command)) {
			this.ROOM_VIEW(session, model);
		} else if (EnumMessageDef.ROOM_INVITE.getCommandName().equals(command)) {
			this.ROOM_INVITE(session, model);
		} else if (EnumMessageDef.ROOM_CHECK.getCommandName().equals(command)) {
			this.ROOM_CHECK(model);
		} else if (EnumMessageDef.CHAT_SEND.getCommandName().equals(command)) {
			this.CHAT_SEND(model);
		} else if (EnumMessageDef.CHAT_RECEIVE.getCommandName().equals(command)) {
			this.CHAT_RECEIVE(model);
		} else if (EnumMessageDef.CHAT_LIST.getCommandName().equals(command)) {
			this.CHAT_LIST(session, model);
		} else if (EnumMessageDef.CHAT_CONFIRM.getCommandName().equals(command)) {
			this.CHAT_CONFIRM(model);
		} else if (EnumMessageDef.FILE_VIEW.getCommandName().equals(command)) {
			FILE_VIEW(model);
		} else if (EnumMessageDef.FILE_UPLOAD.getCommandName().equals(command)) {
			FILE_UPLOAD(model);
		} else if (EnumMessageDef.FILE_DOWNLOAD.getCommandName().equals(command)) {
			FILE_DOWNLOAD(model);
		} else if (EnumMessageDef.MEMBER_LIST.getCommandName().equals(command)) {
			MEMBER_LIST(session, model);
		} else if (EnumMessageDef.MEMBER_VIEW.getCommandName().equals(command)) {
			MEMBER_VIEW(session, model);
		} else if (EnumMessageDef.MEMBER_MODIFY.getCommandName().equals(command)) {
			MEMBER_MODIFY(session, model);
		} else if (EnumMessageDef.MEMBER_REFRESH.getCommandName().equals(command)) {
			MEMBER_REFRESH(model);
		} else if (EnumMessageDef.FRIEND_INVITE.getCommandName().equals(command)) {
			FRIEND_INVITE(session, model);
		} else if (EnumMessageDef.FRIEND_ACCEPT.getCommandName().equals(command)) {
			FRIEND_ACCEPT(session, model);
		} else if (EnumMessageDef.FRIEND_DELETE.getCommandName().equals(command)) {
			FRIEND_DELETE(session, model);
		} else if (EnumMessageDef.FRIEND_ADD_FAVORITE.getCommandName().equals(command)) {
			FRIEND_ADD_FAVORITE(session, model);
		} else if (EnumMessageDef.FRIEND_DELETE_FAVORITE.getCommandName().equals(command)) {
			FRIEND_DELETE_FAVORITE(session, model);
		} else if (EnumMessageDef.FRIEND_CANCEL.getCommandName().equals(command)) {
			FRIEND_CANCEL(session, model);
		} else if (EnumMessageDef.SETTING_VIEW.getCommandName().equals(command)) {
			SETTING_VIEW(session, model);
		} else if (EnumMessageDef.SETTING_MODIFY.getCommandName().equals(command)) {
			SETTING_MODIFY(session, model);
		} else if (EnumMessageDef.CONTACT_LISTFORM.getCommandName().equals(command)) {
			this.CONTACT_LISTFORM(model);
		} else if (EnumMessageDef.CONTACT_LISTFORM2.getCommandName().equals(command)) {
			this.CONTACT_LISTFORM2(model);
		} else if (EnumMessageDef.SECURITY_SEND_KEY.getCommandName().equals(command)) {
			this.SECURITY_SEND_KEY(session, model);
		} else if (EnumMessageDef.SECURITY_GET_KEY.getCommandName().equals(command)) {
			this.SECURITY_GET_KEY(session, model);
		} else if (EnumMessageDef.NOTIFICATION_MODIFY.getCommandName().equals(command)) {
			this.NOTIFICATION_MODIFY(session, model);
		} else if (EnumMessageDef.CHAT_EXTRA_MESSAGE.getCommandName().equals(command)) {
			this.CHAT_EXTRA_MESSAGE(session, model);
		}
	}

	public void CHAT_EXTRA_MESSAGE(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> payload = model.getPAYLOAD();
		if (payload == null || payload.isEmpty())
			return;

		Map<Object, Object> sysUser = userService.getUserByUserId("system");
		payload.put("SYS_USER_UID", sysUser.get("USER_UID"));
		payload.put("ROOM_UID", "SYS-" + payload.get("USER_UID"));
		Map<Object, Object> targetUser = userService.getUserProfile(payload);
		JSONObject json = new JSONObject();
		if (targetUser != null) {
			json.put("USER_ID", targetUser.get("USER_ID"));
			json.put("USER_NM_ENG", targetUser.get("USER_NM_ENG"));
			json.put("USER_NM_KOR", targetUser.get("USER_NM_KOR"));
		}

		final String type = payload.get("TYPE") == null ? null : payload.get("TYPE").toString();
		json.put("TYPE", type);
		switch (type) {
		case "VACATION_CONFIRMATION":
			// xac nhan don xin nghi: chap nhan/tu choi
			final boolean accepted = Boolean.valueOf(payload.get("ACCEPTED").toString());
			json.put("ACCEPTED", accepted);
			payload.put("MSG_CONT", json.toString());
			break;
		case "TASK_REQUEST":
			// task duoc giao
			json.put("SEND_USER_UID", payload.get("SEND_USER_UID"));
			json.put("RECEIVE_USER_UID", payload.get("RECEIVE_USER_UID"));
			json.put("TASK_TITLE", payload.get("TASK_TITLE"));
			json.put("PERMALINK", payload.get("PERMALINK"));
			payload.put("MSG_CONT", json.toString());
			break;
		case "SCHEDULE":
			// thong bao dat phong (truoc 30'), di cong tac (truoc 1h)
			json.put("SCHEDULE_TYPE", payload.get("SCHEDULE_TYPE"));
			json.put("MEETING_ROOM_NAME", payload.get("MEETING_ROOM_NAME"));
			json.put("BUSINESS_TRIP_NAME", payload.get("BUSINESS_TRIP_NAME"));
			payload.put("MSG_CONT", json.toString());
			break;
		case "EMPLOYMENT_CERTIFICATION":
		case "BUSINESS_CARD":
			// thong bao chung nhan lam viec
			// thong bao danh thiep + link?
			json.put("STATUS", payload.get("STATUS"));
			json.put("PERMALINK", payload.get("PERMALINK"));
			payload.put("MSG_CONT", json.toString());
			break;
		case "NOTICE_NEW_MEMBER":
			// thanh vien moi
			json.put("MEMBER_USER_UID", payload.get("MEMBER_USER_UID"));
			payload.put("MSG_CONT", json.toString());
			break;
		case "NOTICE_BOARD":
			// notice board + link
			json.put("TITLE", payload.get("TITLE"));
			json.put("DEPT_NAME", payload.get("DEPT_NAME"));
			json.put("WRITER_NAME", payload.get("WRITER_NAME"));
			json.put("WRITER_POSITION", payload.get("WRITER_POSITION"));
			JSONArray permalinkJsonArray = new JSONArray();
			if (payload.get("DOWNLOAD_LIST") != null) {
				@SuppressWarnings("unchecked")
				List<Map<Object, Object>> permalinkList = (List<Map<Object, Object>>) payload.get("DOWNLOAD_LIST");
				permalinkList.forEach(link -> {
					JSONObject linkJson = new JSONObject();
					linkJson.put("LINK_TITLE", link.get("LINK_TITLE"));
					linkJson.put("LINK", link.get("LINK"));
					permalinkJsonArray.add(linkJson);
				});
			}
			json.put("DOWNLOAD_LIST", permalinkJsonArray);
			json.put("PERMALINK", payload.get("PERMALINK"));
			payload.put("MSG_CONT", json.toString());
			break;
		case "NOTICE_DEPARTMENT":
			json.put("TITLE", payload.get("TITLE"));
			json.put("PERMALINK", payload.get("PERMALINK"));
			payload.put("MSG_CONT", json.toString());
			break;
		}

		// save message to DB & send to online members
		sendSystemMessage(payload);
	}

	private void sendSystemMessage(Map<Object, Object> parameter) throws Exception {
		String msgUid = msgService.gen_chatuid_seq();
		MessageModel data = new MessageModel();
		Map<Object, Object> roomFind = roomService.getRoomByKey(parameter);
		String roomKey = "";
		data.put("ROOM_TYPE", "");
		if (roomFind != null) {
			data.put("ROOM_TYPE", roomFind.get("ROOM_TYPE"));
			roomKey = roomFind.get("KEY_ROOM").toString();
		}

		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", parameter.get("SYS_USER_UID"));
		param.put("MSG_UID", msgUid);
		param.put("ROOM_UID", parameter.get("ROOM_UID"));
		param.put("SEND_DATE", new Date());
		param.put("MSG_CONT", parameter.get("MSG_CONT"));
		param.put("MSG_TYPE_CODE", "EXTRA");
		msgService.insertMsg(param);
		msgService.insertMsgConf(param);
		roomService.updateDateModified(param);

		// CAP NHAT TRANG THAI USER_CHECK, ROOM_OPEN_CHECK;
		List<String> listMembers = new ArrayList<>();
		List<Map<Object, Object>> infoMembers = roomService.getListInfoMemberByRoomUid(param);
		for (Map<Object, Object> infoMember : infoMembers) {
			String memberUid = (String) infoMember.get("USER_UID");
			if (!memberUid.equals(parameter.get("SYS_USER_UID")))
				listMembers.add(memberUid);
		}

		data.setDef(EnumMessageDef.CHAT_RECEIVE);
		data.put("SEND_DATE", param.get("SEND_DATE"));
		data.put("MSG_UID", param.get("MSG_UID"));
		data.put("MSG_CONT", param.get("MSG_CONT"));
		data.put("KEY_ROOM", roomKey);
		data.put("MSG_TYPE_CODE", param.get("MSG_TYPE_CODE"));
		data.put("USER_UID", param.get("USER_UID"));
		data.put("ROOM_UID", param.get("ROOM_UID"));

		Map<Object, Object> userInfo = userService.getUserProfile(param);
		data.put("USER_NM_KOR", userInfo.get("USER_NM_KOR"));
		data.put("USER_NM_ENG", userInfo.get("USER_NM_ENG"));
		data.put("USER_IMG", userInfo.get("USER_IMG"));
		data.put("USER_ID", userInfo.get("USER_ID"));

		List<SessionModel> userOnline = SessionModelStateMng.getInstance().getSessionModelListByUidList(listMembers);
		for (SessionModel session : userOnline) {
			if (!roomKey.equals("")) {

				String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getWebSession().getId());
				String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getWebSession().getId());
				if (modulus == null || modulus.equals("")) {
					System.out.println("mudulus null");
					data.setRESULT("ERROR");
				} else {

					if (modulus == null || modulus.equals("")) {
						System.err.println("Key room null!!!!!");
						// return;
					}
					PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
					byte[] outputData = EncryptionUtil.encryptRSA(pk, roomKey.getBytes());
					roomFind.remove("KEY_ROOM");
					data.put("KEY_ROOM", outputData);
				}

			}
			MessageSender.sendMessageModel(session, MessageSender.convertToTextMessage(data));
		}
	}

	public void NOTIFICATION_MODIFY(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		// Object roomCount = roomService.updateRoomNotification(p_user);
		// model.put("ROOM_COUNT", roomCount + "");
		MessageSender.sendMessageModel(model.getUSER_UID(), model);
	}

	public void FRIEND_DELETE(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		List listRoomId = (List) roomService.getInContactRoom(new HashMap<>(p_user));
		if (listRoomId != null && listRoomId.size() > 0) {
			p_user.put("ROOM_UID", listRoomId.get(0));
		}
		int rs = userService.deleteFriend(new HashMap<>());
		if (rs > 0)
			model.RESULT = "OK";

		// Delete room data
		if (listRoomId != null && listRoomId.size() > 0) {
			deleteRoomWithData((String) listRoomId.get(0));
			model.put("ROOM_UID", listRoomId.get(0));
		}

		MessageSender.sendMessageModel(model.getUSER_UID(), model);
		MessageSender.sendMessageModel((String) model.get("USER_UID_2"), model);

	}

	public void FRIEND_DELETE_FAVORITE(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		Object rs = userService.unFavorite(new HashMap<>(p_user));
		if ((int) rs > 0)
			model.RESULT = "OK";

		MessageSender.sendMessageModel(
				SessionModelStateMng.getInstance().getSessionModelByWSSesion(model.getUSER_UID(), session),
				MessageSender.convertToTextMessage(model));
	}

	public void FRIEND_ADD_FAVORITE(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		Object check = userService.checkIsFriend(p_user);
		if (check == null) {
			userService.addFriend(p_user);
		}
		Object rs = userService.addFavorite(p_user);
		if ((int) rs > 0)
			model.RESULT = "OK";
		MessageSender.sendMessageModel(
				SessionModelStateMng.getInstance().getSessionModelByWSSesion(model.getUSER_UID(), session),
				MessageSender.convertToTextMessage(model));
	}

	public void FRIEND_CANCEL(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		userService.cancelRequest(p_user);

		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", model.getUSER_UID());
		param.put("USER_INVT_UID", model.getPAYLOAD().get("USER_UID_2"));
		param.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());

		// Delete room data
		List<Map<Object, Object>> checkCreateContactChat = userService.getListCheckChatInContact(param);

		if (checkCreateContactChat.size() > 0) {
			String roomUID = checkCreateContactChat.get(0).get("ROOM_UID").toString();
			deleteRoomWithData(roomUID);
			model.put("ROOM_UID", checkCreateContactChat.get(0).get("ROOM_UID"));
		}

		MessageSender.sendMessageModel(model.getUSER_UID(), model);
		MessageSender.sendMessageModel((String) model.getPAYLOAD().get("USER_UID_2"), model);
	}

	public void FRIEND_ACCEPT(WebSocketSession session, MessageModel model) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", model.getUSER_UID());
		p_user.putAll(model.getPAYLOAD());
		p_user.put("USER_INVT_UID", model.getPAYLOAD().get("USER_UID_2"));

		Object rs = userService.acceptFriend(p_user);
		if (rs.equals("1"))
			model.RESULT = "OK";
		p_user.put("ROOM_TYPE", "IN_CONTACT");

		List<Map<Object, Object>> checkCreateContactChat = userService.getListCheckChatInContact(p_user);
		if (checkCreateContactChat.size() > 0) {
			model.put("ROOM_UID", checkCreateContactChat.get(0).get("ROOM_UID"));
		}

		MessageSender.sendMessageModel(
				SessionModelStateMng.getInstance().getSessionModelByWSSesion(model.getUSER_UID(), session),
				MessageSender.convertToTextMessage(model));
		MessageSender.sendMessageModel((String) model.getPAYLOAD().get("USER_UID_2"), model);
	}

	public void FRIEND_INVITE(WebSocketSession session, MessageModel data) throws Exception {
		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID_1", data.getUSER_UID());
		p_user.putAll(data.getPAYLOAD());
		Object rs = userService.sendInvite(p_user);
		if (rs.equals("1"))
			data.RESULT = "OK";

		// Check xem da tao room chua giua 2 nguoi trong danh ba
		String userReq = (String) data.getUSER_UID();
		String userUidPartner = (String) data.get("USER_UID_2");
		Map<Object, Object> checkRoom = checkExistRooomChatInContact(userReq, userUidPartner);

		if (!"ERROR".equals(checkRoom.get("RESULT"))) {
			// room exist
			data.put("ROOM_UID", checkRoom.get("ROOM_UID"));
			data.put("KEY_ROOM", checkRoom.get("KEY_ROOM"));
			roomService.updateRoomOpenCheck(data.getPAYLOAD());
		} else {
			// room not exist
			String roomUid = KeyStateMng.getInstance().getRoomKey();
			String aesKey = genAesKey(data.getUSER_UID(), roomUid);
			String roomName = data.get("ROOM_NM") == null ? "Untitle" : (String) data.get("ROOM_NM");
			data.put("ROOM_UID", roomUid);
			data.put("REG_DATE", new Date());
			data.put("REG_USER_UID", data.getUSER_UID());
			data.put("ROOM_DEFAULT_NAME", roomName);
			data.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
			data.put("KEY_ROOM", aesKey);
			roomService.insertCHAT_ROOM(new HashMap<>(data.getPAYLOAD()));
			// System.out.println(AESKey);

			// Creeate CHAT_USER
			createChatUser(userReq, roomUid, roomName, userReq, EnumRoomType.IN_CONTACT);
			createChatUser(userUidPartner, roomUid, roomName, userReq, EnumRoomType.IN_CONTACT);
		}

		// SEND INVITE OK MSG
		MessageSender.sendMessageModel(
				SessionModelStateMng.getInstance().getSessionModelByWSSesion(data.getUSER_UID(), session),
				MessageSender.convertToTextMessage(data));
		MessageSender.sendMessageModel((String) data.get("USER_UID_2"), data);

		data.put("MSG_CONT", p_user.get("MESSAGE"));
		data.put("CONT_TYPE", "FRIEND_INVITE");
		data.put("USER_UID", userUidPartner);

		CHAT_SEND_INFORM(session, data);
	}

	private void createChatUser(String userUid, String roomUid, String roomName, String userReq, EnumRoomType roomType)
			throws Exception {
		Map<Object, Object> chat_user = new HashMap<>();
		chat_user.put("USER_UID", userUid);
		chat_user.put("ROOM_UID", roomUid);
		chat_user.put("REG_DATE", new Date());
		chat_user.put("ROOM_NM", roomName);
		chat_user.put("USER_INVT_UID", userReq);
		chat_user.put("USER_CHECK", "N");
		chat_user.put("ROOM_TYPE", roomType.getValue());
		chat_user.put("ROOM_OPEN_CHECK", "N");
		if (userUid.equals(userReq)) {
			chat_user.put("ROOM_OPEN_CHECK", "Y");
			chat_user.put("USER_CHECK", "Y");
		}
		userService.insertCHAT_USER(chat_user);
	}

	/*
	 * public void ROOM_CREATE_CONTACT(MessageModel data) throws Exception { //
	 * Create CHAT_CONTACT // Check xem da tao room chua giua 2 nguoi trong danh ba
	 * String userReq = (String) data.getUSER_UID(); String userUidPartner =
	 * (String) data.get("USER_UID"); if (Strings.isEmpty(userReq) ||
	 * Strings.isEmpty(userUidPartner)) return;
	 * 
	 * Map<String, Object> checkRoom = checkExistRooomChatInContact(userReq,
	 * userUidPartner); if (!"ERROR".equals(checkRoom.get("RESULT"))) {
	 * data.put("ROOM_UID", checkRoom.get("ROOM_UID")); data.put("KEY_ROOM",
	 * checkRoom.get("KEY_ROOM"));
	 * roomService.updateRoomOpenCheck((Map)data.getPAYLOAD()); } else { String
	 * roomUid = KeyStateMng.getInstance().getRoomKey(); String aesKey =
	 * genAesKey(data.getUSER_UID(), roomUid); data.put("ROOM_UID", roomUid);
	 * data.put("REG_DATE", new Date()); data.put("REG_USER_UID",
	 * data.getUSER_UID()); data.put("ROOM_DEFAULT_NAME", data.get("ROOM_NM"));
	 * data.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
	 * data.put("KEY_ROOM", aesKey); roomService.insertCHAT_ROOM(data.getPAYLOAD());
	 * 
	 * // System.out.println(AESKey);
	 * 
	 * } String AESKey = data.get("KEY_ROOM").toString(); String modulus =
	 * MainController.keyMap.get("key_" + data.getUSER_UID()); //
	 * System.out.println(modulus); PublicKey pk =
	 * EncryptionUtil.publicKeyFromString(modulus); byte[] outputData =
	 * EncryptionUtil.encryptRSA(pk, AESKey.getBytes()); String room_key =
	 * Base64.getEncoder().encodeToString(outputData); data.put("KEY_ROOM",
	 * room_key);
	 * 
	 * // check data response MessageSender.sendMessageModel(userReq, data); }
	 */

	public void setMessageModel(MessageModel model) {

		try {
			String type = model.getTYPE();
			String category = model.getCATEGORY();
			EnumMessageDef enums = EnumMessageDef.getEnumMessageTypeByTypeCategory(type, category);
			String command = enums.getCommandName();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void pushMessageModel(Map<Object, Object> map) {
		MessageModel model = new MessageModel();

		if (map != null && map.get("USER_UID") != null && !map.get("USER_UID").equals("")) {
			model.setUSER_UID((String) map.get("USER_UID"));
		}
		if (map != null && map.get("TYPE") != null && !map.get("TYPE").equals("")) {
			model.setTYPE((String) map.get("TYPE"));
		}
		if (map != null && map.get("CATEGORY") != null && !map.get("CATEGORY").equals("")) {
			model.setCATEGORY((String) map.get("CATEGORY"));
		}

		if (map != null && map.get("PAYLOAD") != null && !map.get("PAYLOAD").equals("")) {
			model.setPAYLOAD((Map) map.get("PAYLOAD"));
		}

		// MessageCommandManager mcm = new MessageCommandManager();
		setMessageModel(model);

	}

	private String getDateToString() {
		Calendar calendar = Calendar.getInstance();
		String value = new SimpleDateFormat("yyyyMMddHHmmss").format(calendar.getTime());
		return value;
	}

	private String genAesKey(String userUid, String roomUid) {
		String v = getDateToString() + "_" + userUid + "_" + roomUid;
		return v;
	}

	public void LOGIN_IN(WebSocketSession session, MessageModel data) throws Exception {
		boolean logged = false;
		try {
			// check username/session valid
			String httpSessionId = (String) data.getPAYLOAD().get("SESSION_ID");

			if (httpSessionId != null && httpSessionId != "") {
				String username = userInfoDAO.getUserIdFromToken(httpSessionId);
				Map<Object, Object> mapUserInfo = userInfoDAO.getUserInfo(username, httpSessionId);
				if (mapUserInfo != null) {
					String userUid = String.valueOf(mapUserInfo.get("USER_UID"));
					if (userUid != null && userUid.equals(data.getPAYLOAD().get("USERNAME"))) {
						data.setUSER_UID((String) data.getPAYLOAD().get("USERNAME"));

						// add to list
						session.getAttributes().put("USER_UID", userUid);
						SessionModelStateMng.getInstance().setSessionModel(session);
						logged = true;
					}
				}
			}

			List<String> userOnline = SessionModelStateMng.getInstance().getUserUidList();
			List<SessionModel> listSession = SessionModelStateMng.getInstance()
					.getSessionModelListByUidList(userOnline);
			MessageModel dataOnline = new MessageModel();
			dataOnline.setDef(EnumMessageDef.MEMBER_ONLINE);
			dataOnline.put("USER_UID", data.getPAYLOAD().get("USERNAME"));
			if (logged) {
				// send online
				dataOnline.PAYLOAD.put("ONLINE", "Y");
				MessageSender.sendMessageModel(listSession, MessageSender.convertToTextMessage(dataOnline));

				session.sendMessage(MessageSender.convertToTextMessage(data));
			} else {
				// send offline
				dataOnline.PAYLOAD.put("ONLINE", "N");
				MessageSender.sendMessageModel(listSession, MessageSender.convertToTextMessage(dataOnline));
				// close web socket session
				session.close();
				SessionModelStateMng.getInstance().destroySession(session);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			session.close();
			SessionModelStateMng.getInstance().destroySession(session);
		}
	}

	public void MEMBER_ONLINE(WebSocketSession session, MessageModel data) throws Exception {
		try {
			List<String> userOnline = SessionModelStateMng.getInstance().getUserUidList();
			List<SessionModel> listSession = SessionModelStateMng.getInstance()
					.getSessionModelListByUidList(userOnline);
			MessageModel dataOnline = new MessageModel();
			dataOnline.setDef(EnumMessageDef.MEMBER_ONLINE);
			dataOnline.put("USER_UID", data.getPAYLOAD().get("USER_UID"));
			dataOnline.PAYLOAD.put("ONLINE", data.getPAYLOAD().get("ONLINE_YN"));
			MessageSender.sendMessageModel(listSession, MessageSender.convertToTextMessage(dataOnline));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void CHAT_SEARCH(WebSocketSession session, MessageModel data) throws Exception {
		try {
			Map<Object, Object> payload = data.getPAYLOAD();
			Map<Object, Object> result = new HashMap<>();

			if (payload == null || payload.isEmpty() || !payload.containsKey("USER_UID"))
				return;

			final String requestType = payload.get("REQUEST") == null ? "COUNT"
					: String.valueOf(payload.get("REQUEST"));
			MessageModel dataSearch = new MessageModel();
			dataSearch.setDef(EnumMessageDef.CHAT_SEARCH);
			dataSearch.put("USER_UID", payload.get("USER_UID"));

			if ("COUNT".equals(requestType)) {
				int count = msgService.chatSearchCount(payload);
				result.put("COUNT", count);
			} else if ("SEARCH".equals(requestType)) {
				List<Map<Object, Object>> rsList = msgService.chatSearch(payload);
				result.put("RESULT", rsList);
				result.put("start", payload.get("start"));
				result.put("limit", payload.get("limit"));
			} else if ("SEARCH_AROUND".equals(requestType)) {
				List<Map<Object, Object>> rsList = msgService.chatSearchAround(payload);
				result.put("RESULT", rsList);
			}

			result.put("REQUEST", requestType);
			dataSearch.setPAYLOAD(result);
			// MessageSender.sendMessageModel(String.valueOf(payload.get("USER_UID")),
			// dataSearch);
			List<SessionModel> mySessionList = SessionModelStateMng.getInstance()
					.getSessionModelByUID(String.valueOf(payload.get("USER_UID")));
			if (mySessionList != null) {
				for (SessionModel sm : mySessionList) {
					if (sm.getSessions().contains(session)) {
						session.sendMessage(MessageSender.convertToTextMessage(dataSearch));
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void CHAT_CALL(WebSocketSession session, MessageModel data) throws Exception {
		try {
			Map<Object, Object> payload = data.getPAYLOAD();
			if (payload == null || payload.isEmpty() || !payload.containsKey("USER_UID"))
				return;

			if ("CALL_TIMEOUT".equals(payload.get("command"))) {
				this.handleTimeoutCall(session, payload);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void handleTimeoutCall(WebSocketSession session, Map<Object, Object> payload) throws Exception {
		try {
			Boolean insertedTimeoutMsg = true;

			Map<Object, Object> params = new HashMap<>();
			params.put("MSG_UID", payload.get("imcomingMsgId"));
			Map<Object, Object> incomingMsg = msgService.getMsgInfo(params);

			if (incomingMsg != null) {
				Map<String, Object> msgContent = new ObjectMapper().readValue((String) incomingMsg.get("MSG_CONT"),
						new TypeReference<Map<String, Object>>() {
						});

				List<Object> timeoutUsers = (List<Object>) msgContent.get("timeoutUsers");
				if (!timeoutUsers.contains(payload.get("USER_UID"))) {
					insertedTimeoutMsg = false;
					timeoutUsers.add(payload.get("USER_UID"));
					String msgJson = new ObjectMapper().writeValueAsString(msgContent);
					params.put("MSG_CONT", msgJson);
					msgService.updateMsgInfo(params);
				}
			}

			MessageModel mm = new MessageModel();
			mm.setDef(EnumMessageDef.CHAT_CALL);
			mm.put("USER_UID", payload.get("USER_UID"));

			Map<Object, Object> result = new HashMap<>();
			result.put("insertedTimeoutMsg", insertedTimeoutMsg);
			result.put("command", payload.get("command"));
			mm.setPAYLOAD(result);
			SessionModel sm = SessionModelStateMng.getInstance()
					.getSessionModelByWSSesion(String.valueOf(payload.get("USER_UID")), session);
			MessageSender.sendMessageModel(sm, MessageSender.convertToTextMessage(mm));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	private String decryptRsa(PrivateKey privateKey, String securedValue) {
		String decryptedValue = "";
		try {
			Cipher cipher = Cipher.getInstance("RSA", "SunJCE");
			/**
			 * 암호화 된 값은 byte 배열이다. 이를 문자열 폼으로 전송하기 위해 16진 문자열(hex)로 변경한다. 서버측에서도 값을 받을 때 hex
			 * 문자열을 받아서 이를 다시 byte 배열로 바꾼 뒤에 복호화 과정을 수행한다.
			 */
			byte[] encryptedBytes = hexToByteArray(securedValue);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
			decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
		} catch (Exception e) {
			logger.error("decryptRsa Exception Error : " + e.getMessage());
		}
		return decryptedValue;
	}

	private static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() % 2 != 0) {
			return new byte[] {};
		}
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < hex.length(); i += 2) {
			byte value = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
			bytes[(int) Math.floor(i / 2)] = value;
		}
		return bytes;
	}

	public void SECURITY_GET_KEY(WebSocketSession session, MessageModel data) throws Exception {

		Map<String, Object> attributes = session.getAttributes();
		// generate key pair
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048);
		KeyPair keyPair = generator.genKeyPair();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privatKey = keyPair.getPrivate();
		// set private to session
		attributes.put("_private_key_", privatKey);
		RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		String publicKeyModulus = publicSpec.getModulus().toString(16);
		String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

		// return public key to client
		Map<Object, Object> payload = new HashMap<>();
		payload.put("RSA_MODULUS", publicKeyModulus);
		payload.put("RSA_EXPONENT", publicKeyExponent);
		data.setPAYLOAD(payload);

		session.sendMessage(MessageSender.convertToTextMessage(data));
	}

	public void CONTACT_LISTFORM(MessageModel data) throws Exception {

		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", data.getUSER_UID());
		p_user.putAll(data.getPAYLOAD());
		List<Map<Object, Object>> listContact = userService.getContactList02(p_user);

		data.setDATALIST(listContact);
		MessageSender.sendMessageModel(data.getUSER_UID(), data);

	}

	public void CONTACT_LISTFORM2(MessageModel data) throws Exception {

		Map<Object, Object> p_user = new HashMap<>();
		p_user.put("USER_UID", data.getUSER_UID());
		p_user.putAll(data.getPAYLOAD());

		List<Map<Object, Object>> listContact = userService.getUserFriendList(p_user);

		Object roomUid = data.get("ROOM_UID");
		List<Map<Object, Object>> listMembers = roomService.getListMemberByRoomUid03((String) roomUid);
		if (listContact != null && listMembers != null) {
			ListIterator<Map<Object, Object>> li = listContact.listIterator();
			while (li.hasNext()) {
				Map el = li.next();
				boolean flag = false;
				for (Map imember : listMembers) {
					if (imember.get("USER_UID").equals(el.get("USER_UID")))
						flag = true;
				}
				if (flag)
					li.remove();
			}
		}
		data.setDATALIST(listContact);
		MessageSender.sendMessageModel(data.getUSER_UID(), data);

	}

	public void CONTACT_LIST(WebSocketSession session, MessageModel data) throws Exception {
		String user_uid = data.getUSER_UID();
		List<String> userOnline = new ArrayList<>();
		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", data.getUSER_UID());
		param.putAll(data.getPAYLOAD());
		List<Map<Object, Object>> listContact = new ArrayList<>();

		// absent UID list
		List<String> uidList = userService.getAbsentUserUIDList(
				data.getPAYLOAD().get("DEPT_CD") != null ? data.getPAYLOAD().get("DEPT_CD").toString() : null,
				data.getUSER_UID());
		if (uidList == null) {
			uidList = new ArrayList<>();
		}
		
		// If size equals to 1 => this user are not a manager => remove it from the list
		if (uidList.size() == 1 && uidList.get(0).equals(data.getUSER_UID())) {
			uidList.clear();
		}

		data.getPAYLOAD().put("ABSENT_UID_LIST", uidList);

		listContact = userService.getUserFriendList(param);
		userOnline = SessionModelStateMng.getInstance().getUserUidList();

		for (Map<Object, Object> imap : listContact) {
			if (!userOnline.isEmpty() && userOnline.contains(imap.get("USER_UID"))) {
				logger.info("========================> ONLINE");
				imap.put("IS_ONLINE", "Y");
			}
		}

		data.setDATALIST(listContact);
		MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(user_uid, session),
				MessageSender.convertToTextMessage(data));
	}

	public void CONTACT_LIST_bak190327(WebSocketSession session, MessageModel data) throws Exception {
		String user_uid = data.getUSER_UID();
		List userOnline = new ArrayList<>();
		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", data.getUSER_UID());
		param.putAll(data.getPAYLOAD());
		List<Map<Object, Object>> listContact = new ArrayList<>();
		if (param.get("SEARCH_TYPE").equals("global")) {
			listContact = userService.getAllUser(param);

		} else {

			Map<Object, Object> object = (Map<Object, Object>) userService.checkUserInCurrentStruc(param);
			if (object != null) {
				param.put("IN_STRUC", "TRUE");
				param.putAll(object);
			}
			listContact = userService.getUserFriendList(param);
			userOnline = SessionModelStateMng.getInstance().getUserUidList();
		}

		for (Map<Object, Object> imap : listContact) {
			Map<Object, Object> para12 = new HashMap<>();
			para12.put("USER_UID", user_uid);
			para12.put("USER_INVT_UID", imap.get("USER_UID"));
			para12.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
			List<Map<Object, Object>> listCheck = userService.getListCheckChatInContact(para12);
			boolean isCreateRoom = listCheck != null && listCheck.size() > 0;
			if (isCreateRoom) {
				Map<Object, Object> p_chatuser1 = new HashMap<>();
				p_chatuser1.put("ROOM_UID", listCheck.get(0).get("ROOM_UID"));
				p_chatuser1.put("USER_UID", user_uid);
				Map<Object, Object> r_chatuser1 = userService.getChatUserByKey(p_chatuser1);
				imap.put("USER_CHECK", r_chatuser1.get("USER_CHECK"));
				imap.put("ROOM_OPEN_CHECK", r_chatuser1.get("ROOM_OPEN_CHECK"));
				imap.put("ROOM_UID", listCheck.get(0).get("ROOM_UID"));
			} else {
				imap.put("USER_CHECK", "");
				imap.put("ROOM_OPEN_CHECK", "");
				imap.put("ROOM_UID", "");
			}

			if (!userOnline.isEmpty() && userOnline.contains(imap.get("USER_UID"))) {
				imap.put("IS_ONLINE", "Y");
			}
		}

		data.setDATALIST(listContact);
		MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(user_uid, session),
				MessageSender.convertToTextMessage(data));

	}

	public void SECURITY_SEND_KEY(WebSocketSession session, MessageModel data) {
		String clientPublicKey = data.getPAYLOAD().get("KEY").toString();
		if (data.getPAYLOAD().containsKey("KEY_EX")) {
			String clientPublicKeyEx = data.getPAYLOAD().get("KEY_EX").toString();
			MessageModelCmdMng.keyMap.put("keyex_" + session.getId(), clientPublicKeyEx);
		}

		MessageModelCmdMng.keyMap.put("key_" + session.getId(), clientPublicKey);

		MessageModel returnData = new MessageModel();
		returnData.setDef(EnumMessageDef.SECURITY_SEND_KEY);
		returnData.setPAYLOAD(Collections.singletonMap("READY", true));
		MessageSender.sendMessageModel(data.getUSER_UID(), returnData);
	}

	public void ROOM_INVITE(WebSocketSession session, MessageModel data) throws Exception {
		boolean isPermit = true;
		Map<Object, Object> room = roomService.getRoomByKey(data.getPAYLOAD());
		if (room != null) {
			data.put("ROOM_NM", room.get("ROOM_DEFAULT_NAME"));
			// insertChatUser(data);
		}
		Map<Object, Object> chatUser = userService.getChatUserByKey(data.getPAYLOAD());
		if (chatUser != null && !chatUser.isEmpty()) {
			isPermit = false;
		}
		if (isPermit) {
			insertChatUser(data);

			MessageModel msgdata = new MessageModel();
			List<Map<Object, Object>> users = roomService.getListChatUserByRoomUid(data.getPAYLOAD());

			msgdata.setUSER_UID(data.getUSER_UID());
			Map<Object, Object> dataPayload = msgdata.getPAYLOAD();
			dataPayload.putAll(data.getPAYLOAD());
			dataPayload.put("CONT_TYPE", "IN_ROOM");
			dataPayload.put("USER_COUNT", users.size());
			CHAT_SEND_INFORM(session, msgdata);
		}
		MessageSender.sendMessageModel(data.getUSER_UID(), data);
	}

	public void ROOM_CREATE_2(MessageModel data) throws Exception {
		// Create CHAT_ROOM
		// String roomUid = KeyStateMng.getInstance().getRoomKey();
		// String aesKey = genAesKey(data.getUSER_UID(), roomUid);
		String userReq = (String) data.getUSER_UID();

		// data.put("ROOM_UID", roomUid);
		data.put("REG_DATE", new Date());
		data.put("REG_USER_UID", data.getUSER_UID());
		data.put("ROOM_DEFAULT_NAME", data.get("ROOM_NM"));
		data.put("ROOM_TYPE", data.get("ROOM_TYPE"));
		// data.put("KEY_ROOM", aesKey);
		roomService.insertCHAT_ROOM(data.getPAYLOAD());

		String aesKey = genAesKey(data.getUSER_UID(), data.get("ROOM_UID").toString());
		data.put("KEY_ROOM", aesKey);
		roomService.updateRoom(data.getPAYLOAD());

		MessageSender.sendMessageModel(userReq, data);
	}

	public void ROOM_CREATE_CONTACT(WebSocketSession session, MessageModel data) throws Exception {
		// Create CHAT_CONTACT
		// Check xem da tao room chua giua 2 nguoi trong danh ba
		String userReq = (String) data.getUSER_UID();
		String userUidPartner = (String) data.get("USER_UID");
		if (Strings.isEmpty(userReq) || Strings.isEmpty(userUidPartner))
			return;

		Map<Object, Object> checkRoom = checkExistRooomChatInContact(userReq, userUidPartner);
		if (!"ERROR".equals(checkRoom.get("RESULT"))) {
			data.put("ROOM_UID", checkRoom.get("ROOM_UID"));
			data.put("KEY_ROOM", checkRoom.get("KEY_ROOM"));
			roomService.updateRoomOpenCheck(data.getPAYLOAD());
		} else {

			String roomUid = KeyStateMng.getInstance().getRoomKey();
			String aesKey = genAesKey(data.getUSER_UID(), roomUid);
			String roomName = data.get("ROOM_NM") == null ? "Untitle" : (String) data.get("ROOM_NM");
			data.put("ROOM_UID", roomUid);
			data.put("REG_DATE", new Date());
			data.put("REG_USER_UID", data.getUSER_UID());
			data.put("ROOM_DEFAULT_NAME", roomName);
			data.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
			data.put("KEY_ROOM", aesKey);
			roomService.insertCHAT_ROOM(data.getPAYLOAD());
			// System.out.println(AESKey);

			// Creeate CHAT_USER
			createChatUser(userReq, roomUid, roomName, userReq, EnumRoomType.IN_CONTACT);
			createChatUser(userUidPartner, roomUid, roomName, userReq, EnumRoomType.IN_CONTACT);
		}
		String AESKey = data.get("KEY_ROOM").toString();

		String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
		String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
		if (modulus == null || modulus.equals("")) {
			System.err.println("Key room null!!!!!"); //
			return;
		}
		PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);

		byte[] outputData = EncryptionUtil.encryptRSA(pk, AESKey.getBytes());
		String room_key = BaseEncoding.base64().encode(outputData);
		data.put("KEY_ROOM", room_key);

		// check data response
		MessageSender.sendMessageModel(userReq, data);
	}

	public void ROOM_CREATE_CHATROOM(WebSocketSession session, MessageModel data) throws Exception {
		if (Strings.isEmpty(data.getUSER_UID()) || Strings.isEmpty((CharSequence) data.get("ROOM_NM")))
			return;
		// Create CHAT_ROOM
		// String roomUid = KeyStateMng.getInstance().getRoomKey();
		// String aesKey = genAesKey(data.getUSER_UID(), roomUid);
		String userReq = (String) data.getUSER_UID();

		// data.put("ROOM_UID", roomUid);
		data.put("REG_DATE", new Date());
		data.put("REG_USER_UID", data.getUSER_UID());
		data.put("ROOM_DEFAULT_NAME", data.get("ROOM_NM"));
		data.put("ROOM_TYPE", EnumRoomType.IN_CHATROOM.getValue());
		// data.put("KEY_ROOM", aesKey);
		roomService.insertCHAT_ROOM(data.getPAYLOAD());

		String aesKey = genAesKey(data.getUSER_UID(), data.get("ROOM_UID").toString());
		data.put("KEY_ROOM", aesKey);
		roomService.updateRoom(data.getPAYLOAD());

		String AESKey = aesKey;

		String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
		String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
		if (modulus == null || modulus.equals("")) {
			System.err.println("Key room null!!!!!");
			// return;
		}
		PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
		byte[] outputData = EncryptionUtil.encryptRSA(pk, AESKey.getBytes());
		String room_key = BaseEncoding.base64().encode(outputData);
		data.put("KEY_ROOM", room_key);
		System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64String(outputData));

		MessageSender.sendMessageModel(userReq, data);
	}

	@SuppressWarnings("unchecked")
	public void ROOM_CREATE(WebSocketSession session, MessageModel data) throws Exception {
		List<String> list_user_invite = (List<String>) data.get("USER_UID_LIST_INVITE");
		if (list_user_invite == null || list_user_invite.isEmpty())
			return;

		String user_req = data.getUSER_UID();
		String roomType = (String) data.get("ROOM_TYPE");

		// Create CHAT_ROOM
		String roomUid = KeyStateMng.getInstance().getRoomKey();

		String userReq = (String) data.getUSER_UID();
		String roomName = (String) data.get("ROOM_NM");

		data.put("ROOM_UID", roomUid);
		data.put("REG_DATE", new Date());
		data.put("REG_USER_UID", user_req);
		data.put("ROOM_DEFAULT_NAME", roomName);
		data.put("ROOM_TYPE", roomType);

		String aesKey = genAesKey(data.getUSER_UID(), data.get("ROOM_UID").toString());
		data.put("KEY_ROOM", aesKey);
		roomService.insertCHAT_ROOM(data.getPAYLOAD());

		// Send feedback

		String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
		String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
		if (modulus == null || modulus.equals("")) {
			System.err.println("Key room null!!!!!");
			// return;
		}
		PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
		byte[] outputData = EncryptionUtil.encryptRSA(pk, aesKey.getBytes());
		String room_key = BaseEncoding.base64().encode(outputData);
		data.put("KEY_ROOM", room_key);
		System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64String(outputData));

		MessageSender.sendMessageModel(userReq, data);

		// Create CHAT_USER
		list_user_invite.add(user_req);
		List<Map<Object, Object>> userList = new ArrayList<>();
		for (String iUserUid : list_user_invite) {
			if (!StringUtils.isEmpty(iUserUid)) {
				Map<Object, Object> chat_user = new HashMap<>();
				chat_user.put("USER_UID", iUserUid);
				chat_user.put("ROOM_UID", data.get("ROOM_UID"));
				chat_user.put("REG_DATE", new Date());
				chat_user.put("ROOM_NM", roomName);
				chat_user.put("USER_INVT_UID", user_req);
				chat_user.put("USER_CHECK", "N");
				chat_user.put("ROOM_OPEN_CHECK", "N");
				if (iUserUid.equals(user_req)) {
					chat_user.put("ROOM_OPEN_CHECK", "Y");
					chat_user.put("USER_CHECK", "Y");
				}

				if (!userList.contains(chat_user)) {
					userList.add(chat_user);
				}
			}
		}
		userService.insertListCHAT_USER(userList);

		// List users = roomService.getListChatUserByRoomUid(data.getPAYLOAD());
		// Send inform to other user
		MessageModel msgdata = new MessageModel();
		msgdata.setUSER_UID(user_req);
		Map<Object, Object> dataPayload = msgdata.getPAYLOAD();
		dataPayload.putAll(data.getPAYLOAD());
		dataPayload.put("CONT_TYPE", "CREATE_ROOM");
		// dataPayload.put("MSG_TYPE_CODE", "CREATE_ROOM");
		dataPayload.put("ROOM_UID", data.get("ROOM_UID"));
		dataPayload.put("USER_COUNT", userList.size());
		dataPayload.put("USER_UID_LIST_INVITE", list_user_invite.remove(user_req));
		/*
		 * for (String iUserUid : list_user_invite) { if
		 * (!StringUtils.isEmpty(iUserUid)) { dataPayload.put("USER_UID", iUserUid);
		 * CHAT_SEND_INFORM(msgdata); } }
		 */
		CHAT_SEND_INFORM(session, msgdata);
	}

	public void ROOM_MODIFY(WebSocketSession session, MessageModel data) throws Exception {
		String user_uid = data.getUSER_UID();

		Map<Object, Object> p_room = new HashMap<>();
		p_room.putAll(data.getPAYLOAD());
		Map<Object, Object> room = roomService.getRoomByKey(p_room);
		if (room != null && !room.isEmpty()) {
			String type = (String) room.get("ROOM_TYPE");
			if (EnumRoomType.IN_CHATROOM.getValue().equals(type)) {
				room.put("ROOM_DEFAULT_NAME", p_room.get("ROOM_NM"));
				roomService.updateRoom(room);
			} else
				userService.update(p_room);
		}
		MessageSender.sendMessageModel(user_uid, data);

		// Inform other user
		MessageModel msgdata = new MessageModel();
		msgdata.setUSER_UID(user_uid);
		Map<Object, Object> dataPayload = msgdata.getPAYLOAD();
		dataPayload.putAll(data.getPAYLOAD());
		dataPayload.put("CONT_TYPE", "ROOM_MODIFY");
		CHAT_SEND_INFORM(session, msgdata);
	}

	public void ROOM_VIEW(WebSocketSession session, MessageModel data) throws Exception {
		String currentUID = data.getUSER_UID();
		Map<Object, Object> para1 = new HashMap<>();
		para1.put("USER_UID", currentUID);
		para1.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
		para1.put("ROOM_UID", data.getPAYLOAD().get("ROOM_UID"));

		Map<Object, Object> room = roomService.getRoomByKey(para1);

		String AESKey = room.get("KEY_ROOM").toString();
		String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
		String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
		if (modulus == null || modulus.equals("")) {
			System.err.println("Key room null!!!!!");
			// return;
		}
		PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
		byte[] outputData = EncryptionUtil.encryptRSA(pk, AESKey.getBytes());
		String strOutputData = BaseEncoding.base64().encode(outputData);
		data.put("KEY_ROOM", strOutputData);
		data.put("STATUS", room.get("STATUS"));

		MessageSender.sendMessageModel(
				SessionModelStateMng.getInstance().getSessionModelByWSSesion(currentUID, session),
				MessageSender.convertToTextMessage(data));
	}

	public void ROOM_LIST(WebSocketSession session, MessageModel data) {
		try {
			String uid = data.getUSER_UID();
			Map<Object, Object> param = new HashMap<>();
			param.put("USER_UID", data.getUSER_UID());
			if (!"TIME".equals(data.get("ORDER_TYPE")) && !"NAME".equals(data.get("ORDER_TYPE"))
					&& !"UNREAD".equals(data.get("ORDER_TYPE"))) {
				data.put("ORDER_TYPE", "TIME");
			}
			if (Strings.isEmpty(uid))
				return;

			param.putAll(data.getPAYLOAD());

			String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
			String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
			if (modulus == null || modulus.equals("")) {
				System.err.println("Key room null!!!!!"); //
				return;
			}
			PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);

			// paging room list for mobile
			List<Map<Object, Object>> list_inChatRoom;
			int indextStart = 0;
			int recordPerPage = 30;

			if (param.containsKey("START_INDEX")) {
				indextStart = Integer.valueOf(param.get("START_INDEX").toString()) * recordPerPage;
				list_inChatRoom = roomService.getChatroomListPaging(param, indextStart, recordPerPage);
			} else {
				list_inChatRoom = roomService.getChatroomList(param);
			}
			for (Map<Object, Object> iChatRoom : list_inChatRoom) {
				if (null != iChatRoom.get("KEY_ROOM")) {
					String aesKey = iChatRoom.get("KEY_ROOM").toString();
					byte[] outputData = EncryptionUtil.encryptRSA(pk, aesKey.getBytes());
					iChatRoom.put("KEY_ROOM", outputData);
				}
			}
			data.setDATALIST(list_inChatRoom);

			MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(uid, session),
					MessageSender.convertToTextMessage(data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ROOM_OUT(WebSocketSession session, MessageModel data) throws Exception {
		String user_uid_sent_request = data.getUSER_UID();
		String user_uid_out = (String) data.get("USER_UID");
		// Remove in database
		Map<Object, Object> p_remove = new HashMap<>();
		p_remove.putAll(data.getPAYLOAD());// USER_UID, ROOM_UID
		boolean isPermit = true;
		if (!Strings.isEmpty(user_uid_out) && !user_uid_out.equals(user_uid_sent_request)) {
			// remove user out of room
			// check user;
			Map<Object, Object> record = userService.getChatUserByKey(p_remove);
			if (record != null) {
				String user_invt = (String) record.get("USER_INVT_UID");
				if (EnumRoomType.IN_CONTACT.getValue().equals(record.get("ROOM_TYPE"))
						|| !user_uid_sent_request.equals(user_invt))
					isPermit = false;
			}
		}

		if (isPermit) {
			userService.deleteChatUser(p_remove);
			List<?> listUser = userService.getListUid(p_remove);
			if (listUser == null || listUser.size() == 0) {
				deleteRoomWithData((String) data.get("ROOM_UID"));
			} else {
				List<Map<Object, Object>> users = roomService.getListChatUserByRoomUid(data.getPAYLOAD());

				MessageModel msgdata = new MessageModel();
				msgdata.setUSER_UID(user_uid_sent_request);
				Map<Object, Object> dataPayload = msgdata.getPAYLOAD();
				dataPayload.putAll(data.getPAYLOAD());
				dataPayload.put("CONT_TYPE", "OUT_ROOM");
				dataPayload.put("USER_COUNT", users.size());
				CHAT_SEND_INFORM(session, msgdata);
			}

		}
		MessageSender.sendMessageModel(user_uid_sent_request, data);
	}

	public void ROOM_CHECK(MessageModel data) throws Exception {
		String userReq = data.getUSER_UID();
		data.put("USER_UID", userReq);
		userService.updateChatUser_RoomOpenCheck_UserCheck(data.getPAYLOAD());

		MessageSender.sendMessageModel(data.getUSER_UID(), data);
	}

	private Map<Object, Object> checkExistRooomChatInContact(String user1, String user2) throws Exception {
		Map<Object, Object> result = new HashMap<>();
		Map<Object, Object> para1 = new HashMap<>();
		para1.put("USER_UID", user1);
		para1.put("USER_INVT_UID", user2);
		para1.put("ROOM_TYPE", EnumRoomType.IN_CONTACT.getValue());
		List<Map<Object, Object>> checkCreateContactChat = userService.getListCheckChatInContact(para1);

		if (checkCreateContactChat != null && checkCreateContactChat.size() > 0) {
			result.putAll(checkCreateContactChat.get(0));
			result.put("RESULT", "OK");
			return result;
		} else {
			logger.error("checkCreateContactChat null or 0 size");
		}
		result.put("RESULT", "ERROR");
		return result;
	}

	private boolean insertChatUser(MessageModel data) throws Exception {
		Map<Object, Object> chat_user = new HashMap<>();
		chat_user.put("USER_UID", data.get("USER_UID"));
		chat_user.put("ROOM_UID", data.get("ROOM_UID"));
		chat_user.put("REG_DATE", new Date());
		chat_user.put("ROOM_NM", data.get("ROOM_NM"));
		chat_user.put("USER_INVT_UID", data.getUSER_UID());
		chat_user.put("USER_CHECK", "N");
		chat_user.put("ROOM_OPEN_CHECK", "N");

		userService.insertCHAT_USER(chat_user);

		return true;
	}

	String namespace = "chat.main";

	public void CHAT_SEND(MessageModel data) throws Exception {
		// String uid = (String) data.get("SEND_USER_UID");
		String uid = (String) data.getUSER_UID();
		String roomUid = (String) data.getPAYLOAD().get("ROOM_UID");
		String type = (String) data.getPAYLOAD().get("CONT_TYPE");

		Map<Object, Object> roomPara = new HashMap<>();
		roomPara.put("ROOM_UID", roomUid);
		// Map roomFind = adao.map("getRoomByKey", roomPara);
		Map<Object, Object> roomFind = roomService.getRoomByKey(roomPara);
		String roomKey = "";
		data.put("ROOM_TYPE", "");
		if (roomFind != null) {
			data.put("ROOM_TYPE", roomFind.get("ROOM_TYPE"));
			roomKey = roomFind.get("KEY_ROOM").toString();
		}

		String msg = (String) data.getPAYLOAD().get("MSG");

		/*
		 * String _modulus = MessageModelCmdMng.keyMap.get("key_" + wsSession.getId());
		 * String _exponent = MessageModelCmdMng.keyMap.get("keyex_" +
		 * wsSession.getId()); if (!Utils.isNullOrEmpty(_modulus)) { PublicKey pk =
		 * EncryptionUtil.publicKeyFromString(_modulus, _exponent); //PrivateKey
		 * privateKey = EncryptionUtil. //EncryptionUtil.decryptRSA(roomKey,
		 * msg.getBytes()); String message =
		 * EncryptionUtil.decryptAES(Base64.getEncoder().encodeToString(msg.getBytes()),
		 * roomKey); System.out.println("MESSAGE: " + message); byte[] outputData =
		 * EncryptionUtil.encryptRSA(pk, roomKey.getBytes()); }
		 */

		String msgUid = msgService.gen_chatuid_seq();

		String msgDecrypted = EncryptionUtil.decryptAES(msg, roomKey);
		System.out.println("MESSAGE DECRYPT: " + msgDecrypted);

		Map<Object, Object> param = new HashMap<>();
		// String msg_uid = KeyStateMng.getInstance().getMessageKey();
		param.put("USER_UID", uid);
		param.put("MSG_UID", msgUid);
		// param.put("MSG_UID", msg_uid + "");
		param.put("ROOM_UID", roomUid);
		param.put("SEND_DATE", new Date());
		param.put("MSG_CONT", msgDecrypted); // msg
		param.put("MSG_TYPE_CODE", type);
		msgService.insertMsg(param);
		msgService.insertMsgConf(param);

		roomService.updateDateModified(param);

		data.setDef(EnumMessageDef.CHAT_RECEIVE);
		data.put("SEND_DATE", param.get("SEND_DATE"));
		data.put("MSG_UID", param.get("MSG_UID"));
		data.put("MSG_CONT", msg);
		data.put("KEY_ROOM", roomFind.get("KEY_ROOM"));
		data.put("MSG_TYPE_CODE", type);
		data.put("USER_UID", uid);

		// List readInfo =
		Map<Object, Object> user_info = userService.getUserProfile(param);
		data.put("USER_NM_KOR", user_info.get("USER_NM_KOR"));
		data.put("USER_NM_ENG", user_info.get("USER_NM_ENG"));
		data.put("USER_IMG", user_info.get("USER_IMG"));

		if (type.equals("FILE")) {
			saveFileMessage(param.get("MSG_UID").toString(), data);
			String nameFile = (String) ((Map) data.get("FILE")).get("name");
			data.put("FILE_ORI_NM", nameFile);
			Map<Object, Object> file = (Map<Object, Object>) data.get("FILE");
			if (file != null) {
				// Convert to base64 image
				/*
				 * String typeImg = (String) file.get("type"); if (typeImg != null &&
				 * typeImg.startsWith("image")) { String pathImg = (String) file.get("path");
				 * File originalFile = new File(pathImg); String encodedBase64 = null; try {
				 * FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
				 * byte[] bytes = new byte[(int) originalFile.length()];
				 * fileInputStreamReader.read(bytes); encodedBase64 = new
				 * String(Base64.getEncoder().encodeToString(bytes)); } catch
				 * (FileNotFoundException e) { e.printStackTrace(); } catch (IOException e) {
				 * e.printStackTrace(); }
				 * 
				 * data.PAYLOAD.put("BIN_IMG", encodedBase64); }
				 */
			}
		}
		/*
		 * else if (type.equals("FILE_HIS")) { // Send tu lich su file String
		 * msg_uid_old = (String) data.get("MSG_UID_OLD"); Map<String, Object> param3 =
		 * new HashMap<String, Object>(); param3.put("MSG_UID", msg_uid_old);
		 * Map<String, Object> file_old = fileMsgService.getMsgFileMap(param3);
		 * 
		 * Map<Object, Object> param2 = new HashMap<Object, Object>();
		 * param2.put("MSG_UID", msg_uid + ""); param2.put("FILE_ORI_NM",
		 * file_old.get("FILE_ORI_NM")); param2.put("FILE_MODI_NM",
		 * file_old.get("FILE_MODI_NM")); param2.put("FILE_PATH",
		 * file_old.get("FILE_PATH")); param2.put("FILE_SIZE",
		 * file_old.get("FILE_SIZE")); param2.put("FILE_EXTN",
		 * file_old.get("FILE_EXTN")); // param2.put("FILE_VALD_DATE", "FILE");
		 * param2.put("FILE_LARGE_CHECK", "N"); fileMsgService.insertFileMsg(param2);
		 * 
		 * }
		 */
		// CAP NHAT TRANG THAI USER_CHECK, ROOM_OPEN_CHECK;
//		List<String> listMembers = (List<String>) roomService.getListMemberByRoomUid(param);
		List<String> listMembers = new ArrayList<>();
		List<Map<Object, Object>> infoMembers = roomService.getListInfoMemberByRoomUid(param);
		List<String> needNotificationUsers = new ArrayList<>();
		for (Map<Object, Object> infoMember : infoMembers) {
			String memberUid = (String) infoMember.get("USER_UID");
			listMembers.add(memberUid);
			BigDecimal useNotification = (BigDecimal) infoMember.get("USE_NOTIFICATION");
			if (useNotification == null)
				continue;
			int useNotificationInt = Integer.valueOf(useNotification.intValue());
			if (useNotificationInt != 0) {
				needNotificationUsers.add(memberUid);
			}
		}
		List<SessionModel> userOnline = SessionModelStateMng.getInstance().getSessionModelListByUidList(listMembers);
		for (SessionModel session : userOnline) {
			if (!roomKey.equals("")) {

				String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getWebSession().getId());
				String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getWebSession().getId());
				if (modulus == null || modulus.equals("")) {
					System.out.println("mudulus null");
					data.setRESULT("ERROR");
				} else {

					if (modulus == null || modulus.equals("")) {
						System.err.println("Key room null!!!!!");
						// return;
					}
					PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
					byte[] outputData = EncryptionUtil.encryptRSA(pk, roomKey.getBytes());
					roomFind.remove("KEY_ROOM");
					data.put("KEY_ROOM", outputData);
				}

			}
			MessageSender.sendMessageModel(session, MessageSender.convertToTextMessage(data));
		}

		// String userNameSend = (String) user_info.get("USER_NM_ENG");
		for (String userUIdTo : needNotificationUsers) {
			if (uid != null && !uid.equals(userUIdTo)) {
				// notificationService.sendNotificationNewMessage(false, userNameSend, uid,
				// userUIdTo, roomName, "");
			}
		}
	}

	public void CHAT_SEND_INFORM(WebSocketSession session, MessageModel data) throws Exception {
		String uid = data.getUSER_UID();
		String type = (String) data.get("CONT_TYPE");
		Map<Object, Object> roomPara = new HashMap<>();

		// Map roomFind = adao.map("getRoomByKey", roomPara);
		Map<Object, Object> roomFind = null;
		String roomUid = "";
		if (data.getPAYLOAD().containsKey("ROOM_UID")) {
			roomUid = (String) data.get("ROOM_UID");
			roomPara.put("ROOM_UID", roomUid);
			roomFind = roomService.getRoomByKey(roomPara);
		}
		Map<Object, Object> tmp = new HashMap<>();
		tmp.put("USER_UID", uid);
		Map<Object, Object> user1 = userService.getUserProfile(tmp);
		String username1 = (String) user1.get("USER_NAME");
		data.put("USER_NM", username1);

		String uid2 = data.get("USER_UID") != null ? (String) data.get("USER_UID") : null;
		data.put("USER2_UID", uid2);
		Map<Object, Object> user2 = null;
		if (!uid.equals(uid2)) {
			tmp.put("USER_UID", uid2);
			user2 = userService.getUserProfile(tmp);
			if (user2 != null) {
				data.put("USER2_NM", user2.get("USER_NAME"));
				data.put("USER2_IMG", user2.get("USER_IMG"));
			}
		}

		String msg = "";

		if (data.getPAYLOAD().containsKey("MSG_CONT")) {
			msg = data.getPAYLOAD().get("MSG_CONT") == null ? "" : (String) data.getPAYLOAD().get("MSG_CONT");
		} else {
			// CREATE MESSAGE CONTENT
			if (type.equals("CREATE_ROOM")) {
				msg = username1;
			} else if (type.equals("ROOM_MODIFY")) {
				msg = username1 + "," + data.get("ROOM_NM") + ".";
			} else {
				if (uid.equals(uid2)) {
					if (type.equals("OUT_ROOM")) {
						msg = username1;
					} else if (type.equals("IN_ROOM")) {
						msg = username1;
					}
				} else {
					if (user2 != null) {
						if (type.equals("OUT_ROOM")) {
							msg = username1 + "," + user2.get("USER_NAME");
						} else if (type.equals("IN_ROOM")) {
							msg = username1 + "," + user2.get("USER_NAME");
						}
					}
				}
			}
		}

		String roomKey = "";
		if (roomFind != null) {
			data.getPAYLOAD().putAll(roomFind);
			data.put("ROOM_TYPE", roomFind.get("ROOM_TYPE"));
			roomKey = roomFind.get("KEY_ROOM").toString();
			// msg = EncryptionUtil.encryptDES(msg, roomKey);
		}

		// Map<String, Object> param = new HashMap<String, Object>();
		String msg_uid = KeyStateMng.getInstance().getMessageKey();
		Map<Object, Object> param = new HashMap<>();
		param.put("USER_UID", uid);
		param.put("MSG_UID", msg_uid + "");
		param.put("ROOM_UID", roomUid);
		param.put("SEND_DATE", new Date());
		param.put("MSG_CONT", msg);

		param.put("MSG_TYPE_CODE", type);
		msgService.insertMsg(param);
		roomService.updateDateModified(param);

		data.setDef(EnumMessageDef.CHAT_RECEIVE);
		data.put("SEND_DATE", param.get("SEND_DATE"));
		data.put("MSG_UID", msg_uid);
		data.put("MSG_CONT", msg);
		data.put("KEY_ROOM", roomFind.get("KEY_ROOM"));
		data.put("MSG_TYPE_CODE", type);
		data.put("CONT_TYPE", "INFORM");
		data.put("MSG", msg);
		data.put("USER_UID", uid);

		// CAP NHAT TRANG THAI USER_CHECK, ROOM_OPEN_CHECK;
		List<String> listMembers = (List<String>) roomService.getListMemberByRoomUid(param);
		List<SessionModel> sessionOnline = SessionModelStateMng.getInstance().getSessionModelListByUidList(listMembers);

		for (SessionModel each : sessionOnline) {
			if (each.getWebSession().getId().equals(session.getId())
					&& (type.equals("OUT_ROOM") || type.equals("CREATE_ROOM") || type.equals("ROOM_MODIFY"))) {
				continue;
			}
			if (!roomKey.equals("")) {

				String modulus = MessageModelCmdMng.keyMap.get("key_" + each.getWebSession().getId());
				String exponent = MessageModelCmdMng.keyMap.get("keyex_" + each.getWebSession().getId());
				if (modulus == null || modulus.equals("")) {
					System.out.println("mudulus null");
					data.setRESULT("ERROR");
				} else {
					if (modulus == null || modulus.equals("")) {
						System.err.println("Key room null!!!!!");
						// return;
					}
					PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);

					byte[] outputData = EncryptionUtil.encryptRSA(pk, roomKey.getBytes());
					roomFind.remove("KEY_ROOM");
					data.put("KEY_ROOM", outputData);
				}

			}
			MessageSender.sendMessageModel(each, MessageSender.convertToTextMessage(data));
		}
	}

	public void CHAT_INFORM(MessageModel data, List<String> listMembers, List<String> except) throws Exception {
		List<String> userOnline = SessionModelStateMng.getInstance().getUserUidList();

		for (String member_UID : userOnline) {
			if (listMembers.contains(member_UID) && !except.contains(member_UID)) {
				MessageSender.sendMessageModel(member_UID, data);
			}

		}
	}

	/*
	 * Save file msg NDQ 20171128
	 */
	@SuppressWarnings("unchecked")
	private void saveFileMessage(String msg_uid, MessageModel data) throws Exception {
		String roomUid = (String) data.get("ROOM_UID");
		Map<Object, Object> file = (Map<Object, Object>) data.get("FILE");
		Map<Object, Object> roomPara = new HashMap<>();
		roomPara.put("ROOM_UID", roomUid);
		Map<Object, Object> roomFind = roomService.getRoomByKey(roomPara);
		data.put("ROOM_TYPE", "");
		if (roomFind != null) {
			data.put("ROOM_TYPE", roomFind.get("ROOM_TYPE"));
		}
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put("MSG_UID", msg_uid + "");
		param.put("FILE_ORI_NM", file.get("name"));
		param.put("FILE_MODI_NM", file.get("savename"));
		param.put("FILE_PATH", file.get("path"));
		param.put("FILE_SIZE", file.get("size"));
		param.put("FILE_EXTN", file.get("ext"));
		// param.put("FILE_VALD_DATE", "FILE");
		param.put("FILE_TYPE", file.get("type"));
		param.put("FILE_LARGE_CHECK", "N");

		// adao.insert(namespace,"insertMsg01",param);
		fileMsgService.insertFileMsg(param);
	}

	public void CHAT_RECEIVE(MessageModel data) throws Exception {
		MessageModel mm = new MessageModel();
		String uid = data.getUSER_UID();
		mm.setUSER_UID(uid);
		mm.setDef(EnumMessageDef.CHAT_RECEIVE);
	}

	public void CHAT_LIST(WebSocketSession session, MessageModel data) throws Exception {
		String uid = data.getUSER_UID();
		String roomUid = (String) data.get("ROOM_UID");
		if (Strings.isEmpty(roomUid) || Strings.isEmpty(uid))
			return;

		Map<Object, Object> parap = new HashMap<>();
		parap.put("ROOM_UID", roomUid);
		parap.put("USER_UID", uid);
		Map<Object, Object> fRoom = roomService.getRoomTypeByKey(parap);
		if (fRoom == null || fRoom.isEmpty())
			return;

		data.put("STATUS", fRoom.get("STATUS"));
		Map<Object, Object> payload = data.getPAYLOAD();
		if (payload.containsKey("GROUP_KEY") && null != payload.get("GROUP_KEY")
				&& !"".equals(payload.get("GROUP_KEY").toString())) {
			data.put("GROUP_KEY", payload.get("GROUP_KEY"));
		} else {
			String AESKey = (String) fRoom.get("KEY_ROOM");

			String modulus = MessageModelCmdMng.keyMap.get("key_" + session.getId());
			String exponent = MessageModelCmdMng.keyMap.get("keyex_" + session.getId());
			if (modulus == null || modulus.equals("")) {
				System.err.println("Key room null!!!!!");
				// return;
			}
			PublicKey pk = EncryptionUtil.publicKeyFromString(modulus, exponent);
			byte[] outputData = EncryptionUtil.encryptRSA(pk, AESKey.getBytes());
			String room_key = BaseEncoding.base64().encode(outputData);
			data.put("KEY_ROOM", room_key);

		}
		int recordPerPage;
		if (payload.containsKey("MOBILE")) {
			recordPerPage = 20;
		} else {
			recordPerPage = 100;
		}

		int indextStart = payload.get("START_INDEX") != null ? Integer.valueOf(payload.get("START_INDEX").toString())
				: 0;

		List<Map<Object, Object>> listMsg = msgService.getListMsgByRoomUid(parap, indextStart, recordPerPage);
		Map<Object, Object> room = roomService.getRoomByKey(payload);
		String keyRoom = room == null ? null : String.valueOf(room.get("KEY_ROOM"));
		List<Map<Object, Object>> listMsgNew = new ArrayList<>();
		for (int i = listMsg.size() - 1; i >= 0; i--) {
			Map<Object, Object> iMsg = listMsg.get(i);
			if (!Utils.isNullOrEmpty(keyRoom)) {
				// encrypt plain text to return to client
				if ("TEXT,FILE,CALL".indexOf(iMsg.get("MSG_TYPE_CODE").toString()) > -1) {
					iMsg.put("MSG_CONT", EncryptionUtil.encryptAES2(String.valueOf(iMsg.get("MSG_CONT")), keyRoom));
				}
			}
			listMsgNew.add(iMsg);
		}

		data.setDATALIST(listMsgNew);
		MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(uid, session),
				MessageSender.convertToTextMessage(data));
	}

	public void CHAT_CONFIRM(MessageModel data) throws Exception {
		// Confirm MSG_UID
		String uid = data.getUSER_UID();
		data.put("USER_UID", uid);
		data.put("READ_CHECK", "Y");
		Object obj = data.getPAYLOAD().get("LIST_READ");

		if (obj != null && !obj.equals("")) {
			List<String> listRead = (List<String>) obj;

			Map<Object, Object> param = new HashMap<>();
			param.putAll(data.getPAYLOAD());

			if (listRead.size() > 0) {
				// param.put("MSG_UIDS", listRead);
				msgService.insertMsgConfirmBatch(param);

				List<String> listMembers = (List<String>) roomService.getListMemberByRoomUid(data.getPAYLOAD());

				List<Map<Object, Object>> confirmData = msgService
						.getListMsgByRoomUidAndUserUIDAsConfirm(data.getPAYLOAD());
				data.setDATALIST(confirmData);
				List<SessionModel> listSession = SessionModelStateMng.getInstance()
						.getSessionModelListByUidList(listMembers);
				MessageSender.sendMessageModel(listSession, MessageSender.convertToTextMessage(data));
			}
		}
	}

	public void FILE_VIEW(MessageModel data) throws Exception {
		MessageModel mm = new MessageModel();
		String uid = data.getUSER_UID();
		mm.setUSER_UID(uid);
		mm.setDef(EnumMessageDef.FILE_VIEW);
	}

	public void FILE_UPLOAD(MessageModel data) throws Exception {
		MessageModel mm = new MessageModel();
		String uid = data.getUSER_UID();
		mm.setUSER_UID(uid);
		mm.setDef(EnumMessageDef.FILE_UPLOAD);
	}

	public void FILE_DOWNLOAD(MessageModel data) throws Exception {
		MessageModel mm = new MessageModel();
		String uid = data.getUSER_UID();
		mm.setUSER_UID(uid);
		mm.setDef(EnumMessageDef.FILE_DOWNLOAD);
	}

	public void MEMBER_LIST(WebSocketSession session, MessageModel data) throws Exception {
		String uid = data.getUSER_UID();
		String roomUid = (String) data.get("ROOM_UID");
		List<Map<Object, Object>> listMembers = roomService.getListMemberByRoomUid02(roomUid);
		if (listMembers != null && listMembers.size() > 0) {
			data.put("ROOM_NM", listMembers.get(0).get("ROOM_NM"));
			data.put("ROOM_TYPE", listMembers.get(0).get("ROOM_TYPE"));
		} else {
			data.put("ROOM_NM", "?");
		}
		data.setDATALIST(listMembers);
		MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(uid, session),
				MessageSender.convertToTextMessage(data));
	}

	public void MEMBER_VIEW(WebSocketSession session, MessageModel data) throws Exception {
		String user_uid_sent_request = data.getUSER_UID();
		String user_uid_getinfo = (String) data.getPAYLOAD().get("USER_UID");
		List<String> userOnline = SessionModelStateMng.getInstance().getUserUidList();

		Map<Object, Object> params = new HashMap<>();
		params.put("USER_UID_1", user_uid_sent_request);
		params.put("USER_UID_2", user_uid_getinfo);

		Map<Object, Object> userDetail = userService.getUserDetail(params);
		if (userOnline.contains(user_uid_getinfo)) {
			userDetail.put("IS_ONLINE", "Y");
		}
		if (userDetail != null) {
			data.PAYLOAD.putAll(userDetail);
			MessageSender.sendMessageModel(
					SessionModelStateMng.getInstance().getSessionModelByWSSesion(user_uid_sent_request, session),
					MessageSender.convertToTextMessage(data));
		}
	}

	public void MEMBER_MODIFY(WebSocketSession session, MessageModel data) throws Exception {
		String uid = data.getUSER_UID();
		String user_nm = (String) data.get("USER_NM_TMP");
		if (user_nm != null && user_nm != "") {
			data.put("USER_UID", uid);
			userService.updateUser_Nm_Eng(data.getPAYLOAD());
			MessageSender.sendMessageModel(SessionModelStateMng.getInstance().getSessionModelByWSSesion(uid, session),
					MessageSender.convertToTextMessage(data));
		}
	}

	public void MEMBER_REFRESH(MessageModel data) throws Exception { // Send Message
		MessageModel mm = new MessageModel();
		String uid = data.getUSER_UID();
		mm.setUSER_UID(uid);
		mm.setDef(EnumMessageDef.MEMBER_REFRESH);
	}

	public void SETTING_VIEW(WebSocketSession session, MessageModel data) throws Exception {
		Map<Object, Object> para01 = new HashMap<>();
		para01.put("USER_UID", data.getUSER_UID());
		Map<Object, Object> config = confService.getConfigByKey(para01);
		if (config == null || config.size() == 0) {
			// Insert new
			Map<Object, Object> para02 = new HashMap<>();
			para02.putAll(data.getPAYLOAD());
			para02.put("SKIN_UID", data.get("SKIN_UID"));
			para02.put("READ_CHECK", "Y");
			para02.put("NOTIFY_SOUND_YN", "Y");
			para02.put("CONF_FONT_SIZE", 6);
			para02.put("USER_UID", data.getUSER_UID());
			para02.put("REG_USER_UID", data.getUSER_UID());
			confService.insertConfig(new HashMap<>(para02));
			config = para02;
		}
		data.getPAYLOAD().putAll(config);
		MessageSender.sendMessageModel(data.getUSER_UID(), data);
	}

	public void SETTING_MODIFY(WebSocketSession session, MessageModel data) throws Exception {

		Map<Object, Object> para01 = new HashMap<>();
		para01.putAll(data.getPAYLOAD());
		para01.put("USER_UID", data.getUSER_UID());
		para01.put("MODI_USER_UID", data.getUSER_UID());
		confService.updateConfig(para01);
		MessageSender.sendMessageModel(data.getUSER_UID(), data);
	}

	public void deleteRoomWithData(String pRoomUid) throws Exception {

		Map<Object, Object> map = new HashMap<>();
		map.put("ROOM_UID", pRoomUid);
		List<Map<Object, Object>> files = fileMsgService.getListFileByRoomId(map);

		try {
			if (files != null) {
				for (Object i : files) {
					// String uploadRootPath =
					// context.getRealPath(AdmConstance.UPLOAD_ROOT_PATH_NAME);
					Map<Object, Object> im = (Map<Object, Object>) i;
					String filePath = (String) im.get("FILE_PATH");

					Map mapRootPath = ResourceUtil.getMessageMap("system.dir.upload.rootPath");
					String rootPath = (String) mapRootPath.get("MESSAGE");
					Map mapContextPath = ResourceUtil.getMessageMap("system.dir.upload.contextPath");
					String contextPath = (String) mapContextPath.get("MESSAGE");

					if (filePath.length() > contextPath.length() && filePath.indexOf(contextPath) == 0) {
						filePath = rootPath + filePath.substring(contextPath.length());
					}

					if (!Strings.isEmpty(filePath)) {
						File checkFile = new File(filePath);
						if (checkFile.exists()) {
							checkFile.delete();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		fileMsgService.deleteFileByRoomUid(pRoomUid);
		msgService.deleteConfirmByRoomUid(pRoomUid);
		msgService.deleteMsgByRoomUid(pRoomUid);
		userService.deleteUserByRoomUid(pRoomUid);
		roomService.deleteChatroom(pRoomUid);
	}

	private void deleteRoomWithData2(Map<Object, Object> param) throws Exception {
		List<Map<Object, Object>> files = new ArrayList<>();
		Object rsF = fileMsgService.getListFileByRoomId(param);
		try {
			if (!rsF.toString().equals("0")) {
				files = (List<Map<Object, Object>>) rsF;
				for (Map<Object, Object> im : files) {
					// String uploadRootPath =
					// context.getRealPath(AdmConstance.UPLOAD_ROOT_PATH_NAME);
					String filePath = (String) im.get("FILE_PATH");

					Map mapRootPath = ResourceUtil.getMessageMap("system.dir.upload.rootPath");
					String rootPath = (String) mapRootPath.get("MESSAGE");
					Map mapContextPath = ResourceUtil.getMessageMap("system.dir.upload.contextPath");
					String contextPath = (String) mapContextPath.get("MESSAGE");

					if (filePath.length() > contextPath.length() && filePath.indexOf(contextPath) == 0) {
						filePath = rootPath + filePath.substring(contextPath.length());
					}

					if (!Strings.isEmpty(filePath)) {
						File checkFile = new File(filePath);
						if (checkFile.exists()) {
							checkFile.delete();
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		String pRoomUid = param.get("ROOM_UID").toString();
		fileMsgService.deleteFileByRoomUid(pRoomUid);
		msgService.deleteConfirmByRoomUid(pRoomUid);
		msgService.deleteMsgByRoomUid(pRoomUid);
		userService.deleteUserByRoomUid(pRoomUid);
		roomService.deleteChatroom(pRoomUid);
	}

	/*
	 * private void autoMakeFrd(String userUid1, String userUid2) throws Exception {
	 * Map<Object, Object> p1 = new HashMap<>(); String invtUid =
	 * KeyStateMng.getInstance().getRoomKey(); p1.put("USER_UID_1", userUid1);
	 * p1.put("USER_UID_2", userUid2); p1.put("INVT_UID", invtUid); p1.put("FAV_YN",
	 * "N"); p1.put("MSG_GRT", "Auto greating"); p1.put("ACCEPT_YN", "Y");
	 * p1.put("AUTO_YN", "Y"); String frdUid =
	 * KeyStateMng.getInstance().getRoomKey(); p1.put("FRD_UID", frdUid);
	 * 
	 * frdService.insertAutoMakeFrd(p1); }
	 */

}
