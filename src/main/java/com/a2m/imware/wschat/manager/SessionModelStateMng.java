package com.a2m.imware.wschat.manager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.adapter.standard.StandardWebSocketSession;

import com.a2m.imware.wschat.model.EnumMessageDef;
import com.a2m.imware.wschat.model.MessageModel;
import com.a2m.imware.wschat.model.SessionModel;
import com.a2m.imware.wschat.model.SessionModel.TYPE;

public class SessionModelStateMng {

	private volatile static SessionModelStateMng ssm;

	private Map<String, List<SessionModel>> sessionModelMap = new HashMap<String, List<SessionModel>>();

	public static SessionModelStateMng getInstance() {
		if (ssm == null) {// 있는지 체크 없으면
			synchronized (SessionModelStateMng.class) {
				if (ssm == null) {
					ssm = new SessionModelStateMng(); // 생성한뒤
				}
			}
		}
		return ssm;// 생성자를 넘긴다.
	}

	public void setSessionModel(String key, List<SessionModel> sm) {
		sessionModelMap.put(key, sm);
	}

	public void setSessionModel(WebSocketSession session) {

		SessionModel sm = new SessionModel(session);

		List<SessionModel> listSM = sessionModelMap.get(sm.getPersonal_id());
		if (listSM == null) {
			listSM = new ArrayList<SessionModel>();
		} else {
			TYPE typeSession = sm.getType();
			// only allow 1 session with 1 user on mobile
			if (typeSession == TYPE.MOBILE) {
				Iterator<SessionModel> iterator = listSM.iterator();
				while (iterator.hasNext()) {
					SessionModel sessionModel = iterator.next();
					if (TYPE.MOBILE.equals(sessionModel.getType())) {
						listSM.remove(sessionModel);
					}
				}
			}
		}
		listSM.add(sm);
		sessionModelMap.put(sm.getPersonal_id(), listSM);
	}

	/**
	 * 기능명
	 * 
	 * @작성일 : 2017. 9. 21.
	 * @작성자 : keim
	 * @프로그램설명 :
	 * @진행상태: TO-DO, DEBUG, TEST, COMPLETE
	 */
	public void destroySessionModel(SessionModel sm) {

		if (sessionModelMap.containsValue(sm)) {
			destroySession(getUidBySessionModel(sm));
		} else {
			System.out.println("session이 Null 입니다.");
		}
	}

	/**
	 * destroySession
	 * 
	 * @Description :after modified - delete only ws session
	 * @Output : void
	 * @Create : Nov 28, 2018
	 * @Author : AnhPV
	 * @Status : TO-DO, DEBUG, TEST, COMPLETE
	 */
	public void destroySession(WebSocketSession session) {
		String uid = getUidBySession(session);
		List<SessionModel> listSess = (List) ((Map) sessionModelMap).get(uid);
		if (listSess != null) {
			for (int i = 0; i < listSess.size(); i++) {
				if (listSess.get(i).getWebSession().equals(session)) {
					// set offline
					List<String> userOnline = SessionModelStateMng.getInstance().getUserUidList();
					userOnline.remove(uid); // remove current user uid from online list
					List<SessionModel> listSession = SessionModelStateMng.getInstance().getSessionModelListByUidList(userOnline);
					MessageModel dataOnline = new MessageModel();
					dataOnline.setDef(EnumMessageDef.MEMBER_ONLINE);
					dataOnline.put("USER_UID", uid);
					// send offline
					dataOnline.PAYLOAD.put("ONLINE", "N");
					MessageSender.sendMessageModel(listSession, MessageSender.convertToTextMessage(dataOnline));
					
					listSess.remove(i);
					break;
				}
			}
			if (!listSess.isEmpty()) {
				((Map) sessionModelMap).put(uid, listSess);
			} else {
				((Map) sessionModelMap).remove(uid);
			}
		}
	}

	public void destroySession(String UID_SessionId) {
		if (UID_SessionId != null && !UID_SessionId.equals(""))
			sessionModelMap.remove(UID_SessionId);
	}

	public String getUidBySessionModel(SessionModel sm) {
		String key = null;
		for (String o : sessionModelMap.keySet()) {
			if (sessionModelMap.get(o).equals(sm)) {
				key = o;
				break;
			}
		}
		return key;
	}

	/**
	 * 기능명
	 * 
	 * @작성일 : 2017. 9. 21.
	 * @작성자 : keim
	 * @프로그램설명 :
	 * @진행상태: TO-DO, DEBUG, TEST, COMPLETE
	 */
	public String getUidBySession(WebSocketSession session) {
		String key = null;
		boolean flag = false;
		for (Map.Entry<String, List<SessionModel>> entry : sessionModelMap.entrySet()) {
			for (SessionModel sm : entry.getValue()) {
				if (sm.containsSession(session)) {
					key = entry.getKey();
					flag = true;
					break;
				}
			}
			if (flag) {
				break;
			}
		}
		return key;
	}

	public List<SessionModel> getSessionModelBySession(StandardWebSocketSession session) {
		return sessionModelMap.get(getUidBySession(session));
	}

	public List<SessionModel> getSessionModelByUID(String UID) {
		List<SessionModel> sm = null;
		sm = (List<SessionModel>) sessionModelMap.get(UID);

		return sm;
	}

	public List<SessionModel> getSessionModelListByUidList(List<String> uidList) {
		List<SessionModel> smlist = new ArrayList<SessionModel>();

		for (String key : uidList) {
			List<SessionModel> lstSm = getSessionModelByUID(key);
			if (lstSm != null) {
				for (SessionModel sm : lstSm) {
					smlist.add(sm);
				}
			}
		}

		return smlist;
	}

	public List<String> getUserOfflineByUidList(List<String> uidList) {
		List<String> smlist = new ArrayList<String>();

		for (String key : uidList) {
			List<SessionModel> lstSm = getSessionModelByUID(key);
			if (lstSm == null || lstSm.isEmpty()) {
				smlist.add(key);
			}
		}
		return smlist;
	}

	public Map<String, List<SessionModel>> getSessionModelMap() {
		return sessionModelMap;
	}

	public List<String> getUserUidList() {
		List<String> list = new ArrayList<String>();
		list.addAll(sessionModelMap.keySet());
		return list;

	}

	public void printAll() {
		Class<? extends SessionModelStateMng> cls = this.getClass();
		Field[] fields = cls.getDeclaredFields();

		try {
			for (Field fd : fields) {
				System.out.println(fd.getName() + ": " + fd.get(this));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void printSessionModelList() {
	}

	public SessionModel getSessionModelByWSSesion(String user_uid, WebSocketSession session) {
		List<SessionModel> list = SessionModelStateMng.getInstance().getSessionModelByUID(user_uid);
		for (SessionModel each : list) {
			if (each.getWebSession() == session) {
				return each;
			}
		}
		return null;
	}

}
