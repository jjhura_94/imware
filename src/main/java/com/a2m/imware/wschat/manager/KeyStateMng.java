package com.a2m.imware.wschat.manager;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.a2m.imware.wschat.util.CommonUtil;

import kr.a2mvn.largefileupload.common.CastUtil;
//import javax.websocket.Session;

/**
 * @author "keim"
 *
 */
public class KeyStateMng {

	private volatile static KeyStateMng ksm;

//	private Map< String, SessionModel > sessionMap = new HashMap();	

	private Map<String, Object> messageKey;
	private Map<String, Object> roomKey;

	public static KeyStateMng getInstance() {
		if (ksm == null) {// 있는지 체크 없으면
			synchronized (KeyStateMng.class) {
				if (ksm == null) {
					ksm = new KeyStateMng(); // 생성한뒤
				}
			}
		}
		return ksm;// 생성자를 넘긴다.
	}

	private KeyStateMng() {
		messageKey = new HashMap<String, Object>();
		roomKey = new HashMap<String, Object>();

		messageKey.put("time", Calendar.getInstance().getTimeInMillis());
		messageKey.put("seq", 0);

		roomKey.put("time", Calendar.getInstance().getTimeInMillis());
		roomKey.put("seq", 0);
	}

	public String getMessageKey() {
		String key = "";

		// 현쟈시간이 저장된 시가보다 작거나 같으면
		if ((Long) messageKey.get("time") >= Calendar.getInstance().getTimeInMillis()) {
			int seq = (int) messageKey.get("seq");
			messageKey.put("key", ++seq);
			key = genKey(messageKey);
		} else {
			messageKey.put("time", Calendar.getInstance().getTimeInMillis());
			messageKey.put("seq", 0);
			key = genKey(messageKey);
		}
		return key;
	}

	public String getRoomKey() {
		String key = "";

		// 현쟈시간이 저장된 시가보다 작거나 같으면
		if ((Long) roomKey.get("time") >= Calendar.getInstance().getTimeInMillis()) {
			int seq = (int) roomKey.get("seq");
			roomKey.put("key", ++seq);
			key = genKey(roomKey);
		} else {
			roomKey.put("time", Calendar.getInstance().getTimeInMillis());
			roomKey.put("seq", 0);
			key = genKey(roomKey);
		}
		return key;
	}

	private String genKey(Map<String, Object> obj) {
		String key;
		key = CastUtil.castToString(obj.get("time")) + CommonUtil.lpad("0", CastUtil.castToString(obj.get("seq")), 4);

		return key;
	}

}
