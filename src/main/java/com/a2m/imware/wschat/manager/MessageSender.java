package com.a2m.imware.wschat.manager;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.a2m.imware.wschat.model.MessageModel;
import com.a2m.imware.wschat.model.SessionModel;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageSender {

	private static final Logger logger = LoggerFactory.getLogger(MessageSender.class);

	public static void sendMessageModel(String UID, MessageModel messageModel) {
		// List<SessionModel> sessionList =
		// SessionStateManager.getInstance().getSessionModelListByUID(UserList);

		List<SessionModel> lstSm = new ArrayList<>();
		lstSm = SessionModelStateMng.getInstance().getSessionModelByUID(UID);
		if (null != lstSm) {
			messageModel.setUSER_REC_UID(UID);
			for (SessionModel sm : lstSm) {
				sendMessageModel(sm, convertToTextMessage(messageModel));
			}
			logger.info("SendMsgModel sent");
		}
	}

	public static void sendMessageModel(List<String> UserList, MessageModel messangeModel) {
		List<SessionModel> sessionList = SessionModelStateMng.getInstance().getSessionModelListByUidList(UserList);
		for (SessionModel sm : sessionList) {
			// de biet duoc mess duoc gui di dau
			if (sm != null) {
				messangeModel.setUSER_REC_UID(sm.getPersonal_id());
				TextMessage textMessage = convertToTextMessage(messangeModel);
				sendMessageModel(sm, textMessage);
			}
		}
	}

	public static void sendMessageModel(SessionModel sm, TextMessage textMessage) {
		if (sm == null)
			return;

		for (WebSocketSession wss : (List<WebSocketSession>) sm.getSessions()) {
			try {
				if (wss != null && wss.isOpen()) {
					wss.sendMessage(textMessage);
					logger.info("Session sent: " + wss.getId());
				}
			} catch (Exception e) {
				logger.error("#sendMessageModel(SessionModel sm, TextMessage textMessage): ", e);
			}
		}
	}

	/**
	 * sendMessageModel
	 * 
	 * @Description :
	 * @Output : void
	 * @Create : Nov 27, 2018
	 * @Author : AnhPV
	 * @Status : TO-DO, DEBUG, TEST, COMPLETE
	 */
	public static void sendMessageModel(List<SessionModel> listSM, TextMessage textMessage) {
		if (listSM == null || listSM.isEmpty())
			return;

		for (SessionModel each : listSM) {
			for (WebSocketSession wss : (List<WebSocketSession>) each.getSessions()) {
				try {
					if (wss != null && wss.isOpen()) {
						wss.sendMessage(textMessage);
						logger.info("Session sent: " + wss.getId());
					}
				} catch (Exception e) {
					logger.error("#sendMessageModel(List<SessionModel> listSM, TextMessage textMessage): ", e);
				}
			}
		}
	}

	public static TextMessage convertToTextMessage(MessageModel model) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, false);
		String str = null;
		try {
			str = mapper.writeValueAsString(model);
		} catch (Exception e) {
			logger.error("#convertToTextMessage(MessageModel model): ", e);
		}
		TextMessage message = new TextMessage(str);
		return message;
	}

}
