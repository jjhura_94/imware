package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * Map with TNDM_NOTICE_COMMENT table.
 *
 * @author HungDM
 *
 */
public class TndmNoticeComment extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ID")
    private Long id;

    @MapKey("NOTICE_ID")
    private Long noticeId;

    @MapKey("CONTENT")
    private String content;

    @MapKey("UP_COMMENT_ID")
    private Long upCommentId;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("IS_EDITED")
    private Boolean isEdited;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("NAME_EN")
    private String nameEn;

    @MapKey("NAME_CH")
    private String nameCh;

    @MapKey("IMG_PATH")
    private String imgPath;

    private String deptName;
    private String position;

    List<TndmNoticeComment> children;

    public TndmNoticeComment() {
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUpCommentId() {
        return upCommentId;
    }

    public void setUpCommentId(Long upCommentId) {
        this.upCommentId = upCommentId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameCh() {
        return nameCh;
    }

    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public List<TndmNoticeComment> getChildren() {
        return children;
    }

    public void setChildren(List<TndmNoticeComment> children) {
        this.children = children;
    }

}
