/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.a2m.imware.validation.ValidationCode;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsRoomBookingResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("ROOM_ID")
	private Long roomId;

	@MapKey("ROOM_NAME")
	private String roomName;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("START_TIME")
	private Date startTime;

	@MapKey("START_TIME_STRING")
	private String startTimeString;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("END_TIME")
	private Date endTime;

	@MapKey("END_TIME_STRING")
	private String endTimeString;

	@MapKey("CREATED_BY")
	private String createdBy;

	@MapKey("CREATED_DATE")
	private Date createdDate;

	@MapKey("CREATED_DATE_STRING")
	private String createDateString;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("DEPT_CODE")
	private String deptCode;

	@MapKey("DEPT_NAME")
	private String deptName;

	@MapKey("PURPOSE")
	private String purpose;

	@MapKey("UPDATED_BY")
	private String updatedBy;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	@MapKey("NAME_KR")
	private String nameKr;

	@MapKey("DUTY_NAME")
	private String dutyName;

	private String sessUserId;

	public TcdsRoomBookingResponse() {

	}

	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : ROOM_ID (BIGINT)
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Long getRoomId() {
		return this.roomId;
	}

	// --- DATABASE MAPPING : START_TIME (TIMESTAMP)
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	// --- DATABASE MAPPING : END_TIME (TIMESTAMP)
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	// --- DATABASE MAPPING : CREATED_BY (VARCHAR)
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	// --- DATABASE MAPPING : DEPT_CODE (VARCHAR)
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptCode() {
		return this.deptCode;
	}

	// --- DATABASE MAPPING : PURPOSE (LONGTEXT)
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPurpose() {
		return this.purpose;
	}

	// --- DATABASE MAPPING : UPDATED_BY (VARCHAR)
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	// --- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public String getCreateDateString() {
		return createDateString;
	}

	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getUpdatedDateString() {
		return updatedDateString;
	}

	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}

	public String getStartTimeString() {
		return startTimeString;
	}

	public void setStartTimeString(String startTimeString) {
		this.startTimeString = startTimeString;
	}

	public String getEndTimeString() {
		return endTimeString;
	}

	public void setEndTimeString(String endTimeString) {
		this.endTimeString = endTimeString;
	}

	public String getSessUserId() {
		return sessUserId;
	}

	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

}