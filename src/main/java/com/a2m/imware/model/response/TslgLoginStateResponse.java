/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.a2m.imware.validation.ValidationCode;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TslgLoginStateResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("LOGIN_NO")
	private Long loginNo;

	@MapKey("USER_UID")
	private String userUid;

	@MapKey("USER_ID")
	private String userId;

	@MapKey("NAME_KR")
	private String nameKr;

	@MapKey("IP")
	private String ip;

	@MapKey("DATE_DAY")
	private Date dateDay;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("LOGIN_DATE")
	private Date loginDate;

	@MapKey("LOGIN_DATE_STRING")
	private String loginDateString;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("LOGOUT_DATE")
	private Date logoutDate;

	@MapKey("LOGOUT_DATE_STRING")
	private String logoutDateString;

	@MapKey("DEPT_CODE")
	private String deptCode;

	@MapKey("DEPT_NAME")
	private String deptName;

	@MapKey("SESS_ID")
	private String sessId;

	public TslgLoginStateResponse() {

	}

	public Long getLoginNo() {
		return loginNo;
	}

	public void setLoginNo(Long loginNo) {
		this.loginNo = loginNo;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getDateDay() {
		return dateDay;
	}

	public void setDateDay(Date dateDay) {
		this.dateDay = dateDay;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginDateString() {
		return loginDateString;
	}

	public void setLoginDateString(String loginDateString) {
		this.loginDateString = loginDateString;
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	public String getLogoutDateString() {
		return logoutDateString;
	}

	public void setLogoutDateString(String logoutDateString) {
		this.logoutDateString = logoutDateString;
	}

	public String getSessId() {
		return sessId;
	}

	public void setSessId(String sessId) {
		this.sessId = sessId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

}