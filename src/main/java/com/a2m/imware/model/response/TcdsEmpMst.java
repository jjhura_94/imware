package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * 
 * @author Tien
 *
 */
public class TcdsEmpMst extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("USER_ID")
	private Long userId;

	@MapKey("EMP_NO")
	private Long empNo;

	@MapKey("PWD")
	private String pwd;

	@MapKey("NAME_KR")
	private String nameKr;

	@MapKey("NAME_EN")
	private Date nameEn;
	
	@MapKey("NAME_CH")
	private Date namech;

	@MapKey("DOB")
	private Date dob;

	@MapKey("DOB_STRING")
	private String dobString;

	@MapKey("GENDER")
	private String gender;
	
	@MapKey("START_DATE")
	private Date startDate;

	@MapKey("START_DATE_STRING")
	private String startDateString;

	@MapKey("END_DATE")
	private Date endDate;

	@MapKey("END_DATE_STRING")
	private String endDateString;

	@MapKey("DEPT_CODE")
	private String deptCode;

	@MapKey("SPOT")
	private String spot;
	
	@MapKey("POS_CODE")
	private String posCode;

	@MapKey("DUTY_CODE")
	private String dutyCode;
	
	@MapKey("DUTY_NAME")
	private String dutyName;

	@MapKey("CELL_PHONE")
	private String cellPhone;

	@MapKey("EXT_NUMBER")
	private String extNumber;
	
	@MapKey("EMAIL")
	private String email;
	
	@MapKey("SE_NUMBER")
	private String seNumber;
	
	@MapKey("ADDRESS")
	private String address;
	
	@MapKey("IMG_PATH")
	private String imgPath;
	
	@MapKey("SIGNATURE_PATH")
	private String signaturePath;

	@MapKey("CREATED_BY")
	private String createdBy;

	@MapKey("CREATED_DATE")
	private Date createdDate;

	@MapKey("CREATED_DATE_STRING")
	private String createDateString;

	@MapKey("UPDATED_BY")
	private String updatedBy;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	private String sessUserId;


}
