/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;


public class TndmFormMgtResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String formName;

    private String formContent;

    private String formType;

    private String formCategory;

    private String description;

    private String createdBy;

    private Date createdDate;

    private String updatedBy;

    private Date updatedDate;

    private Boolean useYn;

    private String formTypeName;

    private String formCategoryName;

    public TndmFormMgtResponse() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : FORM_NAME (VARCHAR)
    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return this.formName;
    }

    //--- DATABASE MAPPING : FORM_CONTENT (LONGTEXT)
    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }

    public String getFormContent() {
        return this.formContent;
    }

    //--- DATABASE MAPPING : FORM_TYPE (VARCHAR)
    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormType() {
        return this.formType;
    }

    //--- DATABASE MAPPING : FORM_CATEGORY(VARCHAR)
    public String getFormCategory() {
        return formCategory;
    }

    public void setFormCategory(String formCategory) {
        this.formCategory = formCategory;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

    //--- DATABASE MAPPING : USE_YN (BIT)
    public void setUseYn(Boolean useYn) {
        this.useYn = useYn;
    }

    public Boolean getUseYn() {
        return this.useYn;
    }

    public String getFormTypeName() {
        return formTypeName;
    }

    public void setFormTypeName(String formTypeName) {
        this.formTypeName = formTypeName;
    }

    public String getFormCategoryName() {
        return formCategoryName;
    }

    public void setFormCategoryName(String formCategoryName) {
        this.formCategoryName = formCategoryName;
    }
}