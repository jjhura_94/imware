/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.a2m.imware.validation.ValidationCode;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsEmpVacationRequestResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("EMP_NO")
	private Long empNo;

	@MapKey("DEPT_NAME")
	private String deptName;

	@MapKey("NAME_KR")
	private String nameKr;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("VACATION_TYPE")
	private String vacationType;

	@MapKey("VACATION_TYPE_STRING")
	private String vacationTypeString;

	@MapKey("VACATION_TYPE2")
	private String vacationType2;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("FROM_DATE")
	private Date fromDate;

	@MapKey("FROM_DATE_STRING")
	private String fromDateString;

	@MapKey("TIME_LEAVE")
	private Integer timeLeave;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("TO_DATE")
	private Date toDate;

	@MapKey("TO_DATE_STRING")
	private String toDateString;

	@MapKey("REASON")
	private String reason;

	@MapKey("DESTINATION")
	private String destination;

	@MapKey("CONTACT_NETWORK")
	private String contactNetwork;

	@MapKey("REMARK")
	private String remark;

	@MapKey("STATUS")
	private String status;

	@MapKey("STATUS_STRING")
	private String statusString;

	@MapKey("CREATED_DATE")
	private Date createdDate;

	@MapKey("CREATED_DATE_STRING")
	private String createdDateString;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	@MapKey("CHECK_APPROVAL_AND_REJECT")
	private String checkApprovalAndReject;

	@MapKey("USER_UID")
	private String userUid;

	@MapKey("CELL_PHONE")
	private String cellPhone;

	@MapKey("DUTY_CODE")
	private String dutyCode;

	@MapKey("DUTY_NAME")
	private String dutyName;

	private String sessUserId;

	private String deptCode;

	public TcdsEmpVacationRequestResponse() {

	}

	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : EMP_NO (BIGINT)
	public void setEmpNo(Long empNo) {
		this.empNo = empNo;
	}

	public Long getEmpNo() {
		return this.empNo;
	}

	// --- DATABASE MAPPING : VACATION_TYPE (VARCHAR)
	public void setVacationType(String vacationType) {
		this.vacationType = vacationType;
	}

	public String getVacationType() {
		return this.vacationType;
	}

	// --- DATABASE MAPPING : FROM_DATE (DATETIME)
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	// --- DATABASE MAPPING : TO_DATE (DATETIME)
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	// --- DATABASE MAPPING : REASON (VARCHAR)
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return this.reason;
	}

	// --- DATABASE MAPPING : DESTINATION (VARCHAR)
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestination() {
		return this.destination;
	}

	// --- DATABASE MAPPING : STATUS (VARCHAR)
	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	// --- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public String getCreatedDateString() {
		return createdDateString;
	}

	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}

	public String getUpdatedDateString() {
		return updatedDateString;
	}

	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}

	public String getSessUserId() {
		return sessUserId;
	}

	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}

	public Integer getTimeLeave() {
		return timeLeave;
	}

	public void setTimeLeave(Integer timeLeave) {
		this.timeLeave = timeLeave;
	}

	public String getFromDateString() {
		return fromDateString;
	}

	public void setFromDateString(String fromDateString) {
		this.fromDateString = fromDateString;
	}

	public String getToDateString() {
		return toDateString;
	}

	public void setToDateString(String toDateString) {
		this.toDateString = toDateString;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getStatusString() {
		return statusString;
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getVacationTypeString() {
		return vacationTypeString;
	}

	public void setVacationTypeString(String vacationTypeString) {
		this.vacationTypeString = vacationTypeString;
	}

	public String getVacationType2() {
		return vacationType2;
	}

	public void setVacationType2(String vacationType2) {
		this.vacationType2 = vacationType2;
	}

	public String getContactNetwork() {
		return contactNetwork;
	}

	public void setContactNetwork(String contactNetwork) {
		this.contactNetwork = contactNetwork;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCheckApprovalAndReject() {
		return checkApprovalAndReject;
	}

	public void setCheckApprovalAndReject(String checkApprovalAndReject) {
		this.checkApprovalAndReject = checkApprovalAndReject;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

}