/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;
import java.io.Serializable;
import java.util.Date;


public class TcdsEmpEduResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long eduId;
    private Long empNo;
    private String eduLevel;
    private Date startDate;
    private Date endDate;
    private String schoolName;
    private String majorName;
    private String graduationStatus;
    private String degree;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    private String eduLevelName;
    private String graduationStatusName;
    private String degreeName;

    public TcdsEmpEduResponse() {
       
	}


    //--- DATABASE MAPPING : EDU_ID (BIGINT)
    public void setEduId(Long eduId) {
        this.eduId = eduId;
    }

    public Long getEduId() {
        return this.eduId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : EDU_LEVEL (VARCHAR)
    public void setEduLevel(String eduLevel) {
        this.eduLevel = eduLevel;
    }

    public String getEduLevel() {
        return this.eduLevel;
    }

    //--- DATABASE MAPPING : START_DATE (DATETIME)
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    //--- DATABASE MAPPING : END_DATE (DATETIME)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    //--- DATABASE MAPPING : SCHOOL_NAME (VARCHAR)
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    //--- DATABASE MAPPING : MAJOR_NAME (VARCHAR)
    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getMajorName() {
        return this.majorName;
    }

    //--- DATABASE MAPPING : GRADUATION_STATUS (VARCHAR)
    public void setGraduationStatus(String graduationStatus) {
        this.graduationStatus = graduationStatus;
    }

    public String getGraduationStatus() {
        return this.graduationStatus;
    }

    //--- DATABASE MAPPING : DEGREE (VARCHAR)
    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDegree() {
        return this.degree;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

    public String getEduLevelName() {
        return eduLevelName;
    }

    public void setEduLevelName(String eduLevelName) {
        this.eduLevelName = eduLevelName;
    }

    public String getGraduationStatusName() {
        return graduationStatusName;
    }

    public void setGraduationStatusName(String graduationStatusName) {
        this.graduationStatusName = graduationStatusName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }
}