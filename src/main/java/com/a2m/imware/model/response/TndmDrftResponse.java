/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.model.request.TndmDrftSpendingRequest;
import com.a2m.imware.validation.ValidationCode;


public class TndmDrftResponse extends TndmDrftSpendingRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String drftDocNo; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docNo; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date createdDate; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String drftTitle; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String drftContent; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String drftStatus; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 1, message = ValidationCode.Size)
    private Boolean recentYn; 
    
    @Size(max = 19, message = ValidationCode.Size)
    private Long formId; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docUserUid; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date docDate; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docStatus; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docDeptCode; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String docDeptName; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String tagDoc; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Integer secGrade; 
    
    private String drftType; 
    
    private String userId; 
    
    private Boolean foreignOrigin;
    
    private Long idMeeting; 
    
    private Date fromDate; 
    
    private Date toDate; 
    
    private String place; 
    
    private String meetingTitle; 
    
    private String position; 
    
    private Date fromDateBus; 
    
    private Date toDateBus; 
    
    private String placeBus; 
    
    private String purpose; 
    
    private Double transportationCost; 
    
    private Double workingMoney; 
    
    private Double foodMoney; 
    
    private Double roomCharge; 
    
    private Double etc; 

    private String meetingDate;
    
    private String attached;
    
    private String sendTo;
    
    private String nameKr;
    
    private String sancType;
    
    private String finalFileName;
    
    private Date dateWrite; 
    private String writer; 
    private String purposeMeeting; 
    private String specialNote; 
    
    private Date submittedDate;
    
    private String typeOfBusinessTrip; 

    private String country; 
    
    private String destination;
	
	private Date term;
	
	private String visitingAgency;

    @Size(max = 100, message = ValidationCode.Size)
    private String traveler; 
    
    private Double oneWay; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String twoWay; 
    
    private Double toll; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String busTripDomestic; 
    
    private String purposeBus; 
    
    private Long idBus;
    
    private Long countAttach;
    
    private Long idReferrer;
    
	private Long vacationId;

    public TndmDrftResponse() {
       
	}

    public Long getVacationId() {
		return vacationId;
	}

	public void setVacationId(Long vacationId) {
		this.vacationId = vacationId;
	}

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : DOC_NO (VARCHAR)
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocNo() {
        return this.docNo;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : DRFT_TITLE (VARCHAR)
    public void setDrftTitle(String drftTitle) {
        this.drftTitle = drftTitle;
    }

    public String getDrftTitle() {
        return this.drftTitle;
    }

    //--- DATABASE MAPPING : DRFT_CONTENT (LONGTEXT)
    public void setDrftContent(String drftContent) {
        this.drftContent = drftContent;
    }

    public String getDrftContent() {
        return this.drftContent;
    }

    //--- DATABASE MAPPING : DRFT_STATUS (VARCHAR)
    public void setDrftStatus(String drftStatus) {
        this.drftStatus = drftStatus;
    }

    public String getDrftStatus() {
        return this.drftStatus;
    }

    //--- DATABASE MAPPING : RECENT_YN (BIT)
    public void setRecentYn(Boolean recentYn) {
        this.recentYn = recentYn;
    }

    public Boolean getRecentYn() {
        return this.recentYn;
    }

    //--- DATABASE MAPPING : FORM_ID (BIGINT)
    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getFormId() {
        return this.formId;
    }

  //--- DATABASE MAPPING : DRFT_TYPE (VARCHAR)
    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getDrftType() {
        return this.drftType;
    }


	public String getDocUserUid() {
		return docUserUid;
	}


	public void setDocUserUid(String docUserUid) {
		this.docUserUid = docUserUid;
	}


	public Date getDocDate() {
		return docDate;
	}


	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}


	public String getDocStatus() {
		return docStatus;
	}


	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}


	public String getDocDeptCode() {
		return docDeptCode;
	}


	public void setDocDeptCode(String docDeptCode) {
		this.docDeptCode = docDeptCode;
	}


	public String getDocDeptName() {
		return docDeptName;
	}


	public void setDocDeptName(String docDeptName) {
		this.docDeptName = docDeptName;
	}


	public String getTagDoc() {
		return tagDoc;
	}


	public void setTagDoc(String tagDoc) {
		this.tagDoc = tagDoc;
	}


	public Integer getSecGrade() {
		return secGrade;
	}


	public void setSecGrade(Integer secGrade) {
		this.secGrade = secGrade;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Boolean getForeignOrigin() {
		return foreignOrigin;
	}


	public void setForeignOrigin(Boolean foreignOrigin) {
		this.foreignOrigin = foreignOrigin;
	}

	

	public Long getIdMeeting() {
		return idMeeting;
	}


	public void setIdMeeting(Long idMeeting) {
		this.idMeeting = idMeeting;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public String getPlace() {
		return place;
	}


	public void setPlace(String place) {
		this.place = place;
	}


	public String getMeetingTitle() {
		return meetingTitle;
	}


	public void setMeetingTitle(String meetingTitle) {
		this.meetingTitle = meetingTitle;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public Date getFromDateBus() {
		return fromDateBus;
	}


	public void setFromDateBus(Date fromDateBus) {
		this.fromDateBus = fromDateBus;
	}


	public Date getToDateBus() {
		return toDateBus;
	}


	public void setToDateBus(Date toDateBus) {
		this.toDateBus = toDateBus;
	}


	public String getPlaceBus() {
		return placeBus;
	}


	public void setPlaceBus(String placeBus) {
		this.placeBus = placeBus;
	}


	public String getPurpose() {
		return purpose;
	}


	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}


	public Double getTransportationCost() {
		return transportationCost;
	}


	public void setTransportationCost(Double transportationCost) {
		this.transportationCost = transportationCost;
	}


	public Double getWorkingMoney() {
		return workingMoney;
	}


	public void setWorkingMoney(Double workingMoney) {
		this.workingMoney = workingMoney;
	}


	public Double getFoodMoney() {
		return foodMoney;
	}


	public void setFoodMoney(Double foodMoney) {
		this.foodMoney = foodMoney;
	}


	public Double getRoomCharge() {
		return roomCharge;
	}


	public void setRoomCharge(Double roomCharge) {
		this.roomCharge = roomCharge;
	}


	public Double getEtc() {
		return etc;
	}


	public void setEtc(Double etc) {
		this.etc = etc;
	}


	public String getMeetingDate() {
		return meetingDate;
	}


	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}


	public String getAttached() {
		return attached;
	}


	public void setAttached(String attached) {
		this.attached = attached;
	}


	public String getSendTo() {
		return sendTo;
	}


	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}


	public String getSancType() {
		return sancType;
	}


	public void setSancType(String sancType) {
		this.sancType = sancType;
	}


	public String getFinalFileName() {
		return finalFileName;
	}


	public void setFinalFileName(String finalFileName) {
		this.finalFileName = finalFileName;
	}


	public Date getDateWrite() {
		return dateWrite;
	}


	public void setDateWrite(Date dateWrite) {
		this.dateWrite = dateWrite;
	}


	public String getWriter() {
		return writer;
	}


	public void setWriter(String writer) {
		this.writer = writer;
	}



	public String getPurposeMeeting() {
		return purposeMeeting;
	}


	public void setPurposeMeeting(String purposeMeeting) {
		this.purposeMeeting = purposeMeeting;
	}


	public String getSpecialNote() {
		return specialNote;
	}


	public void setSpecialNote(String specialNote) {
		this.specialNote = specialNote;
	}


	public Date getSubmittedDate() {
		return submittedDate;
	}


	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}


	public String getTypeOfBusinessTrip() {
		return typeOfBusinessTrip;
	}


	public void setTypeOfBusinessTrip(String typeOfBusinessTrip) {
		this.typeOfBusinessTrip = typeOfBusinessTrip;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getTraveler() {
		return traveler;
	}


	public void setTraveler(String traveler) {
		this.traveler = traveler;
	}


	public Double getOneWay() {
		return oneWay;
	}


	public void setOneWay(Double oneWay) {
		this.oneWay = oneWay;
	}


	public String getTwoWay() {
		return twoWay;
	}


	public void setTwoWay(String twoWay) {
		this.twoWay = twoWay;
	}


	public Double getToll() {
		return toll;
	}


	public void setToll(Double toll) {
		this.toll = toll;
	}


	public String getBusTripDomestic() {
		return busTripDomestic;
	}


	public void setBusTripDomestic(String busTripDomestic) {
		this.busTripDomestic = busTripDomestic;
	}


	public String getPurposeBus() {
		return purposeBus;
	}


	public void setPurposeBus(String purposeBus) {
		this.purposeBus = purposeBus;
	}


	public Long getIdBus() {
		return idBus;
	}


	public void setIdBus(Long idBus) {
		this.idBus = idBus;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public Date getTerm() {
		return term;
	}


	public void setTerm(Date term) {
		this.term = term;
	}


	public String getVisitingAgency() {
		return visitingAgency;
	}


	public void setVisitingAgency(String visitingAgency) {
		this.visitingAgency = visitingAgency;
	}


	public Long getCountAttach() {
		return countAttach;
	}


	public void setCountAttach(Long countAttach) {
		this.countAttach = countAttach;
	}


	public Long getIdReferrer() {
		return idReferrer;
	}


	public void setIdReferrer(Long idReferrer) {
		this.idReferrer = idReferrer;
	}
	
	
    
}