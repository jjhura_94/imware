/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TsstMenuResponse implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private String menuId; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String menuNm; 
    
    @Size(max = 255, message = ValidationCode.Size)
    private String menuNmEn; 
    
    @Size(max = 255, message = ValidationCode.Size)
    private String menuNmVi; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Integer lev; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String upMenuId; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String useYn; 
    
    @Size(max = 255, message = ValidationCode.Size)
    private String url; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Integer ordNo; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String menuType;
    
    @Size(max = 255, message = ValidationCode.Size)
    private String description; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
    
    @Size(max = 19, message = ValidationCode.Size)
    private Date createdDate; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String updatedBy; 
    
    @Size(max = 19, message = ValidationCode.Size)
    private Date updatedDate; 

    public TsstMenuResponse() {
       
	}


    //--- DATABASE MAPPING : MENU_ID (VARCHAR)
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuId() {
        return this.menuId;
    }

    //--- DATABASE MAPPING : MENU_NM (VARCHAR)
    public void setMenuNm(String menuNm) {
        this.menuNm = menuNm;
    }

    public String getMenuNm() {
        return this.menuNm;
    }

    //--- DATABASE MAPPING : MENU_NM_EN (VARCHAR)
    public void setMenuNmEn(String menuNmEn) {
        this.menuNmEn = menuNmEn;
    }

    public String getMenuNmEn() {
        return this.menuNmEn;
    }

    //--- DATABASE MAPPING : MENU_NM_VI (VARCHAR)
    public void setMenuNmVi(String menuNmVi) {
        this.menuNmVi = menuNmVi;
    }

    public String getMenuNmVi() {
        return this.menuNmVi;
    }

    //--- DATABASE MAPPING : LEV (INT)
    public void setLev(Integer lev) {
        this.lev = lev;
    }

    public Integer getLev() {
        return this.lev;
    }

    //--- DATABASE MAPPING : UP_MENU_ID (VARCHAR)
    public void setUpMenuId(String upMenuId) {
        this.upMenuId = upMenuId;
    }

    public String getUpMenuId() {
        return this.upMenuId;
    }

    //--- DATABASE MAPPING : USE_YN (VARCHAR)
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getUseYn() {
        return this.useYn;
    }

    //--- DATABASE MAPPING : URL (VARCHAR)
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    //--- DATABASE MAPPING : ORD_NO (INT)
    public void setOrdNo(Integer ordNo) {
        this.ordNo = ordNo;
    }

    public Integer getOrdNo() {
        return this.ordNo;
    }

    //--- DATABASE MAPPING : MENU_TYPE (VARCHAR)
    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getMenuType() {
        return this.menuType;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }


}