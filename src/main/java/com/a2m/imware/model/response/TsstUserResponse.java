/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;
import personal.aug.convert.annotations.MapKey;

public class TsstUserResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("USER_UID")
    private String userUid;
    private String userId;
    private String pwd;
    private Long empNo;
    private String userType;
    private String useYn;
    private Boolean isFirstLogin;
    private String resetPwdToken;
    private Date tokenExpired;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    public TsstUserResponse() {

    }


    //--- DATABASE MAPPING : USER_UID (VARCHAR)
    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserUid() {
        return this.userUid;
    }

    //--- DATABASE MAPPING : USER_ID (VARCHAR)
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    //--- DATABASE MAPPING : PWD (VARCHAR)
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPwd() {
        return this.pwd;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : USER_TYPE (VARCHAR)
    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return this.userType;
    }

    //--- DATABASE MAPPING : USE_YN (VARCHAR)
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getUseYn() {
        return this.useYn;
    }

    //--- DATABASE MAPPING : IS_FIRST_LOGIN (BIT)
    public void setIsFirstLogin(Boolean isFirstLogin) {
        this.isFirstLogin = isFirstLogin;
    }

    public Boolean getIsFirstLogin() {
        return this.isFirstLogin;
    }

    //--- DATABASE MAPPING : RESET_PWD_TOKEN (VARCHAR)
    public void setResetPwdToken(String resetPwdToken) {
        this.resetPwdToken = resetPwdToken;
    }

    public String getResetPwdToken() {
        return this.resetPwdToken;
    }

    //--- DATABASE MAPPING : TOKEN_EXPIRED (TIMESTAMP)
    public void setTokenExpired(Date tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public Date getTokenExpired() {
        return this.tokenExpired;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }


}