/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.a2m.imware.validation.ValidationCode;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsRoomMstResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;

	@MapKey("ROOM_NAME")
	@NotNull(message = ValidationCode.NotNull)
	private String roomName;

	@MapKey("DESCRIPTION")
	private String description;

	@MapKey("NUM_OF_SEAT")
	private Integer numOfSeat;

	@MapKey("CREATED_BY")
	private String createdBy;

	@MapKey("CREATED_DATE")
	private Date createdDate;

	@MapKey("CREATED_DATE_STRING")
	private String createDateString;

	@MapKey("UPDATED_BY")
	private String updatedBy;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	private String sessUserId;

	public TcdsRoomMstResponse() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNumOfSeat() {
		return numOfSeat;
	}

	public void setNumOfSeat(Integer numOfSeat) {
		this.numOfSeat = numOfSeat;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreateDateString() {
		return createDateString;
	}

	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDateString() {
		return updatedDateString;
	}

	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}

	public String getSessUserId() {
		return sessUserId;
	}

	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}

}