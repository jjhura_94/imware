/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

public class TcdsEmpCertResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long certId;
    private Long empNo;
    private String qualification;
    private String publisher;
    private Date acquisitionDate;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    private String fleSeq;

    public TcdsEmpCertResponse() {

    }

    public String getFleSeq() {
        return fleSeq;
    }

    public void setFleSeq(String fleSeq) {
        this.fleSeq = fleSeq;
    }

    //--- DATABASE MAPPING : CERT_ID (BIGINT)
    public void setCertId(Long certId) {
        this.certId = certId;
    }

    public Long getCertId() {
        return this.certId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : QUALIFICATION (VARCHAR)
    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getQualification() {
        return this.qualification;
    }

    //--- DATABASE MAPPING : PUBLISHER (VARCHAR)
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisher() {
        return this.publisher;
    }

    //--- DATABASE MAPPING : ACQUISITION_DATE (TIMESTAMP)
    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public Date getAcquisitionDate() {
        return this.acquisitionDate;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
