/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsEmpVacationApprovalResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID_RQST")
	private Long idRqst;

	@MapKey("USER_UID_RQST")
	private String userUidRqst;

	@MapKey("EMP_NO_RQST")
	private Long empNoRqst;

	@MapKey("DEPT_NAME")
	private String deptName;

	@MapKey("NAME_KR")
	private String nameKr;

	@MapKey("VACATION_TYPE_RQST")
	private String vacationTypeRqst;

	@MapKey("VACATION_TYPE_STRING_RQST")
	private String vacationTypeStringRqst;

	@MapKey("FROM_DATE_RQST")
	private Date fromDateRqst;

	@MapKey("FROM_DATE_RQST_STRING")
	private String fromDateRqstString;

	@MapKey("TIME_LEAVE_RQST")
	private Integer timeLeaveRqst;

	@MapKey("TO_DATE_RQST")
	private Date toDateRqst;

	@MapKey("TO_DATE_RQST_STRING")
	private String toDateRqstString;

	@MapKey("REASON_RQST")
	private String reasonRqst;

	@MapKey("DESTINATION_RQST")
	private String destinationRqst;

	@MapKey("CONTACT_NETWORK_RQST")
	private String contactNetworkRqst;

	@MapKey("REMARK_RQST")
	private String remarkRqst;

	@MapKey("STATUS_RQST")
	private String statusRqst;

	@MapKey("STATUS_STRING_RQST")
	private String statusStringRqst;

	@MapKey("ID_APVL")
	private Long idApvl;

	@MapKey("USER_UID_APVL")
	private String userUidApvl;

	@MapKey("STATUS_APVL")
	private String statusApvl;

	@MapKey("STATUS_STRING_APVL")
	private String statusStringApvl;

	@MapKey("NOTE_APVL")
	private String noteApvl;

	@MapKey("CHECK_APPROVAL_AND_REJECT")
	private Boolean checkApprovalAndReject;

	public TcdsEmpVacationApprovalResponse() {

	}

	public Long getIdRqst() {
		return idRqst;
	}

	public void setIdRqst(Long idRqst) {
		this.idRqst = idRqst;
	}

	public Long getEmpNoRqst() {
		return empNoRqst;
	}

	public void setEmpNoRqst(Long empNoRqst) {
		this.empNoRqst = empNoRqst;
	}

	public String getVacationTypeRqst() {
		return vacationTypeRqst;
	}

	public void setVacationTypeRqst(String vacationTypeRqst) {
		this.vacationTypeRqst = vacationTypeRqst;
	}

	public Date getFromDateRqst() {
		return fromDateRqst;
	}

	public void setFromDateRqst(Date fromDateRqst) {
		this.fromDateRqst = fromDateRqst;
	}

	public Integer getTimeLeaveRqst() {
		return timeLeaveRqst;
	}

	public void setTimeLeaveRqst(Integer timeLeaveRqst) {
		this.timeLeaveRqst = timeLeaveRqst;
	}

	public Date getToDateRqst() {
		return toDateRqst;
	}

	public void setToDateRqst(Date toDateRqst) {
		this.toDateRqst = toDateRqst;
	}

	public String getReasonRqst() {
		return reasonRqst;
	}

	public void setReasonRqst(String reasonRqst) {
		this.reasonRqst = reasonRqst;
	}

	public String getDestinationRqst() {
		return destinationRqst;
	}

	public void setDestinationRqst(String destinationRqst) {
		this.destinationRqst = destinationRqst;
	}

	public String getStatusRqst() {
		return statusRqst;
	}

	public void setStatusRqst(String statusRqst) {
		this.statusRqst = statusRqst;
	}

	public Long getIdApvl() {
		return idApvl;
	}

	public void setIdApvl(Long idApvl) {
		this.idApvl = idApvl;
	}

	public String getUserUidApvl() {
		return userUidApvl;
	}

	public void setUserUidApvl(String userUidApvl) {
		this.userUidApvl = userUidApvl;
	}

	public String getStatusApvl() {
		return statusApvl;
	}

	public void setStatusApvl(String statusApvl) {
		this.statusApvl = statusApvl;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getVacationTypeStringRqst() {
		return vacationTypeStringRqst;
	}

	public void setVacationTypeStringRqst(String vacationTypeStringRqst) {
		this.vacationTypeStringRqst = vacationTypeStringRqst;
	}

	public String getStatusStringRqst() {
		return statusStringRqst;
	}

	public void setStatusStringRqst(String statusStringRqst) {
		this.statusStringRqst = statusStringRqst;
	}

	public String getStatusStringApvl() {
		return statusStringApvl;
	}

	public void setStatusStringApvl(String statusStringApvl) {
		this.statusStringApvl = statusStringApvl;
	}

	public String getNoteApvl() {
		return noteApvl;
	}

	public void setNoteApvl(String noteApvl) {
		this.noteApvl = noteApvl;
	}

	public Boolean getCheckApprovalAndReject() {
		return checkApprovalAndReject;
	}

	public void setCheckApprovalAndReject(Boolean checkApprovalAndReject) {
		this.checkApprovalAndReject = checkApprovalAndReject;
	}

	public String getUserUidRqst() {
		return userUidRqst;
	}

	public void setUserUidRqst(String userUidRqst) {
		this.userUidRqst = userUidRqst;
	}

	public String getContactNetworkRqst() {
		return contactNetworkRqst;
	}

	public void setContactNetworkRqst(String contactNetworkRqst) {
		this.contactNetworkRqst = contactNetworkRqst;
	}

	public String getRemarkRqst() {
		return remarkRqst;
	}

	public void setRemarkRqst(String remarkRqst) {
		this.remarkRqst = remarkRqst;
	}

	public String getFromDateRqstString() {
		return fromDateRqstString;
	}

	public void setFromDateRqstString(String fromDateRqstString) {
		this.fromDateRqstString = fromDateRqstString;
	}

	public String getToDateRqstString() {
		return toDateRqstString;
	}

	public void setToDateRqstString(String toDateRqstString) {
		this.toDateRqstString = toDateRqstString;
	}

}