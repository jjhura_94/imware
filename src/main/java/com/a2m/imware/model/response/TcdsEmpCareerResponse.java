/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

public class TcdsEmpCareerResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long careerId;
    private Long empNo;
    private String businessName;
    private Date startDate;
    private Date endDate;
    private String responsibilities;
    private String client;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    private String positionTitle;

    public TcdsEmpCareerResponse() {

    }

    public String getPositionTitle() {
        return positionTitle;
    }

    public void setPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
    }

    //--- DATABASE MAPPING : CAREER_ID (BIGINT)
    public void setCareerId(Long careerId) {
        this.careerId = careerId;
    }

    public Long getCareerId() {
        return this.careerId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : BUSINESS_NAME (VARCHAR)
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessName() {
        return this.businessName;
    }

    //--- DATABASE MAPPING : START_DATE (TIMESTAMP)
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    //--- DATABASE MAPPING : END_DATE (TIMESTAMP)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    //--- DATABASE MAPPING : RESPONSIBILITIES (VARCHAR)
    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getResponsibilities() {
        return this.responsibilities;
    }

    //--- DATABASE MAPPING : CLIENT (VARCHAR)
    public void setClient(String client) {
        this.client = client;
    }

    public String getClient() {
        return this.client;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
