/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

public class TcdsEmpMstResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long empNo;

    private String nameKr;

    private String nameEn;

    private String nameCh;

    private String dutyCode;

    private Date startDate;

    private Date endDate;

    private Date dob;

    private String email;

    private String cellPhone;

    private String extNumber;

    private String zipCode;

    private String address;

    private String address2;

    private String gender;

    private String imgPath;

    private String signaturePath;

    private String seNumber;

    private String status;

    private String deptCode;

    private String userId;

    private String userUid;

    private String deptNm;

    private String deptName;

    private String dutyName;

    private String absentYn;
    private String absentUserId;
    private Date absFromDate;
    private Date absToDate;

    //Work day num
    private Integer workExperience;

    public TcdsEmpMstResponse() {

    }

    public String getAbsentUserId() {
        return absentUserId;
    }

    public void setAbsentUserId(String absentUserId) {
        this.absentUserId = absentUserId;
    }

    public Date getAbsFromDate() {
        return absFromDate;
    }

    public void setAbsFromDate(Date absFromDate) {
        this.absFromDate = absFromDate;
    }

    public Date getAbsToDate() {
        return absToDate;
    }

    public void setAbsToDate(Date absToDate) {
        this.absToDate = absToDate;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getAbsentYn() {
        return absentYn;
    }

    public void setAbsentYn(String absentYn) {
        this.absentYn = absentYn;
    }

    public Long getEmpNo() {
        return empNo;
    }

    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameCh() {
        return nameCh;
    }

    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

    public String getDutyCode() {
        return dutyCode;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getExtNumber() {
        return extNumber;
    }

    public void setExtNumber(String extNumber) {
        this.extNumber = extNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getSignaturePath() {
        return signaturePath;
    }

    public void setSignaturePath(String signaturePath) {
        this.signaturePath = signaturePath;
    }

    public String getSeNumber() {
        return seNumber;
    }

    public void setSeNumber(String seNumber) {
        this.seNumber = seNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserUId() {
        return userUid;
    }

    public void setUserUId(String userUid) {
        this.userUid = userUid;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public Integer getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(Integer workExperience) {
        this.workExperience = workExperience;
    }

}
