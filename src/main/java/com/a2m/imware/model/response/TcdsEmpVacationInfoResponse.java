/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsEmpVacationInfoResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;

	@MapKey("EMP_NO")
	private Long empNo;

	@MapKey("NAME_KR")
	private String nameKr;

	@MapKey("BASE_YEAR")
	private Integer baseYear;

	@MapKey("YEAR_OF_ENTRY")
	private Integer yearOfEntry;

	@MapKey("ANNUAL_DAYS")
	private Integer annualDays;

	@MapKey("AWARDED_DAYS")
	private Integer awardedDays;

	@MapKey("USED_DAYS")
	private Integer usedDays;

	@MapKey("START_DATE")
	private Date startDate;

	public TcdsEmpVacationInfoResponse() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Long empNo) {
		this.empNo = empNo;
	}

	public Integer getBaseYear() {
		return baseYear;
	}

	public void setBaseYear(Integer baseYear) {
		this.baseYear = baseYear;
	}

	public Integer getYearOfEntry() {
		return yearOfEntry;
	}

	public void setYearOfEntry(Integer yearOfEntry) {
		this.yearOfEntry = yearOfEntry;
	}

	public Integer getAnnualDays() {
		return annualDays;
	}

	public void setAnnualDays(Integer annualDays) {
		this.annualDays = annualDays;
	}

	public Integer getAwardedDays() {
		return awardedDays;
	}

	public void setAwardedDays(Integer awardedDays) {
		this.awardedDays = awardedDays;
	}

	public Integer getUsedDays() {
		return usedDays;
	}

	public void setUsedDays(Integer usedDays) {
		this.usedDays = usedDays;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}