/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.response;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.a2m.imware.validation.ValidationCode;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

public class TcdsEmpVacationApvlResponse extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("VACATION_RQST_ID")
	private Long vacationRqstId;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("USER_UID")
	private String userUid;

	@NotNull(message = ValidationCode.NotNull)
	@MapKey("STATUS")
	private String status;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	@MapKey("NOTE")
	private String note;

	public TcdsEmpVacationApvlResponse() {

	}

	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : VACATION_RQST_ID (BIGINT)
	public void setVacationRqstId(Long vacationRqstId) {
		this.vacationRqstId = vacationRqstId;
	}

	public Long getVacationRqstId() {
		return this.vacationRqstId;
	}

	// --- DATABASE MAPPING : USER_UID (VARCHAR)
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getUserUid() {
		return this.userUid;
	}

	// --- DATABASE MAPPING : STATUS (VARCHAR)
	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	// --- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	// --- DATABASE MAPPING : NOTE (VARCHAR)
	public void setNote(String note) {
		this.note = note;
	}

	public String getNote() {
		return this.note;
	}

	public String getUpdatedDateString() {
		return updatedDateString;
	}

	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}

}