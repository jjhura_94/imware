package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.a2m.imware.util.Pager;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * Map with TNDM_NOTICE table.
 *
 * @author HungDM
 *
 */
public class TndmNotice extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ID")
    private Long id;

    @MapKey("NOTICE_TITLE")
    private String noticeTitle;

    @MapKey("CONTENT")
    private String content;

    @MapKey("POPUP_YN")
    private String popupYn;

    @MapKey("POPUP_FROM")
    private Date popupFrom;

    @MapKey("POPUP_TO")
    private Date popupTo;

    @MapKey("OUT_YN")
    private String outYn;

    @MapKey("DEPT_CODE")
    private String deptCode;

    @MapKey("DEPT_NAME")
    private String deptName;

    @MapKey("IS_DELETED")
    private Boolean isDeleted;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("UPDATED_BY")
    private String upldatedBy;

    @MapKey("UPDATED_DATE")
    private Date updatedDate;

    @MapKey("IS_EDITED")
    private Boolean isEdited;

    @MapKey("VIEW_COUNT")
    private Integer viewCount;

    @MapKey("POSITION")
    private String position;

    private Pager pager;

    private List<TccoFile> attachment;

    private List<TccoFile> filesToAdd;

    private List<TccoFile> filesToDelete;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("NAME_EN")
    private String nameEn;

    @MapKey("NAME_CH")
    private String nameCh;
    @MapKey("CREATE_TIME_FROM")
    private String createTimeFrom;
    @MapKey("CREATE_TIME_TO")
    private String createTimeTo;
    @MapKey("TIME_TYPE")
    private int timeType;

    @MapKey("USER_IDS")
    private String userIds;

    public TndmNotice() {
        super();
        this.popupYn = "N";
        this.outYn = "N";
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getTimeType() {
        return timeType;
    }

    public void setTimeType(int timeType) {
        this.timeType = timeType;
    }

    public String getCreateTimeFrom() {
        return createTimeFrom;
    }

    public void setCreateTimeFrom(String createTimeFrom) {
        this.createTimeFrom = createTimeFrom;
    }

    public String getCreateTimeTo() {
        return createTimeTo;
    }

    public void setCreateTimeTo(String createTimeTo) {
        this.createTimeTo = createTimeTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPopupYn() {
        return popupYn;
    }

    public void setPopupYn(String popupYn) {
        this.popupYn = popupYn;
    }

    public Date getPopupFrom() {
        return popupFrom;
    }

    public void setPopupFrom(Date popupFrom) {
        this.popupFrom = popupFrom;
    }

    public Date getPopupTo() {
        return popupTo;
    }

    public void setPopupTo(Date popupTo) {
        this.popupTo = popupTo;
    }

    public String getOutYn() {
        return outYn;
    }

    public void setOutYn(String outYn) {
        this.outYn = outYn;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpldatedBy() {
        return upldatedBy;
    }

    public void setUpldatedBy(String upldatedBy) {
        this.upldatedBy = upldatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<TccoFile> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<TccoFile> attachment) {
        this.attachment = attachment;
    }

    public List<TccoFile> getFilesToAdd() {
        return filesToAdd;
    }

    public void setFilesToAdd(List<TccoFile> filesToAdd) {
        this.filesToAdd = filesToAdd;
    }

    public List<TccoFile> getFilesToDelete() {
        return filesToDelete;
    }

    public void setFilesToDelete(List<TccoFile> filesToDelete) {
        this.filesToDelete = filesToDelete;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameCh() {
        return nameCh;
    }

    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

    @Override
    public String toString() {
        return "TndmNotice [id=" + id + ", noticeTitle=" + noticeTitle + ", content=" + content + ", popupYn=" + popupYn
                + ", popupFrom=" + popupFrom + ", popupTo=" + popupTo + ", outYn=" + outYn + ", deptCode=" + deptCode
                + ", deptName=" + deptName + ", isDeleted=" + isDeleted + ", createdBy=" + createdBy + ", createdDate="
                + createdDate + ", upldatedBy=" + upldatedBy + ", updatedDate=" + updatedDate + ", isEdited=" + isEdited
                + ", viewCount=" + viewCount + ", pager=" + pager + ", attachment=" + attachment + "]";
    }

}
