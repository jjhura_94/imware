/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.a2m.imware.validation.ValidationCode;


public class TsstRoleRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   
    private String roleId; 
    
    @Size(max = 255, message = ValidationCode.Size)
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    private String roleNm; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String useYn; 
    
    @Size(max = 255, message = ValidationCode.Size)
    private String description; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
    
    private Date createdDate;
    
    private String createdDateString;
    
    @Size(max = 20, message = ValidationCode.Size)
    private String updatedBy; 
    
    private Date updatedDate; 
    
    private String updatedDateString;
    
    private String state;
    

    public TsstRoleRequest() {
       
	}

    //--- DATABASE MAPPING : ROLE_ID (VARCHAR)
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return this.roleId;
    }

    //--- DATABASE MAPPING : ROLE_NM (VARCHAR)
    public void setRoleNm(String roleNm) {
        this.roleNm = roleNm;
    }

    public String getRoleNm() {
        return this.roleNm;
    }

    //--- DATABASE MAPPING : USE_YN (VARCHAR)
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getUseYn() {
        return this.useYn;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }


	public String getCreatedDateString() {
		return createdDateString;
	}


	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}


	public String getUpdatedDateString() {
		return updatedDateString;
	}


	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}
  
	
}