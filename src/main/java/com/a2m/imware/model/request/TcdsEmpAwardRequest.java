/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.validation.ValidationCode;

public class TcdsEmpAwardRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long awardId;
    private Long empNo;
    @Size(max = 255, message = ValidationCode.Size)
    private String awardName;
    @Size(max = 255, message = ValidationCode.Size)
    private String awardOrg;
    private Date awardDate;
    @Size(max = 255, message = ValidationCode.Size)
    private String award;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    private String awardDateStr;
    private long awardDateLong;

    private String fileSeq;

    public TcdsEmpAwardRequest() {

    }

    public String getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(String fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getAwardDateStr() {
        return awardDateStr;
    }

    public void setAwardDateStr(String awardDateStr) {
        this.awardDateStr = awardDateStr;
    }

    public long getAwardDateLong() {
        return awardDateLong;
    }

    public void setAwardDateLong(long awardDateLong) {
        this.awardDateLong = awardDateLong;
    }

    //--- DATABASE MAPPING : AWARD_ID (BIGINT)
    public void setAwardId(Long awardId) {
        this.awardId = awardId;
    }

    public Long getAwardId() {
        return this.awardId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : AWARD_NAME (VARCHAR)
    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public String getAwardName() {
        return this.awardName;
    }

    //--- DATABASE MAPPING : AWARD_ORG (VARCHAR)
    public void setAwardOrg(String awardOrg) {
        this.awardOrg = awardOrg;
    }

    public String getAwardOrg() {
        return this.awardOrg;
    }

    //--- DATABASE MAPPING : AWARD_DATE (TIMESTAMP)
    public void setAwardDate(Date awardDate) {
        this.awardDate = awardDate;
    }

    public Date getAwardDate() {
        return this.awardDate;
    }

    //--- DATABASE MAPPING : AWARD (VARCHAR)
    public void setAward(String award) {
        this.award = award;
    }

    public String getAward() {
        return this.award;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
