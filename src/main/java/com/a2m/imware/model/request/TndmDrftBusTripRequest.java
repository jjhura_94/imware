/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDrftBusTripRequest implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long idBus; 
    @Size(max = 20, message = ValidationCode.Size)
    private String drftDocNo; 
    @Size(max = 255, message = ValidationCode.Size)
    private String position; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date fromDateBus; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date toDateBus; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String placeBus; 
    @Size(max = 255, message = ValidationCode.Size)
    private String purpose; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double transportationCost; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double workingMoney; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double foodMoney; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double roomCharge; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double etc; 

    public TndmDrftBusTripRequest() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public Long getIdBus() {
		return idBus;
	}


	public void setIdBus(Long idBus) {
		this.idBus = idBus;
	}

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }
    

	public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : POSITION (VARCHAR)
    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return this.position;
    }

   

    public Date getFromDateBus() {
		return fromDateBus;
	}


	public void setFromDateBus(Date fromDateBus) {
		this.fromDateBus = fromDateBus;
	}


	public Date getToDateBus() {
		return toDateBus;
	}


	public void setToDateBus(Date toDateBus) {
		this.toDateBus = toDateBus;
	}
    
    //--- DATABASE MAPPING : PURPOSE (VARCHAR)
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPlaceBus() {
		return placeBus;
	}


	public void setPlaceBus(String placeBus) {
		this.placeBus = placeBus;
	}


	public String getPurpose() {
        return this.purpose;
    }

    //--- DATABASE MAPPING : TRANSPORTATION_COST (DOUBLE)
    public void setTransportationCost(Double transportationCost) {
        this.transportationCost = transportationCost;
    }

    public Double getTransportationCost() {
        return this.transportationCost;
    }

    //--- DATABASE MAPPING : WORKING_MONEY (DOUBLE)
    public void setWorkingMoney(Double workingMoney) {
        this.workingMoney = workingMoney;
    }

    public Double getWorkingMoney() {
        return this.workingMoney;
    }

    //--- DATABASE MAPPING : FOOD_MONEY (DOUBLE)
    public void setFoodMoney(Double foodMoney) {
        this.foodMoney = foodMoney;
    }

    public Double getFoodMoney() {
        return this.foodMoney;
    }

    //--- DATABASE MAPPING : ROOM_CHARGE (DOUBLE)
    public void setRoomCharge(Double roomCharge) {
        this.roomCharge = roomCharge;
    }

    public Double getRoomCharge() {
        return this.roomCharge;
    }

    //--- DATABASE MAPPING : ETC (DOUBLE)
    public void setEtc(Double etc) {
        this.etc = etc;
    }

    public Double getEtc() {
        return this.etc;
    }


}