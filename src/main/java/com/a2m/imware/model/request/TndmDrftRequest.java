/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDrftRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String drftDocNo; 
    
    private String docNo; 
    
    private String createdBy; 
       
    private Date createdDate; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String drftTitle; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String drftContent; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String drftStatus; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 1, message = ValidationCode.Size)
    private Boolean recentYn; 
    
    @Size(max = 19, message = ValidationCode.Size)
    private Long formId; 
    
    private String drftType; 
    
    private Boolean foreignOrgin; 
    
    private String sendTo;

    public TndmDrftRequest() {
       
	}

    public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	//--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : DOC_NO (VARCHAR)
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocNo() {
        return this.docNo;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : DRFT_TITLE (VARCHAR)
    public void setDrftTitle(String drftTitle) {
        this.drftTitle = drftTitle;
    }

    public String getDrftTitle() {
        return this.drftTitle;
    }

    //--- DATABASE MAPPING : DRFT_CONTENT (LONGTEXT)
    public void setDrftContent(String drftContent) {
        this.drftContent = drftContent;
    }

    public String getDrftContent() {
        return this.drftContent;
    }

    //--- DATABASE MAPPING : DRFT_STATUS (VARCHAR)
    public void setDrftStatus(String drftStatus) {
        this.drftStatus = drftStatus;
    }

    public String getDrftStatus() {
        return this.drftStatus;
    }

    //--- DATABASE MAPPING : RECENT_YN (BIT)
    public void setRecentYn(Boolean recentYn) {
        this.recentYn = recentYn;
    }

    public Boolean getRecentYn() {
        return this.recentYn;
    }

    //--- DATABASE MAPPING : FORM_ID (BIGINT)
    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getFormId() {
        return this.formId;
    }

    //--- DATABASE MAPPING : DRFT_TYPE (VARCHAR)
    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getDrftType() {
        return this.drftType;
    }


	public Boolean getForeignOrgin() {
		return foreignOrgin;
	}


	public void setForeignOrgin(Boolean foreignOrgin) {
		this.foreignOrgin = foreignOrgin;
	}
    
    

}