/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.validation.ValidationCode;
import com.fasterxml.jackson.annotation.JsonFormat;

public class TcdsEmpEduRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long eduId;
    private Long empNo;
    @Size(max = 20, message = ValidationCode.Size)
    private String eduLevel;
    @JsonFormat(pattern = "yyyy-MM")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM")
    private Date endDate;
    @Size(max = 255, message = ValidationCode.Size)
    private String schoolName;
    @Size(max = 100, message = ValidationCode.Size)
    private String majorName;
    @Size(max = 20, message = ValidationCode.Size)
    private String graduationStatus;
    @Size(max = 20, message = ValidationCode.Size)
    private String degree;
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy;
    private Date createdDate;
    @Size(max = 20, message = ValidationCode.Size)
    private String updatedBy;
    private Date updatedDate;

    private long startDateLong;
    private long endDateLong;

    private String startDateStr;
    private String endDateStr;

    public TcdsEmpEduRequest() {

    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }

    public long getStartDateLong() {
        return startDateLong;
    }

    public void setStartDateLong(long startDateLong) {
        this.startDateLong = startDateLong;
    }

    public long getEndDateLong() {
        return endDateLong;
    }

    public void setEndDateLong(long endDateLong) {
        this.endDateLong = endDateLong;
    }

    //--- DATABASE MAPPING : EDU_ID (BIGINT)
    public void setEduId(Long eduId) {
        this.eduId = eduId;
    }

    public Long getEduId() {
        return this.eduId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : EDU_LEVEL (VARCHAR)
    public void setEduLevel(String eduLevel) {
        this.eduLevel = eduLevel;
    }

    public String getEduLevel() {
        return this.eduLevel;
    }

    //--- DATABASE MAPPING : START_DATE (DATETIME)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    //--- DATABASE MAPPING : END_DATE (DATETIME)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    //--- DATABASE MAPPING : SCHOOL_NAME (VARCHAR)
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    //--- DATABASE MAPPING : MAJOR_NAME (VARCHAR)
    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getMajorName() {
        return this.majorName;
    }

    //--- DATABASE MAPPING : GRADUATION_STATUS (VARCHAR)
    public void setGraduationStatus(String graduationStatus) {
        this.graduationStatus = graduationStatus;
    }

    public String getGraduationStatus() {
        return this.graduationStatus;
    }

    //--- DATABASE MAPPING : DEGREE (VARCHAR)
    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDegree() {
        return this.degree;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }
}
