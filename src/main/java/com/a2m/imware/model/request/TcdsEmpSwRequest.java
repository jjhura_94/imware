/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


import com.a2m.imware.validation.ValidationCode;

public class TcdsEmpSwRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long swId;
    private Long empNo;
    @Size(max = 255, message = ValidationCode.Size)
    private String swType;
    @Size(max = 255, message = ValidationCode.Size)
    private String productName;
    private Date startDate;
    private Date expiredDate;
    private Integer useAmout;
    @Size(max = 255, message = ValidationCode.Size)
    private String description;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    private long startDateLong;
    private long expiredDateLong;

    private String startDateStr;
    private String expiredDateStr;

    public TcdsEmpSwRequest() {

    }

    public long getStartDateLong() {
        return startDateLong;
    }

    public void setStartDateLong(long startDateLong) {
        this.startDateLong = startDateLong;
    }

    public long getExpiredDateLong() {
        return expiredDateLong;
    }

    public void setExpiredDateLong(long expiredDateLong) {
        this.expiredDateLong = expiredDateLong;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getExpiredDateStr() {
        return expiredDateStr;
    }

    public void setExpiredDateStr(String expiredDateStr) {
        this.expiredDateStr = expiredDateStr;
    }

    //--- DATABASE MAPPING : SW_ID (BIGINT)
    public void setSwId(Long swId) {
        this.swId = swId;
    }

    public Long getSwId() {
        return this.swId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : SW_TYPE (VARCHAR)
    public void setSwType(String swType) {
        this.swType = swType;
    }

    public String getSwType() {
        return this.swType;
    }

    //--- DATABASE MAPPING : PRODUCT_NAME (VARCHAR)
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return this.productName;
    }

    //--- DATABASE MAPPING : START_DATE (TIMESTAMP)
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    //--- DATABASE MAPPING : EXPIRED_DATE (TIMESTAMP)
    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getExpiredDate() {
        return this.expiredDate;
    }

    //--- DATABASE MAPPING : USE_AMOUT (INT)
    public void setUseAmout(Integer useAmout) {
        this.useAmout = useAmout;
    }

    public Integer getUseAmout() {
        return this.useAmout;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
