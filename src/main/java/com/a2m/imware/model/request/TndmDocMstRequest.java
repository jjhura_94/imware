/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDocMstRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    
    private String docNo; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docUserUid; 
    
    @NotNull(message = ValidationCode.NotNull)
    private Date docDate; 
    
    private String docStatus; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docDeptCode; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String docDeptName; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date createdDate; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String tagDoc; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Integer secGrade; 
    
    private Date submittedDate; 

	private Date finishedDate; 
	
	private String originDocNo;

	private String exportFileSeq;
	
	private Long vacationId;

    public Long getVacationId() {
		return vacationId;
	}

	public void setVacationId(Long vacationId) {
		this.vacationId = vacationId;
	}

	public String getExportFileSeq() {
		return exportFileSeq;
	}

	public void setExportFileSeq(String exportFileSeq) {
		this.exportFileSeq = exportFileSeq;
	}
	
	public String getOriginDocNo() {
		return originDocNo;
	}

	public void setOriginDocNo(String originDocNo) {
		this.originDocNo = originDocNo;
	}

    public Date getSubmittedDate() {
		return submittedDate;
	}


	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}


	public Date getFinishedDate() {
		return finishedDate;
	}


	public void setFinishedDate(Date finishedDate) {
		this.finishedDate = finishedDate;
	}

    //--- DATABASE MAPPING : DOC_NO (VARCHAR)
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocNo() {
        return this.docNo;
    }

    //--- DATABASE MAPPING : DOC_USER_UID (VARCHAR)
    public void setDocUserUid(String docUserUid) {
        this.docUserUid = docUserUid;
    }

    public String getDocUserUid() {
        return this.docUserUid;
    }

    //--- DATABASE MAPPING : DOC_DATE (TIMESTAMP)
    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public Date getDocDate() {
        return this.docDate;
    }

    //--- DATABASE MAPPING : DOC_STATUS (VARCHAR)
    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocStatus() {
        return this.docStatus;
    }

    //--- DATABASE MAPPING : DOC_DEPT_CODE (VARCHAR)
    public void setDocDeptCode(String docDeptCode) {
        this.docDeptCode = docDeptCode;
    }

    public String getDocDeptCode() {
        return this.docDeptCode;
    }

    //--- DATABASE MAPPING : DOC_DEPT_NAME (VARCHAR)
    public void setDocDeptName(String docDeptName) {
        this.docDeptName = docDeptName;
    }

    public String getDocDeptName() {
        return this.docDeptName;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : TAG_DOC (VARCHAR)
    public void setTagDoc(String tagDoc) {
        this.tagDoc = tagDoc;
    }

    public String getTagDoc() {
        return this.tagDoc;
    }

    //--- DATABASE MAPPING : SEC_GRADE (INT)
    public void setSecGrade(Integer secGrade) {
        this.secGrade = secGrade;
    }

    public Integer getSecGrade() {
        return this.secGrade;
    }


}