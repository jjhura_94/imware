package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.List;

public class Sys0105Request implements Serializable {
    private List<TcdsEmpMstRequest> dataForCreate;
    private List<TcdsEmpMstRequest> dataForUpdate;
    private List<TcdsEmpMstRequest> dataForDelete;

    public List<TcdsEmpMstRequest> getDataForCreate() {
        return dataForCreate;
    }

    public void setDataForCreate(List<TcdsEmpMstRequest> dataForCreate) {
        this.dataForCreate = dataForCreate;
    }

    public List<TcdsEmpMstRequest> getDataForUpdate() {
        return dataForUpdate;
    }

    public void setDataForUpdate(List<TcdsEmpMstRequest> dataForUpdate) {
        this.dataForUpdate = dataForUpdate;
    }

    public List<TcdsEmpMstRequest> getDataForDelete() {
        return dataForDelete;
    }

    public void setDataForDelete(List<TcdsEmpMstRequest> dataForDelete) {
        this.dataForDelete = dataForDelete;
    }
}
