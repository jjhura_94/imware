/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;

import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDrftAttachRequest implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id; 
    
    private String drftDocNo; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Integer drftAttachOrdNo; 
    
    @Size(max = 100, message = ValidationCode.Size)
    private String atchFleSeq; 

    public TndmDrftAttachRequest() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : DRFT_ATTACH_ORD_NO (INT)
    public void setDrftAttachOrdNo(Integer drftAttachOrdNo) {
        this.drftAttachOrdNo = drftAttachOrdNo;
    }

    public Integer getDrftAttachOrdNo() {
        return this.drftAttachOrdNo;
    }

    //--- DATABASE MAPPING : ATCH_FLE_SEQ (VARCHAR)
    public void setAtchFleSeq(String atchFleSeq) {
        this.atchFleSeq = atchFleSeq;
    }

    public String getAtchFleSeq() {
        return this.atchFleSeq;
    }


}