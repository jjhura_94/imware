/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;


public class TndmDrftSpendingRequest implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private Long idSpen; 
    
    private String drftDocNo; 
   
    private String spendingType; 
   
    private Date fromDateSpen; 
   
    private Date toDateSpen; 
   
    private String account; 
    
    private String purposeSpen; 
    
    private String bankName; 
    
    private String accountNumber; 
   
    private String accountName; 
    
    private Double cost; 
    
    private Double vat; 

    public TndmDrftSpendingRequest() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public Long getIdSpen() {
		return idSpen;
	}


	public void setIdSpen(Long idSpen) {
		this.idSpen = idSpen;
	}

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }


	public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : SPENDING_TYPE (VARCHAR)
    public void setSpendingType(String spendingType) {
        this.spendingType = spendingType;
    }

    public String getSpendingType() {
        return this.spendingType;
    }

    //--- DATABASE MAPPING : FROM_DATE (DATETIME)
    public Date getFromDateSpen() {
		return fromDateSpen;
	}


	public void setFromDateSpen(Date fromDateSpen) {
		this.fromDateSpen = fromDateSpen;
	}

    //--- DATABASE MAPPING : TO_DATE (DATETIME)
    
	public Date getToDateSpen() {
		return toDateSpen;
	}


	public void setToDateSpen(Date toDateSpen) {
		this.toDateSpen = toDateSpen;
	}

    //--- DATABASE MAPPING : ACCOUNT (VARCHAR)
    public void setAccount(String account) {
        this.account = account;
    }


	public String getAccount() {
        return this.account;
    }

    //--- DATABASE MAPPING : PURPOSE (VARCHAR)
    

    //--- DATABASE MAPPING : BANK_NAME (VARCHAR)
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPurposeSpen() {
		return purposeSpen;
	}


	public void setPurposeSpen(String purposeSpen) {
		this.purposeSpen = purposeSpen;
	}


	public String getBankName() {
        return this.bankName;
    }

    //--- DATABASE MAPPING : ACCOUNT_NUMBER (VARCHAR)
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    //--- DATABASE MAPPING : ACCOUNT_NAME (VARCHAR)
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountName() {
        return this.accountName;
    }

    //--- DATABASE MAPPING : COST (DOUBLE)
    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getCost() {
        return this.cost;
    }

    //--- DATABASE MAPPING : VAT (DOUBLE)
    public void setVat(Double vat) {
        this.vat = vat;
    }

    public Double getVat() {
        return this.vat;
    }


}