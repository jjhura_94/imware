/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.validation.ValidationCode;

public class TsstUserRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userUid;
    @Size(max = 20, message = ValidationCode.Size)
    private String userId;
    @Size(max = 255, message = ValidationCode.Size)
    private String pwd;
    private Long empNo;
    @Size(max = 20, message = ValidationCode.Size)
    private String userType;
    @Size(max = 1, message = ValidationCode.Size)
    private String useYn;
    private Boolean isFirstLogin;
    @Size(max = 50, message = ValidationCode.Size)
    private String resetPwdToken;
    private Date tokenExpired;
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy;
    private Date createdDate;
    @Size(max = 20, message = ValidationCode.Size)
    private String updatedBy;
    private Date updatedDate;

    private String newPwd;
    private String oldPwd;

    private String newPwdUpdate;
    private String oldPwdUpdate;

    private boolean changePwd;
    
//    private TcdsEmpMstRequest tcdsEmpMstRequest; //Use update empMst;
    

    public TsstUserRequest() {

    }

    public boolean isChangePwd() {
        return changePwd;
    }

    public void setChangePwd(boolean changePwd) {
        this.changePwd = changePwd;
    }

    public String getNewPwdUpdate() {
        return newPwdUpdate;
    }

    public void setNewPwdUpdate(String newPwdUpdate) {
        this.newPwdUpdate = newPwdUpdate;
    }

    public String getOldPwdUpdate() {
        return oldPwdUpdate;
    }

    public void setOldPwdUpdate(String oldPwdUpdate) {
        this.oldPwdUpdate = oldPwdUpdate;
    }

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }

    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }

    //--- DATABASE MAPPING : USER_UID (VARCHAR)
    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserUid() {
        return this.userUid;
    }

    //--- DATABASE MAPPING : USER_ID (VARCHAR)
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    //--- DATABASE MAPPING : PWD (VARCHAR)
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPwd() {
        return this.pwd;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : USER_TYPE (VARCHAR)
    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return this.userType;
    }

    //--- DATABASE MAPPING : USE_YN (VARCHAR)
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getUseYn() {
        return this.useYn;
    }

    //--- DATABASE MAPPING : IS_FIRST_LOGIN (BIT)
    public void setIsFirstLogin(Boolean isFirstLogin) {
        this.isFirstLogin = isFirstLogin;
    }

    public Boolean getIsFirstLogin() {
        return this.isFirstLogin;
    }

    //--- DATABASE MAPPING : RESET_PWD_TOKEN (VARCHAR)
    public void setResetPwdToken(String resetPwdToken) {
        this.resetPwdToken = resetPwdToken;
    }

    public String getResetPwdToken() {
        return this.resetPwdToken;
    }

    //--- DATABASE MAPPING : TOKEN_EXPIRED (TIMESTAMP)
    public void setTokenExpired(Date tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public Date getTokenExpired() {
        return this.tokenExpired;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
