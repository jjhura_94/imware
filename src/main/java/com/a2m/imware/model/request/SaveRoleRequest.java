package com.a2m.imware.model.request;

import java.util.List;

import javax.validation.Valid;

public class SaveRoleRequest {
	
	@Valid
	private List<TsstRoleRequest> tsstRole;
	
	private String sessUserId;
	
	private String idsDel;
	
	
	
	public SaveRoleRequest() {
	
	}
	public SaveRoleRequest(List<TsstRoleRequest> tsstRole, String sessUserId, String idsDel) {
		this.tsstRole = tsstRole;
		this.sessUserId = sessUserId;
		this.idsDel = idsDel;
	}
	public List<TsstRoleRequest> getTsstRole() {
		return tsstRole;
	}
	public void setTsstRole(List<TsstRoleRequest> tsstRole) {
		this.tsstRole = tsstRole;
	}
	public String getSessUserId() {
		return sessUserId;
	}
	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}
	public String getIdsDel() {
		return idsDel;
	}
	public void setIdsDel(String idsDel) {
		this.idsDel = idsDel;
	}
	
	
	
}
