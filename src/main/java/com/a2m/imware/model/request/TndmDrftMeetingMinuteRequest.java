/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDrftMeetingMinuteRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Long idMeeting; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String drftDocNo; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date fromDate; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date toDate; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String place; 
    @Size(max = 255, message = ValidationCode.Size)
    private String meetingTitle; 
    
    private String meetingDate;
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String attached;
    
    private Date dateWrite; 
    private String writer; 
    private String purposeMeeting; 
    private String specialNote; 

    public TndmDrftMeetingMinuteRequest() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public Long getIdMeeting() {
		return idMeeting;
	}


	public void setIdMeeting(Long idMeeting) {
		this.idMeeting = idMeeting;
	}


    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

	public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : FROM_DATE (DATETIME)
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getFromDate() {
        return this.fromDate;
    }

    //--- DATABASE MAPPING : TO_DATE (DATETIME)
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDate() {
        return this.toDate;
    }

    //--- DATABASE MAPPING : PLACE (VARCHAR)
    public void setPlace(String place) {
        this.place = place;
    }

    public String getPlace() {
        return this.place;
    }

    //--- DATABASE MAPPING : MEETING_TITLE (VARCHAR)
    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle;
    }

    public String getMeetingTitle() {
        return this.meetingTitle;
    }


	public String getMeetingDate() {
		return meetingDate;
	}


	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}


	public String getAttached() {
		return attached;
	}


	public void setAttached(String attached) {
		this.attached = attached;
	}


	public Date getDateWrite() {
		return dateWrite;
	}


	public void setDateWrite(Date dateWrite) {
		this.dateWrite = dateWrite;
	}


	public String getWriter() {
		return writer;
	}


	public void setWriter(String writer) {
		this.writer = writer;
	}

	

	public String getPurposeMeeting() {
		return purposeMeeting;
	}


	public void setPurposeMeeting(String purposeMeeting) {
		this.purposeMeeting = purposeMeeting;
	}


	public String getSpecialNote() {
		return specialNote;
	}


	public void setSpecialNote(String specialNote) {
		this.specialNote = specialNote;
	}

	

}