/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.validation.ValidationCode;

public class TcdsEmpEquipRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long equipId;
    private Long empNo;
    @Size(max = 50, message = ValidationCode.Size)
    private String ipAddress;
    @Size(max = 20, message = ValidationCode.Size)
    private String equipType;
    @Size(max = 50, message = ValidationCode.Size)
    private String modelName;
    @Size(max = 50, message = ValidationCode.Size)
    private String cpu;
    @Size(max = 50, message = ValidationCode.Size)
    private String ram;
    @Size(max = 50, message = ValidationCode.Size)
    private String hdd;
    @Size(max = 50, message = ValidationCode.Size)
    private String odd;
    @Size(max = 50, message = ValidationCode.Size)
    private String vga;
    @Size(max = 50, message = ValidationCode.Size)
    private String os;
    @Size(max = 50, message = ValidationCode.Size)
    private String monitor1;
    @Size(max = 50, message = ValidationCode.Size)
    private String monitor2;
    @Size(max = 50, message = ValidationCode.Size)
    private String monitor3;
    @Size(max = 50, message = ValidationCode.Size)
    private String monitor4;
    private Date purchaseDate;
    @Size(max = 255, message = ValidationCode.Size)
    private String description;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

    private long purchaseDateLong;
    private String purchaseDateStr;

    public TcdsEmpEquipRequest() {

    }

    public long getPurchaseDateLong() {
        return purchaseDateLong;
    }

    public void setPurchaseDateLong(long purchaseDateLong) {
        this.purchaseDateLong = purchaseDateLong;
    }

    public String getPurchaseDateStr() {
        return purchaseDateStr;
    }

    public void setPurchaseDateStr(String purchaseDateStr) {
        this.purchaseDateStr = purchaseDateStr;
    }

    //--- DATABASE MAPPING : EQUIP_ID (BIGINT)
    public void setEquipId(Long equipId) {
        this.equipId = equipId;
    }

    public Long getEquipId() {
        return this.equipId;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : IP_ADDRESS (VARCHAR)
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    //--- DATABASE MAPPING : EQUIP_TYPE (VARCHAR)
    public void setEquipType(String equipType) {
        this.equipType = equipType;
    }

    public String getEquipType() {
        return this.equipType;
    }

    //--- DATABASE MAPPING : MODEL_NAME (VARCHAR)
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return this.modelName;
    }

    //--- DATABASE MAPPING : CPU (VARCHAR)
    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getCpu() {
        return this.cpu;
    }

    //--- DATABASE MAPPING : RAM (VARCHAR)
    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getRam() {
        return this.ram;
    }

    //--- DATABASE MAPPING : HDD (VARCHAR)
    public void setHdd(String hdd) {
        this.hdd = hdd;
    }

    public String getHdd() {
        return this.hdd;
    }

    //--- DATABASE MAPPING : ODD (VARCHAR)
    public void setOdd(String odd) {
        this.odd = odd;
    }

    public String getOdd() {
        return this.odd;
    }

    //--- DATABASE MAPPING : VGA (VARCHAR)
    public void setVga(String vga) {
        this.vga = vga;
    }

    public String getVga() {
        return this.vga;
    }

    //--- DATABASE MAPPING : OS (VARCHAR)
    public void setOs(String os) {
        this.os = os;
    }

    public String getOs() {
        return this.os;
    }

    //--- DATABASE MAPPING : MONITOR1 (VARCHAR)
    public void setMonitor1(String monitor1) {
        this.monitor1 = monitor1;
    }

    public String getMonitor1() {
        return this.monitor1;
    }

    //--- DATABASE MAPPING : MONITOR2 (VARCHAR)
    public void setMonitor2(String monitor2) {
        this.monitor2 = monitor2;
    }

    public String getMonitor2() {
        return this.monitor2;
    }

    //--- DATABASE MAPPING : MONITOR3 (VARCHAR)
    public void setMonitor3(String monitor3) {
        this.monitor3 = monitor3;
    }

    public String getMonitor3() {
        return this.monitor3;
    }

    //--- DATABASE MAPPING : MONITOR4 (VARCHAR)
    public void setMonitor4(String monitor4) {
        this.monitor4 = monitor4;
    }

    public String getMonitor4() {
        return this.monitor4;
    }

    //--- DATABASE MAPPING : PURCHASE_DATE (TIMESTAMP)
    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getPurchaseDate() {
        return this.purchaseDate;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

}
