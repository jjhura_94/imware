package com.a2m.imware.model.request;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.apv.TndmDrftBusTripDetail;
import com.a2m.imware.validation.ValidationCode;

public class DrftRequest extends TndmDrftSpendingRequest implements Serializable{

    private static final long serialVersionUID = 1L;

    private String drftDocNo; 
    
    private String docNo; 
    
    private String docUserUid; 
    
    private String createdBy; 
       
    private Date createdDate; 
    
    private String drftTitle; 
    
    private String drftContent; 
    
    private String drftStatus; 
    
    private Boolean recentYn; 
    
    private Long formId; 
    
    private String drftType; 
    
    private Boolean foreignOrigin; 
    
    private String docDeptName; 
    
    private String docDeptCode; 
    
    private String userId; 
    
    private String userUid; 
    
    private String type; 
    
    private String meetingDate; 
    
    private String place; 
    
    private String placeBus; 
    
    private Long idMeeting; 
    
    private String attendcharacter;
    
    private Long idBus; 
    
    private String position; 

    private Date fromDateBus; 

    private Date toDateBus; 

    private String purpose; 
    
    private Double transportationCost; 
    
    private Double workingMoney; 
    
    private Double foodMoney; 
    
    private Double roomCharge; 
    
    private Double etc; 
    
    private String attached; 
    
    private List<TccoFile> files;
    
    private Boolean changedFile;
    
    private List<TccoFile> filesDeleted;
    
    private List<TndmDrftApvlLine> apvLine;
    
    private List<TndmDrftRequest> reference;
    
    private List<TsstUserRequest> referrer;
    
    private String sendTo;
    
    private String spendingType; 
    
    private Date dateWrite; 
    private String writer; 
    private String purposeMeeting; 
    private String specialNote; 
    
    private String typeOfBusinessTrip; 

    private String country; 

    @Size(max = 100, message = ValidationCode.Size)
    private String traveler; 
    
    private Double oneWay; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String twoWay; 
    
    private Double toll; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String busTripDomestic; 
    
    private String purposeBus; 
    
    private List<TndmDrftBusTripDetail> busTripDetail;
    
	private String destination;
	
	private Date term;
	
	private String visitingAgency;
	
	private String docStatus;
	
	private Long vacationId;

    public DrftRequest() {
       
	}

    
    public Long getVacationId() {
		return vacationId;
	}

	public void setVacationId(Long vacationId) {
		this.vacationId = vacationId;
	}

    public String getSendTo() {
		return sendTo;
	}



	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}



	//--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : DOC_NO (VARCHAR)
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocNo() {
        return this.docNo;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : DRFT_TITLE (VARCHAR)
    public void setDrftTitle(String drftTitle) {
        this.drftTitle = drftTitle;
    }

    public String getDrftTitle() {
        return this.drftTitle;
    }

    //--- DATABASE MAPPING : DRFT_CONTENT (LONGTEXT)
    public void setDrftContent(String drftContent) {
        this.drftContent = drftContent;
    }

    public String getDrftContent() {
        return this.drftContent;
    }

    //--- DATABASE MAPPING : DRFT_STATUS (VARCHAR)
    public void setDrftStatus(String drftStatus) {
        this.drftStatus = drftStatus;
    }

    public String getDrftStatus() {
        return this.drftStatus;
    }

    //--- DATABASE MAPPING : RECENT_YN (BIT)
    public void setRecentYn(Boolean recentYn) {
        this.recentYn = recentYn;
    }

    public Boolean getRecentYn() {
        return this.recentYn;
    }

    //--- DATABASE MAPPING : FORM_ID (BIGINT)
    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getFormId() {
        return this.formId;
    }

    //--- DATABASE MAPPING : DRFT_TYPE (VARCHAR)
    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getDrftType() {
        return this.drftType;
    }


	public Boolean getForeignOrigin() {
		return foreignOrigin;
	}


	public void setForeignOrigin(Boolean foreignOrigin) {
		this.foreignOrigin = foreignOrigin;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserUid() {
		return userUid;
	}


	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getDocDeptName() {
		return docDeptName;
	}


	public void setDocDeptName(String docDeptName) {
		this.docDeptName = docDeptName;
	}


	public String getDocDeptCode() {
		return docDeptCode;
	}


	public void setDocDeptCode(String docDeptCode) {
		this.docDeptCode = docDeptCode;
	}


	public String getMeetingDate() {
		return meetingDate;
	}


	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}



	public String getPlace() {
		return place;
	}


	public void setPlace(String place) {
		this.place = place;
	}


	public String getAttendcharacter() {
		return attendcharacter;
	}


	public void setAttendcharacter(String attendcharacter) {
		this.attendcharacter = attendcharacter;
	}


	public Long getIdMeeting() {
		return idMeeting;
	}


	public void setIdMeeting(Long idMeeting) {
		this.idMeeting = idMeeting;
	}


	public String getAttached() {
		return attached;
	}


	public void setAttached(String attached) {
		this.attached = attached;
	}



	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public String getPurpose() {
		return purpose;
	}


	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}


	public Double getTransportationCost() {
		return transportationCost;
	}


	public void setTransportationCost(Double transportationCost) {
		this.transportationCost = transportationCost;
	}


	public Double getWorkingMoney() {
		return workingMoney;
	}


	public void setWorkingMoney(Double workingMoney) {
		this.workingMoney = workingMoney;
	}


	public Double getFoodMoney() {
		return foodMoney;
	}


	public void setFoodMoney(Double foodMoney) {
		this.foodMoney = foodMoney;
	}


	public Double getRoomCharge() {
		return roomCharge;
	}


	public void setRoomCharge(Double roomCharge) {
		this.roomCharge = roomCharge;
	}


	public Double getEtc() {
		return etc;
	}


	public void setEtc(Double etc) {
		this.etc = etc;
	}


	public Long getIdBus() {
		return idBus;
	}


	public void setIdBus(Long idBus) {
		this.idBus = idBus;
	}


	public String getPlaceBus() {
		return placeBus;
	}


	public void setPlaceBus(String placeBus) {
		this.placeBus = placeBus;
	}


	public Date getFromDateBus() {
		return fromDateBus;
	}


	public void setFromDateBus(Date fromDateBus) {
		this.fromDateBus = fromDateBus;
	}


	public Date getToDateBus() {
		return toDateBus;
	}


	public void setToDateBus(Date toDateBus) {
		this.toDateBus = toDateBus;
	}


	public List<TccoFile> getFiles() {
		return files;
	}


	public void setFiles(List<TccoFile> files) {
		this.files = files;
	}


	public Boolean getChangedFile() {
		return changedFile;
	}


	public void setChangedFile(Boolean changedFile) {
		this.changedFile = changedFile;
	}


	public List<TccoFile> getFilesDeleted() {
		return filesDeleted;
	}


	public void setFilesDeleted(List<TccoFile> filesDeleted) {
		this.filesDeleted = filesDeleted;
	}


	public List<TndmDrftApvlLine> getApvLine() {
		return apvLine;
	}


	public void setApvLine(List<TndmDrftApvlLine> apvLine) {
		this.apvLine = apvLine;
	}



	public List<TndmDrftRequest> getReference() {
		return reference;
	}



	public void setReference(List<TndmDrftRequest> reference) {
		this.reference = reference;
	}



	public List<TsstUserRequest> getReferrer() {
		return referrer;
	}



	public void setReferrer(List<TsstUserRequest> referrer) {
		this.referrer = referrer;
	}



	public String getSpendingType() {
		return spendingType;
	}



	public void setSpendingType(String spendingType) {
		this.spendingType = spendingType;
	}



	public Date getDateWrite() {
		return dateWrite;
	}



	public void setDateWrite(Date dateWrite) {
		this.dateWrite = dateWrite;
	}



	public String getWriter() {
		return writer;
	}



	public void setWriter(String writer) {
		this.writer = writer;
	}




	public String getPurposeMeeting() {
		return purposeMeeting;
	}



	public void setPurposeMeeting(String purposeMeeting) {
		this.purposeMeeting = purposeMeeting;
	}



	public String getSpecialNote() {
		return specialNote;
	}



	public void setSpecialNote(String specialNote) {
		this.specialNote = specialNote;
	}



	public String getDocUserUid() {
		return docUserUid;
	}



	public void setDocUserUid(String docUserUid) {
		this.docUserUid = docUserUid;
	}



	public String getTypeOfBusinessTrip() {
		return typeOfBusinessTrip;
	}



	public void setTypeOfBusinessTrip(String typeOfBusinessTrip) {
		this.typeOfBusinessTrip = typeOfBusinessTrip;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public String getTraveler() {
		return traveler;
	}



	public void setTraveler(String traveler) {
		this.traveler = traveler;
	}



	public Double getOneWay() {
		return oneWay;
	}



	public void setOneWay(Double oneWay) {
		this.oneWay = oneWay;
	}



	public String getTwoWay() {
		return twoWay;
	}



	public void setTwoWay(String twoWay) {
		this.twoWay = twoWay;
	}



	public Double getToll() {
		return toll;
	}



	public void setToll(Double toll) {
		this.toll = toll;
	}



	public String getBusTripDomestic() {
		return busTripDomestic;
	}


	public void setBusTripDomestic(String busTripDomestic) {
		this.busTripDomestic = busTripDomestic;
	}


	public String getPurposeBus() {
		return purposeBus;
	}


	public void setPurposeBus(String purposeBus) {
		this.purposeBus = purposeBus;
	}



	public List<TndmDrftBusTripDetail> getBusTripDetail() {
		return busTripDetail;
	}



	public void setBusTripDetail(List<TndmDrftBusTripDetail> busTripDetail) {
		this.busTripDetail = busTripDetail;
	}



	public String getDestination() {
		return destination;
	}



	public void setDestination(String destination) {
		this.destination = destination;
	}



	public Date getTerm() {
		return term;
	}



	public void setTerm(Date term) {
		this.term = term;
	}



	public String getVisitingAgency() {
		return visitingAgency;
	}



	public void setVisitingAgency(String visitingAgency) {
		this.visitingAgency = visitingAgency;
	}



	public String getDocStatus() {
		return docStatus;
	}



	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
    
	
	
}
