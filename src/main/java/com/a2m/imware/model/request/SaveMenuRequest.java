package com.a2m.imware.model.request;

import java.util.List;

import javax.validation.Valid;

public class SaveMenuRequest {
	@Valid
	private List<TsstMenuRequest> tsstMenu;
	
	private String sessUserId;
	
	private String idsDel;


	public List<TsstMenuRequest> getTsstMenu() {
		return tsstMenu;
	}

	public void setTsstMenu(List<TsstMenuRequest> tsstMenu) {
		this.tsstMenu = tsstMenu;
	}

	public String getSessUserId() {
		return sessUserId;
	}

	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}

	public String getIdsDel() {
		return idsDel;
	}

	public void setIdsDel(String idsDel) {
		this.idsDel = idsDel;
	}
	
	
}
