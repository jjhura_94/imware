/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.service.util.RowStatus;
import com.a2m.imware.validation.ValidationCode;

public class TcdsEmpMstRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long empNo;
    @Size(max = 255, message = ValidationCode.Size)
    private String nameKr;
    @Size(max = 255, message = ValidationCode.Size)
    private String nameEn;
    @Size(max = 255, message = ValidationCode.Size)
    private String nameCh;
    @Size(max = 20, message = ValidationCode.Size)
    private String dutyCode;

    private Date startDate;

    private Date endDate;

    private Date dob;
    @Size(max = 100, message = ValidationCode.Size)
    private String email;
    @Size(max = 255, message = ValidationCode.Size)
    private String cellPhone;
    private String extNumber;
    @Size(max = 255, message = ValidationCode.Size)
    private String zipCode;
    @Size(max = 255, message = ValidationCode.Size)
    private String address;
    @Size(max = 255, message = ValidationCode.Size)
    private String address2;
    @Size(max = 1, message = ValidationCode.Size)
    private String gender;
    @Size(max = 100, message = ValidationCode.Size)
    private String imgPath;
    @Size(max = 100, message = ValidationCode.Size)
    private String signaturePath;
    @Size(max = 255, message = ValidationCode.Size)
    private String seNumber;
    @Size(max = 1, message = ValidationCode.Size)
    private String status;
    @Size(max = 20, message = ValidationCode.Size)
    private String deptCode;

    private String absentYn;
    private String absentUserId;
    private Date absFromDate;
    private Date absToDate;
    private String userId;

    public TcdsEmpMstRequest() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    

    public Date getAbsFromDate() {
        return absFromDate;
    }

    public void setAbsFromDate(Date absFromDate) {
        this.absFromDate = absFromDate;
    }

    public Date getAbsToDate() {
        return absToDate;
    }

    public void setAbsToDate(Date absToDate) {
        this.absToDate = absToDate;
    }

    public String getAbsentUserId() {
        return absentUserId;
    }

    public void setAbsentUserId(String absentUserId) {
        this.absentUserId = absentUserId;
    }

    public String getAbsentYn() {
        return absentYn;
    }

    public void setAbsentYn(String absentYn) {
        this.absentYn = absentYn;
    }

    //--- DATABASE MAPPING : EMP_NO (BIGINT)
    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Long getEmpNo() {
        return this.empNo;
    }

    //--- DATABASE MAPPING : NAME_KR (VARCHAR)
    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameKr() {
        return this.nameKr;
    }

    //--- DATABASE MAPPING : NAME_EN (VARCHAR)
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return this.nameEn;
    }

    //--- DATABASE MAPPING : NAME_CH (VARCHAR)
    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

    public String getNameCh() {
        return this.nameCh;
    }

    //--- DATABASE MAPPING : DUTY_CODE (VARCHAR)
    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyCode() {
        return this.dutyCode;
    }

    //--- DATABASE MAPPING : START_DATE (TIMESTAMP)
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    //--- DATABASE MAPPING : END_DATE (TIMESTAMP)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    //--- DATABASE MAPPING : DOB (DATETIME)
    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getDob() {
        return this.dob;
    }

    //--- DATABASE MAPPING : EMAIL (VARCHAR)
    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    //--- DATABASE MAPPING : CELL_PHONE (VARCHAR)
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCellPhone() {
        return this.cellPhone;
    }

    //--- DATABASE MAPPING : EXT_NUMBER (VARCHAR)
    public void setExtNumber(String extNumber) {
        this.extNumber = extNumber;
    }

    public String getExtNumber() {
        return this.extNumber;
    }

    //--- DATABASE MAPPING : ZIP_CODE (VARCHAR)
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    //--- DATABASE MAPPING : ADDRESS (VARCHAR)
    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    //--- DATABASE MAPPING : ADDRESS_2 (VARCHAR)
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2() {
        return this.address2;
    }

    //--- DATABASE MAPPING : GENDER (VARCHAR)
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }

    //--- DATABASE MAPPING : IMG_PATH (VARCHAR)
    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPath() {
        return this.imgPath;
    }

    //--- DATABASE MAPPING : SIGNATURE_PATH (VARCHAR)
    public void setSignaturePath(String signaturePath) {
        this.signaturePath = signaturePath;
    }

    public String getSignaturePath() {
        return this.signaturePath;
    }

    //--- DATABASE MAPPING : SE_NUMBER (VARCHAR)
    public void setSeNumber(String seNumber) {
        this.seNumber = seNumber;
    }

    public String getSeNumber() {
        return this.seNumber;
    }

    //--- DATABASE MAPPING : STATUS (VARCHAR)
    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    //--- DATABASE MAPPING : DEPT_CODE (VARCHAR)
    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptCode() {
        return this.deptCode;
    }
}
