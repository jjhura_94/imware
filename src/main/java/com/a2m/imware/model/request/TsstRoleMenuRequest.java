/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.request;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TsstRoleMenuRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String roleId; 
    
    private String menuId; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String readYn; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String wrtYn; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String modYn;
    
    @Size(max = 1, message = ValidationCode.Size)
    private String delYn; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String pntYn; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String excDnYn; 
    
    @Size(max = 1, message = ValidationCode.Size)
    private String mngYn; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    private String createdBy; 
    
    @NotNull(message = ValidationCode.NotNull)
    private Date createdDate; 

    public TsstRoleMenuRequest() {
       
	}


    //--- DATABASE MAPPING : ROLE_ID (VARCHAR)
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return this.roleId;
    }

    //--- DATABASE MAPPING : MENU_ID (VARCHAR)
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuId() {
        return this.menuId;
    }

    //--- DATABASE MAPPING : READ_YN (VARCHAR)
    public void setReadYn(String readYn) {
        this.readYn = readYn;
    }

    public String getReadYn() {
        return this.readYn;
    }

    //--- DATABASE MAPPING : WRT_YN (VARCHAR)
    public void setWrtYn(String wrtYn) {
        this.wrtYn = wrtYn;
    }

    public String getWrtYn() {
        return this.wrtYn;
    }

    //--- DATABASE MAPPING : MOD_YN (VARCHAR)
    public void setModYn(String modYn) {
        this.modYn = modYn;
    }

    public String getModYn() {
        return this.modYn;
    }

    //--- DATABASE MAPPING : DEL_YN (VARCHAR)
    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getDelYn() {
        return this.delYn;
    }

    //--- DATABASE MAPPING : PNT_YN (VARCHAR)
    public void setPntYn(String pntYn) {
        this.pntYn = pntYn;
    }

    public String getPntYn() {
        return this.pntYn;
    }

    //--- DATABASE MAPPING : EXC_DN_YN (VARCHAR)
    public void setExcDnYn(String excDnYn) {
        this.excDnYn = excDnYn;
    }

    public String getExcDnYn() {
        return this.excDnYn;
    }

    //--- DATABASE MAPPING : MNG_YN (VARCHAR)
    public void setMngYn(String mngYn) {
        this.mngYn = mngYn;
    }

    public String getMngYn() {
        return this.mngYn;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }


}