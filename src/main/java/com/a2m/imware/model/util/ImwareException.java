package com.a2m.imware.model.util;

/**
 * @author duongbq
 * @since 13-04-2018 Description: TODO
 */
public class ImwareException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int code;
	private String message;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private ImwareException(int code, String message) {
		this.code = code;
		this.message = message;
	}

	
	public static ImwareException exception(int code, String message) {
		return new ImwareException(code, message);
	}
}
