/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 *
 * @author Admin
 */
public class TndmIssuance extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;
    @MapKey("ID")
    private Long id;

    @MapKey("DEPT_NM")
    private String deptNm;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("SUBMISSION")
    private String submission;

    @MapKey("PURPOSE")
    private String purpose;

    @MapKey("SUBMIT_DATE")
    private Date submitDate;

    @MapKey("SUBMIT_DATE_STR")
    private String submitDateString;

    @MapKey("NOTE")
    private String note;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("STATUS")
    private String status;
    private String statusName;

    @MapKey("CREATED_NAME")
    private String createdName;

    private List<TccoFile> attachment;

    private List<TccoFile> filesToAdd;

    private List<TccoFile> filesToDelete;

    public List<TccoFile> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<TccoFile> attachment) {
        this.attachment = attachment;
    }

    public List<TccoFile> getFilesToAdd() {
        return filesToAdd;
    }

    public void setFilesToAdd(List<TccoFile> filesToAdd) {
        this.filesToAdd = filesToAdd;
    }

    public List<TccoFile> getFilesToDelete() {
        return filesToDelete;
    }

    public void setFilesToDelete(List<TccoFile> filesToDelete) {
        this.filesToDelete = filesToDelete;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getSubmitDateString() {
        return submitDateString;
    }

    public void setSubmitDateString(String submitDateString) {
        this.submitDateString = submitDateString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getSubmission() {
        return submission;
    }

    public void setSubmission(String submission) {
        this.submission = submission;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
