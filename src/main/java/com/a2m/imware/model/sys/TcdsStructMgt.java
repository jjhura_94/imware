package com.a2m.imware.model.sys;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;
import com.a2m.imware.validation.ynField.YnField;

public class TcdsStructMgt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String structId;

	@Size(max = 255, message = ValidationCode.Size)
	private String structNm;

	@Size(max = 1, message = ValidationCode.Size)
	@YnField
	private String useYn;

	private Date fromDate;
	
	@Size(max = 255, message = ValidationCode.Size)
	private String description;

	private String createdBy;

	private Date createdDate;

	public TcdsStructMgt() {

	}

	// --- DATABASE MAPPING : STRUCT_ID (VARCHAR)
	public void setStructId(String structId) {
		this.structId = structId;
	}

	public String getStructId() {
		return this.structId;
	}

	// --- DATABASE MAPPING : STRUCT_NM (VARCHAR)
	public void setStructNm(String structNm) {
		this.structNm = structNm;
	}

	public String getStructNm() {
		return this.structNm;
	}

	// --- DATABASE MAPPING : USE_YN (VARCHAR)
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getUseYn() {
		return this.useYn;
	}

	// --- DATABASE MAPPING : FROM_DATE (TIMESTAMP)
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	// --- DATABASE MAPPING : DESCRIPTION (VARCHAR)
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	// --- DATABASE MAPPING : CREATED_BY (VARCHAR)
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

}