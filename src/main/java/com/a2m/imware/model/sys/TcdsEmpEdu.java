package com.a2m.imware.model.sys;

import java.io.Serializable;
import java.util.Date;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * 
 * @author Tien
 *
 */
public class TcdsEmpEdu extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("EDU_ID")
	private Long eduId;

	@MapKey("EMP_NO")
	private Long empNo;

	@MapKey("EDU_LEVEL")
	private String eduLevel;

	@MapKey("EDU_LEVEL_NAME")
	private String eduLevelName;

	@MapKey("START_DATE")
	private Date startDate;

	@MapKey("START_DATE_STRING")
	private String startDateString;

	@MapKey("END_DATE")
	private Date endDate;

	@MapKey("END_DATE_STRING")
	private String endDateString;

	@MapKey("SCHOOL_NAME")
	private String schoolName;

	@MapKey("MAJOR_NAME")
	private String majorName;

	@MapKey("GRADUATION_STATUS")
	private String graduationStatus;

	@MapKey("GRADUATION_STATUS_NAME")
	private String graduationStatusName;

	@MapKey("DEGREE")
	private String degree;

	@MapKey("DEGREE_NAME")
	private String degreeName;

	@MapKey("CREATED_BY")
	private String createdBy;

	@MapKey("CREATED_DATE")
	private Date createdDate;

	@MapKey("CREATED_DATE_STRING")
	private String createDateString;

	@MapKey("UPDATED_BY")
	private String updatedBy;

	@MapKey("UPDATED_DATE")
	private Date updatedDate;

	@MapKey("UPDATED_DATE_STRING")
	private String updatedDateString;

	private String sessUserId;

	public TcdsEmpEdu() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TcdsEmpEdu(Long eduId, Long empNo, String eduLevel, String eduLevelName, Date startDate,
			String startDateString, Date endDate, String endDateString, String schoolName, String majorName,
			String graduationStatus, String graduationStatusName, String degree, String degreeName) {
		super();
		this.eduId = eduId;
		this.empNo = empNo;
		this.eduLevel = eduLevel;
		this.eduLevelName = eduLevelName;
		this.startDate = startDate;
		this.startDateString = startDateString;
		this.endDate = endDate;
		this.endDateString = endDateString;
		this.schoolName = schoolName;
		this.majorName = majorName;
		this.graduationStatus = graduationStatus;
		this.graduationStatusName = graduationStatusName;
		this.degree = degree;
		this.degreeName = degreeName;
	}

	public Long getEduId() {
		return eduId;
	}

	public void setEduId(Long eduId) {
		this.eduId = eduId;
	}

	public Long getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Long empNo) {
		this.empNo = empNo;
	}

	public String getEduLevel() {
		return eduLevel;
	}

	public void setEduLevel(String eduLevel) {
		this.eduLevel = eduLevel;
	}

	public String getEduLevelName() {
		return eduLevelName;
	}

	public void setEduLevelName(String eduLevelName) {
		this.eduLevelName = eduLevelName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStartDateString() {
		return startDateString;
	}

	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEndDateString() {
		return endDateString;
	}

	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public String getGraduationStatus() {
		return graduationStatus;
	}

	public void setGraduationStatus(String graduationStatus) {
		this.graduationStatus = graduationStatus;
	}

	public String getGraduationStatusName() {
		return graduationStatusName;
	}

	public void setGraduationStatusName(String graduationStatusName) {
		this.graduationStatusName = graduationStatusName;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreateDateString() {
		return createDateString;
	}

	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDateString() {
		return updatedDateString;
	}

	public void setUpdatedDateString(String updatedDateString) {
		this.updatedDateString = updatedDateString;
	}

	public String getSessUserId() {
		return sessUserId;
	}

	public void setSessUserId(String sessUserId) {
		this.sessUserId = sessUserId;
	}

}
