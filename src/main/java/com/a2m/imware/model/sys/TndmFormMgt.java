package com.a2m.imware.model.sys;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;
import com.a2m.imware.validation.ynField.YnField;

@SuppressWarnings("deprecation")
public class TndmFormMgt implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 255, message = ValidationCode.Size)
	private String formName;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 2147483647, message = ValidationCode.Size)
//	@SafeHtml(message = ValidationCode.SafeHtml)
	private String formContent;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String formCategory;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String formType;

	@Size(max = 255, message = ValidationCode.Size)
	private String description;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String createdBy;

	private Date createdDate;

	@Size(max = 20, message = ValidationCode.Size)
	private String updatedBy;

	private Date updatedDate;

	@YnField
	private String useYn;
	
	private String formTypeName;
	private String formTypeNameEn;
	private String formCategoryName;
	private String formCategoryNameEn;

	public String getFormTypeName() {
		return formTypeName;
	}

	public void setFormTypeName(String formTypeName) {
		this.formTypeName = formTypeName;
	}

	public String getFormTypeNameEn() {
		return formTypeNameEn;
	}

	public void setFormTypeNameEn(String formTypeNameEn) {
		this.formTypeNameEn = formTypeNameEn;
	}

	public String getFormCategoryName() {
		return formCategoryName;
	}

	public void setFormCategoryName(String formCategoryName) {
		this.formCategoryName = formCategoryName;
	}

	public String getFormCategoryNameEn() {
		return formCategoryNameEn;
	}

	public void setFormCategoryNameEn(String formCategoryNameEn) {
		this.formCategoryNameEn = formCategoryNameEn;
	}


	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : FORM_NAME (VARCHAR)
	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getFormName() {
		return this.formName;
	}

	// --- DATABASE MAPPING : FORM_CONTENT (LONGTEXT)
	public void setFormContent(String formContent) {
		this.formContent = formContent;
	}

	public String getFormContent() {
		return this.formContent;
	}

	// --- DATABASE MAPPING : FORM_CATEGORY (VARCHAR)
	public void setFormCategory(String formCategory) {
		this.formCategory = formCategory;
	}

	public String getFormCategory() {
		return this.formCategory;
	}

	// --- DATABASE MAPPING : FORM_TYPE (VARCHAR)
	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormType() {
		return this.formType;
	}

	// --- DATABASE MAPPING : DESCRIPTION (VARCHAR)
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	// --- DATABASE MAPPING : CREATED_BY (VARCHAR)
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	// --- DATABASE MAPPING : UPDATED_BY (VARCHAR)
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	// --- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	// --- DATABASE MAPPING : USE_YN (BIT)
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getUseYn() {
		return this.useYn;
	}
}
