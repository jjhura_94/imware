package com.a2m.imware.model.sys;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;

public class TcdsDeptStruct implements Serializable {

	private static final long serialVersionUID = 1L;


	private String deptCode;
	
	private String structId;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 255, message = ValidationCode.Size)
	private String deptNm;
	
	@PositiveOrZero(message = ValidationCode.PositiveOrZero)
	private Integer lev;
	
	@Positive(message = ValidationCode.Positive)
	private Integer ordNo;
	
	@Size(max = 20, message = ValidationCode.Size)
	private String upDeptCode;
	
	@Size(max = 20, message = ValidationCode.Size)
	private String tagDoc;
	
	@Size(max = 20, message = ValidationCode.Size)
	private String deptHead;
	
	@Size(max = 255, message = ValidationCode.Size)
	private String deptHeadName;


	public TcdsDeptStruct() {

	}

	// --- DATABASE MAPPING : DEPT_CODE (VARCHAR)
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptCode() {
		return this.deptCode;
	}

	// --- DATABASE MAPPING : STRUCT_ID (VARCHAR)
	public void setStructId(String structId) {
		this.structId = structId;
	}

	public String getStructId() {
		return this.structId;
	}

	// --- DATABASE MAPPING : DEPT_NM (VARCHAR)
	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	public String getDeptNm() {
		return this.deptNm;
	}

	// --- DATABASE MAPPING : LEV (INT)
	public void setLev(Integer lev) {
		this.lev = lev;
	}

	public Integer getLev() {
		return this.lev;
	}

	// --- DATABASE MAPPING : ORD_NO (INT)
	public void setOrdNo(Integer ordNo) {
		this.ordNo = ordNo;
	}

	public Integer getOrdNo() {
		return this.ordNo;
	}

	// --- DATABASE MAPPING : UP_DEPT_CODE (VARCHAR)
	public void setUpDeptCode(String upDeptCode) {
		this.upDeptCode = upDeptCode;
	}

	public String getUpDeptCode() {
		return this.upDeptCode;
	}

	// --- DATABASE MAPPING : TAG_DOC (VARCHAR)
	public void setTagDoc(String tagDoc) {
		this.tagDoc = tagDoc;
	}

	public String getTagDoc() {
		return this.tagDoc;
	}

	// --- DATABASE MAPPING : DEPT_HEAD (VARCHAR)
	public void setDeptHead(String deptHead) {
		this.deptHead = deptHead;
	}

	public String getDeptHead() {
		return this.deptHead;
	}
	
	public String getDeptHeadName() {
		return deptHeadName;
	}

	public void setDeptHeadName(String deptHeadName) {
		this.deptHeadName = deptHeadName;
	}

}
