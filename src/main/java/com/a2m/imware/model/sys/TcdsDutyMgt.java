package com.a2m.imware.model.sys;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;
import com.a2m.imware.validation.ynField.YnField;

public class TcdsDutyMgt implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dutyCode;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 100, message = ValidationCode.Size)
	private String dutyName;
	
	@YnField
	private String useYn;
	
	@NotNull(message = ValidationCode.NotNull)
	@Positive(message = ValidationCode.Positive)
	private Integer dutyOrd;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String updatedBy;
	
	private Date updatedDate;

	public TcdsDutyMgt() {

	}
    

	// --- DATABASE MAPPING : DUTY_CODE (VARCHAR)
	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getDutyCode() {
		return this.dutyCode;
	}

	// --- DATABASE MAPPING : DUTY_NAME (VARCHAR)
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

	public String getDutyName() {
		return this.dutyName;
	}

	// --- DATABASE MAPPING : USE_YN (VARCHAR)
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getUseYn() {
		return this.useYn;
	}

	// --- DATABASE MAPPING : DUTY_ORD (INT)
	public void setDutyOrd(Integer dutyOrd) {
		this.dutyOrd = dutyOrd;
	}

	public Integer getDutyOrd() {
		return this.dutyOrd;
	}

	// --- DATABASE MAPPING : CREATED_BY (VARCHAR)
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	// --- DATABASE MAPPING : UPDATED_BY (VARCHAR)
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	// --- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

}
