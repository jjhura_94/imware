package com.a2m.imware.model;

/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.a2m.imware.validation.ValidationCode;

public class TsstUserRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String roleId;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String userUid;

	public TsstUserRole() {

	}

	// --- DATABASE MAPPING : ROLE_ID (VARCHAR)
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleId() {
		return this.roleId;
	}

	// --- DATABASE MAPPING : USER_UID (VARCHAR)
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getUserUid() {
		return this.userUid;
	}

}