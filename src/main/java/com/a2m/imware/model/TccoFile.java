package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * Map with TCCO_FILE table.
 *
 * @author HungDM
 *
 */
public class TccoFile extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ATCH_FLE_SEQ")
    private String atchFleSeq;

    @MapKey("FLE_KEY")
    private String fleKey;

    @MapKey("FLE_TP")
    private String fleTp;

    @MapKey("FLE_PATH")
    private String flePath;

    @MapKey("FLE_NM")
    private String fleNm;

    @MapKey("NEW_FLE_NM")
    private String newFleNm;

    @MapKey("FLE_SZ")
    private String fleSz;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("UPDATED_BY")
    private String updatedBy;

    @MapKey("UPDATED_DATE")
    private Date updatedDate;

    private String createdDateStr;
    private String updatedDateStr;

    public TccoFile() {
        super();
    }

    public TccoFile(String atchFleSeq, String fleKey, String fleType, String flePath, String fleNm, String newFleNm,
            String fleSz, String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
        super();
        this.atchFleSeq = atchFleSeq;
        this.fleKey = fleKey;
        this.fleTp = fleType;
        this.flePath = flePath;
        this.fleNm = fleNm;
        this.newFleNm = newFleNm;
        this.fleSz = fleSz;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.updatedBy = updatedBy;
        this.updatedDate = updatedDate;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getUpdatedDateStr() {
        return updatedDateStr;
    }

    public void setUpdatedDateStr(String updatedDateStr) {
        this.updatedDateStr = updatedDateStr;
    }

    public String getAtchFleSeq() {
        return atchFleSeq;
    }

    public void setAtchFleSeq(String atchFleSeq) {
        this.atchFleSeq = atchFleSeq;
    }

    public String getFleKey() {
        return fleKey;
    }

    public void setFleKey(String fleKey) {
        this.fleKey = fleKey;
    }

    public String getFleTp() {
        return fleTp;
    }

    public void setFleTp(String fleTp) {
        this.fleTp = fleTp;
    }

    public String getFlePath() {
        return flePath;
    }

    public void setFlePath(String flePath) {
        this.flePath = flePath;
    }

    public String getFleNm() {
        return fleNm;
    }

    public void setFleNm(String fleNm) {
        this.fleNm = fleNm;
    }

    public String getNewFleNm() {
        return newFleNm;
    }

    public void setNewFleNm(String newFleNm) {
        this.newFleNm = newFleNm;
    }

    public String getFleSz() {
        return fleSz;
    }

    public void setFleSz(String fleSz) {
        this.fleSz = fleSz;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
