package com.a2m.imware.model.task;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;

public class TndmTaskComment implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long commentId;

    private Long taskId;

    private Long upCommentId;

    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String content;

    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy;

    private Date createdDate;

    private Boolean isEdited;

    private List<TndmTaskComment> children;

    private String nickName;
    private String deptName;
    private String position;

    private List<TndmTaskCommentAttach> attachFiles;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<TndmTaskCommentAttach> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TndmTaskCommentAttach> attachFiles) {
        this.attachFiles = attachFiles;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<TndmTaskComment> getChildren() {
        return children;
    }

    public void setChildren(List<TndmTaskComment> children) {
        this.children = children;
    }

    // --- DATABASE MAPPING : ID (BIGINT)
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getCommentId() {
        return this.commentId;
    }

    // --- DATABASE MAPPING : TASK_ID (BIGINT)
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    // --- DATABASE MAPPING : UP_COMMENT_ID (BIGINT)
    public void setUpCommentId(Long upCommentId) {
        this.upCommentId = upCommentId;
    }

    public Long getUpCommentId() {
        return this.upCommentId;
    }

    // --- DATABASE MAPPING : CONTENT (LONGTEXT)
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    // --- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    // --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    // --- DATABASE MAPPING : IS_EDITED (BIT)
    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Boolean getIsEdited() {
        return this.isEdited;
    }

}
