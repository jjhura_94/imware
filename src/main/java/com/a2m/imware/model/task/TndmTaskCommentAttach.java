package com.a2m.imware.model.task;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.validation.ValidationCode;

public class TndmTaskCommentAttach extends TccoFile implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id; 

    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Long commentId; 

    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 100, message = ValidationCode.Size)
    private String attachFleSeq; 




    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : COMMENT_ID (BIGINT)
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getCommentId() {
        return this.commentId;
    }

    //--- DATABASE MAPPING : ATTACH_FLE_SEQ (VARCHAR)
    public void setAttachFleSeq(String attachFleSeq) {
        this.attachFleSeq = attachFleSeq;
    }

    public String getAttachFleSeq() {
        return this.attachFleSeq;
    }


}