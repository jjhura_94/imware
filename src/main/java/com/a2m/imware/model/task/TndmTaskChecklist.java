/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.task;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmTaskChecklist implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private Long chkListId; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 100, message = ValidationCode.Size)
    private String content; 
    
    private Long taskId; 
    
    private String status; 

    public TndmTaskChecklist() {
       
	}


    //--- DATABASE MAPPING : CHK_LIST_ID (BIGINT)
    public void setChkListId(Long chkListId) {
        this.chkListId = chkListId;
    }

    public Long getChkListId() {
        return this.chkListId;
    }

    //--- DATABASE MAPPING : CONTENT (VARCHAR)
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    //--- DATABASE MAPPING : TASK_ID (BIGINT)
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    //--- DATABASE MAPPING : STATUS (VARCHAR)
    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }


}