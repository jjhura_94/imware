package com.a2m.imware.model.task;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.validation.ValidationCode;

public class TndmTaskMgt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String assigneeId;

	@NotNull(message = ValidationCode.NotNull)

	private Date fromDate;

	@NotNull(message = ValidationCode.NotNull)

	private Date toDate;

	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 255, message = ValidationCode.Size)
	private String title;

	@Size(max = 2147483647, message = ValidationCode.Size)
	private String content;

	private String status;

	private String createdBy;

	private Date createdDate;

	private String updatedBy;

	private Date updatedDate;

	private String statusNameKr;
	private String statusNameEn;
	private String createdByEmpNameKr;
	private String DEPT_CODE;
	private String deptName;
	private String nameKr;
	private String position;
	private String dutyName;

	private boolean favourite;

	private List<TndmTaskAttach> attachment;
	private int attachmentCount;

	@Valid
	@NotNull
	@NotEmpty
	private List<TndmTaskRecipient> recipients;

	@Valid
	@NotNull
	private List<TndmTaskReferrer> referrers;

	private boolean changedFile;
	private List<TccoFile> newFiles;
	private List<TccoFile> deletedFiles;

	List<String> checklists;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<String> getChecklists() {
		return checklists;
	}

	public void setChecklists(List<String> checklists) {
		this.checklists = checklists;
	}

	public String getCreatedByEmpNameKr() {
		return createdByEmpNameKr;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getAttachmentCount() {
		return attachmentCount;
	}

	public void setAttachmentCount(int attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	public List<TndmTaskAttach> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<TndmTaskAttach> attachment) {
		this.attachment = attachment;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDEPT_CODE() {
		return DEPT_CODE;
	}

	public void setDEPT_CODE(String DEPT_CODE) {
		this.DEPT_CODE = DEPT_CODE;
	}

	public void setCreatedByEmpNameKr(String createdByEmpNameKr) {
		this.createdByEmpNameKr = createdByEmpNameKr;
	}

	public String getStatusNameKr() {
		return statusNameKr;
	}

	public void setStatusNameKr(String statusNameKr) {
		this.statusNameKr = statusNameKr;
	}

	public String getStatusNameEn() {
		return statusNameEn;
	}

	public void setStatusNameEn(String statusNameEn) {
		this.statusNameEn = statusNameEn;
	}

	public List<TccoFile> getNewFiles() {
		return newFiles;
	}

	public void setNewFiles(List<TccoFile> newFiles) {
		this.newFiles = newFiles;
	}

	public List<TccoFile> getDeletedFiles() {
		return deletedFiles;
	}

	public void setDeletedFiles(List<TccoFile> deletedFiles) {
		this.deletedFiles = deletedFiles;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<TndmTaskRecipient> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<TndmTaskRecipient> taskRecipients) {
		this.recipients = taskRecipients;
	}

	public List<TndmTaskReferrer> getReferrers() {
		return referrers;
	}

	public void setReferrers(List<TndmTaskReferrer> taskReferrers) {
		this.referrers = taskReferrers;
	}

	public boolean getChangedFile() {
		return changedFile;
	}

	public void setChangedFile(boolean changedFile) {
		this.changedFile = changedFile;
	}

	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : ASSIGNEE_ID (VARCHAR)
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getAssigneeId() {
		return this.assigneeId;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

}
