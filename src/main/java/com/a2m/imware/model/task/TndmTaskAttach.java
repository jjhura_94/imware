package com.a2m.imware.model.task;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.validation.ValidationCode;

public class TndmTaskAttach extends TccoFile implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; 
    
    @NotNull(message = ValidationCode.NotNull)
    private Long taskId; 
    
    private String attachFleSeq; 

    public TndmTaskAttach() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : TASK_ID (BIGINT)
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    //--- DATABASE MAPPING : ATTACH_FLE_SEQ (VARCHAR)
    public void setAttachFleSeq(String attachFleSeq) {
        this.attachFleSeq = attachFleSeq;
    }

    public String getAttachFleSeq() {
        return this.attachFleSeq;
    }

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
	public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final TndmTaskAttach other = (TndmTaskAttach) obj;
        if (this.id == null) {
            if (other.id != null)
                return false;
        } else if (!this.id.equals(other.id))
            return false;
        return true;
    }


}
