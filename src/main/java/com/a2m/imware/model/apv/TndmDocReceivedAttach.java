/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.validation.ValidationCode;


public class TndmDocReceivedAttach extends TccoFile implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Long docReceivedId; 
    
    private String attachFleSeq; 

    public TndmDocReceivedAttach() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : DOC_RECEIVED_ID (BIGINT)
    public void setDocReceivedId(Long docReceivedId) {
        this.docReceivedId = docReceivedId;
    }

    public Long getDocReceivedId() {
        return this.docReceivedId;
    }

    //--- DATABASE MAPPING : ATTACH_FLE_SEQ (VARCHAR)
    public void setAttachFleSeq(String attachFleSeq) {
        this.attachFleSeq = attachFleSeq;
    }

    public String getAttachFleSeq() {
        return this.attachFleSeq;
    }


}