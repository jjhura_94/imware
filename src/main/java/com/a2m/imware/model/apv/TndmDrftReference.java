/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.validation.ValidationCode;


public class TndmDrftReference extends TccoFile implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private Long id; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String drftDocNo; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String docNo; 
    
    private String drftTitle;
    
    private String nameKr;
    
    private String docDeptName;

    public TndmDrftReference() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : DOC_NO (VARCHAR)
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getDocNo() {
        return this.docNo;
    }


	public String getDrftTitle() {
		return drftTitle;
	}


	public void setDrftTitle(String drftTitle) {
		this.drftTitle = drftTitle;
	}


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}


	public String getDocDeptName() {
		return docDeptName;
	}


	public void setDocDeptName(String docDeptName) {
		this.docDeptName = docDeptName;
	}
    


}