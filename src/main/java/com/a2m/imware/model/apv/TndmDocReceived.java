/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.response.TcdsEmpMst;
import com.a2m.imware.validation.ValidationCode;


public class TndmDocReceived implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String receiveFrom; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String officialNo; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String title; 
    
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String content; 
    
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 

    private Date createdDate; 
    
    private List<TccoFile> files;
    
    private Boolean changedFile;
    
    private List<TccoFile> filesDeleted;
    
    private String nameKr;
    
    private String userUid;
    
    private String receiver;

    public TndmDocReceived() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : RECEIVE_FROM (VARCHAR)
    public void setReceiveFrom(String receiveFrom) {
        this.receiveFrom = receiveFrom;
    }

    public String getReceiveFrom() {
        return this.receiveFrom;
    }

    //--- DATABASE MAPPING : OFFICIAL_NO (VARCHAR)
    public void setOfficialNo(String officialNo) {
        this.officialNo = officialNo;
    }

    public String getOfficialNo() {
        return this.officialNo;
    }

    //--- DATABASE MAPPING : TITLE (VARCHAR)
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    //--- DATABASE MAPPING : CONTENT (LONGTEXT)
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }


	public List<TccoFile> getFiles() {
		return files;
	}


	public void setFiles(List<TccoFile> files) {
		this.files = files;
	}


	public Boolean getChangedFile() {
		return changedFile;
	}


	public void setChangedFile(Boolean changedFile) {
		this.changedFile = changedFile;
	}


	public List<TccoFile> getFilesDeleted() {
		return filesDeleted;
	}


	public void setFilesDeleted(List<TccoFile> filesDeleted) {
		this.filesDeleted = filesDeleted;
	}


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}


	public String getUserUid() {
		return userUid;
	}


	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}


	public String getReceiver() {
		return receiver;
	}


	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}


}