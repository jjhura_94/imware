/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;

public class TndmReferrerFavorite extends TndmReferrerFavoriteDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 100, message = ValidationCode.Size)
	private String createdBy;
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 100, message = ValidationCode.Size)
	private String name;

	private Date createdDate;
	
	private String type;

	

	List<TndmReferrerFavoriteDetail> details;

	public TndmReferrerFavorite() {

	}

	// --- DATABASE MAPPING : ID (BIGINT)
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// --- DATABASE MAPPING : CREATED_BY (VARCHAR)
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	// --- DATABASE MAPPING : NAME (VARCHAR)
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	// --- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public List<TndmReferrerFavoriteDetail> getDetails() {
		return details;
	}

	public void setDetails(List<TndmReferrerFavoriteDetail> details) {
		this.details = details;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}