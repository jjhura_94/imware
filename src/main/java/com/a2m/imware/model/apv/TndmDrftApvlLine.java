package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;

public class TndmDrftApvlLine implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idApvlLine;
	
	private String drftDocNo;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String sancUserUid;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String sancType;
	
	@NotNull(message = ValidationCode.NotNull)
	@Positive(message = ValidationCode.Positive)
	private Integer sancOrdNo;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String sancDutyCode;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 255, message = ValidationCode.Size)
	private String sancDutyName;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 20, message = ValidationCode.Size)
	private String sancDeptCode;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 255, message = ValidationCode.Size)
	private String sancDeptName;
	
	@NotNull(message = ValidationCode.NotNull)
	@NotBlank(message = ValidationCode.NotBlank)
	@Size(max = 1, message = ValidationCode.Size)
	private String sancYn;
	
	private Date sancDate;

	private String docNo;
	
	private String name;


	public TndmDrftApvlLine() {

	}
	
	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	// --- DATABASE MAPPING : ID (BIGINT)
	
	
	// --- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
	public void setDrftDocNo(String drftDocNo) {
		this.drftDocNo = drftDocNo;
	}

	public Long getIdApvlLine() {
		return idApvlLine;
	}

	public void setIdApvlLine(Long idApvlLine) {
		this.idApvlLine = idApvlLine;
	}

	public String getDrftDocNo() {
		return this.drftDocNo;
	}

	// --- DATABASE MAPPING : SANC_USER_UID (VARCHAR)
	public void setSancUserUid(String sancUserUid) {
		this.sancUserUid = sancUserUid;
	}

	public String getSancUserUid() {
		return this.sancUserUid;
	}

	// --- DATABASE MAPPING : SANC_TYPE (VARCHAR)
	public void setSancType(String sancType) {
		this.sancType = sancType;
	}

	public String getSancType() {
		return this.sancType;
	}

	// --- DATABASE MAPPING : SANC_ORD_NO (INT)
	public void setSancOrdNo(Integer sancOrdNo) {
		this.sancOrdNo = sancOrdNo;
	}

	public Integer getSancOrdNo() {
		return this.sancOrdNo;
	}

	// --- DATABASE MAPPING : SANC_DUTY_CODE (VARCHAR)
	public void setSancDutyCode(String sancDutyCode) {
		this.sancDutyCode = sancDutyCode;
	}

	public String getSancDutyCode() {
		return this.sancDutyCode;
	}

	// --- DATABASE MAPPING : SANC_DUTY_NAME (VARCHAR)
	public void setSancDutyName(String sancDutyName) {
		this.sancDutyName = sancDutyName;
	}

	public String getSancDutyName() {
		return this.sancDutyName;
	}

	// --- DATABASE MAPPING : SANC_DEPT_CODE (VARCHAR)
	public void setSancDeptCode(String sancDeptCode) {
		this.sancDeptCode = sancDeptCode;
	}

	public String getSancDeptCode() {
		return this.sancDeptCode;
	}

	// --- DATABASE MAPPING : SANC_DEPT_NAME (VARCHAR)
	public void setSancDeptName(String sancDeptName) {
		this.sancDeptName = sancDeptName;
	}

	public String getSancDeptName() {
		return this.sancDeptName;
	}

	// --- DATABASE MAPPING : SANC_YN (VARCHAR)
	public void setSancYn(String sancYn) {
		this.sancYn = sancYn;
	}

	public String getSancYn() {
		return this.sancYn;
	}

	// --- DATABASE MAPPING : SANC_DATE (TIMESTAMP)
	public void setSancDate(Date sancDate) {
		this.sancDate = sancDate;
	}

	public Date getSancDate() {
		return this.sancDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TndmDrftApvlLine [idApvlLine=" + idApvlLine + ", drftDocNo=" + drftDocNo + ", sancUserUid="
				+ sancUserUid + ", sancType=" + sancType + ", sancOrdNo=" + sancOrdNo + ", sancDutyCode=" + sancDutyCode
				+ ", sancDutyName=" + sancDutyName + ", sancDeptCode=" + sancDeptCode + ", sancDeptName=" + sancDeptName
				+ ", sancYn=" + sancYn + ", sancDate=" + sancDate + ", docNo=" + docNo + ", name="
				+ name + "]";
	}
	
	

}
