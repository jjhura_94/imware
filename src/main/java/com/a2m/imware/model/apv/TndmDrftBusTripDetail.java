/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmDrftBusTripDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long busTripDetailId; 
    @Size(max = 100, message = ValidationCode.Size)
    private String traveler; 
    
    private Date applicationDate; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double oilPrice; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double transportationCost; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double dailyExpenses; 
    @Size(max = 10, message = ValidationCode.Size)
    private Integer dailyExpensesDays; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double foodExpenses; 
    @Size(max = 10, message = ValidationCode.Size)
    private Integer foodExpensesDays; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double roomCharge; 
    @Size(max = 20, message = ValidationCode.Size)
    private Double etc; 
    @Size(max = 30, message = ValidationCode.Size)
    private Double sum; 
    @Size(max = 19, message = ValidationCode.Size)
    private Long busTripId; 
    
    @Size(max = 10, message = ValidationCode.Size)
    private Double exchangeRateDaily; 
    @Size(max = 10, message = ValidationCode.Size)
    private Integer roomChargeDays; 
    @Size(max = 10, message = ValidationCode.Size)
    private Double exchangeRateFood; 
    @Size(max = 10, message = ValidationCode.Size)
    private Double exchangeRateRoom;
    
    private Double exchangeRate;
    
    private String dutyName;
    
    private Double sumBefore;
    
    private String travelerDept;


    public TndmDrftBusTripDetail() {
       
	}


    //--- DATABASE MAPPING : BUS_TRIP_DETAIL_ID (BIGINT)
    public void setBusTripDetailId(Long busTripDetailId) {
        this.busTripDetailId = busTripDetailId;
    }

    public Long getBusTripDetailId() {
        return this.busTripDetailId;
    }


    //--- DATABASE MAPPING : APPLICATION_DATE (DATETIME)
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Date getApplicationDate() {
        return this.applicationDate;
    }

    //--- DATABASE MAPPING : OIL_PRICE (DOUBLE)
    public void setOilPrice(Double oilPrice) {
        this.oilPrice = oilPrice;
    }

    public Double getOilPrice() {
        return this.oilPrice;
    }

    //--- DATABASE MAPPING : TRANSPORTATION_COST (DOUBLE)
    public void setTransportationCost(Double transportationCost) {
        this.transportationCost = transportationCost;
    }

    public Double getTransportationCost() {
        return this.transportationCost;
    }

    //--- DATABASE MAPPING : DAILY_EXPENSES (DOUBLE)
    public void setDailyExpenses(Double dailyExpenses) {
        this.dailyExpenses = dailyExpenses;
    }

    public Double getDailyExpenses() {
        return this.dailyExpenses;
    }

    
    //--- DATABASE MAPPING : FOOD_EXPENSES (DOUBLE)
    public void setFoodExpenses(Double foodExpenses) {
        this.foodExpenses = foodExpenses;
    }

    public Double getFoodExpenses() {
        return this.foodExpenses;
    }
   

    //--- DATABASE MAPPING : ROOM_CHARGE (DOUBLE)
    public void setRoomCharge(Double roomCharge) {
        this.roomCharge = roomCharge;
    }

    public Double getRoomCharge() {
        return this.roomCharge;
    }

    //--- DATABASE MAPPING : ETC (DOUBLE)
    public void setEtc(Double etc) {
        this.etc = etc;
    }

    public Double getEtc() {
        return this.etc;
    }

    //--- DATABASE MAPPING : SUM (DOUBLE)
    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Double getSum() {
        return this.sum;
    }

    //--- DATABASE MAPPING : BUS_TRIP_ID (BIGINT)
    public void setBusTripId(Long busTripId) {
        this.busTripId = busTripId;
    }

    public Long getBusTripId() {
        return this.busTripId;
    }


	public String getTraveler() {
		return traveler;
	}


	public void setTraveler(String traveler) {
		this.traveler = traveler;
	}


	public Integer getDailyExpensesDays() {
		return dailyExpensesDays;
	}


	public void setDailyExpensesDays(Integer dailyExpensesDays) {
		this.dailyExpensesDays = dailyExpensesDays;
	}


	public Integer getFoodExpensesDays() {
		return foodExpensesDays;
	}


	public void setFoodExpensesDays(Integer foodExpensesDays) {
		this.foodExpensesDays = foodExpensesDays;
	}

	public Integer getRoomChargeDays() {
		return roomChargeDays;
	}


	public void setRoomChargeDays(Integer roomChargeDays) {
		this.roomChargeDays = roomChargeDays;
	}


	public Double getExchangeRateDaily() {
		return exchangeRateDaily;
	}


	public void setExchangeRateDaily(Double exchangeRateDaily) {
		this.exchangeRateDaily = exchangeRateDaily;
	}


	public Double getExchangeRateFood() {
		return exchangeRateFood;
	}


	public void setExchangeRateFood(Double exchangeRateFood) {
		this.exchangeRateFood = exchangeRateFood;
	}


	public Double getExchangeRateRoom() {
		return exchangeRateRoom;
	}


	public void setExchangeRateRoom(Double exchangeRateRoom) {
		this.exchangeRateRoom = exchangeRateRoom;
	}


	public Double getExchangeRate() {
		return exchangeRate;
	}


	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public String getDutyName() {
		return dutyName;
	}


	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}


	public Double getSumBefore() {
		return sumBefore;
	}


	public void setSumBefore(Double sumBefore) {
		this.sumBefore = sumBefore;
	}


	public String getTravelerDept() {
		return travelerDept;
	}


	public void setTravelerDept(String travelerDept) {
		this.travelerDept = travelerDept;
	}
    
	

}