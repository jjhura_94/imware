package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;

public class TndmApvlHist implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id; 

    private String drftDocNo; 

    private String sancUserUid; 

    private String sancYn; 

    private Date sancDate; 

    private String comment; 
    
    private String nameKr; 
    
    private String dutyName; 
    
    private String deptNm; 
    
    private String docStatus;
    
    private String method;

    public String getMethod() {
		return method;
	}


	public void setMethod(String method) {
		this.method = method;
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
    public void setDrftDocNo(String drftDocNo) {
        this.drftDocNo = drftDocNo;
    }

    public String getDrftDocNo() {
        return this.drftDocNo;
    }

    //--- DATABASE MAPPING : SANC_USER_UID (VARCHAR)
    public void setSancUserUid(String sancUserUid) {
        this.sancUserUid = sancUserUid;
    }

    public String getSancUserUid() {
        return this.sancUserUid;
    }

    //--- DATABASE MAPPING : SANC_YN (VARCHAR)
    public void setSancYn(String sancYn) {
        this.sancYn = sancYn;
    }

    public String getSancYn() {
        return this.sancYn;
    }

    //--- DATABASE MAPPING : SANC_DATE (TIMESTAMP)
    public void setSancDate(Date sancDate) {
        this.sancDate = sancDate;
    }

    public Date getSancDate() {
        return this.sancDate;
    }

    //--- DATABASE MAPPING : COMMENT (LONGTEXT)
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return this.comment;
    }


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}


	public String getDutyName() {
		return dutyName;
	}


	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}


	public String getDeptNm() {
		return deptNm;
	}


	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}


	public String getDocStatus() {
		return docStatus;
	}


	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

    
}
