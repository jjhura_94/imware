/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;

public class TndmDrftBusTrip implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String drftDocNo;
	
	private String typeOfBusinessTrip;

	private Date fromDate;

	private Date toDate;

	private String country;
	@Size(max = 255, message = ValidationCode.Size)
	private String purpose;
	private Double transportationCost;
	private Double workingMoney;
	private Double foodMoney;
	private Double roomCharge;
	private Double etc;
	@Size(max = 100, message = ValidationCode.Size)
	private String traveler;
	private Double oneWay;
	@Size(max = 1, message = ValidationCode.Size)
	private String twoWay;
	private Double toll;

	@Size(max = 1, message = ValidationCode.Size)
	private String busTripDomestic;
	
	@Size(max = 255, message = ValidationCode.Size)
	private String destination;
	
	private Date term;
	@Size(max = 255, message = ValidationCode.Size)
	private String visitingAgency;

	public TndmDrftBusTrip() {

	}

	// --- DATABASE MAPPING : BUS_TRIP_ID (BIGINT)

	// --- DATABASE MAPPING : DRFT_DOC_NO (VARCHAR)
	public void setDrftDocNo(String drftDocNo) {
		this.drftDocNo = drftDocNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDrftDocNo() {
		return this.drftDocNo;
	}

	// --- DATABASE MAPPING : TYPE_OF_BUSINESS_TRIP (VARCHAR)
	public void setTypeOfBusinessTrip(String typeOfBusinessTrip) {
		this.typeOfBusinessTrip = typeOfBusinessTrip;
	}

	public String getTypeOfBusinessTrip() {
		return this.typeOfBusinessTrip;
	}

	// --- DATABASE MAPPING : FROM_DATE (DATETIME)
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	// --- DATABASE MAPPING : TO_DATE (DATETIME)
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	// --- DATABASE MAPPING : COUNTRY (VARCHAR)
	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return this.country;
	}

	// --- DATABASE MAPPING : PURPOSE (VARCHAR)
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPurpose() {
		return this.purpose;
	}

	// --- DATABASE MAPPING : TRANSPORTATION_COST (DOUBLE)
	public void setTransportationCost(Double transportationCost) {
		this.transportationCost = transportationCost;
	}

	public Double getTransportationCost() {
		return this.transportationCost;
	}

	// --- DATABASE MAPPING : WORKING_MONEY (DOUBLE)
	public void setWorkingMoney(Double workingMoney) {
		this.workingMoney = workingMoney;
	}

	public Double getWorkingMoney() {
		return this.workingMoney;
	}

	// --- DATABASE MAPPING : FOOD_MONEY (DOUBLE)
	public void setFoodMoney(Double foodMoney) {
		this.foodMoney = foodMoney;
	}

	public Double getFoodMoney() {
		return this.foodMoney;
	}

	// --- DATABASE MAPPING : ROOM_CHARGE (DOUBLE)
	public void setRoomCharge(Double roomCharge) {
		this.roomCharge = roomCharge;
	}

	public Double getRoomCharge() {
		return this.roomCharge;
	}

	// --- DATABASE MAPPING : ETC (DOUBLE)
	public void setEtc(Double etc) {
		this.etc = etc;
	}

	public Double getEtc() {
		return this.etc;
	}

	// --- DATABASE MAPPING : TRAVELER (VARCHAR)
	public void setTraveler(String traveler) {
		this.traveler = traveler;
	}

	public String getTraveler() {
		return this.traveler;
	}

	// --- DATABASE MAPPING : ONE_WAY (DOUBLE)
	public void setOneWay(Double oneWay) {
		this.oneWay = oneWay;
	}

	public Double getOneWay() {
		return this.oneWay;
	}

	// --- DATABASE MAPPING : RETURN (VARCHAR)

	// --- DATABASE MAPPING : TOLL (DOUBLE)
	public void setToll(Double toll) {
		this.toll = toll;
	}

	public String getTwoWay() {
		return twoWay;
	}

	public void setTwoWay(String twoWay) {
		this.twoWay = twoWay;
	}

	public Double getToll() {
		return this.toll;
	}

	public String getBusTripDomestic() {
		return busTripDomestic;
	}

	public void setBusTripDomestic(String busTripDomestic) {
		this.busTripDomestic = busTripDomestic;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getTerm() {
		return term;
	}

	public void setTerm(Date term) {
		this.term = term;
	}

	public String getVisitingAgency() {
		return visitingAgency;
	}

	public void setVisitingAgency(String visitingAgency) {
		this.visitingAgency = visitingAgency;
	}
	
	

}