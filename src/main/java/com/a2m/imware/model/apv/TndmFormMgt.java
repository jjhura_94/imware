/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import com.a2m.imware.validation.ValidationCode;

import org.hibernate.validator.constraints.Range;

import com.a2m.imware.validation.ValidationCode;


public class TndmFormMgt implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Long id; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 255, message = ValidationCode.Size)
    private String formName; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String formContent; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String formCategory; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String formType; 
    @Size(max = 255, message = ValidationCode.Size)
    private String description; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 19, message = ValidationCode.Size)
    private Date createdDate; 
    @Size(max = 20, message = ValidationCode.Size)
    private String updatedBy; 
    @Size(max = 19, message = ValidationCode.Size)
    private Date updatedDate; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 1, message = ValidationCode.Size)
    private String useYn; 

    public TndmFormMgt() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : FORM_NAME (VARCHAR)
    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormName() {
        return this.formName;
    }

    //--- DATABASE MAPPING : FORM_CONTENT (LONGTEXT)
    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }

    public String getFormContent() {
        return this.formContent;
    }

    //--- DATABASE MAPPING : FORM_CATEGORY (VARCHAR)
    public void setFormCategory(String formCategory) {
        this.formCategory = formCategory;
    }

    public String getFormCategory() {
        return this.formCategory;
    }

    //--- DATABASE MAPPING : FORM_TYPE (VARCHAR)
    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormType() {
        return this.formType;
    }

    //--- DATABASE MAPPING : DESCRIPTION (VARCHAR)
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : UPDATED_BY (VARCHAR)
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : UPDATED_DATE (TIMESTAMP)
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getUpdatedDate() {
        return this.updatedDate;
    }

    //--- DATABASE MAPPING : USE_YN (VARCHAR)
    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getUseYn() {
        return this.useYn;
    }


}