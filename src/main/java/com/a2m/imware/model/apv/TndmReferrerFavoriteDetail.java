/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import com.a2m.imware.validation.ValidationCode;

import org.hibernate.validator.constraints.Range;

import com.a2m.imware.validation.ValidationCode;


public class TndmReferrerFavoriteDetail implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private Long idDetail; 
    @Size(max = 100, message = ValidationCode.Size)
    private String userUid; 
   
    private Long favoriteId; 
    
    private String empNo;

	private String nameKr;
	private String nameEn;
	private String nameCh;
	private String userId;
	private String deptNm;
	private String dutyName;

       
	public TndmReferrerFavoriteDetail() {
	}

    //--- DATABASE MAPPING : ID (BIGINT)
    public Long getIdDetail() {
		return idDetail;
	}


	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}

    //--- DATABASE MAPPING : USER_UID (VARCHAR)
    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }


	public String getUserUid() {
        return this.userUid;
    }

    //--- DATABASE MAPPING : FAVORITE_ID (BIGINT)
    public void setFavoriteId(Long favoriteId) {
        this.favoriteId = favoriteId;
    }

    public Long getFavoriteId() {
        return this.favoriteId;
    }

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getNameKr() {
		return nameKr;
	}

	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getNameCh() {
		return nameCh;
	}

	public void setNameCh(String nameCh) {
		this.nameCh = nameCh;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}


}