/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.a2m.imware.validation.ValidationCode;


public class TndmApvlLineFavoriteDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    
    private Long id; 
    
    private String sancUserUid; 
    
    private String sancType; 
    
    
    private Integer sancOrdNo; 
   
    private String sancDutyCode; 
    
    private String sancDutyName; 
   
    private String sancDeptCode; 
    
    private String sancDeptName; 
 
    private Date createdDate; 
   
    private Long favoriteId; 
    
    private String nameKr;
    
	private String nameEn;
	
	private String nameCh;
	

    public TndmApvlLineFavoriteDetail() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : SANC_USER_UID (VARCHAR)
    public void setSancUserUid(String sancUserUid) {
        this.sancUserUid = sancUserUid;
    }

    public String getSancUserUid() {
        return this.sancUserUid;
    }

    //--- DATABASE MAPPING : SANC_TYPE (VARCHAR)
    public void setSancType(String sancType) {
        this.sancType = sancType;
    }

    public String getSancType() {
        return this.sancType;
    }

    //--- DATABASE MAPPING : SANC_ORD_NO (INT)
    public void setSancOrdNo(Integer sancOrdNo) {
        this.sancOrdNo = sancOrdNo;
    }

    public Integer getSancOrdNo() {
        return this.sancOrdNo;
    }

    //--- DATABASE MAPPING : SANC_DUTY_CODE (VARCHAR)
    public void setSancDutyCode(String sancDutyCode) {
        this.sancDutyCode = sancDutyCode;
    }

    public String getSancDutyCode() {
        return this.sancDutyCode;
    }

    //--- DATABASE MAPPING : SANC_DUTY_NAME (VARCHAR)
    public void setSancDutyName(String sancDutyName) {
        this.sancDutyName = sancDutyName;
    }

    public String getSancDutyName() {
        return this.sancDutyName;
    }

    //--- DATABASE MAPPING : SANC_DEPT_CODE (VARCHAR)
    public void setSancDeptCode(String sancDeptCode) {
        this.sancDeptCode = sancDeptCode;
    }

    public String getSancDeptCode() {
        return this.sancDeptCode;
    }

    //--- DATABASE MAPPING : SANC_DEPT_NAME (VARCHAR)
    public void setSancDeptName(String sancDeptName) {
        this.sancDeptName = sancDeptName;
    }

    public String getSancDeptName() {
        return this.sancDeptName;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : FAVORITE_ID (BIGINT)
    public void setFavoriteId(Long favoriteId) {
        this.favoriteId = favoriteId;
    }

    public Long getFavoriteId() {
        return this.favoriteId;
    }


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}


	public String getNameEn() {
		return nameEn;
	}


	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	public String getNameCh() {
		return nameCh;
	}


	public void setNameCh(String nameCh) {
		this.nameCh = nameCh;
	}

    

}