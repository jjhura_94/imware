/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.apv;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class TndmApvlLineFavorite extends TndmApvlLineFavoriteDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    
    private Long id; 
    
    private String createdBy; 
    
    private String name; 

    private Date createdDate; 
    
    private Long idDetail;
    
    private List<TndmApvlLineFavoriteDetail> apvlLineFavoriteDetails;

    public TndmApvlLineFavorite() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : NAME (VARCHAR)
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }


	public Long getIdDetail() {
		return idDetail;
	}


	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}


	public List<TndmApvlLineFavoriteDetail> getApvlLineFavoriteDetails() {
		return apvlLineFavoriteDetails;
	}


	public void setApvlLineFavoriteDetails(List<TndmApvlLineFavoriteDetail> apvlLineFavoriteDetails) {
		this.apvlLineFavoriteDetails = apvlLineFavoriteDetails;
	}

	
    

}