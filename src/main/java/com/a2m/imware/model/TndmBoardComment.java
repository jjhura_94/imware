/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 *
 * @author Admin
 */
public class TndmBoardComment extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;
    @MapKey("ID")
    private Long id;

    @MapKey("BOARD_ID")
    private Long noticeId;

    @MapKey("CONTENT")
    private String content;

    @MapKey("UP_COMMENT_ID")
    private Long upCommentId;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("IS_EDITED")
    private Boolean isEdited;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("NAME_EN")
    private String nameEn;

    @MapKey("NAME_CH")
    private String nameCh;

    @MapKey("IMG_PATH")
    private String imgPath;

    private String deptName;
    private String position;

    List<TndmBoardComment> children;

    public TndmBoardComment() {
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUpCommentId() {
        return upCommentId;
    }

    public void setUpCommentId(Long upCommentId) {
        this.upCommentId = upCommentId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameCh() {
        return nameCh;
    }

    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public List<TndmBoardComment> getChildren() {
        return children;
    }

    public void setChildren(List<TndmBoardComment> children) {
        this.children = children;
    }

}
