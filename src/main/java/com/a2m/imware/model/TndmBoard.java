/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.model;

import com.a2m.imware.util.Pager;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * Map with TNDM_BOARD table.
 *
 * @author Admin
 */
public class TndmBoard extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ID")
    private Long id;

    @MapKey("BOARD_TITLE")
    private String boardTitle;

    @MapKey("CONTENT")
    private String content;

    @MapKey("PRIVACY")
    private String privacy;

    @MapKey("BOARD_TYPE")
    private String boardType;

    @MapKey("DEPT_CODE")
    private String deptCode;

    @MapKey("DEPT_NAME")
    private String deptName;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("UPDATE_BY")
    private String updateby;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("UPDATE_DATE")
    private Date updateDate;

    @MapKey("IS_EDITED")
    private Boolean isEdited;

    @MapKey("VIEW_COUNT")
    private int viewCount;

    @MapKey("typeData")
    private String typeData;

    private Pager pager;

    private List<TccoFile> attachment;

    private List<TccoFile> filesToAdd;

    private List<TccoFile> filesToDelete;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("NAME_EN")
    private String nameEn;

    @MapKey("NAME_CH")
    private String nameCh;

    @MapKey("CREATE_TIME_FROM")
    private String createTimeFrom;
    @MapKey("CREATE_TIME_TO")
    private String createTimeTo;
    @MapKey("TIME_TYPE")
    private int timeType;
    @MapKey("USER_IDS")
    private String userIds;

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public String getCreateTimeFrom() {
        return createTimeFrom;
    }

    public void setCreateTimeFrom(String createTimeFrom) {
        this.createTimeFrom = createTimeFrom;
    }

    public String getCreateTimeTo() {
        return createTimeTo;
    }

    public void setCreateTimeTo(String createTimeTo) {
        this.createTimeTo = createTimeTo;
    }

    public int getTimeType() {
        return timeType;
    }

    public void setTimeType(int timeType) {
        this.timeType = timeType;
    }

    public String getTypeData() {
        return typeData;
    }

    public void setTypeData(String typeData) {
        this.typeData = typeData;
    }

    public TndmBoard() {
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<TccoFile> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<TccoFile> attachment) {
        this.attachment = attachment;
    }

    public List<TccoFile> getFilesToAdd() {
        return filesToAdd;
    }

    public void setFilesToAdd(List<TccoFile> filesToAdd) {
        this.filesToAdd = filesToAdd;
    }

    public List<TccoFile> getFilesToDelete() {
        return filesToDelete;
    }

    public void setFilesToDelete(List<TccoFile> filesToDelete) {
        this.filesToDelete = filesToDelete;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBoardTitle() {
        return boardTitle;
    }

    public void setBoardTitle(String boardTitle) {
        this.boardTitle = boardTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameCh() {
        return nameCh;
    }

    public void setNameCh(String nameCh) {
        this.nameCh = nameCh;
    }

}
