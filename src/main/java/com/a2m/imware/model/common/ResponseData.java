package com.a2m.imware.model.common;

public class ResponseData<T> {

	private String message;

	private boolean error;

	private T data;

	public ResponseData(String message, boolean error, T data) {
		super();
		this.message = message;
		this.error = error;
		this.data = data;
	}

	public static <T> ResponseData<T> success(String message, T data) {
		return new ResponseData<>(message, false, data);
	}

	public static <T> ResponseData<T> success(T data) {
		return new ResponseData<>(null, false, data);
	}
	
	public static ResponseData<String> success() {
		return new ResponseData<>(null, false, "Success");
	}

	public static <T> ResponseData<T> error(String message, T data) {
		return new ResponseData<>(message, true, data);
	}

	public static <T> ResponseData<T> error(T data) {
		return new ResponseData<>(null, true, data);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
