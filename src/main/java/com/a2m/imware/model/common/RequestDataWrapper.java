package com.a2m.imware.model.common;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

public class RequestDataWrapper<T> {

	@Valid
	private List<T> listForUpdate;
	@Valid
	private List<T> listForCreate;

	private Map<String, String> additionalParams;
	private List<String> idListForDelete;

	public List<T> getListForUpdate() {
		return listForUpdate;
	}

	public void setListForUpdate(List<T> listForUpdate) {
		this.listForUpdate = listForUpdate;
	}

	public List<T> getListForCreate() {
		return listForCreate;
	}

	public void setListForCreate(List<T> listForCreate) {
		this.listForCreate = listForCreate;
	}

	public Map<String, String> getAdditionalParams() {
		return additionalParams;
	}

	public void setAdditionalParams(Map<String, String> additionalParams) {
		this.additionalParams = additionalParams;
	}

	public List<String> getIdListForDelete() {
		return idListForDelete;
	}

	public void setIdListForDelete(List<String> idListForDelete) {
		this.idListForDelete = idListForDelete;
	}

}
