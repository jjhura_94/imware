/*
 * Copyright (C) Hitachi Distribution Software Co., Ltd. 2018.
 */
package com.a2m.imware.model.common;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import com.a2m.imware.validation.ValidationCode;
import personal.aug.convert.MapAndObjectConversion;


public class TndmComment extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; 
   
    private String postId; 
    @Size(max = 19, message = ValidationCode.Size)
    private Long upCommentId; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 2147483647, message = ValidationCode.Size)
    private String commentContent; 
    @NotNull(message = ValidationCode.NotNull)
    @NotBlank(message = ValidationCode.NotBlank)
    @Size(max = 20, message = ValidationCode.Size)
    private String createdBy; 
   
    private Date createdDate; 
    @Size(max = 1, message = ValidationCode.Size)
    private Boolean isEdited; 
    @Size(max = 100, message = ValidationCode.Size)
    private String tableName; 
    
    private Date updatedDate; 
    
    private String nameCh; 
    
    private String nameEn; 
    
    private String nameKr; 

    public TndmComment() {
       
	}


    //--- DATABASE MAPPING : ID (BIGINT)
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : TASK_ID (BIGINT)
    
    public String getPostId() {
		return postId;
	}


	public void setPostId(String postId) {
		this.postId = postId;
	}
    //--- DATABASE MAPPING : UP_COMMENT_ID (BIGINT)
    public void setUpCommentId(Long upCommentId) {
        this.upCommentId = upCommentId;
    }

   

	public Long getUpCommentId() {
        return this.upCommentId;
    }

    //--- DATABASE MAPPING : CONTENT (LONGTEXT)
	 public String getCommentContent() {
			return commentContent;
		}


	public void setCommentContent(String commentContent) {
			this.commentContent = commentContent;
	}

    //--- DATABASE MAPPING : CREATED_BY (VARCHAR)
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

   
	public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : CREATED_DATE (TIMESTAMP)
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    //--- DATABASE MAPPING : IS_EDITED (BIT)
    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Boolean getIsEdited() {
        return this.isEdited;
    }

    //--- DATABASE MAPPING : TABLE_NAME (VARCHAR)
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return this.tableName;
    }


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public String getNameCh() {
		return nameCh;
	}


	public void setNameCh(String nameCh) {
		this.nameCh = nameCh;
	}


	public String getNameEn() {
		return nameEn;
	}


	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	public String getNameKr() {
		return nameKr;
	}


	public void setNameKr(String nameKr) {
		this.nameKr = nameKr;
	}
    
	

}