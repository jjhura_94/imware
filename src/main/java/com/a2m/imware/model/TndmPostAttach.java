package com.a2m.imware.model;

import java.io.Serializable;

import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 * Mapping with TNDM_POST_ATTACH table.
 * 
 * @author HungDM
 *
 */
public class TndmPostAttach extends MapAndObjectConversion implements Serializable {

	private static final long serialVersionUID = 1L;

	@MapKey("ID")
	private Long id;
	
	@MapKey("POST_ID")
	private Long postId;
	
	@MapKey("POST_TYPE")
	private Integer postType;
	
	@MapKey("ATCH_FLE_SEQ")
	private String atchFleSeq;

	public TndmPostAttach() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Integer getPostType() {
		return postType;
	}

	public void setPostType(Integer postType) {
		this.postType = postType;
	}

	public String getAtchFleSeq() {
		return atchFleSeq;
	}

	public void setAtchFleSeq(String atchFleSeq) {
		this.atchFleSeq = atchFleSeq;
	}

}
