/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 *
 * @author Admin
 */
public class TndmBusinessCard extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ID")
    private Long id;

    @MapKey("NAME_KR")
    private String nameKr;

    @MapKey("NAME_EN")
    private String nameEn;

    @MapKey("DEPT_NM_KR")
    private String deptKr;

    @MapKey("DEPT_NM_EN")
    private String deptEn;

    @MapKey("POSTION_KR")
    private String positionKr;

    @MapKey("POSITION_EN")
    private String positionEn;

    @MapKey("DUTY_KR")
    private String dutyKr;

    @MapKey("DUTY_EN")
    private String dutyEn;

    @MapKey("PHONE_NUMBER")
    private String phone;

    @MapKey("EXT_NUMBER")
    private String ext;

    @MapKey("FAX_NUMBER")
    private String fax;

    @MapKey("MOBILE_NUMBER")
    private String mobile;

    @MapKey("EMAIL")
    private String email;

    @MapKey("NOTE")
    private String note;

    @MapKey("STATUS")
    private String status;

    private String statusName;

    @MapKey("CREATED_BY")
    private String createdBy;

    @MapKey("CREATED_DATE")
    private Date createdDate;

    @MapKey("UPDATED_BY")
    private String updateBy;

    @MapKey("UPDATED_DATE")
    private Date updatedDate;

    private List<TccoFile> attachment;

    private List<TccoFile> filesToAdd;

    private List<TccoFile> filesToDelete;

    public List<TccoFile> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<TccoFile> attachment) {
        this.attachment = attachment;
    }

    public List<TccoFile> getFilesToAdd() {
        return filesToAdd;
    }

    public void setFilesToAdd(List<TccoFile> filesToAdd) {
        this.filesToAdd = filesToAdd;
    }

    public List<TccoFile> getFilesToDelete() {
        return filesToDelete;
    }

    public void setFilesToDelete(List<TccoFile> filesToDelete) {
        this.filesToDelete = filesToDelete;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDeptKr() {
        return deptKr;
    }

    public void setDeptKr(String deptKr) {
        this.deptKr = deptKr;
    }

    public String getDeptEn() {
        return deptEn;
    }

    public void setDeptEn(String deptEn) {
        this.deptEn = deptEn;
    }

    public String getPositionKr() {
        return positionKr;
    }

    public void setPositionKr(String positionKr) {
        this.positionKr = positionKr;
    }

    public String getPositionEn() {
        return positionEn;
    }

    public void setPositionEn(String positionEn) {
        this.positionEn = positionEn;
    }

    public String getDutyKr() {
        return dutyKr;
    }

    public void setDutyKr(String dutyKr) {
        this.dutyKr = dutyKr;
    }

    public String getDutyEn() {
        return dutyEn;
    }

    public void setDutyEn(String dutyEn) {
        this.dutyEn = dutyEn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
