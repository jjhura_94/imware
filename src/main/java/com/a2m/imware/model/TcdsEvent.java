/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.model;

import java.io.Serializable;
import java.util.Date;
import personal.aug.convert.MapAndObjectConversion;
import personal.aug.convert.annotations.MapKey;

/**
 *
 * @author Admin
 */
public class TcdsEvent extends MapAndObjectConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @MapKey("ID")
    private Long id;

    @MapKey("EVENT_NAME")
    private String eventName;

    @MapKey("EVENT_TYPE")
    private String eventType;

    @MapKey("SHARE_TYPE")
    private String shareType;

    @MapKey("FROM_DATE")
    private Date fromDate;

    @MapKey("TO_DATE")
    private Date toDate;

    @MapKey("ALARM_DATE")
    private Date alarmDate;

    @MapKey("CONTENT")
    private String content;

    @MapKey("PLACE")
    private String place;

    @MapKey("USER_ID")
    private String userId;

    @MapKey("CREATE_DATE")
    private Date createdDate;

    @MapKey("UPDATED_DATE")
    private Date updatedDate;

    private String shareName;

    @MapKey("FROM_DATE_STRING")
    private String fromDateString;

    @MapKey("TO_DATE_STRING")
    private String toDateString;

    @MapKey("ALARM_DATE_STRING")
    private String alarmDateString;

    @MapKey("NAME_KR")
    private String nameKr;

    private Long fromLong;
    private Long toLong;
    private long alarmLong;

    public long getAlarmLong() {
        return alarmLong;
    }

    public void setAlarmLong(long alarmLong) {
        this.alarmLong = alarmLong;
    }

    public String getAlarmDateString() {
        return alarmDateString;
    }

    public void setAlarmDateString(String alarmDateString) {
        this.alarmDateString = alarmDateString;
    }

    public Date getAlarmDate() {
        return alarmDate;
    }

    public void setAlarmDate(Date alarmDate) {
        this.alarmDate = alarmDate;
    }

    public Long getFromLong() {
        return fromLong;
    }

    public void setFromLong(Long fromLong) {
        this.fromLong = fromLong;
    }

    public Long getToLong() {
        return toLong;
    }

    public void setToLong(Long toLong) {
        this.toLong = toLong;
    }

    public String getNameKr() {
        return nameKr;
    }

    public void setNameKr(String nameKr) {
        this.nameKr = nameKr;
    }

    public String getFromDateString() {
        return fromDateString;
    }

    public void setFromDateString(String fromDateString) {
        this.fromDateString = fromDateString;
    }

    public String getToDateString() {
        return toDateString;
    }

    public void setToDateString(String toDateString) {
        this.toDateString = toDateString;
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
