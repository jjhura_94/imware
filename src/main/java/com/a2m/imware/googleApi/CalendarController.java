package com.a2m.imware.googleApi;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/calendarAPI")
public class CalendarController {

	private static Logger logger = LoggerFactory.getLogger(CalendarController.class);

	@Autowired
	private CalendarService calendarService;

	@PostMapping("/updateCalendar")
	public void updateCalendar() {
		try {
			calendarService.updateCalendar();
		} catch (IOException e) {
			logger.info("IOException.", e.getMessage());
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			logger.info("GeneralSecurityException.", e.getMessage());
			e.printStackTrace();
		} catch (ParseException e) {
			logger.info("ParseException.", e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			logger.info("SQLException.", e.getMessage());
			e.printStackTrace();
		}
	}

}
