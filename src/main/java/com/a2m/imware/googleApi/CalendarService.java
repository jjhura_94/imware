package com.a2m.imware.googleApi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.TsstDateWorkingDAO;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

@Service
public class CalendarService {

	private static final String APPLICATION_NAME = "CalendarProject";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "/credential/credentials.json";

	@Autowired
	private TsstDateWorkingDAO dateWorkingDAO;

	public void updateCalendar() throws IOException, GeneralSecurityException, ParseException, SQLException {
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		// Iterate over the events in the specified calendar
		String pageToken = null;

		Date toDay = new Date();
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(toDay);
		int yearNow = calendar.get(java.util.Calendar.YEAR);

		// Insert all vacation with 2021 date now (01/01/2021)
		String startDateString = yearNow + "-01-01";
		String endDateString = yearNow + 1 + "-12-31";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateTime startDate = new DateTime(formatter.parse(startDateString).getTime());
		DateTime endDate = new DateTime(formatter.parse(endDateString).getTime());

		do {
			Events events = service.events().list("vi.south_korea#holiday@group.v.calendar.google.com")
					.setPageToken(pageToken).setTimeMin(startDate).setTimeMax(endDate).execute();
			List<Event> items = events.getItems();
			for (Event event : items) {
				Map<Object, Object> map = new HashMap<Object, Object>();
				System.out.println(event.getSummary());
				if (!event.getSummary().equals("Arbor Day") && !event.getSummary().equals("Parents' Day")
						&& !event.getSummary().equals("Teacher's Day") && !event.getSummary().equals("Constitution Day")
						&& !event.getSummary().equals("Armed Forces Day") && !event.getSummary().equals("Christmas Eve")
						&& !event.getSummary().equals("New Year's Eve")
						&& !event.getSummary().equals("Temporary Holiday")) {
					map.put("DATE_WORKING", event.getStart().getDate().toString());
					map.put("NAME", event.getSummary());
					map.put("IS_WORKING_DAY", 0);
					dateWorkingDAO.insertTsstDateWorking(map);
					System.out.println("OK");
				} else {

					System.out.println("NO");
				}
				System.out.println(event);
			}
			pageToken = events.getNextPageToken();
		} while (pageToken != null);
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = CalendarController.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8090).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}
}
