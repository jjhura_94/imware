package com.a2m.imware.interceptor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.common.menu.MenuPermission;
import com.a2m.imware.common.menu.MenuPermissionType;
import com.a2m.imware.model.util.ImwareException;

public class MenuPermissionInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if(handler instanceof HandlerMethod) {
			HandlerMethod handlerMethod = (HandlerMethod) handler;
			Method method = handlerMethod.getMethod();
			if (method != null && method.isAnnotationPresent(MenuPermission.class)) {
				MenuPermission mpAnnotation = method.getAnnotation(MenuPermission.class);
				MenuPermissionType[] requiredPermissions = mpAnnotation.permissions();
				boolean matchAny = mpAnnotation.matchAny();

				Class<?> clazz = method.getDeclaringClass();
				String menuUrl =  null;
				try {
					Field field = clazz.getDeclaredField(ImwareStringUtils.MENU_URL);
					menuUrl = (String) field.get(null);
				}
				catch (NoSuchFieldException ex) {
					if(clazz.isAnnotationPresent(RequestMapping.class)) {
						RequestMapping rma = clazz.getAnnotation(RequestMapping.class);
						String[] apiUrls = rma.value();
						if(apiUrls != null && apiUrls.length > 0) {
							String apiUrl = apiUrls[0];
							if(apiUrl != null) {
								menuUrl = apiUrl.replace("/api/", ImwareStringUtils.EMPTY);
							}
						}
					}
				}

				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				Map<String, String> authorities = new HashMap<>();
				authentication.getAuthorities().forEach(a -> {
					String authorityString = a.getAuthority();
					String[] authority = authorityString.split(Pattern.quote(ImwareStringUtils.DOLLAR_SIGN));
					authorities.put(authority[0], authority[1]);
				});

				if (menuUrl == null || !checkMenuPermission(requiredPermissions, authorities.get(menuUrl), matchAny)) {
					throw ImwareException.exception(403, "403 Forbidden: You don't have permission to this menu.");
				}
			}
		}
		
		return true;
	}

	private boolean checkMenuPermission(MenuPermissionType[] requiredPermissions, String authority, boolean matchAny) {
		if (authority == null)
			return false;
		if (requiredPermissions == null || requiredPermissions.length == 0)
			return true;

		if (matchAny)
			return checkMenuPermissionMatchAny(requiredPermissions, authority);
		else
			return checkMenuPermissionMatchAll(requiredPermissions, authority);
	}

	private boolean checkMenuPermissionMatchAll(MenuPermissionType[] requiredPermissions, String authority) {
		for (MenuPermissionType menuPermission : requiredPermissions) {
			if (!ImwareStringUtils.YES.equals(String.valueOf(authority.charAt(menuPermission.getOrder() * 2)))) {
				return false;
			}
		}
		return true;
	}

	private boolean checkMenuPermissionMatchAny(MenuPermissionType[] requiredPermissions, String authority) {
		for (MenuPermissionType menuPermission : requiredPermissions) {
			if (ImwareStringUtils.YES.equals(String.valueOf(authority.charAt(menuPermission.getOrder() * 2)))) {
				return true;
			}
		}
		return false;
	}
}
