package com.a2m.imware.schedule;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.a2m.imware.service.cms.Cms0102Service;
import com.a2m.imware.service.cms.Cms010401Service;
import com.a2m.imware.service.cms.Cms0105Service;

@Component
public class Scheduling {

    @Autowired
    private Cms010401Service cms010401Service;

    @Autowired
    private Cms0102Service cms0102Service;
    @Autowired
    private Cms0105Service cms0105Service;

    private static final Logger logger = LoggerFactory.getLogger(Scheduling.class);

//    @Scheduled(fixedRate = 10000)
    @Scheduled(fixedRate = 60000) //30p
    public void scheduleMessageSchedule() {
        try {
            cms0105Service.getListSendMessage();
            logger.info("[Schedule]Send message...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(fixedRate = 60000)
    public void scheduleMessageBookingRoom() {
        try {
            cms010401Service.scheduleMessageBookingRoom();
            logger.info("Send message...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 0 0 1 OCT ?")
    public void scheduleInsertVacationDays() {
        try {
            cms0102Service.insertScheduleVacationDays();
            logger.info("Insert vacation days...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update schedule vacation days
     */
    @Scheduled(cron = "0 0 0 ? * *")
//	@Scheduled(cron = "0 0 18 ? * *")
    public void scheduleUpdateVacationDays() {
        try {
            cms0102Service.updateScheduleVacationDays();
            logger.info("Update vacation days...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
