package com.a2m.imware.validation;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path;

import org.hibernate.validator.internal.engine.path.NodeImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.a2m.imware.model.common.ResponseData;

@ControllerAdvice
@ResponseBody
public class ValidationResponseExceptionHandler extends ResponseEntityExceptionHandler {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		logger.error(" ---------- Validation: ConstraintViolationException ----------");
		logger.error(ex.getConstraintViolations().toString());
		
		Map<Object, Object> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(error -> {
			this.buildErrorMessage(errors, error);
		});

		// Handle for submit List
		// Return error List, not include single property key
		if (errors.size() == 1) {
			Object firstError = null;
			Map.Entry<Object, Object> entry = errors.entrySet().iterator().next();
			firstError = entry.getValue();
			if (firstError instanceof Map) {
				return new ResponseEntity<>(ResponseData.error(ex.getClass().getName(), firstError),
						HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<>(ResponseData.error(ex.getClass().getName(), errors), HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.error("---------- Validation: MethodArgumentNotValidException ----------");
		logger.error(ex.getBindingResult().getAllErrors().toString());
		
		Map<Object, Object> errors = new HashMap<>();

		ex.getBindingResult().getAllErrors().forEach((error) -> {
			ConstraintViolation<?> source = error.unwrap(ConstraintViolation.class);
			this.buildErrorMessage(errors, source);
		});

		return new ResponseEntity<>(ResponseData.error(ex.getClass().getName(), errors), HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		return new ResponseEntity<>(ResponseData.error(ex.getClass().getName(), ex.getMessage()),
				HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings("unchecked")
	private void buildErrorMessage(Map<Object, Object> errorMessages, ConstraintViolation<?> error) {
		PathImpl path = (PathImpl) error.getPropertyPath();
		Iterator<Path.Node> it = path.iterator();
		Map<Object, Object> parent = errorMessages;
		System.out.println(path.toString());
		while (it.hasNext()) {
			Path.Node p = it.next();
			NodeImpl node = (NodeImpl) p;

			// Skip if key is method name
			if (node.getKind() == ElementKind.METHOD)
				continue;

			// Get index of element in List, I don't know why node.getIndex() return parent.index, not index itself
			Integer index = null;
			try {
				Field indexField = NodeImpl.class.getDeclaredField("index");
				indexField.setAccessible(true);
				index = (Integer) indexField.get(node);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			// 
			if (index != null) {
				Object nestedObj = parent.get(node.getName());
				// List but it is a Map, with key is number: 0, 1...
				Map<Object, Object> nestedList = null;
				if (nestedObj != null) {
					nestedList = (Map<Object, Object>) nestedObj;
				} else {
					nestedList = new HashMap<>();
					parent.put(node.getName(), nestedList);
				}
				parent = (Map<Object, Object>) nestedList.get(String.valueOf(index));
				if (parent == null) {
					Map<Object, Object> child = new HashMap<>();
					nestedList.put(String.valueOf(index), child);
					parent = child;
				}
			} else {
				if (node.getValue() != null) {
					boolean isNotSimpleProperty = !BeanUtils.isSimpleProperty(node.getValue().getClass());
					if (isNotSimpleProperty) {
						Object nestedObj = parent.get(node.getName());
						if (nestedObj != null) {
							parent = (Map<Object, Object>) nestedObj;
						} else {
							Map<Object, Object> child = new HashMap<>();
							parent.put(node.getName(), child);
							parent = child;
						}
					} else {
						// Leaf Node
						parent.put(node.getName(), error.getMessage());
					}
				} else {
					// Leaf node
					parent.put(node.getName(), error.getMessage());
				}
			}

		}
	}

}
