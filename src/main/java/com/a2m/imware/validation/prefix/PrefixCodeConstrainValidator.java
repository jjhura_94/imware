package com.a2m.imware.validation.prefix;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PrefixCodeConstrainValidator implements ConstraintValidator<PrefixCode, String>{
	
	private String empCodePrefix;
	
	@Override
	public void initialize(PrefixCode empCode) {
		empCodePrefix = empCode.value();
	}

	@Override
	public boolean isValid(String empCode, ConstraintValidatorContext context) {
		if(empCode != null && empCode.startsWith(empCodePrefix)) {
			return true;
		}
		return false;
	}
}
