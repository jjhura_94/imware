package com.a2m.imware.validation.ynField;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.a2m.imware.common.ImwareStringUtils;

@Constraint(validatedBy = { UseYnConstrainValidator.class })
@Target(value = { ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface YnField {
	
	public String message() default "Error.InvalidYnField";
	
	public Class<?>[] groups() default {};
	
	public Class<? extends Payload>[] payload() default {};
	
}

class UseYnConstrainValidator implements ConstraintValidator<YnField, String>{

	@Override
	public boolean isValid(String ynValue, ConstraintValidatorContext context) {
		if(ImwareStringUtils.YES.equals(ynValue) || ImwareStringUtils.NO.equals(ynValue)) {
			return true;
		}
		return false;
	}
}