package com.a2m.imware.common;

public class ApvConstantUtils {
	// Sanction type
	public static final String SUBMIT = "10-01";
	public static final String REVIEW = "10-02";
	public static final String APPROVE = "10-03";
	public static final String ARBITRARY_APPROVE = "10-04";
	public static final String PROXY_APPROVE = "10-05";
	public static final String PARALLEL_CO_APPROVE = "10-06";
	public static final String ORDERED_CO_APPROVE = "10-07";
	
	public static final String Y = "Y";
	public static final String N = "N";

	/*
	 * - case 1, 4, 3, 5, 7, 8 -> end approval line, update all draft_apvl_line with
	 *   SANC_ORD_NO >= current SANC_ORD_NO
	 * - case 0, 2 is normal case, update only current draft_apvl_line
	 * - case 7, 8, 9 update SANC_TYPE of current draft_apvl_line to PROXY(006-005)
	 * - case 6 insert delegate user into draft_apvl_line with SANC_ORD_NO = current SANC_ORD_NO + 1
	 * 	 and increase SANC_ORD_NO of the rest of the line to 1
	 */
	// Approval Case
	public static final Integer NORMAL_APRROVAL_CASE = 0;
	public static final Integer CUR_LINE_IS_LAST_APVL_LINE = 1;
	public static final Integer TYPE_ARBITRARY = 4;
	public static final Integer TYPE_PROXY_AND_NEXT_APPROVER_IS_ARBITRARY = 3;
	public static final Integer TYPE_PROXY_AND_NEXT_APPROVER_IS_APPROVE = 5;
	public static final Integer TYPE_PROXY_AND_NEXT_APPROVER_IS_NORMAL = 2;
	public static final Integer CUR_USER_IS_NOT_PROXY_AND_DELEGATE = 6;
	public static final Integer CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_ARBITRARY = 7;
	public static final Integer CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_APPROVE = 8;
	public static final Integer CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_NORMAL = 9;

	public enum DocStatus {
		DRAFT("001-001"), SUBMITTED("001-002"), APPROVING("001-003"), APPROVED("001-004"), REJECT("001-005"),
		RETURN("001-006"), WITHDRAW("001-007"), CO_APPROVING("001-008"), LINE_CO_APPROVING("001-008-001"),
		ORDERED_CO_APPROVING("001-008-002");

		private String value;

		DocStatus(String id) {
			this.value = id;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum DrftStatus {
		DRAFT("001-001"), SUBMITTED("001-002"), APPROVING("001-003"), APPROVED("001-004"), REJECT("001-005"),
		RETURN("001-006"), WITHDRAW("001-007"), CO_APPROVING("001-008"), LINE_CO_APPROVING("001-008-001"),
		ORDERED_CO_APPROVING("001-008-002");

		private String value;

		DrftStatus(String id) {
			this.value = id;
		}

		public String getValue() {
			return value;
		}
	}

	public enum ApprovalType {
		SUBMIT("006-001"), REVIEW("006-002"), APPROVE("006-003"), ARBITRARY_APPROVE("006-004"), PROX_APPROVE("006-005"),
		PARALLEL_DEPT_COOPERATE_APPROVE("006-006"), ORDERED_DEPT_COOPERATE_APPROVE("006-007"),
		EX_POST_FACTO_REPORT("006-008"), ORDERED_COOPERATE_APPROVE("006-009"), UNORDERED_COOPERATE_APPROVE("006-010");

		private String value;

		ApprovalType(String id) {
			this.value = id;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum FormType {
		LETTER("06-01"), METTING("06-02"), BUSINESS("06-03"), EXPENSE("06-04"), BUSINESS_REPORT("06-05"),
		VACATION("06-06"), SENDING_LETTER("06-07"), BIZ_REPORT_OVERSEAS_DETAIL("22-01"), BIZ_REPORT_DOMESTIC_DETAIL("22-02"),
		BIZ_OVERSEAS_DETAIL("22-03"), BIZ_DOMESTIC_DETAIL("22-04");

		private String value;

		FormType(String id) {
			this.value = id;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum State {
		UPDATE("U"), DELETE("D"), CREATE("C");

		private String value;

		State(String id) {
			this.value = id;
		}

		public String getValue() {
			return value;
		}
	}
}
