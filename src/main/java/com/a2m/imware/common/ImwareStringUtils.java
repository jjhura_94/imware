package com.a2m.imware.common;

import java.text.DecimalFormat;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class ImwareStringUtils extends StringUtils {
	public static String MENU_URL = "MENU_URL";
	public static String DOLLAR_SIGN = "$";
	public static String AT_SIGN = "@";
	public static String YES = "Y";
	public static String NO = "N";
	public static String DOT = ".";
	public static String APPROVE = "APPROVE";
	public static String REJECT = "REJECT";
	
	
	// Constant for ID prefix
	public static String DUTY_ID_PREFIX = "PST";
	public static String STRUCT_ID_PREFIX = "STRU_";
	public static String DEPT_ID_PREFIX = "DEPT_";
	
	public static String genNextId(String maxId, DecimalFormat formatter, String prefix) {
		if(ImwareStringUtils.isEmpty(maxId)) {
			maxId = "0";
		}
		maxId = maxId.replaceAll(prefix, "");
		String newId = prefix + formatter.format((Integer.parseInt(maxId) + 1));
		return newId;
	}
	
	public static String getForInQuery(String str, String delim){
    	StringTokenizer stz = new StringTokenizer(str, delim);
    	String inStr = "";
    	int i = 0;
    	while(stz.hasMoreTokens()){
    		inStr = inStr+ (i==0?"":",") + "'"+stz.nextToken().trim()+"'";
    		i++;
    	}
    	return inStr;
    }
	
	public static String nullToEmpty(String str) {
		if(str == null) {
			return ImwareStringUtils.EMPTY;
		}
		return str.trim();
	}
	

}
