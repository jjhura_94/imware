package com.a2m.imware.common.menu;

public enum MenuPermissionType {
	READ(0), WRITE(1), MODIFY(2), DELETE(3), PRINT(4), EXCEL(5), MANAGE(6);
	
	private final int order;

	MenuPermissionType(int order) {
		this.order = order;
	}

	public int getOrder() {
		return order;
	}
}
