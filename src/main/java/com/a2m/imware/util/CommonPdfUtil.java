package com.a2m.imware.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Value;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.IOException;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.font.FontProvider;

public class CommonPdfUtil {
	
    @Value("${dir.upload.pdf}")
    private String pdfUploadDir;
    
	public Map<Object, Object> genPdf(String html) throws IOException, java.io.IOException {
		Validate.notBlank(html, "Html content cannot be blank!!!");
		
		System.out.println(html);
        String atchFileSeq = java.util.UUID.randomUUID().toString();
        String fileNamePhysical = atchFileSeq.replace("-", "");
        String path = pdfUploadDir + "/" + fileNamePhysical + ".pdf";
        
        if (!(new File(pdfUploadDir)).isDirectory()) {
            new File(pdfUploadDir).mkdirs();
        }
        
        
		PdfDocument pdf = new PdfDocument(new PdfWriter(path));
		Document doc = new Document(pdf, PageSize.A4);

		ConverterProperties converterProperties = new ConverterProperties();
		FontProvider dfp = new DefaultFontProvider(true, true, true);
		converterProperties.setFontProvider(dfp);

		HtmlConverter.convertToPdf(html, pdf, converterProperties);
        
		
        Map<Object, Object> fileInfo = new HashMap<>();
        fileInfo.put("fileNamePhysical", fileNamePhysical);
        fileInfo.put("FLE_TP", "pdf");
        fileInfo.put("ATCH_FLE_SEQ", atchFileSeq);
        File f = new File(path);
        String FLE_NM = fileNamePhysical + ".pdf";
        String NEW_FLE_NM = fileNamePhysical + ".pdf";
        fileInfo.put("FLE_PATH", NEW_FLE_NM);
        fileInfo.put("FLE_NM", FLE_NM);
        fileInfo.put("NEW_FLE_NM", NEW_FLE_NM);
        fileInfo.put("FLE_SZ", f.length());
        
		return fileInfo;
	}
}
