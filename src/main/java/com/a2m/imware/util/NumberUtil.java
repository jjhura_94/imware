package com.a2m.imware.util;

import java.text.DecimalFormat;

import com.a2m.imware.common.ImwareStringUtils;

public class NumberUtil {

	public static Double zeroIfNull(Double a) {
		return a == null ? 0d : a;
	}

	public static Integer zeroIfNull(Integer a) {
		return a == null ? 0 : a;
	}
	
	public static Double defaultIfNull(Double a, Double b) {
		return a == null ? b : a;
	}
	
	public static Integer defaultIfNull(Integer a, Integer b) {
		return a == null ? b : a;
	}

	public static String formatNumber(Double a) {
		DecimalFormat decimalFormat = new DecimalFormat("###,###.##");
		return a == null ? ImwareStringUtils.EMPTY : decimalFormat.format(a);
	}

	public static String formatNumber(Integer a) {
		return a == null ? ImwareStringUtils.EMPTY : String.valueOf(a);
	}

	public static <T> String emptyIfNull(T a) {
		return a == null ? ImwareStringUtils.EMPTY : String.valueOf(a);
	}

	public static String formatCost(Double cost, Integer day, Double exRate) {
		return formatCost(cost, Double.valueOf(zeroIfNull(day)), exRate);
	}
	public static String formatCost(Double cost, Double day, Double exRate) {
		cost = zeroIfNull(cost);
		day = zeroIfNull(day);
		exRate = defaultIfNull(exRate, 1d);

		DecimalFormat decimalFormat = new DecimalFormat("###,###.##");
		return decimalFormat.format(cost * day * exRate);
	}
	
	public static String formatCostWithEfficientDay(Double cost, Integer day, Double exRate) {
		day = zeroIfNull(day);
		Double eDay = coEfficientDay(Double.valueOf(day));
		return formatCost(cost, eDay, exRate);
	}
	
	public static String formatRoomCostWithEfficientDay(Double cost, Integer day, Double exRate) {
		if(day == null) {
			day = 0;
		}
		
		Double eDay = coEfficientDay(Double.valueOf(day + 1)) - 1;
		return formatCost(cost, eDay, exRate);
	}

	public static Double coEfficientDay(Double day) {
		day = zeroIfNull(day);
		
		if (day < 7) {
			return day;
		} else {
			if (day < 15) {
				return 6 + (day - 6) * 90 / 100;
			} else if (day < 30) {
				return 6 + 8 * 90 / 100 + (day - 14) * 80 / 100;
			} else if (day < 60) {
				return 6 + 8 * 90 / 100 + 15 * 80 / 100 + (day - 29) * 70 / 100;
			} else {
				return 6 + 8 * 90 / 100 + 15 * 80 / 100 + 30 * 70 / 100 + (day - 59) * 60 / 100;
			}
		}
	}

}
