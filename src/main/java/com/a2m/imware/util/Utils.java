package com.a2m.imware.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

public class Utils {

    private static final String AUTHORIZATION = "authorization";

    public static Properties commonProps;

    static {
        commonProps = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("largefileupload.properties");
            commonProps.load(inputStream);
        } catch (Exception e) {
            System.out.println("Can't load properties file!\nDetail: " + e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    System.out.println("Can't close input stream!\nDetail: " + e);
                }
            }
        }
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static void deleteFilesFromDisk(List<String> fileNameList) throws Exception {
        if (fileNameList == null || fileNameList.isEmpty()) {
            return;
        }
        final String uploadDir = commonProps.getProperty("path.default.uploaddir");
        if (isNullOrEmpty(uploadDir)) {
            throw new Exception("Cannot get upload directory!");
        }

        fileNameList.forEach(fileName -> {
            if (isNullOrEmpty(fileName)) {
                return;
            }

            String path = uploadDir + (uploadDir.endsWith("/") || uploadDir.endsWith("\\") ? "" : File.separator)
                    + fileName;
            File file = new File(path);
            if (!file.exists()) {
                return;
            }
            file.delete();
        });
    }

    /**
     * Convert date keys in the given map to Long (milliseconds since epoch).
     *
     * @param map is the map with the keys need to convert date to time.
     * @param keysConvert key list (each key keep a date value => can call
     * "getTime()" method).
     * @author HungDM
     */
    public static void convertDateToTimeSinceEpoch(Map<Object, Object> map, List<Object> keysConvert) {
        if (map == null || map.isEmpty() || keysConvert == null || keysConvert.isEmpty()) {
            return;
        }

        keysConvert.forEach(key -> {
            if (key == null || !map.containsKey(key)) {
                return;
            }
            final Object date = map.get(key);
            if (date == null || !(date instanceof java.util.Date || date instanceof java.sql.Date
                    || date instanceof java.sql.Timestamp || date instanceof java.sql.Time)) {
                return;
            }

            Long time = null;
            if (date instanceof java.util.Date) {
                time = ((java.util.Date) date).getTime();
            } else if (date instanceof java.sql.Date) {
                time = ((java.sql.Date) date).getTime();
            } else if (date instanceof java.sql.Timestamp) {
                time = ((java.sql.Timestamp) date).getTime();
            } else if (date instanceof java.sql.Time) {
                time = ((java.sql.Time) date).getTime();
            }

            if (time == null) {
                return;
            }

            map.put(key, time);
        });
    }

    public static void convertEpochTimeToDate(Map<Object, Object> map, List<Object> keysConvert) {
        if (map == null || map.isEmpty() || keysConvert == null || keysConvert.isEmpty()) {
            return;
        }

        keysConvert.forEach(key -> {
            if (key == null || !map.containsKey(key)) {
                return;
            }
            final String date = String.valueOf(map.get(key));
            Long time = null;
            try {
                time = Long.parseLong(date);
            } catch (NumberFormatException e) {
                time = null;
            }

            if (time == null) {
                return;
            }

            map.put(key, new Date(time));
        });
    }
    
    public static String getTokenFromRequest(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders(AUTHORIZATION);
		String headerValue = "";
		while (headers.hasMoreElements()) {
			headerValue = headers.nextElement();
		}
		if (headerValue != null && !"".equals(headerValue)) {
			String els[] = headerValue.split(" ");
			if (els.length > 1) {
				return els[1];
			}
		}
		return "";
	}
    
    public static String getForInQuery(String str, String delim){
    	StringTokenizer stz = new StringTokenizer(str, delim);
    	String inStr = "";
    	int i = 0;
    	while(stz.hasMoreTokens()){
    		inStr = inStr+ (i==0?"":",") + "'"+stz.nextToken().trim()+"'";
    		i++;
    	}
    	return inStr;
    }

}
