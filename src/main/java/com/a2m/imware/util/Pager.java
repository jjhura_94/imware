package com.a2m.imware.util;

import personal.aug.convert.MapAndObjectConversion;

/**
 * @author HungDM
 *
 */
public class Pager extends MapAndObjectConversion {

	private Integer start;
	private Integer limit;

	public Pager() {
	}

	public Pager(Integer start, Integer limit) {
		super();
		this.start = start;
		this.limit = limit;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * Get LIMIT SQL string.
	 * 
	 * @return an empty string if start & limit parameters are null, else return a string like "LIMIT start_value, limit_value"
	 * @author HungDM
	 */
	public String toSqlString() {
		String result = "";
		if (start == null || limit == null)
			return result;
		
		result = String.format("LIMIT %d, %d", start, limit);
		return result;
	}
	
}
