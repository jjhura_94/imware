package com.a2m.imware.util;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author QuangDV
 */
public class CommonFileUtil {
    public static byte[] convertToBytes(File file){
        try {
            return FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static byte[] getFileByPath(String path){
        if(StringUtils.isEmpty(path)) return new byte[0];

        File file = FileUtils.getFile(path);
        return convertToBytes(file);
    }

    public static File save(String path, MultipartFile multipartFile) throws IOException {
        File file = new File(path);
        multipartFile.transferTo(file);
        return file;
    }

    public static String getExt(String nameOrPath){
        int dotIndex = nameOrPath.lastIndexOf(".");
        if(dotIndex<0) return null;
        return nameOrPath.substring(dotIndex);
    }

    public static String getFileName(String path){
        int seperatorIndex = path.lastIndexOf("/");
        if(seperatorIndex<0) return null;
        return path.substring(++seperatorIndex);
    }

    public static String getDir(String path){
        int seperatorIndex = path.lastIndexOf("/");
        if(seperatorIndex<0) return null;
        return path.substring(0, seperatorIndex);
    }

    public static String replaceFileName(String newPrefixFileName, String originFileName){
        return newPrefixFileName + getExt(originFileName);
    }

    private static void mdkrs(String fileRealativePath){
        if(fileRealativePath==null) return;
        String dir = getDir(fileRealativePath);
        File file = new File(dir);
        if (!file.exists()) {
            if (file.mkdirs()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
    }

    private static void create(String fileRelativePath) throws IOException {
        if(fileRelativePath==null) return;
        File file = new File(fileRelativePath);
        if (!file.exists()) {
            if (file.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("Failed to create file!");
            }
        }
    }

    public static String format(String fileRelativePath){
        int i = 0;
        while (i<fileRelativePath.length() && fileRelativePath.charAt(i)=='\\'){
            fileRelativePath = fileRelativePath.substring(i+1);
            i=0;
        }

        return fileRelativePath;
    }

    public static void write(String path,  MultipartFile multipartFile) throws IOException {
        if(path==null || multipartFile==null) return;
        byte[] bytes = multipartFile.getBytes();
        BufferedOutputStream bs = null;

        mdkrs(path);
        create(path);

        try {
            FileOutputStream fs = new FileOutputStream(path);
            bs = new BufferedOutputStream(fs);
            bs.write(bytes);
            bs.close();
            bs = null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bs != null) try {
            bs.close();
        } catch (Exception e) {
        }
    }

    public static byte[] read(String path){
        if(path==null) return new byte[0];

        File file = new File(path);
        if(file==null) return new byte[0];

        try {
            byte[] bytes = FileUtils.readFileToByteArray(file);
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    public static String getResourceAsString(String path){
        try {
            return StreamUtils.copyToString( new ClassPathResource(path).getInputStream(), Charset.defaultCharset()  );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
