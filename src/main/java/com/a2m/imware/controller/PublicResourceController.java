package com.a2m.imware.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.util.Utils;

@Controller
@RequestMapping("/public/resource")
public class PublicResourceController {

	@Autowired
	private TccoFileService tccoFileService;
	
    @Autowired
    Environment environment;
	
	@ResponseBody
	@RequestMapping(value = "/imageView/{fileSequence}", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_GIF_VALUE, MediaType.ALL_VALUE }, method = RequestMethod.GET)
	public byte[] imageView(HttpServletRequest request, HttpServletResponse response, @PathVariable("fileSequence") String fileSequence) {
		if (Utils.isNullOrEmpty(fileSequence))
			return null;
			
		InputStream is = null;
		try {
			TccoFile tccoFile = tccoFileService.findBySequence(fileSequence);
			if (tccoFile == null || Utils.isNullOrEmpty(tccoFile.getNewFleNm()))
				return null;
			
			String path = Utils.commonProps.getProperty("path.default.uploaddir") + File.separator + tccoFile.getNewFleNm();
			File fileToDownload = new File(path);
			if (!fileToDownload.exists() || !fileToDownload.isFile())
				return null;
			// get your file as InputStream
			is = new FileInputStream(fileToDownload);
			// copy it to response's OutputStream
			return org.apache.commons.io.IOUtils.toByteArray(is);
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	@GetMapping(value = "/pdfView/{fileNamePhysical}", produces = { MediaType.APPLICATION_PDF_VALUE })
	public ResponseEntity<byte[]> pdfView(@PathVariable("fileNamePhysical") String fileNamePhysical) {
		if (Utils.isNullOrEmpty(fileNamePhysical))
			return null;
			
		InputStream is = null;
		try {
			String filename = fileNamePhysical + ".pdf";
			String path = environment.getProperty("dir.upload.pdf") + "/" + filename;
			
			File fileToDownload = new File(path);
			if (!fileToDownload.exists() || !fileToDownload.isFile())
				return null;

			is = new FileInputStream(fileToDownload);
			byte[] contents = org.apache.commons.io.IOUtils.toByteArray(is);
			
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_PDF);
		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		    headers.add("Content-Disposition", "inline; filename=" + filename);
		    
		    ResponseEntity<byte[]> res = new ResponseEntity<>(contents, headers, HttpStatus.OK);
		    return res;
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

}
