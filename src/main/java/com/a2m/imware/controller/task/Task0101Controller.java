package com.a2m.imware.controller.task;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.ApvConstantUtils;
import com.a2m.imware.common.DateUtil;
import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.common.menu.MenuPermission;
import com.a2m.imware.common.menu.MenuPermissionType;
import com.a2m.imware.config.CommonTccoFileConfig;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.common.ResponseData;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.task.TndmTaskAttach;
import com.a2m.imware.model.task.TndmTaskChecklist;
import com.a2m.imware.model.task.TndmTaskComment;
import com.a2m.imware.model.task.TndmTaskCommentAttach;
import com.a2m.imware.model.task.TndmTaskMgt;
import com.a2m.imware.model.task.TndmTaskRecipient;
import com.a2m.imware.model.task.TndmTaskReferrer;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.task.Task0101Service;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.CommonFileUtil;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.TaskRequestMessage;
import com.google.common.base.Strings;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/task/task0101")
public class Task0101Controller {

    public static String MENU_URL = "task/task0101";

    @Autowired
    private Task0101Service task0101Service;
    @Autowired
    private CommonDeptService commonDeptService;
    @Autowired
    private CommonTccoFileConfig commonTccoFileConfig;
    @Autowired
    private TccoFileService tccoFileService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private ExtraMessageUtil extraMessageUtil;

    @GetMapping("/getTaskList")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public Object getTaskList(HttpServletRequest request, @RequestParam Map<Object, Object> arg) throws SQLException, ImwareException, ParseException, ParseException {
        PageResponse pageResponse = new PageResponse();
        if (arg.get("deptCode") != null && !Strings.isNullOrEmpty(arg.get("deptCode").toString())) {
            List<String> userIds = commonDeptService.getUidOfUser(request, arg.get("deptCode"));
            arg.put("USER_IDS", String.join(", ", userIds));
        }  
        List<TndmTaskMgt> tasks = task0101Service.searchTndmTaskMgt(arg);
        Map<Object, Object> params = new HashMap<>();
//        List<String> deptCodes = tasks.stream().map(t -> t.getDEPT_CODE()).distinct().collect(toList());
//        Map<String, String> deptDatas = new HashMap();
//        if (deptCodes != null && !deptCodes.isEmpty()) {
//            deptDatas = commonDeptService.getByDeptCodes(deptCodes);
//        }
        for (TndmTaskMgt task : tasks) {

            params.put("taskId", task.getId());
            List<TndmTaskRecipient> recipients = this.task0101Service.searchTndmTaskRecipient(params);
            task.setRecipients(recipients);
        }
        pageResponse.setDatas(tasks);
        pageResponse.setCount(task0101Service.countTndmTaskMgt(arg));
        return pageResponse;
    }

    @GetMapping("/getById")
    public Object getById(@RequestParam Long id) throws SQLException {
        return task0101Service.getById(id);
    }

    @PostMapping("/createTask")
    @MenuPermission(permissions = MenuPermissionType.WRITE)
    @TransactionalWithRollback
    public ResponseData<?> createTask(@RequestBody @Valid TndmTaskMgt task, HttpServletRequest request) throws SQLException, ImwareException {
        this.task0101Service.insertTndmTaskMgt(task);

        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        for (TndmTaskRecipient rec : task.getRecipients()) {
            rec.setTaskId(task.getId());
            this.task0101Service.insertTndmTaskRecipient(rec);
            this.sendMessage(task, rec.getRecipientId(), user.get("USER_UID").toString());
        }
        for (TndmTaskReferrer ref : task.getReferrers()) {
            ref.setTaskId(task.getId());
            this.sendMessage(task, ref.getReferrerId(), user.get("USER_UID").toString());
            this.task0101Service.insertTndmTaskReferrer(ref);
        }

        for (TccoFile file : task.getNewFiles()) {
            TndmTaskAttach attachment = new TndmTaskAttach();
            attachment.setTaskId(task.getId());
            attachment.setAttachFleSeq(file.getAtchFleSeq());
            this.task0101Service.insertTndmTaskAttach(attachment);
        }

        if (task.getChecklists() != null) {
            for (String item : task.getChecklists()) {
                TndmTaskChecklist checklist = new TndmTaskChecklist();
                checklist.setContent(item);
                checklist.setStatus(ApvConstantUtils.N);
                checklist.setTaskId(task.getId());
                task0101Service.insertTndmTaskChecklist(checklist);
            }
        }

        return ResponseData.success();
    }

    @GetMapping("/getTaskReferrers")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public List<TndmTaskReferrer> getTaskReferrers(@RequestParam Map<Object, Object> arg) throws SQLException {
        return this.task0101Service.searchTndmTaskReferrer(arg);
    }

    @GetMapping("/getTaskAttachments")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public List<TndmTaskAttach> getTgetTaskAttachmentsaskReferrers(@RequestParam Map<Object, Object> arg) throws SQLException {
        return this.task0101Service.searchTndmTaskAttach(arg);
    }

    @PostMapping("/updateTask")
    @MenuPermission(permissions = MenuPermissionType.WRITE)
    @TransactionalWithRollback
    public ResponseData<?> updateTask(@RequestBody @Valid TndmTaskMgt task) throws SQLException {
        this.task0101Service.updateTndmTaskMgt(task);
        Map<Object, Object> params = new HashMap<>();
        params.put("taskId", task.getId());

        List<TndmTaskReferrer> oldTaskReferrers = this.task0101Service.searchTndmTaskReferrer(params);
        for (TndmTaskReferrer ref : oldTaskReferrers) {
            if (!task.getReferrers().contains(ref)) {
                this.task0101Service.deleteTndmTaskReferrer(ref.getId());
            }
        }

        for (TndmTaskReferrer ref : task.getReferrers()) {
            if (ref.getId() == null) {
                ref.setTaskId(task.getId());
                this.task0101Service.insertTndmTaskReferrer(ref);
            }
        }

        List<TndmTaskRecipient> oldTaskRecipients = this.task0101Service.searchTndmTaskRecipient(params);
        for (TndmTaskRecipient rec : oldTaskRecipients) {
            if (!task.getRecipients().contains(rec)) {
                this.task0101Service.deleteTndmTaskRecipient(rec.getId());
            }
        }
        for (TndmTaskRecipient rec : task.getRecipients()) {
            if (rec.getId() == null) {
                rec.setTaskId(task.getId());
                this.task0101Service.insertTndmTaskRecipient(rec);
            }
        }

        if (task.getChangedFile()) {
            for (TccoFile file : task.getDeletedFiles()) {
                this.task0101Service.deleteTndmTaskAttach(file.getAtchFleSeq());
            }
            for (TccoFile file : task.getNewFiles()) {
                TndmTaskAttach attachment = new TndmTaskAttach();
                attachment.setTaskId(task.getId());
                attachment.setAttachFleSeq(file.getAtchFleSeq());
                this.task0101Service.insertTndmTaskAttach(attachment);
            }
        }

        if (task.getChecklists() != null) {
            task0101Service.deleteTndmTaskChecklist(task.getId());
            for (String item : task.getChecklists()) {
                TndmTaskChecklist checklist = new TndmTaskChecklist();
                checklist.setContent(item);
                checklist.setStatus(ApvConstantUtils.N);
                checklist.setTaskId(task.getId());
                task0101Service.insertTndmTaskChecklist(checklist);
            }
        }

        return ResponseData.success();
    }

    @GetMapping("/getTaskComments")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public Object getTaskComments(@RequestParam Map<Object, Object> arg) throws SQLException {
        Map<Object, Object> params = new HashMap<>();
        params.put("taskId", arg.get("taskId"));
        params.put("level", 1);
        List<TndmTaskComment> level1Comments = this.task0101Service.searchTndmTaskComment(params);

        params.remove("level");
        for (TndmTaskComment level1Comment : level1Comments) {
            params.put("upCommentId", level1Comment.getCommentId());
            level1Comment.setChildren(this.task0101Service.searchTndmTaskComment(params));

            params.put("commentId", level1Comment.getCommentId());
            level1Comment.setAttachFiles(this.task0101Service.searchTndmTaskCommentAttach(params));
            params.remove("commentId");
        }
        return level1Comments;
    }

    @PostMapping("/createTaskComment")
    @MenuPermission(permissions = MenuPermissionType.WRITE)
    @TransactionalWithRollback
    public TndmTaskComment createTaskComment(@RequestBody @Valid TndmTaskComment comment) throws SQLException {
        boolean hasAttachFiles = comment.getCommentId() == null && comment.getAttachFiles() != null;
        this.task0101Service.insertTndmTaskComment(comment);
        if (hasAttachFiles) {
            for (TndmTaskCommentAttach attach : comment.getAttachFiles()) {
                attach.setAttachFleSeq(attach.getAtchFleSeq());
                attach.setCommentId(comment.getCommentId());
                this.task0101Service.insertTndmTaskCommentAttach(attach);
            }

        }
        return comment;
    }

    @PostMapping("/updateTaskComment")
    @MenuPermission(permissions = MenuPermissionType.WRITE)
    @TransactionalWithRollback
    public ResponseData<?> updateTaskComment(@RequestBody @Valid TndmTaskComment comment) throws SQLException {
        this.task0101Service.updateTndmTaskComment(comment);
        return ResponseData.success(comment);
    }

    @PostMapping("/deleteTaskComment")
    @MenuPermission(permissions = MenuPermissionType.DELETE)
    @TransactionalWithRollback
    public ResponseData<?> deleteTaskComment(@RequestBody TndmTaskComment comment) throws SQLException {
        TndmTaskComment commentFromDb = this.task0101Service.getTndmTaskComment(comment.getCommentId());
        this.task0101Service.deleteTndmTaskComment(commentFromDb);
        return ResponseData.success();
    }

    @PostMapping("/deleteTask")
    @MenuPermission(permissions = MenuPermissionType.DELETE)
    @TransactionalWithRollback
    public ResponseData<?> deleteTask(@RequestBody Map<String, Long> arg) throws SQLException {
        Long taskId = arg.get("taskId");
        this.task0101Service.deleteTndmTaskMgt(taskId);
        return ResponseData.success();
    }

    @PostMapping("/updateFavourite")
    @TransactionalWithRollback
    public Object updateFavourite(@RequestParam String ids, @RequestParam boolean favourite) {
        if (Strings.isNullOrEmpty(ids)) {
            AjaxResult ajaxResult = new AjaxResult();
            ajaxResult.setMessage("Ids id not empty");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
        List<Long> taskIds = Arrays.asList(ids.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        task0101Service.updateFavourite(taskIds, favourite);
        return ResponseData.success();
    }

    @GetMapping("/get-checklist")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public Object getChecklist(@RequestParam Map<Object, Object> arg) throws SQLException {
        return task0101Service.searchTndmTaskChecklist(arg);
    }

    @GetMapping("/getTask/{taskId}")
    @MenuPermission(permissions = MenuPermissionType.READ)
    public Object getTndmTaskMgt(@PathVariable("taskId") Long taskId) throws SQLException {
        return this.task0101Service.getTndmTaskMgt(taskId);
    }

    @PostMapping("/add-checkList")
    public Object addCheckList(@RequestBody TndmTaskChecklist taskChecklist) throws SQLException {
        return this.task0101Service.insertTndmTaskChecklist(taskChecklist);
    }

    @PostMapping("/update-status-checklist")
    public Object updateStatusCheckList(@RequestBody TndmTaskChecklist taskChecklist) throws SQLException {
        return this.task0101Service.updateTndmTaskChecklist(taskChecklist);
    }

    @RequestMapping(value = "/save-comment-attach", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Object uploadAvatar(@RequestParam("file") MultipartFile multipartFile, @RequestParam Long commentId, HttpServletRequest request) throws Exception {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        TccoFile tccoFile = setDefaultValues(multipartFile, userId);
        String dir = commonTccoFileConfig.getPathDefaultUploaddir() + "\\";
        try {
            CommonFileUtil.save(dir.concat(tccoFile.getNewFleNm()), multipartFile);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        TccoFile output = tccoFileService.saveTccoFile(tccoFile);
        //update user: position = 1: avatar , 2 = signature
//        doUpload3(multipartFile);
        TndmTaskCommentAttach attach = new TndmTaskCommentAttach();
        attach.setCommentId(commentId);
        attach.setAttachFleSeq(tccoFile.getAtchFleSeq());
        task0101Service.insertTndmTaskCommentAttach(attach);
        return output;
    }

    public TccoFile setDefaultValues(MultipartFile multipartFile, String userId) throws Exception {

        TccoFile tccoFile = new TccoFile();
        tccoFile.setAtchFleSeq(UUID.randomUUID().toString());
        tccoFile.setCreatedDateStr(DateUtil.convertDateToStringDB(new Date()));
        tccoFile.setCreatedBy(userId);
        Long fleSz = multipartFile.getSize();
        String fleNm = multipartFile.getOriginalFilename();
        String newFleNm = CommonFileUtil.replaceFileName(tccoFile.getAtchFleSeq(), fleNm);
        String fileTp = CommonFileUtil.getExt(fleNm).replace(".", "");
        tccoFile.setFleSz(fleSz.toString());
        tccoFile.setFleNm(fleNm);
        tccoFile.setNewFleNm(newFleNm);
        tccoFile.setFleTp(fileTp);
        tccoFile.setFlePath(newFleNm);
        return tccoFile;
    }

    public void sendMessage(TndmTaskMgt task, String userId, String senderId) throws ImwareException {
        TaskRequestMessage message = TaskRequestMessage.builder()
                .sendUserUID(senderId)
                .receiveUserUID(userId)
                .taskTitle(task.getTitle())
                .permalink("/#/task/task0102/detail?id=" + task.getId())
                .userUID(userId)
                .build();
        Thread thread = new Thread() {
            public void run() {
                message.setUserUid(userId);
                extraMessageUtil.taskRequest(message);
            }
        };
        thread.start();
    }
}
