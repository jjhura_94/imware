package com.a2m.imware.controller.apv;

import java.sql.SQLException;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.apv.TndmDocReceived;
import com.a2m.imware.model.apv.TndmDocReceivedAttach;
import com.a2m.imware.model.request.DrftRequest;
import com.a2m.imware.model.request.TndmDrftAttachRequest;
import com.a2m.imware.service.apv.Apv0107Service;

@RestController
@RequestMapping(value = "/api/apv/apv0107")
@SuppressWarnings({})
@Validated
public class Apv0107Controller {
	
	@Autowired
	private Apv0107Service apv0107Service;
	
	@GetMapping("/search")
	public Object search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return apv0107Service.searchTndmDocReceived(arg);
	}
	
	@GetMapping("/get-attach")
	public Object getAttach(@RequestParam Map<Object, Object> arg) throws SQLException {
		return apv0107Service.searchTndmDocReceivedAttach(arg);
	}
	
	@PostMapping("/save")
	@Transactional(rollbackFor = { Exception.class })
	public Object save(@RequestBody @Valid TndmDocReceived arg) throws Exception {
		if(arg.getId() == null) {
			apv0107Service.insertTndmDocReceived(arg);
//			insert attch file
			for (TccoFile file : arg.getFiles()) {
				TndmDocReceivedAttach attachRequest = new TndmDocReceivedAttach();
				attachRequest.setDocReceivedId(arg.getId());
				attachRequest.setAttachFleSeq(file.getAtchFleSeq());
				apv0107Service.insertTndmDocReceivedAttach(attachRequest);
			}
		}
		else {
			apv0107Service.updateTndmDocReceived(arg);
			if(arg.getChangedFile()) {
				for (TccoFile file : arg.getFilesDeleted()) {
					apv0107Service.deleteTndmDocReceivedAttach(file.getAtchFleSeq());
				}
				for (TccoFile file : arg.getFiles()) {
					TndmDocReceivedAttach attachRequest = new TndmDocReceivedAttach();
					attachRequest.setDocReceivedId(arg.getId());
					attachRequest.setAtchFleSeq(file.getAtchFleSeq());
					apv0107Service.insertTndmDocReceivedAttach(attachRequest);
				}
			}
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
