package com.a2m.imware.controller.apv;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.dao.common.ReferrerDAO;
import com.a2m.imware.model.apv.TndmApvlLineFavorite;
import com.a2m.imware.model.apv.TndmApvlLineFavoriteDetail;
import com.a2m.imware.service.apv.ApvLineProcessingService;

@RestController
@RequestMapping(value = "/api/apv/apvLine")
@Validated
public class ApvLineProcessingController {
	public static final String MENU_URL = "init_me";


	@Autowired
	private ApvLineProcessingService apvLineService;
	
	@Autowired
	private ReferrerDAO referrerDAO;
	
	@GetMapping("/search/{userUid}")
	public List<TndmApvlLineFavorite> searchTndmApvlLineFavorite(@PathVariable("userUid") String userUid) throws SQLException {
		Map<Object, Object> arg = new HashMap<Object, Object>();
		arg.put("createdBy", userUid);
		return apvLineService.searchTndmApvlLineFavorite(arg);
	}
	
	
	@PostMapping("/save")
	@Transactional(rollbackFor = { Exception.class , SQLException.class })
	public Object save(@RequestBody @Valid TndmApvlLineFavorite arg) throws Exception {
		Date currentDate = new Date();
		arg.setCreatedDate(currentDate);
		apvLineService.insertTndmApvlLineFavorite(arg);
		if(arg.getApvlLineFavoriteDetails() != null) {
			int index = arg.getApvlLineFavoriteDetails().size();
			for (TndmApvlLineFavoriteDetail ele : arg.getApvlLineFavoriteDetails()) {
				ele.setFavoriteId(arg.getId());
				ele.setSancOrdNo(index);
				apvLineService.insertTndmApvlLineFavoriteDetail(ele);
				index --;
			}
		}
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping("/delete-favorite-refer/{id}")
    @TransactionalWithRollback
    public ResponseEntity<?> deleteFavoriteRefer(@PathVariable("id") Long id) throws Exception {
		referrerDAO.deleteTndmReferrerFavoriteDetail(id);
		referrerDAO.deleteTndmReferrerFavorite(id);
    	return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
