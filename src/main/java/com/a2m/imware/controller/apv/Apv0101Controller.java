package com.a2m.imware.controller.apv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.MutablePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.a2m.imware.common.ApvConstantUtils;
import com.a2m.imware.common.ApvConstantUtils.DocStatus;
import com.a2m.imware.common.ApvConstantUtils.DrftStatus;
import com.a2m.imware.common.ApvConstantUtils.FormType;
import com.a2m.imware.common.DateUtil;
import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.dao.apv.Apv0101DAO;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.apv.TndmApvlHist;
import com.a2m.imware.model.apv.TndmDocReceived;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.apv.TndmDrftBusTrip;
import com.a2m.imware.model.apv.TndmDrftBusTripDetail;
import com.a2m.imware.model.apv.TndmDrftReference;
import com.a2m.imware.model.apv.TndmDrftReferer;
import com.a2m.imware.model.common.ResponseData;
import com.a2m.imware.model.request.DrftRequest;
import com.a2m.imware.model.request.TndmDocMstRequest;
import com.a2m.imware.model.request.TndmDrftAttachRequest;
import com.a2m.imware.model.request.TndmDrftMeetingMinuteRequest;
import com.a2m.imware.model.request.TndmDrftRequest;
import com.a2m.imware.model.request.TndmDrftSpendingRequest;
import com.a2m.imware.model.request.TsstUserRequest;
import com.a2m.imware.model.response.TcdsEmpMstResponse;
import com.a2m.imware.model.response.TcdsEmpVacationApprovalResponse;
import com.a2m.imware.model.response.TcdsEmpVacationRequestResponse;
import com.a2m.imware.model.response.TndmDrftAttachResponse;
import com.a2m.imware.model.sys.TndmFormMgt;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.Com0101Service;
import com.a2m.imware.service.Com0102Service;
import com.a2m.imware.service.apv.Apv0101Service;
import com.a2m.imware.service.apv.Apv0107Service;
import com.a2m.imware.service.apv.ApvLineProcessingService;
import com.a2m.imware.service.apv.ApvPdfService;
import com.a2m.imware.service.cms.Cms0102Service;
import com.a2m.imware.service.common.ComSeqService;
import com.a2m.imware.service.common.CommSTDService;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.Sys0112Service;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.a2m.imware.service.task.Task0101Service;
import com.a2m.imware.util.NumberUtil;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.TaskRequestMessage;
import com.lowagie.text.pdf.BaseFont;

@RestController
@RequestMapping(value = "/api/apv/apv0101")
@SuppressWarnings({})
@Validated
public class Apv0101Controller {

    @Value("${dir.upload.pdf}")
    private String pdfUploadDir;

    @Value("${dir.font}")
    private String configFontDir;

    @Autowired
    ServletContext servletContext;

    @Autowired
    private Sys0112Service sys0112Service;

    @Autowired
    private TccoFileService tccoFileService;

    @Autowired
    Environment environment;

    @Autowired
    private Sys0105Service sys0105Service;

    @Autowired
    private Apv0101Service apv0101Service;
    
    @Autowired
    private Apv0107Service apv0107Service;

    @Autowired
    private CommSTDService comStdService;

    @Autowired
    private ComSeqService comSeqService;

    @Autowired
    private Apv0101DAO apv0101dao;

    @Autowired
    private ExtraMessageUtil extraMessageUtil;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private Com0102Service com0102Service;
    @Autowired
    private Com0101Service com0101Service;
    @Autowired
    private Task0101Service task0101Service;
    
    @Autowired
    private ApvPdfService apvPdfService;
    
    @Autowired
    private ApvLineProcessingService apvLineProcessingService;

	@Autowired
	private Cms0102Service cms0102Service;

    @GetMapping("/search")
    public Object search(@RequestParam Map<Object, Object> arg) throws SQLException {
        return apv0101Service.searchTndmDrft(arg);
    }

    @GetMapping("/count-doc")
    public Object countDoc(@RequestParam Map<Object, Object> arg, HttpServletRequest request) throws SQLException, Exception {
        Object userUid = arg.get("userUid");
        Map<Object, Object> rq = new HashMap<Object, Object>();
        rq.put("docStatus", "'001-002','001-003'");
        rq.put("recentYn", 1);
        rq.put("apvUserId", userUid);
//        rq.put("submittedDate", new Date());

        int waitingApv = apv0101dao.countTndmDrftWaitingApv(rq);

        rq = new HashMap<Object, Object>();
        rq.put("docStatus", "'001-002','001-003','001-006'");
        rq.put("recentYn", 1);
        rq.put("currentUserUid", userUid);
        rq.put("apvView", "progress");
//        rq.put("submittedDate", new Date());

        int proccessApv = apv0101dao.count(rq);

        Map<Object, Object> result = new HashMap<Object, Object>();
        Map<Object, Object> comCount = this.countCommunity(request);
        Map<Object, Object> taskCount = this.countTask(request);
        
        
        rq = new HashMap<Object, Object>();
        rq.put("docStatus", "'001-004'");
        rq.put("recentYn", 1);
        rq.put("currentUserUid", userUid);
        rq.put("apvView", "reference");
        rq.put("notInSending", true);
//        rq.put("submittedDate", new Date());

        int reference = apv0101dao.count(rq);
        

        result.put("waitingApv", waitingApv);
        result.put("proccessApv", proccessApv);
        result.put("reference", reference);
        result.putAll(comCount);
        result.putAll(taskCount);
        return result;
    }

    @GetMapping("/get-apvl-his/{drftDocNo}")
    public Object getApvlHis(@PathVariable("drftDocNo") String drftDocNo) throws SQLException {
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("drftDocNo", drftDocNo);
        return this.apv0101Service.searchTndmApvlHist(params);
    }

    @GetMapping("/search-waiting-apv")
    public Object searchTndmDrftWaitingApv(@RequestParam Map<Object, Object> arg) throws SQLException {
        return apv0101Service.searchTndmDrftWaitingApv(arg);
    }

    @GetMapping("/get-drft-attach")
    public Object searchTndmDrftAttach(@RequestParam Map arg) throws SQLException {
        return apv0101Service.searchTndmDrftAttach(arg);
    }

    @GetMapping("/get-apvline")
    public Object getApvline(@RequestParam Map arg) throws SQLException {
        return apv0101Service.searchTndmDrftApvlLine(arg);
    }

    @GetMapping("/get-reference-referrer")
    public Object getReferenceReferrer(@RequestParam Map arg) throws SQLException {
        Map result = new HashMap<Object, Object>();
        result.put("reference", apv0101Service.searchTndmDrftReference(arg));
        result.put("referrer", apv0101Service.searchTndmDrftReferer(arg));
        return result;
    }
    
    @PostMapping("/save-referrer")
    public Object saveReferrer(@RequestBody List<TndmDrftReferer> tndmDrftReferer) throws SQLException {
    	for (TndmDrftReferer item : tndmDrftReferer) {
            if(item.getId() != null) {
            	apv0101Service.updateTndmDrftReferer(item);
            }
            else {
            	apv0101Service.insertTndmDrftReferer(item);
            }
        }
    	return new ResponseEntity<Object>(HttpStatus.OK);
    }
    
    @GetMapping("/get-bustrip-detail/{busTripId}")
    public Object getBusTripDetail(@PathVariable("busTripId") Long busTripId) throws SQLException {
        return apv0101dao.getTndmDrftBusTripDetail(busTripId);
    }

    @PostMapping("/save-drft")
    @Transactional(rollbackFor = {Exception.class})
    public Object save(@RequestBody @Valid DrftRequest arg) throws Exception {
        Date currentDate = new Date();
//		init doc master save to db
        TndmDocMstRequest docMst = new TndmDocMstRequest();
        docMst.setDocNo(comSeqService.getSeq("SEQ_DOC_ID"));
        docMst.setCreatedBy(arg.getCreatedBy());
        docMst.setDocDate(currentDate);
        docMst.setCreatedDate(currentDate);
        docMst.setDocDeptCode(arg.getDocDeptCode());
        docMst.setDocDeptName(arg.getDocDeptName());
        docMst.setDocUserUid(arg.getUserUid());
        if ("drft".equals(arg.getType())) {
            docMst.setDocStatus(DocStatus.DRAFT.getValue());
        } else {
            docMst.setDocStatus(DocStatus.APPROVING.getValue());
            docMst.setSubmittedDate(currentDate);
            if (arg.getApvLine() != null && arg.getApvLine().size() > 0) {
                extraMessageUtil.taskRequest(TaskRequestMessage.builder()
                        .sendUserUID(arg.getCreatedBy())
                        .receiveUserUID(arg.getApvLine().get(arg.getApvLine().size() - 2).getSancUserUid())
                        .taskTitle(arg.getDrftTitle())
                        .permalink("/#/apv/apv0102")
                        .userUID(arg.getApvLine().get(arg.getApvLine().size() - 2).getSancUserUid())
                        .build()
                );
            }

        }
        apv0101Service.insertTndmDocMst(docMst);

//		init drft save to db
        TndmDrftRequest drft = new TndmDrftRequest();
        drft.setDocNo(docMst.getDocNo());
        drft.setDrftDocNo(comSeqService.getSeq("SEQ_DRFT_ID"));
        drft.setCreatedDate(currentDate);
        drft.setCreatedBy(arg.getCreatedBy());
        drft.setDrftTitle(arg.getDrftTitle());
        drft.setDrftContent(arg.getDrftContent());
        drft.setSendTo(arg.getSendTo());
        drft.setRecentYn(true);
//		drft.setFormId(formId);
        drft.setForeignOrgin(arg.getForeignOrigin());
        drft.setDrftType(arg.getDrftType());
        if ("drft".equals(arg.getType())) {
            drft.setDrftStatus(DrftStatus.DRAFT.getValue());
        } else {
            drft.setDrftStatus(DrftStatus.SUBMITTED.getValue());
        }
        
//        insert apv history
        apv0101Service.insertTndmDrft(drft);
        if (!"drft".equals(arg.getType())) {
            TndmApvlHist apvlHist = new TndmApvlHist();
            apvlHist.setDrftDocNo(drft.getDrftDocNo());
            apvlHist.setSancUserUid(arg.getUserUid());
            apvlHist.setSancYn(ApvConstantUtils.Y);
            apvlHist.setSancDate(currentDate);
            apvlHist.setMethod(DocStatus.SUBMITTED.getValue());
            apv0101Service.insertTndmApvlHist(apvlHist);
        }
//		insert reference
        for (TndmDrftRequest item : arg.getReference()) {
            TndmDrftReference reference = new TndmDrftReference();
            reference.setDocNo(item.getDocNo());
            reference.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReference(reference);
        }

        for (TsstUserRequest item : arg.getReferrer()) {
            TndmDrftReferer drftReferer = new TndmDrftReferer();
            drftReferer.setUserUid(item.getUserUid());
            drftReferer.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReferer(drftReferer);
        }

//		drft type = metting
        if (FormType.METTING.getValue().equals(arg.getDrftType())) {
            TndmDrftMeetingMinuteRequest minuteRequest = new TndmDrftMeetingMinuteRequest();
            minuteRequest.setDrftDocNo(drft.getDrftDocNo());
            minuteRequest.setMeetingTitle(arg.getDrftTitle());
            minuteRequest.setPlace(arg.getPlace());
            minuteRequest.setMeetingDate(arg.getMeetingDate());
            minuteRequest.setAttached(arg.getAttached());
            minuteRequest.setDateWrite(arg.getDateWrite());
            minuteRequest.setWriter(arg.getWriter());
            minuteRequest.setPurposeMeeting(arg.getPurposeMeeting());
            minuteRequest.setSpecialNote(arg.getSpecialNote());
            apv0101Service.insertTndmDrftMeetingMinute(minuteRequest);
        } else if (FormType.BUSINESS.getValue().equals(arg.getDrftType()) 
        		|| FormType.BUSINESS_REPORT.getValue().equals(arg.getDrftType())) {
            TndmDrftBusTrip busTripRequest = new TndmDrftBusTrip();
            busTripRequest.setDrftDocNo(drft.getDrftDocNo());
            busTripRequest.setTypeOfBusinessTrip(arg.getTypeOfBusinessTrip());
            busTripRequest.setFromDate(arg.getFromDateBus());
            busTripRequest.setToDate(arg.getToDateBus());
            busTripRequest.setCountry(arg.getCountry());
            busTripRequest.setPurpose(arg.getPurposeBus());
            busTripRequest.setTraveler(arg.getTraveler());
            busTripRequest.setOneWay(arg.getOneWay());
            busTripRequest.setTwoWay(arg.getTwoWay());
            busTripRequest.setToll(arg.getToll());
            busTripRequest.setBusTripDomestic(arg.getBusTripDomestic());
            busTripRequest.setDestination(arg.getDestination());
            busTripRequest.setVisitingAgency(arg.getVisitingAgency());
            busTripRequest.setTerm(arg.getTerm());
            apv0101Service.insertTndmDrftBusTrip(busTripRequest);
            if(arg.getBusTripDetail() != null) {
            	for (TndmDrftBusTripDetail item : arg.getBusTripDetail()) {
            		item.setBusTripId(busTripRequest.getId());
            		apv0101dao.insertTndmDrftBusTripDetail(item);
				}
            }
        } else if (FormType.EXPENSE.getValue().equals(arg.getDrftType())) {
            TndmDrftSpendingRequest spendingRequest = new TndmDrftSpendingRequest();
            spendingRequest.setDrftDocNo(drft.getDrftDocNo());
            spendingRequest.setAccount(arg.getAccount());
            spendingRequest.setAccountName(arg.getAccountName());
            spendingRequest.setAccountNumber(arg.getAccountNumber());
            spendingRequest.setBankName(arg.getBankName());
            spendingRequest.setCost(arg.getCost());
            spendingRequest.setFromDateSpen(arg.getFromDateSpen());
            spendingRequest.setToDateSpen(arg.getToDateSpen());
            spendingRequest.setPurposeSpen(arg.getPurposeSpen());
            spendingRequest.setVat(arg.getVat());
            spendingRequest.setSpendingType(arg.getSpendingType());
            apv0101Service.insertTndmDrftSpending(spendingRequest);
        }

//		insert attch file
        int index = 1;
        for (TccoFile file : arg.getFiles()) {
            TndmDrftAttachRequest attachRequest = new TndmDrftAttachRequest();
            attachRequest.setDrftAttachOrdNo(index);
            attachRequest.setDrftDocNo(drft.getDrftDocNo());
            attachRequest.setAtchFleSeq(file.getAtchFleSeq());
            apv0101Service.insertTndmDrftAttach(attachRequest);
            index++;
        }
        if (arg.getApvLine() != null) {
            index = arg.getApvLine().size();
            for (TndmDrftApvlLine item : arg.getApvLine()) {
                item.setDocNo(drft.getDocNo());
                item.setDrftDocNo(drft.getDrftDocNo());
                item.setSancDate(currentDate);
                item.setSancOrdNo(index);
                if (index == 1 && !"drft".equals(arg.getType())) {
                    item.setSancYn("Y");
                } else {
                    item.setSancYn("N");
                }
                index--;
                apv0101Service.insertTndmDrftApvlLine(item);
            }
        }

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PutMapping("/update-drft")
    @Transactional(rollbackFor = {Exception.class})
    public Object update(@RequestBody @Valid DrftRequest arg) throws Exception {
        Date currentDate = new Date();
//		update doc master save to db
        TndmDocMstRequest docMst = new TndmDocMstRequest();
        docMst.setDocNo(arg.getDocNo());
//      IF DOCUMENT approved then cannot update drft status
        if(!DrftStatus.APPROVED.getValue().equals(arg.getDocStatus())) {
        	if ("drft".equals(arg.getType())) {
                docMst.setDocStatus(DocStatus.DRAFT.getValue());
            } else {
                docMst.setDocStatus(DocStatus.SUBMITTED.getValue());
                docMst.setSubmittedDate(currentDate);
            }
        }
        
        apv0101Service.updateTndmDocMst(docMst);
//		init drft save to db
        TndmDrftRequest drft = new TndmDrftRequest();
        drft.setDrftDocNo(arg.getDrftDocNo());
        drft.setDrftTitle(arg.getDrftTitle());
        drft.setDrftContent(arg.getDrftContent());
        drft.setRecentYn(true);
//		todo
        drft.setForeignOrgin(arg.getForeignOrigin());
        drft.setDrftType(arg.getDrftType());
        drft.setSendTo(arg.getSendTo());
//        IF DOCUMENT approved then cannot update drft status
        if(!DrftStatus.APPROVED.getValue().equals(arg.getDrftStatus())) {
        	if ("drft".equals(arg.getType())) {
                drft.setDrftStatus(DrftStatus.DRAFT.getValue());
            } else {
                drft.setDrftStatus(DrftStatus.SUBMITTED.getValue());
                if (arg.getApvLine() != null && arg.getApvLine().size() > 0) {
                    extraMessageUtil.taskRequest(TaskRequestMessage.builder()
                            .sendUserUID(arg.getCreatedBy())
                            .receiveUserUID(arg.getApvLine().get(arg.getApvLine().size() - 2).getSancUserUid())
                            .taskTitle(arg.getDrftTitle())
                            .permalink("/#/apv/apv0102")
                            .userUID(arg.getApvLine().get(arg.getApvLine().size() - 2).getSancUserUid())
                            .build()
                    );
                }
            }
        }
//        insert apv history
        apv0101Service.updateTndmDrft(drft);
        if (!"drft".equals(arg.getType())) {
            TndmApvlHist apvlHist = new TndmApvlHist();
            apvlHist.setDrftDocNo(drft.getDrftDocNo());
            apvlHist.setSancUserUid(arg.getDocUserUid());
            apvlHist.setSancYn(ApvConstantUtils.Y);
            apvlHist.setSancDate(currentDate);
            apvlHist.setMethod(DocStatus.SUBMITTED.getValue());
            apv0101Service.insertTndmApvlHist(apvlHist);
        }
//		update reference
        apv0101Service.deleteTndmDrftReference(drft.getDrftDocNo());
        for (TndmDrftRequest item : arg.getReference()) {
            TndmDrftReference reference = new TndmDrftReference();
            reference.setDocNo(item.getDocNo());
            reference.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReference(reference);
        }
        apv0101Service.deleteTndmDrftReferer(drft.getDrftDocNo());

        for (TsstUserRequest item : arg.getReferrer()) {
            TndmDrftReferer drftReferer = new TndmDrftReferer();
            drftReferer.setUserUid(item.getUserUid());
            drftReferer.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReferer(drftReferer);
        }
//		drft type = metting
        if (FormType.METTING.getValue().equals(arg.getDrftType())) {
            TndmDrftMeetingMinuteRequest minuteRequest = new TndmDrftMeetingMinuteRequest();
            minuteRequest.setIdMeeting(arg.getIdMeeting());
            minuteRequest.setDrftDocNo(drft.getDrftDocNo());
            minuteRequest.setMeetingTitle(arg.getDrftTitle());
            minuteRequest.setPlace(arg.getPlace());
            minuteRequest.setMeetingDate(arg.getMeetingDate());
            minuteRequest.setAttached(arg.getAttached());
            minuteRequest.setDateWrite(arg.getDateWrite());
            minuteRequest.setWriter(arg.getWriter());
            minuteRequest.setPurposeMeeting(arg.getPurposeMeeting());
            minuteRequest.setSpecialNote(arg.getSpecialNote());
            apv0101Service.updateTndmDrftMeetingMinute(minuteRequest);
        } else if (
        		FormType.BUSINESS.getValue().equals(arg.getDrftType())
        		|| FormType.BUSINESS_REPORT.getValue().equals(arg.getDrftType())
        		) {
            TndmDrftBusTrip busTripRequest = new TndmDrftBusTrip();
            busTripRequest.setId(arg.getIdBus());
            busTripRequest.setDrftDocNo(drft.getDrftDocNo());
            busTripRequest.setTypeOfBusinessTrip(arg.getTypeOfBusinessTrip());
            busTripRequest.setFromDate(arg.getFromDateBus());
            busTripRequest.setToDate(arg.getToDateBus());
            busTripRequest.setCountry(arg.getCountry());
            busTripRequest.setPurpose(arg.getPurposeBus());
            busTripRequest.setTraveler(arg.getTraveler());
            busTripRequest.setOneWay(arg.getOneWay());
            busTripRequest.setTwoWay(arg.getTwoWay());
            busTripRequest.setToll(arg.getToll());
            busTripRequest.setBusTripDomestic(arg.getBusTripDomestic());
            busTripRequest.setDestination(arg.getDestination());
            busTripRequest.setVisitingAgency(arg.getVisitingAgency());
            busTripRequest.setTerm(arg.getTerm());
            apv0101Service.updateTndmDrftBusTrip(busTripRequest);
            if(arg.getBusTripDetail() != null) {
            	for (TndmDrftBusTripDetail item : arg.getBusTripDetail()) {
            		if(item.getBusTripDetailId() == null || item.getBusTripDetailId() == 0) {
            			item.setBusTripId(busTripRequest.getId());
            			apv0101dao.insertTndmDrftBusTripDetail(item);
            		}
            		else {
            			apv0101dao.updateTndmDrftBusTripDetail(item);
					}
				}
            }
            else{
            	apv0101dao.deleteTndmDrftBusTripDetail(busTripRequest.getId());
            }
        } else if (FormType.EXPENSE.getValue().equals(arg.getDrftType())) {
            TndmDrftSpendingRequest spendingRequest = new TndmDrftSpendingRequest();
            spendingRequest.setIdSpen(arg.getIdSpen());
            spendingRequest.setDrftDocNo(drft.getDrftDocNo());
            spendingRequest.setAccount(arg.getAccount());
            spendingRequest.setAccountName(arg.getAccountName());
            spendingRequest.setAccountNumber(arg.getAccountNumber());
            spendingRequest.setBankName(arg.getBankName());
            spendingRequest.setCost(arg.getCost());
            spendingRequest.setFromDateSpen(arg.getFromDateSpen());
            spendingRequest.setToDateSpen(arg.getToDateSpen());
            spendingRequest.setPurposeSpen(arg.getPurposeSpen());
            spendingRequest.setVat(arg.getVat());
            spendingRequest.setSpendingType(arg.getSpendingType());
            apv0101Service.updateTndmDrftSpending(spendingRequest);
        }

//		update attach file
        int index = 1;
        if (arg.getChangedFile()) {
            for (TccoFile file : arg.getFilesDeleted()) {
                apv0101Service.deleteTndmDrftAttachBySeq(file.getAtchFleSeq());
            }
            for (TccoFile file : arg.getFiles()) {
                TndmDrftAttachRequest attachRequest = new TndmDrftAttachRequest();
                attachRequest.setDrftAttachOrdNo(index);
                attachRequest.setDrftDocNo(drft.getDrftDocNo());
                attachRequest.setAtchFleSeq(file.getAtchFleSeq());
                apv0101Service.insertTndmDrftAttach(attachRequest);
                index++;
            }
        }

//		update apv line
        if (arg.getApvLine() != null) {
            index = arg.getApvLine().size();

            apv0101Service.deleteTndmDrftApvlLine(drft.getDrftDocNo());
            for (TndmDrftApvlLine item : arg.getApvLine()) {
                item.setDocNo(drft.getDocNo());
                item.setDrftDocNo(drft.getDrftDocNo());
                if (item.getSancDate() == null) {
                    item.setSancDate(currentDate);
                }
                item.setSancOrdNo(index);
                if (index == 1 && !"drft".equals(arg.getType())) {
                    item.setSancYn("Y");
                } else {
                    item.setSancYn("N");
                }
                index--;
                apv0101Service.insertTndmDrftApvlLine(item);
            }
        }

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PostMapping("/clone-drft")
    @Transactional(rollbackFor = {Exception.class})
    public Object clone(@RequestBody @Valid DrftRequest arg) throws Exception {
        Date currentDate = new Date();
        String oldDraftDocNo = arg.getDrftDocNo();

        // Update Old Draft
        TndmDrftRequest oldDraft = new TndmDrftRequest();
        oldDraft.setDrftDocNo(oldDraftDocNo);
        oldDraft.setRecentYn(false);
        apv0101Service.updateTndmDrft(oldDraft);

        // Clone Draft
        TndmDrftRequest drft = new TndmDrftRequest();
        drft.setDocNo(arg.getDocNo());
        drft.setDrftDocNo(comSeqService.getSeq("SEQ_DRFT_ID"));
        drft.setCreatedDate(currentDate);
        drft.setCreatedBy(arg.getCreatedBy());
        drft.setDrftTitle(arg.getDrftTitle());
        drft.setDrftContent(arg.getDrftContent());
        drft.setSendTo(arg.getSendTo());
        drft.setRecentYn(true);

        drft.setForeignOrgin(arg.getForeignOrigin());
        drft.setDrftType(arg.getDrftType());
        drft.setDrftStatus(arg.getDrftStatus());

        apv0101Service.insertTndmDrft(drft);

//		reference
        for (TndmDrftRequest item : arg.getReference()) {
            TndmDrftReference reference = new TndmDrftReference();
            reference.setDocNo(item.getDocNo());
            reference.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReference(reference);
        }

        for (TsstUserRequest item : arg.getReferrer()) {
            TndmDrftReferer drftReferer = new TndmDrftReferer();
            drftReferer.setUserUid(item.getUserUid());
            drftReferer.setDrftDocNo(drft.getDrftDocNo());
            apv0101Service.insertTndmDrftReferer(drftReferer);
        }

//		drft type = metting
        if (FormType.METTING.getValue().equals(arg.getDrftType())) {
            TndmDrftMeetingMinuteRequest minuteRequest = new TndmDrftMeetingMinuteRequest();
            minuteRequest.setDrftDocNo(drft.getDrftDocNo());
            minuteRequest.setMeetingTitle(arg.getDrftTitle());
            minuteRequest.setPlace(arg.getPlace());
            minuteRequest.setMeetingDate(arg.getMeetingDate());
            minuteRequest.setAttached(arg.getAttached());
            minuteRequest.setDateWrite(arg.getDateWrite());
            minuteRequest.setWriter(arg.getWriter());
            minuteRequest.setPurposeMeeting(arg.getPurposeMeeting());
            minuteRequest.setSpecialNote(arg.getSpecialNote());
            apv0101Service.insertTndmDrftMeetingMinute(minuteRequest);
        } else if (
        		FormType.BUSINESS.getValue().equals(arg.getDrftType())
        		|| FormType.BUSINESS_REPORT.getValue().equals(arg.getDrftType())
        		) {
            TndmDrftBusTrip busTripRequest = new TndmDrftBusTrip();
            busTripRequest.setDrftDocNo(drft.getDrftDocNo());
            busTripRequest.setTypeOfBusinessTrip(arg.getTypeOfBusinessTrip());
            busTripRequest.setFromDate(arg.getFromDateBus());
            busTripRequest.setToDate(arg.getToDateBus());
            busTripRequest.setCountry(arg.getCountry());
            busTripRequest.setPurpose(arg.getPurposeBus());
            busTripRequest.setTraveler(arg.getTraveler());
            busTripRequest.setOneWay(arg.getOneWay());
            busTripRequest.setTwoWay(arg.getTwoWay());
            busTripRequest.setToll(arg.getToll());
            busTripRequest.setBusTripDomestic(arg.getBusTripDomestic());
            busTripRequest.setDestination(arg.getDestination());
            busTripRequest.setVisitingAgency(arg.getVisitingAgency());
            busTripRequest.setTerm(arg.getTerm());
            apv0101Service.insertTndmDrftBusTrip(busTripRequest);
            if(arg.getBusTripDetail() != null) {
            	for (TndmDrftBusTripDetail item : arg.getBusTripDetail()) {
            		if(item.getBusTripDetailId() == null || item.getBusTripDetailId() == 0) {
            			item.setBusTripId(busTripRequest.getId());
            			apv0101dao.insertTndmDrftBusTripDetail(item);
            		}
            		else {
            			apv0101dao.updateTndmDrftBusTripDetail(item);
					}
				}
            }
            else{
            	apv0101dao.deleteTndmDrftBusTripDetail(busTripRequest.getId());
            }
        } else if (FormType.EXPENSE.getValue().equals(arg.getDrftType())) {
            TndmDrftSpendingRequest spendingRequest = new TndmDrftSpendingRequest();
            spendingRequest.setDrftDocNo(drft.getDrftDocNo());
            spendingRequest.setAccount(arg.getAccount());
            spendingRequest.setAccountName(arg.getAccountName());
            spendingRequest.setAccountNumber(arg.getAccountNumber());
            spendingRequest.setBankName(arg.getBankName());
            spendingRequest.setCost(arg.getCost());
            spendingRequest.setFromDateSpen(arg.getFromDateSpen());
            spendingRequest.setToDateSpen(arg.getToDateSpen());
            spendingRequest.setPurposeSpen(arg.getPurposeSpen());
            spendingRequest.setVat(arg.getVat());
            spendingRequest.setSpendingType(arg.getSpendingType());
            apv0101Service.insertTndmDrftSpending(spendingRequest);
        }

        Map<Object, Object> params = new HashMap<>();
        params.put("drftDocNo", oldDraftDocNo);
        List<?> oldDraftAttaches = apv0101Service.searchTndmDrftAttach(params);
        List<TccoFile> deletedFiles = arg.getFilesDeleted();

        // Update old file
        int index = 1;
        if (oldDraftAttaches != null) {
            for (Object attachOject : oldDraftAttaches) {
                TndmDrftAttachResponse attach = (TndmDrftAttachResponse) attachOject;
                boolean isDeletedFile = false;
                if (deletedFiles != null) {
                    for (TccoFile deletedFile : deletedFiles) {
                        if (attach.getAtchFleSeq().equals(deletedFile.getAtchFleSeq())) {
                            isDeletedFile = true;
                            break;
                        }
                    }
                }

                if (!isDeletedFile) {
                    TndmDrftAttachRequest attachRequest = new TndmDrftAttachRequest();
                    attachRequest.setId(attach.getId());
                    attachRequest.setDrftDocNo(drft.getDrftDocNo());
                    attachRequest.setDrftAttachOrdNo(index);
                    apv0101Service.updateTndmDrftAttach(attachRequest);
                    index++;
                }
            }
        }

        // insert attach file
        if (arg.getFiles() != null) {
            for (TccoFile file : arg.getFiles()) {
                TndmDrftAttachRequest attachRequest = new TndmDrftAttachRequest();
                attachRequest.setDrftAttachOrdNo(index);
                attachRequest.setDrftDocNo(drft.getDrftDocNo());
                attachRequest.setAtchFleSeq(file.getAtchFleSeq());
                apv0101Service.insertTndmDrftAttach(attachRequest);
                index++;
            }
        }

        // clone apvlLine
        if (arg.getApvLine() != null) {
            for (TndmDrftApvlLine apvlLine : arg.getApvLine()) {
                apvlLine.setIdApvlLine(null);
                apvlLine.setDrftDocNo(drft.getDrftDocNo());
                apvlLine.setSancDate(currentDate);
                apv0101Service.insertTndmDrftApvlLine(apvlLine);
            }
        }

        // Clone APVL_HIST
        params = new HashMap<>();
        params.put("drftDocNo", oldDraftDocNo);
        List<TndmApvlHist> apvlHistList = this.apv0101Service.searchTndmApvlHist(params);
        if (apvlHistList != null) {
            for (TndmApvlHist apvlHist : apvlHistList) {
                apvlHist.setId(null);
                apvlHist.setDrftDocNo(drft.getDrftDocNo());
                this.apv0101Service.insertTndmApvlHist(apvlHist);
            }
        }

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

	@PostMapping("/approveDocument")
//	@MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseData<?> approveDocument(@RequestBody Map<Object, Object> arg) throws Exception {
        String currentUserUid = (String) arg.get("userUid");
        String comment = (String) arg.get("comment");
        String approvalAction = (String) arg.get("approvalAction");
        String draftDocNo = (String) arg.get("draftDocNo");

        Map<Object, Object> params = new HashMap<>();
        params.put("draftDocNo", draftDocNo);
        List<TndmDrftApvlLine> apvlLineList = apv0101Service.getDraftApvlLineList(params);
        Validate.notEmpty(apvlLineList, "Empty apvl Line");

        MutablePair<TndmDrftApvlLine, TndmDrftApvlLine> apvlLinePair = apv0101Service
                .getCurrentAndNextApvlLine(apvlLineList, currentUserUid);
        TndmDrftApvlLine currentApvlLine = apvlLinePair.getLeft();
        TndmDrftApvlLine nextApvlLine = apvlLinePair.getRight();
        Validate.notNull(currentApvlLine, "Logged in user does not exist in Apvl Line");

        if (ImwareStringUtils.YES.equals(currentApvlLine.getSancYn())) {
            return ResponseData.error("message.error.youHaveApprovedThisDocument");
        }

        String delegateUserUid = apv0101Service.getDelegateUserUid(nextApvlLine);
        int approvalCase = apv0101Service.getApproveCase(apvlLineList, currentApvlLine, nextApvlLine, delegateUserUid);
        DrftRequest tndmDraft = apv0101Service.getDrftRequest(draftDocNo);
        if (ImwareStringUtils.APPROVE.equals(approvalAction)) {
            Map<Object, Object> internalFile = null;
            Map<Object, Object> sendingOutFile = null;
            TndmDocMstRequest tempDocMst = null;
            if (apv0101Service.isEndApprovalCase(approvalCase)) {
            	apvlLineList.forEach(line -> {
            		line.setSancYn(ImwareStringUtils.YES);
            	});
                tndmDraft.setApvLine(apvlLineList);

            	tempDocMst = new TndmDocMstRequest();
                tempDocMst.setDocNo(tndmDraft.getDocNo());
                tempDocMst.setFinishedDate(new Date());
                tempDocMst.setTagDoc(apv0101Service.genTagDoc(tndmDraft));
                
                if(ApvConstantUtils.FormType.LETTER.getValue().equals(tndmDraft.getDrftType())) {
                	// Letter
                    TndmFormMgt sendingOutForm = null;
                    TndmFormMgt internalForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.LETTER.getValue());
                    if (internalForm == null) {
                    	return this.errorFormNotFound();
                    }
                    if(tndmDraft.getForeignOrigin()) {
                        sendingOutForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.SENDING_LETTER.getValue());
                        if (sendingOutForm == null) {
                        	return this.errorFormNotFound();
                        }
                    	
                        ResponseData<Map<Object, Object>> sendingOutResult = this.export(tndmDraft, tempDocMst, sendingOutForm);
                        sendingOutFile = sendingOutResult.getData();
                    }
                    ResponseData<Map<Object, Object>> internalResult = this.export(tndmDraft, tempDocMst, internalForm);
                    internalFile = internalResult.getData();
                	
                }
                else {
                	TndmFormMgt form = sys0112Service.getActiveTndmFormMgtBy(tndmDraft.getDrftType());
                    if (form == null) {
                    	return this.errorFormNotFound();
                    }
                    
                    ResponseData<Map<Object, Object>> internalResult = this.export(tndmDraft, tempDocMst, form);
                    internalFile = internalResult.getData();
                }
            }
            this.apv0101Service.approveDocument(tndmDraft, apvlLineList, currentApvlLine, approvalCase, comment, delegateUserUid,
            		internalFile, tempDocMst);
            
            if (apv0101Service.isEndApprovalCase(approvalCase) 
            		&& ApvConstantUtils.FormType.LETTER.getValue().equals(tndmDraft.getDrftType())
            		&& tndmDraft.getForeignOrigin()) {
            	apv0101Service.cloneForeignOriginLetter(tndmDraft, sendingOutFile);
            }

        } else if (ImwareStringUtils.REJECT.equals(approvalAction)) {
            this.apv0101Service.rejectDocument(apvlLineList, currentApvlLine, approvalCase, comment);
        }

        extraMessageUtil.taskRequest(TaskRequestMessage.builder()
                .sendUserUID(currentUserUid)
                .receiveUserUID(tndmDraft.getDocUserUid())
                .taskTitle(tndmDraft.getDrftTitle())
                .permalink("/#/apv/apv0104")
                .userUID(tndmDraft.getDocUserUid())
                .build()
        );
        
        if (ApvConstantUtils.FormType.VACATION.getValue().equals(tndmDraft.getDrftType())) {
			Map<Object, Object> vacationApvlMap = new HashMap<Object, Object>();
			vacationApvlMap.put("VACATION_RQST_ID", tndmDraft.getVacationId());
			vacationApvlMap.put("USER_UID", currentUserUid);

			TcdsEmpVacationApprovalResponse vacationApvl = cms0102Service.getVRForUsageHistory(vacationApvlMap);
			if (vacationApvl != null) {
				if (ImwareStringUtils.APPROVE.equals(approvalAction)) {
					vacationApvl.setStatusApvl("15-02");
				} else {
					vacationApvl.setStatusApvl("15-03");
				}
				cms0102Service.updateVacation(vacationApvl);
			} else {
	            return ResponseData.error("message.error.youHaveApprovedThisDocument");
			}
		}

        return ResponseData.success();
    }

    @PostMapping("/exportPdf")
    // @MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseData<Map<Object, Object>> export(@RequestBody Map<Object, Object> params) throws Exception {
    	DrftRequest tndmDraft = apv0101Service.getDrftRequest((String) params.get("draftDocNo"));
    	Validate.notNull(tndmDraft, "Invalid draft doc no.");
    	Boolean showApvlLine = (Boolean) params.get("showApvlLine");
    	if(showApvlLine != null && showApvlLine) {
    		List<TndmDrftApvlLine> apvlLineList = apv0101Service.getDraftApvlLineList(params);
    		tndmDraft.setApvLine(apvlLineList);
    	}
    	else {
    		tndmDraft.setApvLine(null);
    	}
    	
        return this.export(tndmDraft, null, null);
    }
    
    @PostMapping("/exportReceivedLetterPdf")
    // @MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseData<Map<Object, Object>> exportReceivedLetterPdf(@RequestBody Map<Object, Object> params) throws Exception {
    	Long id = ((Number) params.get("id")).longValue();
    	TndmDocReceived docReceived = apv0107Service.getTndmDocReceived(id);
    	Validate.notNull(docReceived);
    	
        TndmFormMgt internalForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.LETTER.getValue());
        if (internalForm == null) {
        	return this.errorFormNotFound();
        }
        String formContent = internalForm.getFormContent();
        
        
        formContent = formContent.replaceAll("_DOC_TYPE_", ImwareStringUtils.nullToEmpty(docReceived.getNameKr()));
        formContent = formContent.replaceAll("_TITLE_", ImwareStringUtils.nullToEmpty(docReceived.getTitle()));
        formContent = formContent.replaceAll("_CONTENT_", ImwareStringUtils.nullToEmpty(docReceived.getContent()));
        formContent = formContent.replaceAll("_CEO_SIGN_", ImwareStringUtils.nullToEmpty(null));
        formContent = formContent.replaceAll("_SIGNATURE_", ImwareStringUtils.nullToEmpty(null));
        formContent = formContent.replaceAll("_SUBMITED_ACCOUNT_", ImwareStringUtils.nullToEmpty(null));
        formContent = formContent.replaceAll("_SUBMITED_MAIL_TAIL_", ImwareStringUtils.nullToEmpty(null));
        
        String tagDoc = ImwareStringUtils.nullToEmpty(docReceived.getOfficialNo());
        String finishedDate = docReceived.getCreatedDate() == null ? ImwareStringUtils.EMPTY : 
        	DateUtil.formatter2.format(docReceived.getCreatedDate());;
        String footerSection = this.genTagDocFooter(tagDoc, finishedDate);
        formContent = formContent.replaceAll("_FOOTER_SECTION_", ImwareStringUtils.nullToEmpty(footerSection));
        
    	
    	Map<Object, Object> fileInfo = this.processPdf(formContent, internalForm);
    	
        return ResponseData.success(fileInfo);
    }
    
    @PostMapping("/previewPdf")
    // @MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseData<Map<Object, Object>> previewPdf(@RequestBody DrftRequest tndmDraft) throws Exception {
        return this.export(tndmDraft, null, null);
    }

    @GetMapping("/get-tndm-form")
    public Object searchTndmFormMgt(@RequestParam Map arg) throws SQLException {
        return apv0101Service.searchTndmFormMgt(arg);
    }
    
    @PostMapping("/sendingLetterPdf")
    // @MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseData<Map<Object, Object>> sendingLetterPdf(@RequestBody Map<Object, Object> params) throws Exception {
    	Map<Object, Object> fileInfo = new HashMap<>();
    	DrftRequest tndmDraft = apv0101Service.getDrftRequest((String) params.get("draftDocNo"));
    	Validate.notNull(tndmDraft, "Invalid draft doc no.");
    	Boolean showApvlLine = (Boolean) params.get("showApvlLine");
    	if(showApvlLine != null && showApvlLine) {
    		List<TndmDrftApvlLine> apvlLineList = apv0101Service.getDraftApvlLineList(params);
    		tndmDraft.setApvLine(apvlLineList);
    	}
    	
        TndmFormMgt sendingOutForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.SENDING_LETTER.getValue());
        TndmFormMgt internalForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.LETTER.getValue());
        if (sendingOutForm == null || internalForm == null) {
        	return this.errorFormNotFound();
        }
    	
    	
        ResponseData<Map<Object, Object>> sendingOutResult = this.export(tndmDraft, null, sendingOutForm);
        ResponseData<Map<Object, Object>> internalResult = this.export(tndmDraft, null, internalForm);
        
        fileInfo.put("sendingOutFile", sendingOutResult.getData());
        fileInfo.put("internalFile", internalResult.getData());
        return ResponseData.success(fileInfo);
    }
    
    public ResponseData<Map<Object, Object>> errorFormNotFound() {
    	Map<Object, Object> err = new HashMap<>();
        err.put("error", "message.error.formNotFound");
        return ResponseData.error(err);
    }

    @TransactionalWithRollback
    public ResponseData<Map<Object, Object>> export(DrftRequest tndmDraft, TndmDocMstRequest tempDocMst, TndmFormMgt form)
            throws Exception {
//        Map<Object, Object> fileInfo = new HashMap<>();

        if(form == null) {
        	form = sys0112Service.getActiveTndmFormMgtBy(tndmDraft.getDrftType());
            if (form == null) {
                return this.errorFormNotFound();
            }
        }


        TndmDocMstRequest docMst = apv0101Service.getTndmDocMst(tndmDraft.getDocNo());
        
        if (tempDocMst != null) {
            docMst.setFinishedDate(tempDocMst.getFinishedDate());
            docMst.setTagDoc(tempDocMst.getTagDoc());
        }
        String tagDoc = docMst.getTagDoc() == null ? ImwareStringUtils.EMPTY : docMst.getTagDoc();

        List<TndmDrftApvlLine> draftApvlLineList = tndmDraft.getApvLine();
        if (draftApvlLineList != null) {
            Collections.sort(draftApvlLineList, new Comparator<TndmDrftApvlLine>() {
                public int compare(TndmDrftApvlLine o1, TndmDrftApvlLine o2) {
                    if (o1.getSancOrdNo() == o2.getSancOrdNo()) {
                        return 0;
                    }
                    return o1.getSancOrdNo() < o2.getSancOrdNo() ? -1 : 1;
                }
            });
        }

        String signature = "";

        String formContent = form.getFormContent();

        if (ApvConstantUtils.FormType.METTING.getValue().equals(form.getFormType())) {

            formContent = formContent.replaceAll("_MEETING_WRITER_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getWriter(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_DATE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getMeetingDate(), ImwareStringUtils.EMPTY));

            String createdDate = DateUtil.formatter2.format(tndmDraft.getCreatedDate());
            formContent = formContent.replaceAll("_MT_CREATED_DATE_",
                    ImwareStringUtils.defaultIfBlank(createdDate, ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_PLACE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getPlace(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_CONTENT_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_ATTENDEES_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getAttached(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_PURPOSE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getPurposeMeeting(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_MEETING_NOTE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getSpecialNote(), ImwareStringUtils.EMPTY));

            signature = this.getSignature(draftApvlLineList);

            formContent = formContent.replaceAll("_SIGNATURE_",
                    ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));

//            fileInfo = apvPdfService.genPdf(formContent);
//        	
//        	return ResponseData.success(fileInfo);
        } 
        else if (ApvConstantUtils.FormType.VACATION.getValue().equals(form.getFormType())) {
        	
        	Long vacationRqstId = tndmDraft.getVacationId();
			Map<Object, Object> vacationRequestMap = new HashMap<Object, Object>();
			vacationRequestMap.put("ID", vacationRqstId);

			TcdsEmpVacationRequestResponse vacationRequest = cms0102Service
					.getTcdsEmpVacationRequest(vacationRequestMap);

			formContent = formContent.replaceAll("_NAME_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getNameKr(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_DUTY_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getDutyName(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_VACATION_TYPE_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getVacationTypeString(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_PHONENUMBER_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getContactNetwork(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_POSITION_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getDutyName(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_REASON_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getReason(), ImwareStringUtils.EMPTY));

			String startDate = DateUtil.formatter2.format(vacationRequest.getFromDate());
			String endDate = DateUtil.formatter2.format(vacationRequest.getToDate());
			int day = vacationRequest.getTimeLeave() / 8;
			int hours = vacationRequest.getTimeLeave() % 8;
			String dayString = String.valueOf(day);
			String hoursString = String.valueOf(hours);
			
			if (vacationRequest.getVacationType().equals("14-00")) {
				dayString = "0.5";
				hoursString = "0";
			} else {
				hours = day - 1;
				hoursString = String.valueOf(hours);
			}

			formContent = formContent.replaceAll("_FROM_",
					ImwareStringUtils.defaultIfBlank(startDate, ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_TO_",
					ImwareStringUtils.defaultIfBlank(endDate, ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_DEST_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getDestination(), ImwareStringUtils.EMPTY));
			formContent = formContent.replaceAll("_REMARK_",
					ImwareStringUtils.defaultIfBlank(vacationRequest.getRemark(), ImwareStringUtils.EMPTY));
			
			if (vacationRequest.getVacationType().equals("14-05")) {
				hoursString = new SimpleDateFormat("HH:mm").format(vacationRequest.getFromDate());
				formContent = formContent.replaceAll("_NIGHT_",
						ImwareStringUtils.defaultIfBlank(hoursString, ImwareStringUtils.EMPTY));
				dayString = new SimpleDateFormat("HH:mm").format(vacationRequest.getToDate());
				formContent = formContent.replaceAll("_DAY_",
						ImwareStringUtils.defaultIfBlank(dayString, ImwareStringUtils.EMPTY));
			} else if (vacationRequest.getVacationType().equals("14-02-01") || vacationRequest.getVacationType().equals("14-02-02")) {
				formContent = formContent.replaceAll("_NIGHT_",
						ImwareStringUtils.defaultIfBlank(hoursString, ImwareStringUtils.EMPTY));
				formContent = formContent.replaceAll("_DAY_일",
						ImwareStringUtils.defaultIfBlank(dayString + "박<br><span style=\"font-family: 휴먼명조; font-size: 12pt;\">비고: " + vacationRequest.getVacationTypeString() + "</span>", ImwareStringUtils.EMPTY));
				
			} else if (vacationRequest.getVacationType().equals("14-00")) {
				formContent = formContent.replaceAll("_NIGHT_",
						ImwareStringUtils.defaultIfBlank(hoursString, ImwareStringUtils.EMPTY));
				String hourCheck = new SimpleDateFormat("HH:mm").format(vacationRequest.getFromDate());
				if (hourCheck.equals("08:00")) {
					formContent = formContent.replaceAll("_DAY_일",
							ImwareStringUtils.defaultIfBlank(dayString + "박<br><span style=\"font-family: 휴먼명조; font-size: 12pt;\">비고: 오전</span>", ImwareStringUtils.EMPTY));
				} else if (hourCheck.equals("14:00")) {
					formContent = formContent.replaceAll("_DAY_일",
							ImwareStringUtils.defaultIfBlank(dayString + "박<br><span style=\"font-family: 휴먼명조; font-size: 12pt;\">비고: 오후</span>", ImwareStringUtils.EMPTY));
				}
			} else {
				formContent = formContent.replaceAll("_NIGHT_",
						ImwareStringUtils.defaultIfBlank(hoursString, ImwareStringUtils.EMPTY));
				formContent = formContent.replaceAll("_DAY_",
						ImwareStringUtils.defaultIfBlank(dayString, ImwareStringUtils.EMPTY));
			}
			
			
			
			
			String tagDocSection = ImwareStringUtils.EMPTY;
			formContent = this.genCommonLetterForm(docMst, draftApvlLineList, formContent, tndmDraft, tagDoc, tagDocSection);
			
//            fileInfo = apvPdfService.genPdf(formContent);
//        	
//        	return ResponseData.success(fileInfo);
        } 
        else if (ApvConstantUtils.FormType.BUSINESS_REPORT.getValue().equals(form.getFormType())) {
        	formContent = formContent.replace("_TAG_DOC_",
                    ImwareStringUtils.defaultIfBlank(tagDoc, ImwareStringUtils.EMPTY));
        	
            String finishedDate = docMst.getFinishedDate() == null ? ImwareStringUtils.EMPTY
                    : DateUtil.formatter2.format(docMst.getFinishedDate());
        	formContent = formContent.replace("_FINISHED_DATE_",
        			ImwareStringUtils.defaultIfBlank(finishedDate, ImwareStringUtils.EMPTY));
        	
			signature = apvPdfService.getBizReportSignature(draftApvlLineList);
			formContent = formContent.replace("_SIGNATURE_",
                  ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));
			
			formContent = formContent.replace("_TRAVELLER_",
					ImwareStringUtils.defaultIfBlank(tndmDraft.getTraveler(), ImwareStringUtils.EMPTY));
			
			String fromDate = tndmDraft.getFromDateBus() == null ? ImwareStringUtils.EMPTY 
					: DateUtil.formatter4.format(tndmDraft.getFromDateBus());
			String toDate = tndmDraft.getToDateBus() == null ? ImwareStringUtils.EMPTY 
					: DateUtil.formatter4.format(tndmDraft.getToDateBus());;
			String period =  fromDate + " ~ " + toDate;
			formContent = formContent.replace("_PERIOD_",
					ImwareStringUtils.defaultIfBlank(period, ImwareStringUtils.EMPTY));
			
			formContent = formContent.replace("_DESTINATION_",
					ImwareStringUtils.defaultIfBlank(tndmDraft.getDestination(), ImwareStringUtils.EMPTY));
			
			formContent = formContent.replace("_VISIT_AGENCY_",
					ImwareStringUtils.defaultIfBlank(tndmDraft.getVisitingAgency(), ImwareStringUtils.EMPTY));
			
			formContent = formContent.replace("_PURPOSE_",
					ImwareStringUtils.defaultIfBlank(tndmDraft.getPurposeBus(), ImwareStringUtils.EMPTY));
			
			formContent = formContent.replace("_REPORT_DETAIL_",
					ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));
			

			TndmFormMgt bizReportDetailForm = null;
			if(ImwareStringUtils.YES.equals(tndmDraft.getTypeOfBusinessTrip())) {
				bizReportDetailForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.BIZ_REPORT_DOMESTIC_DETAIL.getValue());
			}
			else {
				bizReportDetailForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.BIZ_REPORT_OVERSEAS_DETAIL.getValue());
			}
			if (bizReportDetailForm == null) {
            	return this.errorFormNotFound();
            }
			
			
			String reportTripDetail = this.getBizDetail(tndmDraft, bizReportDetailForm);
        
			formContent = formContent.replace("_REPORT_TRIP_DETAIL_",
				ImwareStringUtils.defaultIfBlank(reportTripDetail, ImwareStringUtils.EMPTY));
			
			Map<Object, Object> fileInfo = apvPdfService.genPdf(formContent);
        	return ResponseData.success(fileInfo);
        } 
        else if (ApvConstantUtils.FormType.BUSINESS.getValue().equals(form.getFormType())) {

        	String deptNames = ImwareStringUtils.EMPTY;
        	String positions = ImwareStringUtils.EMPTY;
        	String names = ImwareStringUtils.EMPTY;
        	String place = ImwareStringUtils.EMPTY;
        	
        	
			signature = apvPdfService.getBizReportSignature(draftApvlLineList);
			formContent = formContent.replace("_SIGNATURE_",
                  ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));
        	
	        List<TndmDrftBusTripDetail> trips = tndmDraft.getBusTripDetail();
	        if(trips == null) {
	        	trips = apv0101Service.getTndmDrftBusTripDetail(tndmDraft.getIdBus());
	        }
	        
	        if(trips != null) {
	        	String span = "<span style=\"display:block; padding: 3px;\">_TARGET_</span>";
	        	for(TndmDrftBusTripDetail trip: trips) {
	        		positions = positions + span.replace("_TARGET_", ImwareStringUtils.nullToEmpty(trip.getDutyName()));
	        		names = names + span.replace("_TARGET_", ImwareStringUtils.nullToEmpty(trip.getTraveler()));
	        		deptNames = deptNames + span.replace("_TARGET_", ImwareStringUtils.nullToEmpty(trip.getTravelerDept()));
	        	}

	        }

			String fromDate = tndmDraft.getFromDateBus() == null ? ImwareStringUtils.EMPTY 
					: DateUtil.formatter2.format(tndmDraft.getFromDateBus());
			String toDate = tndmDraft.getToDateBus() == null ? ImwareStringUtils.EMPTY 
					: DateUtil.formatter2.format(tndmDraft.getToDateBus());;
			String period =  fromDate + " ~ " + toDate;
			formContent = formContent.replace("_PERIOD_",
					ImwareStringUtils.defaultIfBlank(period, ImwareStringUtils.EMPTY));

			if(ImwareStringUtils.YES.equals(tndmDraft.getTypeOfBusinessTrip())) {
				if("1".equals(tndmDraft.getBusTripDomestic())) {
					place = "서울";
				}
				else if("2".equals(tndmDraft.getBusTripDomestic())) {
					place = "광역시";
				}
				else if("3".equals(tndmDraft.getBusTripDomestic())) {
					place = "기타지역";
				}
			}
			else {
				place = tndmDraft.getCountry();
			}

            formContent = formContent.replaceAll("_PLACE_",
                    ImwareStringUtils.defaultIfBlank(place, ImwareStringUtils.EMPTY));
            // Actually it's travelers
            formContent = formContent.replaceAll("_DEPARTMENT_",
                    ImwareStringUtils.defaultIfBlank(deptNames, ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_POSITION_",
                    ImwareStringUtils.defaultIfBlank(positions, ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_BIZ_NAME_",
                    ImwareStringUtils.defaultIfBlank(names, ImwareStringUtils.EMPTY));
            
            formContent = formContent.replaceAll("_PURPOSE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getPurposeBus(), ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_BIZ_CONTENT_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));
            

			TndmFormMgt bizDetailForm = null;
			if(ImwareStringUtils.YES.equals(tndmDraft.getTypeOfBusinessTrip())) {
				bizDetailForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.BIZ_DOMESTIC_DETAIL.getValue());
			}
			else {
				bizDetailForm = sys0112Service.getActiveTndmFormMgtBy(ApvConstantUtils.FormType.BIZ_OVERSEAS_DETAIL.getValue());
			}
			if (bizDetailForm == null) {
            	return this.errorFormNotFound();
            }
            
            String tripDetail = this.getBizDetail(tndmDraft, bizDetailForm);
            
//            signature = apvPdfService.getBizReportSignature(draftApvlLineList);
//            formContent = formContent.replaceAll("_SIGNATURE_",
//                    ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));
            
            
            Map<Object, Object> fileInfo = apvPdfService.genPdf(formContent + tripDetail);
        	
        	return ResponseData.success(fileInfo);
        } else if (ApvConstantUtils.FormType.EXPENSE.getValue().equals(form.getFormType())) {
            formContent = formContent.replace("_SP_DETAIL_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));

            
            DecimalFormat decimalFormat = new DecimalFormat("###,###");
            String costStr = "0";
            String costKr = "";
            Long costLong = 0l;
            if(tndmDraft.getCost() != null) {
            	costLong = Math.round(tndmDraft.getCost());
            	costStr = String.valueOf(costLong);
            	
                costKr = Apv0101Service.convertMoneyToHangul(costStr);
                costStr = decimalFormat.format(costLong);
            }
            
            formContent = formContent.replace("_SP_SUM_",
                    ImwareStringUtils.defaultIfBlank(costStr, ImwareStringUtils.EMPTY));
            formContent = formContent.replace("_SP_KR_SUM_",
            		ImwareStringUtils.defaultIfBlank(costKr, ImwareStringUtils.EMPTY));

            signature = this.getSignature(draftApvlLineList);

            formContent = formContent.replace("_SIGNATURE_",
                    ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));

        } 
        
        else if (ApvConstantUtils.FormType.LETTER.getValue().equals(form.getFormType())
        		|| ApvConstantUtils.FormType.SENDING_LETTER.getValue().equals(form.getFormType())) {
            String receiver = "";
//            String submitedDate = docMst.getSubmittedDate() == null ? ImwareStringUtils.EMPTY
//                    : DateUtil.formatter2.format(docMst.getSubmittedDate());
            
           

            formContent = formContent.replace("_CONTENT_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));

            formContent = formContent.replace("_TITLE_",
                    ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftTitle(), ImwareStringUtils.EMPTY));



            String tagDocSection = ImwareStringUtils.EMPTY;
            String ceoSign = ImwareStringUtils.EMPTY;
            if (ApvConstantUtils.FormType.SENDING_LETTER.getValue().equals(form.getFormType())) {
                
                String serverUrl = environment.getProperty("server.host") + ":" + environment.getProperty("server.port");
                
                ceoSign = "<p style=\"margin-top: 10px;font-size:24pt;font-weight:bold;font-family:gungsuh, serif;text-align:center;margin-bottom:15px;\">\r\n"
                        + "              <span>(주) 에이투엠 대표이사</span>\r\n"
                        + "              <img style=\"vertical-align:middle\" src=\"" + serverUrl + "/images/ceo_sign.png\">\r\n"
                        + "          </p>";

//                tagDocSection = "<p style=\"\">\r\n"
//                        + "	<span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">시행 </span>\r\n"
//                        + "	<span style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">_TAG_DOC_ (_FINISH_APPROVING_DATE_)&nbsp;</span>\r\n"
//                        + "	<span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">접수 _DRATER_</span>\r\n"
//                        + "	<span style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">(_SUBMIT_DATE_)</span>\r\n"
//                        + "</p>";
//                tagDocSection = tagDocSection.replace("_TAG_DOC_",
//                        ImwareStringUtils.defaultIfBlank(tagDoc, ImwareStringUtils.EMPTY));
//                tagDocSection = tagDocSection.replace("_FINISH_APPROVING_DATE_",
//                        ImwareStringUtils.defaultIfBlank(finishedDate, ImwareStringUtils.EMPTY));
//                tagDocSection = tagDocSection.replace("_SUBMIT_DATE_",
//                        ImwareStringUtils.defaultIfBlank(submitedDate, ImwareStringUtils.EMPTY));
//
//                TcdsEmpMstResponse drafter = sys0105Service.getEmpMstByUserUid(tndmDraft.getCreatedBy());
//                tagDocSection = tagDocSection.replace("_DRATER_",
//                        ImwareStringUtils.defaultIfBlank(drafter.getNameKr(), ImwareStringUtils.EMPTY));

                receiver = tndmDraft.getSendTo();
            } else {
                receiver = "내부결재";
//                tagDocSection = "<p style=\"\">\r\n"
//                        + "	<span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">접수 _DRATER_</span>"
//                        + "</p>";
//                TcdsEmpMstResponse drafter = sys0105Service.getEmpMstByUserUid(tndmDraft.getCreatedBy());
//                tagDocSection = tagDocSection.replace("_DRATER_",
//                        ImwareStringUtils.defaultIfBlank(drafter.getNameKr(), ImwareStringUtils.EMPTY));
            }

            formContent = formContent.replace("_DOC_TYPE_",
                    ImwareStringUtils.defaultIfBlank(receiver, ImwareStringUtils.EMPTY));
            formContent = formContent.replaceAll("_CEO_SIGN_",
                    ImwareStringUtils.defaultIfBlank(ceoSign, ImwareStringUtils.EMPTY));

            
            formContent = this.genCommonLetterForm(docMst, draftApvlLineList, formContent, tndmDraft, tagDoc, tagDocSection);
            
        } else {
            throw new Exception("Invalid form type!!!");
        }

        Map<Object, Object> fileInfo = this.processPdf(formContent, form);

        return ResponseData.success(fileInfo);

    }
    
    private Map<Object, Object> processPdf(String formContent, TndmFormMgt form) throws Exception {
    	Map<Object, Object> fileInfo = new HashMap<>();
    	
    	formContent = apvPdfService.updateFonts(formContent);

        String atchFileSeq = java.util.UUID.randomUUID().toString();
        String fileNamePhysical = atchFileSeq.replace("-", "");
        String path = pdfUploadDir + "/" + fileNamePhysical + ".pdf";
        fileInfo.put("fileNamePhysical", fileNamePhysical);
        fileInfo.put("FLE_TP", "pdf");
        fileInfo.put("ATCH_FLE_SEQ", atchFileSeq);

        // content = ApvLineProcessingService.addHeaderAndFooter(content, baseURL);
        if (!formContent.startsWith("<html>")) {
            formContent = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body>"
                    + formContent
                    + "<page size=\"a4\" width=\"210mm\" height=\"297mm\" orientation=\"portrait\" margin-left=\"20mm\" margin-top=\"10mm\" margin-right=\"20mm\" margin-bottom=\"10mm\" border-left=\"\" border-left-margin=\"5mm\" border-top=\"\" border-top-margin=\"5mm\" border-right=\"\" border-right-margin=\"5mm\" border-bottom=\"\" border-bottom-margin=\"5mm\" background-color=\"\" background-repeat=\"\" background-size=\"cover\" footnote-seperator=\"1pt solid rgb(0, 0, 0)\" footnote-seperator-align=\"left\" footnote-seperator-width=\"25%\" endnote-seperator=\"1pt solid rgb(0, 0, 0)\" endnote-seperator-align=\"left\" endnote-seperator-width=\"25%\" start-page-numbers=\"1\"><pageheader display=\"both\" height=\"15mm\"></pageheader><pagefooter display=\"both\" height=\"15mm\"></pagefooter><footnotes></footnotes><endnotes></endnotes></page></body></html>";
        }

        org.jsoup.nodes.Document document = Jsoup.parse(formContent, "", Parser.xmlParser());
        document.outputSettings().escapeMode(org.jsoup.nodes.Entities.EscapeMode.xhtml);


        if (!(new File(pdfUploadDir)).isDirectory()) {
            new File(pdfUploadDir).mkdir();
        }

        if (path != null && !path.trim().equals("")) {
            ITextRenderer renderer = new ITextRenderer();

            this.resolveFonts(renderer);

            // Add blank to doc to avoid overlap start
            Document cloneDoc = document;

            try {
//                Element header = cloneDoc.getElementById("template_header");
//                int headerHeight = 0;
//                if (header != null) {
//                    headerHeight = Apv0101Service.getElementHeight(header, renderer);
//                }
//
//                Element footer = cloneDoc.getElementById("template_footer");
//                int footerHeight = 0;
//                if (footer != null) {
//                    footerHeight = Apv0101Service.getElementHeight(footer, renderer);
//                }
//
//                int pageHeight = 0;
//                pageHeight = Apv0101Service.getElementHeight(cloneDoc, renderer);
//
//                int a4Height = Apv0101Service.getA4PageHeight(renderer);
//
//                int pageNum = (pageHeight / a4Height) + 1;
//
//                int eContentHeight = pageNum * (a4Height * 277 / 297) - headerHeight - footerHeight;
//
//                if (header != null) {
//                    header.remove();
//                }
//                if (footer != null) {
//                    footer.remove();
//                    Element body = cloneDoc.getElementsByTag("body").get(0);
//                    Apv0101Service.addBlankToElement(body, renderer, eContentHeight);
//                    if (header != null) {
//                        body.insertChildren(0, header);
//                    }
//
//                    body.insertChildren(-1, footer);
//                }

              
              int a4Height = Apv0101Service.getA4PageHeight(renderer);
              int pageHeight = Apv0101Service.getElementHeight(cloneDoc, renderer);
              int pageNum = (pageHeight / a4Height) + 1;
              
              Element letterContent = cloneDoc.getElementById("letter_id");
              if (letterContent != null) {
            	  
            	  int letterHeight = Apv0101Service.getElementHeight(letterContent, renderer);
            	  
            	  int addHeight = pageNum * a4Height - pageHeight + letterHeight;
            	  String style = letterContent.attr("style");
            	  System.out.println(style);
            	  
            	  if(pageNum == 1) {
            		  addHeight = 20;
            	  }
            	  else {
            		  addHeight = 842 * (pageNum - 1);
            	  }
            	  
            	  int i = 1;
            	  
            	  String tableWrapperStyle = "";
            	  if (ApvConstantUtils.FormType.SENDING_LETTER.getValue().equals(form.getFormType())
            			  || ApvConstantUtils.FormType.LETTER.getValue().equals(form.getFormType())
            			  || ApvConstantUtils.FormType.VACATION.getValue().equals(form.getFormType())) {
            		  tableWrapperStyle = "border: none; vertical-align: top; height: ";
            	  }
            	  else {
            		  tableWrapperStyle = "width: 309px;border-top: none;border-left: 1spx solid black;border-bottom: 1px solid black;"
            		  		+ "border-right: 1px solid black;padding: 1.4pt 5.1pt;height: ";
            	  }
            	  letterContent.attr("style", tableWrapperStyle + addHeight + "px;");
            	  
            	  while(pageNum * a4Height - pageHeight > 0) {
            		  i++;
            		  addHeight = 10 * i;
            		  letterContent.attr("style", tableWrapperStyle + addHeight + "px;");
            		  letterHeight = Apv0101Service.getElementHeight(letterContent, renderer);
            		  System.out.println("letterHeight: " + letterHeight);
            		  pageHeight = Apv0101Service.getElementHeight(cloneDoc, renderer);
            		  System.out.println("i:" + i);
            		  System.out.println("addHeight:" + addHeight);
            	  }
            	  System.out.println("addHeight:" + addHeight);
            	  
            	  addHeight = addHeight -10;
            	  letterContent.attr("style", tableWrapperStyle + addHeight + "px;");
//            	  Apv0101Service.addBlankToElement(letterHeight, renderer, eContentHeight);
              }

            } catch (Exception e) {
                e.printStackTrace();
            }
            // Add blank to doc to avoid overlap end

            String contentPdf = cloneDoc.toString();

            System.out.println(contentPdf);

            contentPdf = contentPdf.replaceAll("</[a-z]{1}:[a-z]*>", "").replaceAll("<[a-z]{1}:[a-z]*>", "");
            renderer.setDocumentFromString(contentPdf);

            renderer.layout();
            FileOutputStream outputStream = new FileOutputStream(path);
            renderer.createPDF(outputStream, true);
            renderer.finishPDF();

            File f = new File(path);
            String FLE_NM = fileNamePhysical + ".pdf";
            String NEW_FLE_NM = fileNamePhysical + ".pdf";
            fileInfo.put("FLE_PATH", NEW_FLE_NM);
            fileInfo.put("FLE_NM", FLE_NM);
            fileInfo.put("NEW_FLE_NM", NEW_FLE_NM);
            fileInfo.put("FLE_SZ", f.length());
        }
        
        return fileInfo;
    }

    
    private String genCommonLetterForm(TndmDocMstRequest docMst, List<TndmDrftApvlLine> draftApvlLineList, String formContent, 
    		DrftRequest tndmDraft, String tagDoc, String tagDocSection) throws SQLException {
    	
    	String finishedDate = docMst.getFinishedDate() == null ? ImwareStringUtils.EMPTY
                : DateUtil.formatter2.format(docMst.getFinishedDate());
        String submitedEmail = ImwareStringUtils.EMPTY;
        
        String submitedMailTail = ImwareStringUtils.EMPTY;
        String submitedAccount = ImwareStringUtils.EMPTY;
        if (draftApvlLineList != null && draftApvlLineList.size() > 0) {
            TndmDrftApvlLine firstApprover = draftApvlLineList.get(0);
            if (firstApprover != null) {
                TcdsEmpMstResponse emp = sys0105Service.getEmpMstByUserUid(firstApprover.getSancUserUid());
                submitedEmail = emp.getEmail();
            }
        }
        if (!ImwareStringUtils.isEmpty(submitedEmail)) {
            String[] fullAccount = submitedEmail.split(ImwareStringUtils.AT_SIGN);
            if (fullAccount.length >= 2) {
                submitedAccount = fullAccount[0];
                submitedMailTail = ImwareStringUtils.AT_SIGN + fullAccount[1];
            }
        }

        formContent = formContent.replace("_CONTENT_",
                ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftContent(), ImwareStringUtils.EMPTY));

        formContent = formContent.replace("_TITLE_",
                ImwareStringUtils.defaultIfBlank(tndmDraft.getDrftTitle(), ImwareStringUtils.EMPTY));

        formContent = formContent.replace("#MAIL_TO",
                ImwareStringUtils.defaultIfBlank(submitedEmail, ImwareStringUtils.EMPTY));
        formContent = formContent.replace("_SUBMITED_MAIL_TAIL_",
                ImwareStringUtils.defaultIfBlank(submitedMailTail, ImwareStringUtils.EMPTY));
        formContent = formContent.replace("_SUBMITED_ACCOUNT_",
                ImwareStringUtils.defaultIfBlank(submitedAccount, ImwareStringUtils.EMPTY));
    	


        String signature = ImwareStringUtils.EMPTY;
        if (draftApvlLineList != null) {
            String approverSign = "<span style=\"margin-right: 15pt; display: inline-block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\">\r\n" + 
            		"    <span style=\"display: inline-block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\">\r\n" + 
            		"        <span style=\"display: block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\">_PROXY_TEXT_</span>\r\n" + 
            		"        <span style=\"display: block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\">_DUTY_</span>\r\n" + 
            		"    </span>\r\n" + 
            		"    <span style=\"margin-left: 5pt; display: inline-block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\">\r\n" + 
            		"        <span style=\"display: block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\"></span>\r\n" + 
            		"        <span style=\"display: block; font-size: 12pt; font-family: Batang; line-height: 1.75; font-weight: 300;\"><em style=\"display: inline-block;\">_APPROVER_</em> </span>\r\n" + 
            		"    </span>\r\n" + 
            		"</span>\r\n" + 
            		"";
            for (TndmDrftApvlLine apvlLine : draftApvlLineList) {
                String proxyText = ImwareStringUtils.EMPTY;
                if(ApvConstantUtils.ARBITRARY_APPROVE.equals(apvlLine.getSancType())) {
                	proxyText = "전결";
                }
                else if(ApvConstantUtils.PROXY_APPROVE.equals(apvlLine.getSancType())) {
                	proxyText = "대결";
                }
                
                String approver = ImwareStringUtils.EMPTY;
                if(ImwareStringUtils.YES.equals(apvlLine.getSancYn())) {
                	TcdsEmpMstResponse emp = sys0105Service.getEmpMstByUserUid(apvlLine.getSancUserUid());
                	approver = emp.getNameKr();
                }
                signature += approverSign.replace("_DUTY_", apvlLine.getSancDutyName())
                				.replace("_APPROVER_", approver)
                				.replace("_PROXY_TEXT_", proxyText);

            }
        }

        formContent = formContent.replaceAll("_SIGNATURE_",
                ImwareStringUtils.defaultIfBlank(signature, ImwareStringUtils.EMPTY));

        String tagDocFooter = this.genTagDocFooter(tagDoc, finishedDate);

        String footerSection = tagDocSection + tagDocFooter;
        formContent = formContent.replaceAll("_FOOTER_SECTION_",
                ImwareStringUtils.defaultIfBlank(footerSection, ImwareStringUtils.EMPTY));
        
        return formContent;
    }
    
    private String genTagDocFooter(String tagDoc, String finishedDate) {
        String tagDocFooter = "<p style=\"margin-top:15pt;font-size:12pt;font-family:Helvetica,Sans-serif;line-height:1.75;font-weight:300;\">시행 _TAG_DOC_ (_FINISH_APPROVING_DATE_)</p>";
        tagDocFooter = tagDocFooter.replace("_TAG_DOC_", tagDoc).replace("_FINISH_APPROVING_DATE_", finishedDate);
        return tagDocFooter;
    }
    
    private String getBizDetail(DrftRequest tndmDraft, TndmFormMgt bizDetailForm) throws SQLException {
    	String bizDetailFormContent = bizDetailForm.getFormContent();
        List<TndmDrftBusTripDetail> trips = tndmDraft.getBusTripDetail();
        if(trips == null) {
        	trips = apv0101Service.getTndmDrftBusTripDetail(tndmDraft.getIdBus());
        }
        String tripDetail = ImwareStringUtils.EMPTY;
		if(trips != null) {
			for(TndmDrftBusTripDetail trip : trips) {
				String detailTripContent = bizDetailFormContent.replaceAll("_TRIP_TRAVELLERS_",
						ImwareStringUtils.defaultIfBlank(trip.getTraveler(), ImwareStringUtils.EMPTY));
				
	            String applicationDate = trip.getApplicationDate() == null ? ImwareStringUtils.EMPTY
	                    : DateUtil.formatter2.format(trip.getApplicationDate());
	            detailTripContent = detailTripContent.replaceAll("_APP_DATE_", applicationDate);
	            detailTripContent = detailTripContent.replaceAll("_OIL_PRICE_", NumberUtil.formatNumber(trip.getOilPrice()));
	            detailTripContent = detailTripContent.replaceAll("_EX_RATE_", NumberUtil.formatNumber(trip.getExchangeRate()));
				
				
	            detailTripContent = detailTripContent.replaceAll("_TRANS_C_", NumberUtil.formatNumber(trip.getTransportationCost()));
			
	            detailTripContent = detailTripContent.replace("_D_EXP_", NumberUtil.formatNumber(trip.getDailyExpenses()));
	            detailTripContent = detailTripContent.replace("_D_EX_D_", NumberUtil.formatNumber(trip.getDailyExpensesDays()));
	            detailTripContent = detailTripContent.replaceAll("_D_EX_C_",
	            		NumberUtil.formatCost(trip.getDailyExpenses(), trip.getDailyExpensesDays(), trip.getExchangeRate()));
	            detailTripContent = detailTripContent.replaceAll("_D_EX_EC_",
	            		NumberUtil.formatCostWithEfficientDay(trip.getDailyExpenses(), trip.getDailyExpensesDays(), trip.getExchangeRate()));
	            
	            detailTripContent = detailTripContent.replace("_F_EXP_", NumberUtil.formatNumber(trip.getFoodExpenses()));
	            detailTripContent = detailTripContent.replace("_F_EX_D_", NumberUtil.formatNumber(trip.getFoodExpensesDays()));
	            detailTripContent = detailTripContent.replaceAll("_F_EX_C_",
	            		NumberUtil.formatCost(trip.getFoodExpenses(), trip.getFoodExpensesDays(), trip.getExchangeRate()));
	            detailTripContent = detailTripContent.replaceAll("_F_EX_EC_",
	            		NumberUtil.formatCost(trip.getFoodExpenses(), trip.getFoodExpensesDays(), trip.getExchangeRate()));
				
	            
	            detailTripContent = detailTripContent.replace("_R_EXP_", NumberUtil.formatNumber(trip.getRoomCharge()));
	            detailTripContent = detailTripContent.replace("_R_EX_D_", NumberUtil.formatNumber(trip.getRoomChargeDays()));
	            detailTripContent = detailTripContent.replaceAll("_R_EX_C_",
	            		NumberUtil.formatCost(trip.getRoomCharge(), trip.getRoomChargeDays(), trip.getExchangeRate()));
	            detailTripContent = detailTripContent.replaceAll("_R_EX_EC_",
	            		NumberUtil.formatCost(trip.getRoomCharge(), trip.getRoomChargeDays(), trip.getExchangeRate()));
				
	            detailTripContent = detailTripContent.replace("_ETC_C_", NumberUtil.formatNumber(trip.getEtc()));
	            detailTripContent = detailTripContent.replace("_SUM_C_", NumberUtil.formatNumber(trip.getSumBefore()));
	            detailTripContent = detailTripContent.replace("_SUM_EC_", NumberUtil.formatNumber(trip.getSum()));

				
				if(ImwareStringUtils.NO.equals(tndmDraft.getTypeOfBusinessTrip())) {
					detailTripContent = detailTripContent.replace("_D_EX_E_", NumberUtil.formatNumber(trip.getExchangeRate()));
					detailTripContent = detailTripContent.replace("_F_EX_E_", NumberUtil.formatNumber(trip.getExchangeRate()));
					detailTripContent = detailTripContent.replace("_R_EX_E_", NumberUtil.formatNumber(trip.getExchangeRate()));
				}

	            tripDetail = tripDetail + detailTripContent;
			}
		}
    	
    	return tripDetail;
    }

    public void resolveFonts(ITextRenderer renderer) throws Exception {
        ITextFontResolver resolver = renderer.getFontResolver();

        File fontsDir = new File(configFontDir);

        if (!fontsDir.exists()) {
            // For development.
            fontsDir = new File(getClass().getResource("/fonts").getFile());
        }


        File[] fontFiles = fontsDir.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                String _name = name.toLowerCase();
                return _name.endsWith(".ttf") || _name.endsWith(".otf");
            }
        });

        if (fontFiles != null) {
            for (int i = 0, len = fontFiles.length; i < len; i++) {
                resolver.addFont(fontFiles[i].getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            }
        }
    }

    public String getSignature(List<TndmDrftApvlLine> draftApvlLineList) throws Exception {
        if (draftApvlLineList == null || draftApvlLineList.size() == 0) {
            return ImwareStringUtils.EMPTY;
        }

        String block1 = "<table cellspacing=\"0\" cellpadding=\"0\" style=\"display: inline-table; border: 1px solid black; border-collapse: collapse; line-height: 15.6933px; font-size: 11pt; font-family: Batang; margin:10px 0px 10px 0px; box-sizing: border-box;\">\r\n"
                + "	<tbody>\r\n" + "		<tr>\r\n"
                + "			<th rowspan=\"2\" style=\"border: 1px solid black; padding: 5px; text-align: left; \">\r\n"
                + "				<span lang=\"ZH-CN\" style=\"font-family: Batang; font-weight: normal; \">결재</span>\r\n"
                + "			</th>";
        StringBuilder block2 = new StringBuilder();
        String block3 = "</tr><tr>";
        StringBuilder block4 = new StringBuilder();
        String block5 = "</tr></tbody></table>";

        String tdOpen = "<td style=\"text-align:center; border: 1px solid black;  padding: 5px;\">";
        String tdClose = "</td>";
        String imgOpen = "<div style=\"width:80px; height:60px;\"><img style=\"object-fit: contain;width:80px; height:60px;\" src=\"";
        String imgClose = "\"></div>";
        
        String serverUrl = environment.getProperty("server.host") + ":" + environment.getProperty("server.port");

        if (draftApvlLineList != null) {
            for (TndmDrftApvlLine apvlLine : draftApvlLineList) {
				TcdsEmpMstResponse emp = sys0105Service.getEmpMstByUserUid(apvlLine.getSancUserUid());
				TccoFile imgFile = tccoFileService.findBySequence(emp.getSignaturePath());
				String signaturePath = imgOpen + imgClose;
				if (ImwareStringUtils.YES.equals(apvlLine.getSancYn()) && imgFile != null) {
					signaturePath = imgOpen + serverUrl + "/resources/" + imgFile.getFlePath() + imgClose;
				}

                System.out.println("signature path: " + signaturePath);
                String dutyName = emp.getDutyName();
                block2.append(tdOpen).append(dutyName).append(tdClose);
                block4.append(tdOpen).append(signaturePath).append(tdClose);
            }
        }

        String signature = new StringBuilder().append(block1).append(block2).append(block3).append(block4)
                .append(block5).toString();
        return signature;
    }

    public Map<Object, Object> countCommunity(HttpServletRequest request) throws ImwareException, Exception {
        Map<Object, Object> result = new HashMap();
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if ((user != null && !user.isEmpty()) && user.get("DEPT_CODE") != null) {
            String deptCode = user.get("DEPT_CODE").toString();
            Map<Object, Object> mapSearch = new HashMap();
            mapSearch.put("BOARD_TYPE", "11-01"); //Suggest
            mapSearch.put("DEPT_CODE", deptCode);
            result.put("comSuggest", com0102Service.countNew(mapSearch));
            mapSearch = new HashMap(); //Dept
            mapSearch.put("BOARD_TYPE", "11-02");
            mapSearch.put("DEPT_CODE", deptCode);
            result.put("comDept", com0102Service.countNew(mapSearch));
            mapSearch = new HashMap(); //Library
            mapSearch.put("BOARD_TYPE", "11-03");
            mapSearch.put("DEPT_CODE", deptCode);
            result.put("comLibrary", com0102Service.countNew(mapSearch));
            result.put("comNotice", com0101Service.countNew());//Notice
        }
        return result;
    }

    public Map<Object, Object> countTask(HttpServletRequest request) throws ImwareException, SQLException {
        Map<Object, Object> result = new HashMap();
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            String uID = user.get("USER_UID").toString();
            Map<Object, Object> mapSearch = new HashMap();
            mapSearch.put("CREATED_BY", uID);
            result.put("taskRequest", task0101Service.countTaskNew(mapSearch));
            mapSearch = new HashMap();
            mapSearch.put("recipientId", uID);
            result.put("taskReception", task0101Service.countTaskNew(mapSearch));
            mapSearch = new HashMap();
            mapSearch.put("referrerId", uID);
            result.put("taskReference", task0101Service.countTaskNew(mapSearch));
        }
        return result;
    }
    
    
    @DeleteMapping("/delete/{docNo}/{drftDocNo}")
    @Transactional(rollbackFor = { Exception.class })
    public Object deleteDoc(@PathVariable("docNo") String docNo,@PathVariable("drftDocNo") String drftDocNo) throws SQLException {
    	DrftRequest tndmDrft = apv0101Service.getDrftRequest(drftDocNo);
    	Map<Object, Object> arg = new HashMap<>();
    	if (ApvConstantUtils.FormType.VACATION.getValue().equals(tndmDrft.getDrftType())) {
    		arg.put("VACATION_RQST_ID",tndmDrft.getVacationId());
    		cms0102Service.deleteTcdsEmpVacationApvl(arg);
    		arg.put("ID",tndmDrft.getVacationId());
    		cms0102Service.deleteTcdsEmpVacationRequest(arg);
    	}
    	arg.put("docNo", docNo);
    	arg.put("drftDocNo", drftDocNo);
    	apv0101Service.deleteDrftDoc(arg);
    	return new ResponseEntity<Object>(HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/pdfView/{fileNamePhysical}", produces = { MediaType.APPLICATION_PDF_VALUE })
	public ResponseEntity<byte[]> pdfView(@PathVariable("fileNamePhysical") String fileNamePhysical) {
		if (Utils.isNullOrEmpty(fileNamePhysical))
			return null;
			
		InputStream is = null;
		try {
			String filename = fileNamePhysical + ".pdf";
			String path = environment.getProperty("dir.upload.pdf") + "/" + filename;
			
			File fileToDownload = new File(path);
			if (!fileToDownload.exists() || !fileToDownload.isFile())
				return null;

			is = new FileInputStream(fileToDownload);
			
			byte[] contents = org.apache.commons.io.IOUtils.toByteArray(is);
			
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_PDF);
		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		    headers.add("Content-Disposition", "inline; filename=" + filename);
		    
		    ResponseEntity<byte[]> res = new ResponseEntity<>(contents, headers, HttpStatus.OK);
		    return res;
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
    @PostMapping("/undoApproval")
    // @MenuPermission(permissions = { MenuPermissionType.MODIFY })
    @TransactionalWithRollback
    public ResponseEntity<?> undoApproval(@RequestBody Map<Object, Object> arg) throws Exception {
        String draftDocNo = (String) arg.get("draftDocNo");
        String userUid = (String) arg.get("userUid");
        Validate.notBlank(draftDocNo, "Invalid draftDocNo");
        Validate.notBlank(userUid, "Invalid userUid");
        
        apv0101Service.deleteTndmApvlHist(arg);

        List<TndmDrftApvlLine> apvlLineList = apv0101Service.getDraftApvlLineList(arg);
        List<TndmDrftApvlLine> filteredApvlLine = apvlLineList.stream().filter(line -> {
        	return userUid.equals(line.getSancUserUid());
        }).collect(Collectors.toList());
        
        Validate.notEmpty(filteredApvlLine, "Invalid Apvl line");
        
        TndmDrftApvlLine apvlLine = filteredApvlLine.get(0);
        apvlLine.setSancYn(ImwareStringUtils.NO);
        apv0101Service.updateTndmDrftApvlLine(apvlLine);
        
        if(apvlLine.getSancUserUid().equals(apvlLineList.get(0).getSancUserUid())) {
        	DrftRequest tndmDrft = apv0101Service.getDrftRequest(draftDocNo);
        	arg.put("draftStatus", ApvConstantUtils.DocStatus.DRAFT.getValue());
        	apv0101Service.updateDraftStatus(arg);
        	
        	arg.put("docNo", tndmDrft.getDocNo());
        	arg.put("docStatus", ApvConstantUtils.DocStatus.DRAFT.getValue());
        	apv0101Service.updateDocMstForApproval(arg);
        	   	
        } else {
        	DrftRequest tndmDrft = apv0101Service.getDrftRequest(draftDocNo);
        	if (ApvConstantUtils.FormType.VACATION.getValue().equals(tndmDrft.getDrftType())) {
    			Map<Object, Object> vacationApvlMap = new HashMap<Object, Object>();
    			vacationApvlMap.put("VACATION_RQST_ID", tndmDrft.getVacationId());
    			vacationApvlMap.put("USER_UID", userUid);

    			TcdsEmpVacationApprovalResponse vacationApvl = cms0102Service.getVRForUsageHistory(vacationApvlMap);
    			if (vacationApvl.getIdRqst() != null) {
    				vacationApvl.setStatusApvl("15-01");
    				cms0102Service.updateVacation(vacationApvl);
    			}
    		}
        }
        
    	return new ResponseEntity<Object>(HttpStatus.OK);
    }
    
    @DeleteMapping("/delete-favorite/{id}")
    @TransactionalWithRollback
    public ResponseEntity<?> deleteFavorite(@PathVariable("id") Long id) throws Exception {
    	apvLineProcessingService.deleteTndmApvlLineFavoriteDetail(id);
    	apvLineProcessingService.deleteTndmApvlLineFavorite(id);
    	return new ResponseEntity<Object>(HttpStatus.OK);
    }
    
    
    /**
	 * TIENNQ
	 * 
	 * @param arg
	 * @return
	 * @throws SQLException
	 */
	@GetMapping("/get-drft-cms0102")
	public Object getTndmDrftCMS0102(@RequestParam Map<Object, Object> arg) throws SQLException {
		return apv0101Service.getTndmDrftCMS0102(arg);
	}
   
}
