package com.a2m.imware.controller.common;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.NoticeNewMemberMessage;

@RestController
@Validated
@RequestMapping(value = "/api/userInfo")
public class UserInfoController {
	
	@Autowired
	private UserInfoService service;
	
	@Autowired
	private ExtraMessageUtil extraMsgUtil;
	
	private static final String AUTHORIZATION = "authorization";

	@GetMapping(value = "/getUserInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<Object, Object> getUserInfo(HttpServletRequest request) {

		// get User ID from access token in header of httpRequest
		String userId = "";
		String token = getTokenFromRequest(request);

		try {
			userId = service.getUserIdFromToken(token);
		} catch (Exception ee) {
			userId = "";
		}

		// get user info by User ID
		Map<Object, Object> res = new HashMap<>();
		try {
			res = service.getUserInfo(userId, token);
		} catch (ImwareException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	private String getTokenFromRequest(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders(AUTHORIZATION);
		String headerValue = "";
		while (headers.hasMoreElements()) {
			headerValue = headers.nextElement();
		}
		if (headerValue != null && !"".equals(headerValue)) {
			String els[] = headerValue.split(" ");
			if (els.length > 1) {
				return els[1];
			}
		}
		return "";
	}

	@PostMapping(value = "/saveLoginInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveLoginInfo(HttpServletRequest request) {
		Map<Object, Object> userInfo = getUserInfo(request);
		userInfo.put("SESS_ID", getTokenFromRequest(request));
		userInfo.put("IP", getRemoteIpAddress(request));
		try {
			service.saveLoginInfo(userInfo);
			// check user is first login
			Boolean isFirstLogin = (Boolean) userInfo.get("IS_FIRST_LOGIN");
			final String userUid = userInfo.get("USER_UID").toString();
			if (isFirstLogin == null || !isFirstLogin) {
				List<String> userIdList = service.getUserIds();
				if (userIdList != null && !userIdList.isEmpty()) {
					// send message notice
					extraMsgUtil.noticeNewMember(NoticeNewMemberMessage.builder()
							.memberUserUID(userUid)
							.userUID(userIdList.toArray(new String[userIdList.size()]))
							.build());
				}
				
				// update first login
				service.updateFirstLogin(userUid);
			}
		} catch (ImwareException e) {
			e.printStackTrace();
			return "false";
		} catch (Exception e) {
			e.printStackTrace();
			return "false";
		}
		return "true";
	}

	private String getRemoteIpAddress(HttpServletRequest request) {
		String remoteAddress = "";
		try {

			if (request != null) {
				remoteAddress = request.getHeader("X-Forwarded-For");
				if (remoteAddress == null || "".equals(remoteAddress)) {
					remoteAddress = request.getRemoteAddr();
				}
			}
			remoteAddress = remoteAddress != null && remoteAddress.contains(",") ? remoteAddress.split(",")[0]
					: remoteAddress;
		} catch (Exception e) {
			return "";
		}
		return remoteAddress;
	}
	
	@PutMapping(value = "/saveLogoutInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveLogoutInfo(HttpServletRequest request) {
		Map<Object, Object> userInfo = getUserInfo(request);
		userInfo.put("SESS_ID", getTokenFromRequest(request));
		try {
			service.saveLogoutInfo(userInfo);
		} catch (ImwareException e) {
			e.printStackTrace();
			return "false";
		} catch (Exception e) {
			e.printStackTrace();
			return "false";
		}
		return "true";
	}

}
