package com.a2m.imware.controller.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.CommonFileUtil;
import com.a2m.imware.util.Utils;
import com.google.common.base.Strings;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/common/file-service")
public class FileServiceController {

    @Autowired
    private TccoFileService tccoFileService;
    
    @Autowired
    Environment environment;

    @GetMapping("/getFile")
    @ResponseBody
    public Object getFile(@RequestParam String seq) throws Exception {
        try {
            if (Utils.isNullOrEmpty(seq)) {
                return null;
            }

            TccoFile file = tccoFileService.findBySequence(seq);
            return file == null ? null : file.toMap();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/getFiles")
    @ResponseBody
    public Object getFiles(@RequestParam("seqs") List<String> fileSequences) throws Exception {
        List<TccoFile> result = new ArrayList<>();
        try {
            if (fileSequences == null || fileSequences.isEmpty()) {
                return result;
            }

            List<TccoFile> list = tccoFileService.findBySequences(fileSequences);
            if (list != null) {
                result = list;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new ArrayList<>();
        }

        return result.stream().map(tccoFile -> tccoFile.toMap());
    }

    @PostMapping("/saveFiles")
    @ResponseBody
    public Object saveFiles(@RequestBody List<Map<Object, Object>> filesData) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            if (filesData == null || filesData.isEmpty()) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("You send nothing to save!");
                return ajaxResult.toMap();
            }

            List<Map<Object, Object>> insertedData = new ArrayList<>();
            for (Map<Object, Object> data : filesData) {
                if (data == null || data.isEmpty()) {
                    continue;
                }

                // make file sequence
                data.put("ATCH_FLE_SEQ", UUID.randomUUID().toString());

                if (tccoFileService.insert(data)) {
                    insertedData.add(data);
                }
            }

            ajaxResult.setStatus(true);
            ajaxResult.setMessage(insertedData.size() + " files inserted");
            List<String> sequences = insertedData.stream()
                    .map(data -> data.get("ATCH_FLE_SEQ") == null ? "" : data.get("ATCH_FLE_SEQ").toString())
                    .collect(Collectors.toList());
            sequences.remove("");
            List<TccoFile> files = tccoFileService.findBySequences(sequences);
            ajaxResult.setResponseData(files == null ? null : files.stream().map(file -> file.toMap()));
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage(e.getLocalizedMessage());
        }

        return ajaxResult.toMap();
    }

    @DeleteMapping("/deleteFiles")
    @ResponseBody
    public Object delete(@RequestParam("seqs") List<String> fileSequences) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            if (fileSequences == null || fileSequences.isEmpty()) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("You send nothing to delete!");
                return ajaxResult.toMap();
            }

            boolean deleted = tccoFileService.delete(fileSequences);

            ajaxResult.setStatus(deleted);
            ajaxResult.setMessage("Delete success");
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage(e.getLocalizedMessage());
        }

        return ajaxResult.toMap();
    }

    // For testing...
    @GetMapping("/findTcdsAttachList")
    @ResponseBody
    public List<Map<Object, Object>> findTcdsEmpAttachList(@RequestParam String refId) {
        List<Map<Object, Object>> result = new ArrayList<>();
        if (Utils.isNullOrEmpty(refId)) {
            return result;
        }
        result = tccoFileService.findTcdsEmpAttaches(refId);
        result.forEach(data -> {
            if (data == null || data.isEmpty() || data.get("ATTACH_FILE_SEQ") == null) {
                return;
            }

            try {
                TccoFile file = tccoFileService.findBySequence(data.get("ATTACH_FILE_SEQ").toString());
                data.put("TCCO_FILE", file.toMap());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return result;
    }

    @PostMapping("/insertTcdsEmpAttaches")
    @ResponseBody
    public Object insertTcdsEmpAttaches(@RequestBody List<Map<Object, Object>> dataList) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            if (dataList == null || dataList.isEmpty()) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("You send nothing to save!");
                return ajaxResult.toMap();
            }

            List<Map<Object, Object>> insertedData = new ArrayList<>();
            for (Map<Object, Object> data : dataList) {
                if (data == null || data.isEmpty()) {
                    continue;
                }

                if (tccoFileService.insertToTcdsEmpAttach(data)) {
                    insertedData.add(data);
                }
            }

            ajaxResult.setStatus(true);
            ajaxResult.setMessage(insertedData.size() + " rows inserted");
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage(e.getLocalizedMessage());
        }

        return ajaxResult.toMap();
    }

    @DeleteMapping("/deleteFromTcdsEmpAttach")
    @ResponseBody
    public Object deleteFromTcdsEmpAttach(@RequestParam List<String> ids) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            if (ids == null || ids.isEmpty()) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("You send nothing to save!");
                return ajaxResult.toMap();
            }

            //boolean deleted = tccoFileService.deleteFromTcdsEmpAttach(ids);
            boolean deleted = tccoFileService.delete(ids);

            ajaxResult.setStatus(deleted);
            ajaxResult.setMessage("Delete success");
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage(e.getLocalizedMessage());
        }

        return ajaxResult.toMap();
    }
    // End test

    @RequestMapping("/downloadZip")
    public void downloadZip(@RequestParam List<String> fileSequences, HttpServletResponse response) {
        if (fileSequences == null || fileSequences.isEmpty()) {
            return;
        }

        try {
            List<TccoFile> tccoFiles = tccoFileService.findBySequences(fileSequences);
            if (tccoFiles == null || tccoFiles.isEmpty()) {
                return;
            }

            List<File> filesToZip = new ArrayList<File>();
            final String baseDir = Utils.commonProps.getProperty("path.default.uploaddir") + File.separator;
            File tempDir = Files.createTempDirectory("IMWARE").toFile();
            for (TccoFile tccoFile : tccoFiles) {
                if (tccoFile == null) {
                    continue;
                }
                File file = new File(baseDir + tccoFile.getNewFleNm());
                if (!file.exists()) {
                    continue;
                }

                File destFile = new File(tempDir.getAbsolutePath() + File.separator + tccoFile.getFleNm());
                File copiedFile = Files.copy(file.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING).toFile();
                if (!copiedFile.exists()) {
                    continue;
                }
                filesToZip.add(copiedFile);
            }

            if (!filesToZip.isEmpty()) {
                File fileToDownload = zipFiles(filesToZip);
                if (fileToDownload != null) {
                    FileInputStream fis = new FileInputStream(fileToDownload);
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment; filename=" + fileToDownload.getName());
                    response.setContentLength((int) fileToDownload.length());
                    IOUtils.copy(fis, response.getOutputStream());
                    response.flushBuffer();
                    fis.close();
                    FileUtils.forceDelete(fileToDownload);
                }
            }
            FileUtils.deleteDirectory(tempDir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/getFileTp")
    public TccoFile getFileTp(@RequestParam String fileSequence) throws Exception {
        if (Strings.isNullOrEmpty(fileSequence)) {
            return null;
        }
        TccoFile tccoFiles = tccoFileService.findBySequence(fileSequence);
        if (tccoFiles == null) {
            return null;
        }
        return tccoFiles;
    }

    @RequestMapping("/downloadFile")
    public void downloadFile(@RequestParam String fileSequence, HttpServletResponse response, HttpServletRequest request) {
        if (Strings.isNullOrEmpty(fileSequence)) {
            return;
        }
        try {
            TccoFile tccoFiles = tccoFileService.findBySequence(fileSequence);
            if (tccoFiles == null) {
                return;
            }
            final String baseDir = Utils.commonProps.getProperty("path.default.uploaddir") + File.separator;
            File file = new File(baseDir + tccoFiles.getNewFleNm());
            FileInputStream inStream = new FileInputStream(file);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            response.setHeader("fileTp", CommonFileUtil.getExt(file.getName()));
            response.setContentLength((int) file.length());
            IOUtils.copy(inStream, response.getOutputStream());

            response.flushBuffer();
            inStream.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    
    @RequestMapping("/download-internal")
    public void downloadFileInternal(@RequestParam String fileSequence, HttpServletResponse response, HttpServletRequest request) {
        if (Strings.isNullOrEmpty(fileSequence)) {
            return;
        }
        try {
            TccoFile tccoFiles = tccoFileService.findBySequence(fileSequence);
            if (tccoFiles == null) {
                return;
            }
            final String baseDir = environment.getProperty("dir.upload.pdf") + File.separator;
            File file = new File(baseDir + tccoFiles.getNewFleNm());
            FileInputStream inStream = new FileInputStream(file);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            response.setHeader("fileTp", CommonFileUtil.getExt(file.getName()));
            response.setContentLength((int) file.length());
            IOUtils.copy(inStream, response.getOutputStream());

            response.flushBuffer();
            inStream.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private File zipFiles(List<File> filesToZip) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("IMWARE-", ".zip");
            FileOutputStream fos = new FileOutputStream(tempFile);
            ZipOutputStream zipOut = new ZipOutputStream(fos);
            for (File fileToZip : filesToZip) {
                FileInputStream fis = new FileInputStream(fileToZip);
                ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                zipOut.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length;
                while ((length = fis.read(bytes)) >= 0) {
                    zipOut.write(bytes, 0, length);
                }
                fis.close();
            }

            zipOut.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tempFile;
    }

}
