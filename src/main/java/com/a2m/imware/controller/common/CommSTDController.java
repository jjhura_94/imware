package com.a2m.imware.controller.common;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.CommSTDService;

@RestController
@Validated
@RequestMapping(value = "/api/comstd")
public class CommSTDController {

	@Autowired
	private CommSTDService service;

	@GetMapping(value = "/getListComm", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Map<Object, Object>> getListComm(@RequestParam Map<Object, Object> arg) throws ImwareException {
		return service.getListComm(arg);
	}

}
