package com.a2m.imware.controller.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.a2m.imware.service.common.ComCodeService;


@RestController
@RequestMapping(value = "/api/common/comCode")
public class ComCodeController {

	@Autowired
	private ComCodeService service;
	
	@GetMapping("/search")
	public List search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTccoStd(arg);
	}

	/**
	 * @author QuangDV
	 * @apiNote Lấy danh sách bản ghi tccoStd theo upCommCd
	 * @param upCommCd
	 * @return
	 * @throws SQLException
	 */
	@GetMapping("/upCommCd/{upCommCd}")
	public List getTccoStds(@PathVariable("upCommCd") String upCommCd) throws SQLException {
		return service.getTccoStds(upCommCd);
	}
}