package com.a2m.imware.controller.common;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.apv.TndmApvlLineFavorite;
import com.a2m.imware.model.apv.TndmApvlLineFavoriteDetail;
import com.a2m.imware.model.apv.TndmReferrerFavorite;
import com.a2m.imware.model.apv.TndmReferrerFavoriteDetail;
import com.a2m.imware.service.common.ReferrerService;

@RestController
@RequestMapping(value = "/api/common/referer")
@Validated
public class ReferrerController {
	
	@Autowired
	private ReferrerService referrerService ;
	
	@GetMapping("/search/{userUid}/{type}")
	public List<TndmReferrerFavorite> searchTndmApvlLineFavorite(@PathVariable("userUid") String userUid,@PathVariable("type") String type) throws SQLException {
		Map<Object, Object> arg = new HashMap<Object, Object>();
		arg.put("createdBy", userUid);
		arg.put("type", type);
		return referrerService.searchTndmReferrerFavorite(arg);
	}
	
	
	@PostMapping("/save")
	@Transactional(rollbackFor = { Exception.class , SQLException.class })
	public Object save(@RequestBody @Valid TndmReferrerFavorite arg) throws Exception {
		Date currentDate = new Date();
		arg.setCreatedDate(currentDate);
		referrerService.insertTndmReferrerFavorite(arg);
		if(arg.getDetails() != null) {
			for (TndmReferrerFavoriteDetail ele : arg.getDetails()) {
				ele.setFavoriteId(arg.getId());
				referrerService.insertTndmReferrerFavoriteDetail(ele);
			}
		}
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
