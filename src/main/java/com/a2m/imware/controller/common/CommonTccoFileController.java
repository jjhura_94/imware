package com.a2m.imware.controller.common;

import com.a2m.imware.config.CommonTccoFileConfig;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.util.CommonFileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.UUID;

/**
 * @author: QuangDV
 * @// TODO: 11/8/2020
 */
@RestController
@RequestMapping(value = "/api/tcco-files")
public class CommonTccoFileController {
    @Autowired
    private CommonTccoFileConfig commonTccoFileConfig;

    @Autowired
    private TccoFileService tccoFileService;

    /**
     * @apiNote Get a byte array of file from disk by seq in TCCO_FILE table
     * @param atchFleSeq
     * @return byte array of file
     * @throws Exception
     */
    @GetMapping("/atch-fle-seq/{atchFleSeq}")
    @ResponseBody
    public byte[] get(@PathVariable String atchFleSeq) throws Exception {
        if (StringUtils.isEmpty(atchFleSeq))
            return new byte[0];

        TccoFile tccoFile = tccoFileService.findBySequence(atchFleSeq);
        if(tccoFile==null || StringUtils.isEmpty(tccoFile.getNewFleNm())) {
            return new byte[0];
        }

        String newFleNm = tccoFile.getNewFleNm();
        String dir = commonTccoFileConfig.getPathDefaultUploaddir();

        byte []bytes = CommonFileUtil.getFileByPath(dir.concat(newFleNm));
        return bytes;
    }

    public TccoFile setDefaultValues(String userId, String atchFleSeq, MultipartFile multipartFile) throws Exception {
        TccoFile oldTccoFile =  StringUtils.isEmpty(atchFleSeq) ? null : tccoFileService.findBySequence(atchFleSeq);

        TccoFile tccoFile = new TccoFile();

        if (oldTccoFile == null) {
            tccoFile.setAtchFleSeq(UUID.randomUUID().toString());

            tccoFile.setCreatedDate(new Date());
            tccoFile.setCreatedBy(userId);
            tccoFile.setUpdatedDate(new Date());
            tccoFile.setUpdatedBy(userId);
        } else {
            tccoFile.setUpdatedDate(new Date());
            tccoFile.setUpdatedBy(userId);
        }

        Long fleSz = multipartFile.getSize();
        String fleNm = multipartFile.getOriginalFilename();
        String fileTp = CommonFileUtil.getExt(fleNm);
        String newFleNm = CommonFileUtil.replaceFileName(tccoFile.getAtchFleSeq(), fleNm);

        tccoFile.setFleSz(fleSz.toString());
        tccoFile.setFleNm(fleNm);
        tccoFile.setNewFleNm(newFleNm);
        tccoFile.setFleTp(fileTp);
        tccoFile.setFlePath(newFleNm);

        return tccoFile;
    }

    /**
     * @apiNote Save file
     * @param userId
     * @param atchFleSeq null - create file, not null - overwrite old file
     * @param multipartFile new file
     * @return TCCO_FILE record
     * @throws Exception
     */
    @PostMapping("/atch-fle-seq")
    @ResponseBody
    
    public TccoFile saveTccoFile(@RequestPart String userId, @RequestPart String atchFleSeq, @RequestPart MultipartFile multipartFile) throws Exception {
        if (multipartFile==null)
            return null;

        TccoFile tccoFile = setDefaultValues(userId, atchFleSeq, multipartFile);

        String dir = commonTccoFileConfig.getPathDefaultUploaddir();
        CommonFileUtil.save(dir.concat(tccoFile.getNewFleNm()), multipartFile);

        return tccoFileService.saveTccoFile(tccoFile);
    }
}
