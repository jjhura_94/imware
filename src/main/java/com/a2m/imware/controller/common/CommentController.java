package com.a2m.imware.controller.common;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.common.TndmComment;
import com.a2m.imware.service.common.TndmCommentService;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.AjaxResult.Code;

@RestController
@RequestMapping(value = "/api/common/comment")
public class CommentController {

	
	@Autowired
	private TndmCommentService service;
	
	@GetMapping("/list")
	public List search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTndmComment(arg);
	}
	
	// insert + update => insert when ID is null, else => insert
    @PostMapping("/save")
    @ResponseBody
    public Object saveComment(@RequestBody(required = false) TndmComment data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        
        try {
            final boolean needUpdate = data.getId() != null && data.getId() > 0;
            if(needUpdate) {
            	data.setUpdatedDate(new Date());
            }
            Map<Object, Object> dataToSave = data.toMap();
            boolean isSuccess = (needUpdate ? service.updateTndmComment(data) : service.insertTndmComment(data)) > 0;
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(Code.FAILED);
            } else {
                ajaxResult.setResponseData(dataToSave.get("ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }
    
    
    @DeleteMapping("/{id}")
	public Object delete(@PathVariable("id") Long id) throws SQLException {
    	Map<Object, Object> result = new HashMap<Object, Object>();
    	result.put("deleted", service.deleteTndmComment(id));
		return result;
	}
}
