package com.a2m.imware.controller.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.service.common.TsstDateWorkingService;


@RestController
@RequestMapping(value = "/api/common/dateWorking")
public class DateWorkingController {

	@Autowired
	private TsstDateWorkingService service;
	
	@GetMapping("/search")
	public List<Map<Object, Object>> search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTsstDateWorking(arg);
	}
}