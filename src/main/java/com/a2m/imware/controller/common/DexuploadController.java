package com.a2m.imware.controller.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/public")
public class DexuploadController {

	@Autowired
	ServletContext application; 
	
	@RequestMapping(value = { "/upload_handler" }, method = RequestMethod.POST)
	public String mgtPreview(Model model, HttpServletRequest request,HttpServletResponse response) {
		try {
			return "upload_handler";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
}
