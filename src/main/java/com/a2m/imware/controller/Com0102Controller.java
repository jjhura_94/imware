package com.a2m.imware.controller;

import com.a2m.imware.model.TndmBoard;
import com.a2m.imware.model.TndmBoardComment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.Com0102Service;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.AjaxResult.Code;
import com.a2m.imware.util.Pager;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.NoticeDepartmentMessage;
import com.google.common.base.Strings;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.a2mvn.largefileupload.common.ParameterUtil;
import org.springframework.web.bind.annotation.DeleteMapping;

@RestController
@RequestMapping("/api/com/com0102")
public class Com0102Controller {

    @Autowired
    private Com0102Service com0102Service;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private ExtraMessageUtil extraMessageUtil;
    @Autowired
    private CommonDeptService commonDeptService;

    @SuppressWarnings("unchecked")
    @GetMapping("/list")
    @ResponseBody
    public Object getBoardList(HttpServletRequest request, @RequestParam(required = false, defaultValue = "0") Integer start, @RequestParam(required = false, defaultValue = "10") Integer limit) throws ImwareException {
        Map<Object, Object> parameters = ParameterUtil.getParameterMap(request);
         List<String> userIds = commonDeptService.getUidOfUser(request, parameters.get("DEPT_CODE"));
        parameters.put("USER_IDS", String.join(", ", userIds));
        TndmBoard filter = new TndmBoard().fromMap(parameters);
        if (filter.getPager() == null) {
            filter.setPager(new Pager());
        }
        filter.getPager().setStart(start);
        filter.getPager().setLimit(limit);
        getBoardType(filter, parameters);//Get Board Type
        try {
            if (!Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && !Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                filter.setTimeType(1);
            } else if (!Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                filter.setTimeType(2);
            } else if (Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && !Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                filter.setTimeType(3);
            }
            if (!Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                Date d = f.parse(filter.getCreateTimeTo());
                long milliseconds = d.getTime() + 86400000L;
                d = new Date(milliseconds);
                filter.setCreateTimeTo(f.format(d));
            }
            List<Map<Object, Object>> dataList = com0102Service.fetch(filter.toMap()).stream().map(obj -> obj.toMap()).collect(Collectors.toList());
            Long count = com0102Service.count(filter.toMap());

            return new PageResponse(dataList, count == null ? 0 : count.intValue());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @GetMapping("/findById")
    @ResponseBody
    public Map<Object, Object> getNotice(@RequestParam Long id) {
        if (id == null || id <= 0) {
            return null;
        }

        try {
            TndmBoard board = com0102Service.findById(id);
            return board == null ? null : board.toMap();
        } catch (Exception e) {
            // ignored
        }

        return null;
    }

    // insert + update => insert when ID is null, else => insert
    @PostMapping("/save")
    @ResponseBody
    public Object save(HttpServletRequest request, @RequestBody(required = false) Map<Object, Object> data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null || data.isEmpty()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        TndmBoard board = new TndmBoard().fromMap(data);
        if (board == null || Utils.isNullOrEmpty(board.getBoardTitle()) || Utils.isNullOrEmpty(board.getContent())) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        getBoardType(board, data);
        if (Utils.isNullOrEmpty(board.getPrivacy())) {
            board.setPrivacy("N");
        }

        try {
            final boolean needUpdate = board.getId() != null && board.getId() > 0;
            boolean isSuccess = false;
            Map<Object, Object> dataToSave = board.toMap();

            if (needUpdate) { // do update
                // remove never update fields
                dataToSave.remove("CREATED_BY");
                dataToSave.remove("CREATED_DATE");
                dataToSave.remove("UPDATED_DATE");
                dataToSave.remove("DEPT_CODE");
                dataToSave.remove("DEPT_NAME");
                com0102Service.update(dataToSave);
                isSuccess = true;
            } else { // do insert
                isSuccess = com0102Service.insert(dataToSave);
                if (board.getPrivacy().equals("N")) {
                    this.sendMessage(board, request);
                }
            }

            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(Code.FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    @GetMapping("commentList")
    @ResponseBody
    public List<Map<Object, Object>> commentList(@RequestParam Long boardId) {
        if (boardId == null || boardId <= 0) {
            return new ArrayList<>(0);
        }

        try {
            List<TndmBoardComment> commentList = com0102Service.findCommentLevel1(boardId);
            return commentList.stream().map(comment -> comment.toMap()).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>(0);
    }

    // insert + update => insert when ID is null, else => insert
    @PostMapping("/saveComment")
    @ResponseBody
    public Object saveComment(@RequestBody(required = false) Map<Object, Object> data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null || data.isEmpty()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        TndmBoardComment comment = new TndmBoardComment().fromMap(data);
        if (comment == null || Utils.isNullOrEmpty(comment.getContent())) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }

        try {
            final boolean needUpdate = comment.getId() != null && comment.getId() > 0;
            Map<Object, Object> dataToSave = comment.toMap();
            boolean isSuccess = needUpdate ? com0102Service.updateComment(dataToSave) : com0102Service.insertComment(dataToSave);
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(Code.FAILED);
            } else {
                ajaxResult.setResponseData(dataToSave.get("ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    @DeleteMapping("/deleteComment")
    @ResponseBody
    public Object deleteComment(@RequestParam Long id) {
        return com0102Service.deleteComment(id);
    }

    /**
     * Get Board Type by 'dataType' in Map data
     *
     * @param board
     * @param params
     */
    public void getBoardType(TndmBoard board, Map<Object, Object> params) {
        board.setBoardType("11-01"); //Suggest
        if (params.containsKey("typeData")) {
            String typeDate = params.get("typeData").toString();
            if (typeDate.equals("2")) {
                board.setBoardType("11-02"); //Department
            } else if (typeDate.equals("3")) {
                board.setBoardType("11-03"); //Library
            }
        }
    }

    public void sendMessage(TndmBoard board, HttpServletRequest request) throws ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        String deptCode = user.get("DEPT_CODE").toString();
        List<String> userIds = commonDeptService.getUidOfUser(deptCode, userId);
        NoticeDepartmentMessage message = NoticeDepartmentMessage.builder()
                .title(board.getBoardTitle())
                .userUID(userIds.toArray(new String[userIds.size()]))
                .build();
        extraMessageUtil.noticeDepartment(message);
    }

}
