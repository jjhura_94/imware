/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.TndmBoardComment;
import com.a2m.imware.model.TndmBusinessCard;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.impl.Com0105SerivceImpl;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.Utils;
import com.google.common.base.Strings;

/**
 *
 * @author Admin
 */
@RestController
@RequestMapping(value = "/api/com/com0105")
public class Com0105Controller {

    @Autowired
    private Com0105SerivceImpl com0105Service;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private CommonDeptService commonDeptService;

    @GetMapping("/search")
    public Object search(HttpServletRequest request, @RequestParam Map<Object, Object> params, @RequestParam(required = false, defaultValue = "0") Integer start, @RequestParam(required = false, defaultValue = "10") Integer limit) throws Exception {
        List<String> userIds = commonDeptService.getUidOfUser(request, params.get("deptCode"));
        params.put("USER_IDS", String.join(", ", userIds));
        params.put("start", start);
        params.put("limit", limit);
        return com0105Service.search(params);
    }

    @GetMapping("/findById")
    public Object findById(@RequestParam Long id) throws Exception {
        return com0105Service.findById(id);
    }

    @PostMapping("/save")
    public Object save(HttpServletRequest request, @RequestBody TndmBusinessCard businessCard) throws Exception {
        String code = getDeptCode(request);
        if (Strings.isNullOrEmpty(code)) {
            AjaxResult ajaxResult = new AjaxResult();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You don't have permission!");
            ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            return ajaxResult;
        }
        return com0105Service.save(businessCard);
    }

    public String getDeptCode(HttpServletRequest request) throws ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            String code = user.get("DEPT_CODE").toString();
            if (!code.equals("DEPT_0080")) {
                return user.get("USER_UID").toString();
            }
        }
        return null;
    }

    @GetMapping("commentList")
    @ResponseBody
    public List<Map<Object, Object>> commentList(@RequestParam Long boardId) {
        if (boardId == null || boardId <= 0) {
            return new ArrayList<>(0);
        }

        try {
            List<TndmBoardComment> commentList = com0105Service.findCommentLevel1(boardId);
            return commentList.stream().map(comment -> comment.toMap()).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>(0);
    }

    @PostMapping("/saveComment")
    @ResponseBody
    public Object saveComment(@RequestBody(required = false) Map<Object, Object> data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null || data.isEmpty()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(AjaxResult.Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        TndmBoardComment comment = new TndmBoardComment().fromMap(data);
        if (comment == null || Utils.isNullOrEmpty(comment.getContent())) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(AjaxResult.Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }

        try {
            final boolean needUpdate = comment.getId() != null && comment.getId() > 0;
            Map<Object, Object> dataToSave = comment.toMap();
            boolean isSuccess = needUpdate ? com0105Service.updateComment(dataToSave) : com0105Service.insertComment(dataToSave);
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            } else {
                ajaxResult.setResponseData(dataToSave.get("ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(AjaxResult.Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    @DeleteMapping("/deleteComment")
    @ResponseBody
    public Object deleteComment(@RequestParam Long id) {
        return com0105Service.deleteComment(id);
    }
}
