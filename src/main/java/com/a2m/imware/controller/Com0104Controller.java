package com.a2m.imware.controller;

import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/com/com0104")
@SuppressWarnings({"rawtypes", "unchecked"})
@Validated
public class Com0104Controller {

    @Autowired
    private Sys0105Service service;
    @Autowired
    private CommonDeptService commonDeptService;

    @GetMapping("/search")
    public Object search(HttpServletRequest request, @RequestParam Map<Object, Object> arg) throws SQLException, ImwareException {
        if (arg.get("deptCode") != null && !Strings.isNullOrEmpty(arg.get("deptCode").toString())) {
            List<String> userIds = commonDeptService.getUidOfUser(request, arg.get("deptCode"));
            arg.put("USER_IDS", String.join(", ", userIds));
        }
        arg.put("status", "Y");
        return service.search(arg);
    }

}
