package com.a2m.imware.controller.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.service.search.SearchService;
import com.a2m.imware.util.Utils;

@RestController
@RequestMapping("/api/search")
public class SearchController {
	@Autowired
	private SearchService searchService;

	@GetMapping("/electronicPayment")
	public Map<Object, Object> getListSearchElectronicPayment(@RequestParam Map<Object, Object> args) {
		List<Map<Object, Object>> listMap = new ArrayList();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		if(args.get("DEPT_CODE") != null) {
			args.replace("DEPT_CODE", Utils.getForInQuery(args.get("DEPT_CODE").toString(), ","));
		}
		int count = 0;
		try {
			count = searchService.getCountElectronicPaymentWithConditions(args);
			if (count > 0) {
				listMap = searchService.getListElectronicPaymentWithConditions(args);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultMap.put("COUNT", count);
		resultMap.put("LIST", listMap);
		return resultMap;
	}

	@GetMapping("/businessManagement")
	public Map<Object, Object> getListBusinessManagement(@RequestParam Map<Object, Object> args) {
		List<Map<Object, Object>> listMap = new ArrayList();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		int count = 0;
		try {
			count = searchService.getCountBusinessManagementWithConditions(args);
			if (count > 0) {
				listMap = searchService.getListBusinessManagementWithConditions(args);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultMap.put("COUNT", count);
		resultMap.put("LIST", listMap);
		return resultMap;
	}

	@GetMapping("/community")
	public Map<Object, Object> getListCommunity(@RequestParam Map<Object, Object> args) {
		List<Map<Object, Object>> listMap = new ArrayList();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		int count = 0;
		try {
			count = searchService.getCountCommunitiesWithConditions(args);
			if (count > 0) {
				listMap = searchService.getListCommunitiesWithConditions(args);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultMap.put("COUNT", count);
		resultMap.put("LIST", listMap);
		return resultMap;
	}

	@GetMapping("/myPage")
	public Map<Object, Object> getListMyPage(@RequestParam Map<Object, Object> args) {
		List<Map<Object, Object>> listMap = new ArrayList();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		int count = 0;
		try {
			count = searchService.getCountMyPageWithConditions(args);
			if (count > 0) {
				listMap = searchService.getListMyPageWithConditions(args);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultMap.put("COUNT", count);
		resultMap.put("LIST", listMap);
		return resultMap;
	}
}
