package com.a2m.imware.controller;

import com.a2m.imware.model.TccoFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.TndmNotice;
import com.a2m.imware.model.TndmNoticeComment;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.Com0101Service;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.AjaxResult.Code;
import com.a2m.imware.util.Pager;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.NoticeBoardMessage;
import com.a2m.imware.wschat.model.extra.NoticeBoardMessage.DownloadLink;
import com.a2m.imware.wschat.util.ParameterUtil;
import com.google.common.base.Strings;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.DeleteMapping;

@RestController
@RequestMapping("/api/com/com0101")
public class Com0101Controller {

    @Autowired
    private Com0101Service com0101Service;
    @Autowired
    private ExtraMessageUtil extraMessageUtil;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private Sys0105Service sys0105Service;
    @Autowired
    private CommonDeptService commonDeptService;

    @SuppressWarnings("unchecked")
    @GetMapping("/list")
    @ResponseBody
    public Object getNoticeList(HttpServletRequest request, @RequestParam(required = false, defaultValue = "0") Integer start, @RequestParam(required = false, defaultValue = "10") Integer limit) throws ImwareException {
        Map<Object, Object> parameters = ParameterUtil.getParameterMap(request);
        List<String> userIds = commonDeptService.getUidOfUser(request, parameters.get("DEPT_CODE"));
        parameters.put("USER_IDS", String.join(", ", userIds));
        TndmNotice filter = new TndmNotice().fromMap(parameters);
        if (filter.getPager() == null) {
            filter.setPager(new Pager());
        }
        filter.getPager().setStart(start);
        filter.getPager().setLimit(limit);
        try {
            if (!Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && !Strings.isNullOrEmpty(filter.getCreateTimeTo())) {

            } else if (!Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                filter.setTimeType(2);
            } else if (Strings.isNullOrEmpty(filter.getCreateTimeFrom()) && !Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                filter.setTimeType(3);
            }
            if (!Strings.isNullOrEmpty(filter.getCreateTimeTo())) {
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                Date d = f.parse(filter.getCreateTimeTo());
                long milliseconds = d.getTime() + 86400000L;
                d = new Date(milliseconds);
                filter.setCreateTimeTo(f.format(d));
            }
            List<Map<Object, Object>> dataList = com0101Service.fetch(filter.toMap()).stream().map(obj -> obj.toMap()).collect(Collectors.toList());
            Long count = com0101Service.count(filter.toMap());

            return new PageResponse(dataList, count == null ? 0 : count.intValue());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @GetMapping("/findById")
    @ResponseBody
    public Map<Object, Object> getNotice(@RequestParam Long id) {
        if (id == null || id <= 0) {
            return null;
        }

        try {
            TndmNotice notice = com0101Service.findById(id);
            return notice == null ? null : notice.toMap();
        } catch (Exception e) {
            // ignored
        }

        return null;
    }

    // insert + update => insert when ID is null, else => insert
    @PostMapping("/save")
    @ResponseBody
    public Object save(HttpServletRequest request, @RequestBody(required = false) Map<Object, Object> data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null || data.isEmpty()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        TndmNotice notice = new TndmNotice().fromMap(data);
        if (notice == null || Utils.isNullOrEmpty(notice.getNoticeTitle()) || Utils.isNullOrEmpty(notice.getContent())) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }

        if (Utils.isNullOrEmpty(notice.getPopupYn())) {
            notice.setPopupYn("N");
        }

        try {
            final boolean needUpdate = notice.getId() != null && notice.getId() > 0;
            boolean isSuccess = false;
            Map<Object, Object> dataToSave = notice.toMap();
            // capture date
            dataToSave.put("POPUP_FROM", data.get("POPUP_FROM"));
            dataToSave.put("POPUP_TO", data.get("POPUP_TO"));
            Utils.convertEpochTimeToDate(dataToSave, Arrays.asList("POPUP_FROM", "POPUP_TO", "CREATED_DATE", "UPDATED_DATE"));

            if (needUpdate) { // do update
                // remove never update fields
                dataToSave.remove("CREATED_BY");
                dataToSave.remove("CREATED_DATE");
                dataToSave.remove("UPDATED_DATE");
                dataToSave.remove("DEPT_CODE");
                dataToSave.remove("DEPT_NAME");
                com0101Service.update(dataToSave);
                isSuccess = true;
            } else { // do insert
                isSuccess = com0101Service.insert(dataToSave);
                this.sendMessage(notice, request);
            }

            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(Code.FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    @GetMapping("commentList")
    @ResponseBody
    public List<Map<Object, Object>> commentList(@RequestParam Long noticeId) {
        if (noticeId == null || noticeId <= 0) {
            return new ArrayList<>(0);
        }

        try {
            List<TndmNoticeComment> commentList = com0101Service.findCommentLevel1(noticeId);
            return commentList.stream().map(comment -> comment.toMap()).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>(0);
    }

    // insert + update => insert when ID is null, else => insert
    @PostMapping("/saveComment")
    @ResponseBody
    public Object saveComment(@RequestBody(required = false) Map<Object, Object> data) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (data == null || data.isEmpty()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        TndmNoticeComment comment = new TndmNoticeComment().fromMap(data);
        if (comment == null || Utils.isNullOrEmpty(comment.getContent())) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }

        try {
            data.put("CONTENT", data.get("CONTENT").toString().trim());
            final boolean needUpdate = comment.getId() != null && comment.getId() > 0;
            Map<Object, Object> dataToSave = comment.toMap();
            boolean isSuccess = needUpdate ? com0101Service.updateComment(dataToSave) : com0101Service.insertComment(dataToSave);
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(Code.FAILED);
            } else {
                ajaxResult.setResponseData(dataToSave.get("ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    @DeleteMapping("/deleteComment")
    @ResponseBody
    public Object deleteComment(@RequestParam Long id) {
        return com0101Service.deleteComment(id);
    }

    public void sendMessage(TndmNotice notice, HttpServletRequest request) throws ImwareException, SQLException {
        List<DownloadLink> downloadLists = new ArrayList();
        if (notice.getFilesToAdd() != null && !notice.getFilesToAdd().isEmpty()) {
            for (Object object : notice.getFilesToAdd()) {
                TccoFile tccoFile = new TccoFile().fromMap((Map<Object, Object>) object);
                DownloadLink downloadLink = new DownloadLink();
                downloadLink.setLink("/common/file-service/downloadFile?fileSequence=" + tccoFile.getAtchFleSeq());
                downloadLink.setLinkTitle(tccoFile.getFleNm());
                downloadLists.add(downloadLink);
            }
        }
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        List<String> userIds = userInfoService.getUserIds();
        NoticeBoardMessage message = NoticeBoardMessage.builder()
                .title(notice.getNoticeTitle())
                .writerDeptName(notice.getDeptName())
                .writerName(notice.getNameKr())
                .writerPosition("")
                .userUID("")
                .downloadList(downloadLists)
                .build();
        if (userIds != null && !userIds.isEmpty()) {
            Map<Object, Object> userInfo = userInfoService.getUserInfo(userId, "");
            message.setUserUid(userIds.toArray(new String[userIds.size()]));
            message.setWriterPosition(userInfo.get("POSITION").toString());
            extraMessageUtil.noticeBoard(message);
        }
    }

}
