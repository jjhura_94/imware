package com.a2m.imware.controller.cms;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.response.TcdsEquipBookingResponse;
import com.a2m.imware.service.cms.Cms010402Service;

@RestController
@RequestMapping(value = "/api/cms/cms0104/cms010402")
public class Cms010402Controller {

	@Autowired
	private Cms010402Service service;

	@GetMapping("/search")
	public Object search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTcdsEquipBooking(arg);
	}

	@GetMapping("/get")
	public TcdsEquipBookingResponse get(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.getTcdsEquipBooking(arg);
	}

	@PostMapping("/create")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object addEquip(@RequestBody @Valid TcdsEquipBookingResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int inserted = service.insertTcdsEquipBooking(arg);
		if (inserted > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@PutMapping("/update")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object updateEquip(@RequestBody @Valid TcdsEquipBookingResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int updated = service.updateTcdsEquipBooking(arg);
		if (updated > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@DeleteMapping("/delete")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object deleteEquip(@RequestBody @Valid TcdsEquipBookingResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int delete = service.deleteTcdsEquipBooking(arg);
		if (delete > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}
}
