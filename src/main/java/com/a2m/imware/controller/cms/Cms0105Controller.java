/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.controller.cms;

import com.a2m.imware.model.TcdsEvent;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.cms.Cms0105Service;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.util.Utils;
import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Admin
 */
@RestController
@RequestMapping(value = "/api/cms/cms0105")
public class Cms0105Controller {

    @Autowired
    private Cms0105Service cms0105Service;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private CommonDeptService commonDeptService;

    @GetMapping("/search")
    public Object search(HttpServletRequest request, @RequestParam Map<Object, Object> params) throws Exception {
        Map<String, String> user = getDeptCode(request);
        List<String> userIds = new ArrayList();
        String depCode = "";
        if (params.get("deptCode") != null) {
            depCode = params.get("deptCode").toString();
        }
        if (user != null && !user.isEmpty()) {
            userIds = commonDeptService.getChildrenOfUser((!Strings.isNullOrEmpty(depCode) ? depCode : user.get("deptCode") ), user.get("userId"));
        }
        if (userIds.isEmpty()) {
            userIds.add("99998888");
        }
        params.put("userIds", userIds);
        params.put("currentId", user.get("userId"));
        return cms0105Service.search(params);
    }

    @GetMapping("/findById")
    public Object findById(@RequestParam Long id) throws Exception {
        return cms0105Service.findById(id);
    }

    @PostMapping("/save")
    public Object save(HttpServletRequest request, @RequestBody TcdsEvent tcdsEvent) throws Exception {
        return cms0105Service.save(tcdsEvent);
    }

    public Map<String, String> getDeptCode(HttpServletRequest request) throws ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            Map<String, String> result = new HashMap();
            result.put("userId", user.get("USER_UID").toString());
            result.put("deptCode", user.get("DEPT_CODE").toString());
            return result;
        }
        return null;
    }

}
