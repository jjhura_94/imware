package com.a2m.imware.controller.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.response.TslgLoginStateResponse;
import com.a2m.imware.service.cms.Cms0103ExcelService;
import com.a2m.imware.service.cms.Cms0103Service;
import com.google.api.client.util.IOUtils;

@RestController
@RequestMapping(value = "/api/cms/cms0103")
public class Cms0103Controller {

	@Autowired
	private Cms0103Service cms0103Service;

	@Autowired
	private Cms0103ExcelService cms0103ExcelService;

	@GetMapping("/search")
	public Object search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return cms0103Service.searchTslgLoginState(arg);
	}

	@GetMapping("/get")
	public TslgLoginStateResponse get(@RequestParam Map<Object, Object> arg) throws SQLException {
		return cms0103Service.getTslgLoginState(arg);
	}

	@PostMapping("/insertLogin")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object insertLogin(@RequestBody @Valid TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int inserted = cms0103Service.insertLogin(arg);
		if (inserted > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@PostMapping("/insertLogout")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object insertLogout(@RequestBody @Valid TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int updated = cms0103Service.insertLogout(arg);
		if (updated > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@GetMapping("/get2")
	public Object searchTslgLoginState2(@RequestParam Map<Object, Object> arg) throws SQLException {
		return cms0103Service.searchTslgLoginState2(arg);
	}

	@PutMapping("/updateSessInfo")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object updateSessInfo(@RequestBody @Valid TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int updated = cms0103Service.updateSessInfo(arg);
		if (updated > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@DeleteMapping("/delete")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object dalete(@RequestBody @Valid TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int delete = cms0103Service.deleteTslgLoginState(arg);
		if (delete > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@ResponseBody
	@GetMapping("/getExcel")
	public void getExcel(@RequestParam Map<Object, Object> arg, HttpServletResponse response)
			throws SQLException, IOException, InvalidFormatException {
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=근태관리.xlsx");
		List<TslgLoginStateResponse> tcdsEmpEquipments = cms0103ExcelService.searchTslgLoginState(arg);
		ByteArrayInputStream resource = new ByteArrayInputStream(
				cms0103ExcelService.writeExcel(tcdsEmpEquipments).toByteArray());
		IOUtils.copy(resource, response.getOutputStream());
	}
}
