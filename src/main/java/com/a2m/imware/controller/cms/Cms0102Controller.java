package com.a2m.imware.controller.cms;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.response.TcdsEmpVacationApprovalResponse;
import com.a2m.imware.model.response.TcdsEmpVacationInfoResponse;
import com.a2m.imware.model.response.TcdsEmpVacationRequestResponse;
import com.a2m.imware.service.cms.Cms0102Service;

@RestController
@RequestMapping(value = "/api/cms/cms0102")
public class Cms0102Controller {

	@Autowired
	private Cms0102Service service;

	@PostMapping("/insertScheduleVacationDays")
	public void insertScheduleVacationDays() throws Exception {
		service.insertScheduleVacationDays();
	}

	@PostMapping("/updateScheduleVacationDays")
	public void updateScheduleVacationDays() throws Exception {
		service.updateScheduleVacationDays();
	}

	@GetMapping("/searchVRVI")
	public Object searchVRVI(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTcdsEmpVacationRequest(arg);
	}

	@GetMapping("/searchVRVI2")
	public Object searchVRVI2(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTcdsEmpVacationRequest2(arg);
	}

	@GetMapping("/searchVRUH")
	public Object searchVRUH(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchVRForUsageHistory(arg);
	}

	@GetMapping("/searchVI")
	public Object searchVI(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTcdsEmpVacationInfo(arg);
	}

	@GetMapping("/searchListDept")
	public Object searchListDept(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchListDept(arg);
	}

	@GetMapping("/getVI")
	public TcdsEmpVacationInfoResponse getTcdsEmpVacationInfo(@RequestParam Map<Object, Object> arg)
			throws SQLException {
		return service.getTcdsEmpVacationInfo(arg);
	}

	@PostMapping("/insertVacation")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object insertVacation(@RequestBody @Valid TcdsEmpVacationRequestResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		String inserted = service.insertVacation(arg);
		if ("Y".equals(inserted)) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@PostMapping("/updateVacation")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object updateVacation(@RequestBody @Valid TcdsEmpVacationApprovalResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		String inserted = service.updateVacation(arg);
		if ("Y".equals(inserted)) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}
	
	@DeleteMapping("/deleteVacation")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object deleteVacation(@RequestBody @Valid TcdsEmpVacationRequestResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int delete = service.deleteTcdsEmpVacation(arg);
		if (delete > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@GetMapping("/countDeptHead")
	public Object countDeptHead(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.countDeptHead(arg);
	}

}
