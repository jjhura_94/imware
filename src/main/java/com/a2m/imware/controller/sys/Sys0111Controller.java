package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.response.TcdsEquipMstResponse;
import com.a2m.imware.service.sys.Sys0111Service;

@RestController
@RequestMapping(value = "/api/sys/sys0111")
public class Sys0111Controller {
	public static String MENU_URL = "sys/sys0111";

	@Autowired
	private Sys0111Service service;

	@GetMapping("/search")
	public Object search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.search(arg);
	}

	@GetMapping("/get")
	public Map<Object, Object> get(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.get(arg);
	}

	@PostMapping("/create")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object create(@RequestBody @Valid TcdsEquipMstResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int inserted = service.create(arg);
		if (inserted > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@PutMapping("/update")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object update(@RequestBody @Valid TcdsEquipMstResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int updated = service.update(arg);
		if (updated > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@DeleteMapping("/delete")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object delete(@RequestBody @Valid TcdsEquipMstResponse arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int delete = service.delete(arg);
		if (delete > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}
}
