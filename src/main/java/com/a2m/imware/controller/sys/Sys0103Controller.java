package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.model.TsstUserRole;
import com.a2m.imware.model.request.SaveRoleRequest;
import com.a2m.imware.model.request.TsstRoleMenuRequest;
import com.a2m.imware.model.request.TsstRoleRequest;
import com.a2m.imware.model.response.TsstRoleMenuResponse;
import com.a2m.imware.service.sys.Sys0103Service;
import com.a2m.imware.validation.ValidationCode;
import org.apache.commons.lang3.StringEscapeUtils;

@RestController
@RequestMapping(value = "/api/sys/sys0103")
@SuppressWarnings({"rawtypes","unchecked"})
@Validated
public class Sys0103Controller {

	@Autowired
	private Sys0103Service service;

	@GetMapping("/search")
	public List search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTsstRole(arg);
	}
	
	@GetMapping("/search/user-role")
	public List searchTsstUserRole(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTsstUserRole(arg);
	}
	

	@PostMapping("/save")
	@Transactional(rollbackFor = { Exception.class })
	public Object save(@RequestBody @Valid SaveRoleRequest arg) throws Exception {
		List<TsstRoleRequest> grid = arg.getTsstRole();
		Date currentDate = new Date();
		for (TsstRoleRequest tsstRoleRequest : grid) {
			if(StringUtils.isNotEmpty(tsstRoleRequest.getRoleId())) {
				if("U".equals(tsstRoleRequest.getState())) {
					tsstRoleRequest.setCreatedDateString(DateUtil.convertDateToStringDB(tsstRoleRequest.getCreatedDate()));
					tsstRoleRequest.setUpdatedBy(arg.getSessUserId());
					tsstRoleRequest.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
					service.updateTsstRole(tsstRoleRequest);
				}
			}
			else {
				tsstRoleRequest.setUpdatedDateString(DateUtil.convertDateToStringDB(tsstRoleRequest.getUpdatedDate()));
				tsstRoleRequest.setCreatedBy(arg.getSessUserId());
				tsstRoleRequest.setCreatedDateString(DateUtil.convertDateToStringDB(currentDate));
				service.insertTsstRole(tsstRoleRequest);
			}
		}
		if(StringUtils.isNotEmpty(arg.getIdsDel())) {
			String escapedXml = StringEscapeUtils.escapeXml(arg.getIdsDel());
			if(!escapedXml.equals(arg.getIdsDel())) {
				throw new Exception("data invalid");
			}
			this.service.deleteTsstRoles(com.a2m.imware.common.ImwareStringUtils.getForInQuery(arg.getIdsDel(), ","));
		}
		Map result = new HashMap<String, String>();
		result.put("RESULT", "Y");
		return result;

	}
	
	@PostMapping("/save/{type}/{id}")
	@Transactional(rollbackFor = { Exception.class })
	public Object saveUserRole(@RequestBody @Valid List<TsstUserRole> arg, @PathVariable("type") String type,
			@NotNull(message = ValidationCode.NotNull)
			@NotBlank(message = ValidationCode.NotBlank)
			@PathVariable("id") String id) throws SQLException {
		if(arg.size() == 0) {
			return null;
		}
		TsstUserRole tsstUserRole = new TsstUserRole();
		
		if("user-role".equals(type)) {
			tsstUserRole.setUserUid(id);
		}
		else {
			tsstUserRole.setRoleId(id);
		}
		service.deleteTsstUserRole(tsstUserRole);
		arg.forEach(ele -> {
			try {
				service.insertTsstUserRole(ele);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
		Map result = new HashMap<String, String>();
		result.put("RESULT", "Y");
		return result;
	}
	
	
	@GetMapping("/search/menu-role")
	public List searchTsstRoleMenu(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTsstRoleMenu(arg);
	}
	
	@PostMapping("/save/menu-role")
	@Transactional(rollbackFor = { Exception.class })
	public Object searchTsstRoleMenu(@RequestBody @Valid List<TsstRoleMenuRequest> arg) throws SQLException {
		for (TsstRoleMenuRequest item : arg) {
			TsstRoleMenuResponse getMenuRole = service.getTsstRoleMenu(item);
			if(getMenuRole != null) {
				service.updateTsstRoleMenu(item);
			}
			else {
				service.insertTsstRoleMenu(item);
			}
		}
		Map result = new HashMap<String, String>();
		result.put("RESULT", "Y");
		return result;
	}

}
