package com.a2m.imware.controller.sys.sys0105;

import com.a2m.imware.model.request.TcdsEmpEduRequest;
import com.a2m.imware.model.response.TcdsEmpEduResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys010501Service;
import com.a2m.imware.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/sys/sys010501")
public class Sys010501Controller {

    @Autowired
    private Sys010501Service service;
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/{empNo}")
    public List getAlledu(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.getAllEduByEmpNo(empNo);
    }
    @GetMapping("/getEduById")
    public TcdsEmpEduResponse getEduById(@RequestParam Long id) throws SQLException {
        return service.getEduById(id);
    }
    @DeleteMapping("/deleteEdu")
    public ResponseEntity deleteEdu(@RequestParam Long id) throws SQLException {
        List<Long> eduIds = new ArrayList();
        eduIds.add(id);
        service.deleteAllByEduIds(eduIds);
        return ResponseEntity.ok(true);
    }

    @DeleteMapping("")
    public ResponseEntity deleteAllByWardIds(@RequestParam("eduIds") List<Long> eduIds) throws SQLException {
        service.deleteAllByEduIds(eduIds);
        return ResponseEntity.ok(true);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity create(HttpServletRequest request, @RequestBody @Valid TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException, ImwareException {

        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            tcdsEmpEduRequest.setCreatedBy(user.get("USER_UID").toString());
            tcdsEmpEduRequest.setEmpNo((Long) user.get("EMP_NO"));
        }
        int inserted = service.insert(tcdsEmpEduRequest);
        return ResponseEntity.ok(inserted);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity update(@RequestBody @Valid TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException {
        int updated = service.update(tcdsEmpEduRequest);;
        return ResponseEntity.ok(updated);
    }
}
