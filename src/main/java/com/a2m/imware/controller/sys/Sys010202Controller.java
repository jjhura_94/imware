package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.model.sys.TcdsEmpEdu;
import com.a2m.imware.service.sys.Sys010202Service;

@RestController
@RequestMapping(value = "/api/sys/sys010202")
public class Sys010202Controller {

	@Autowired
	private Sys010202Service service;

	@GetMapping("/list")
	public Object getAllEdu(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.getAllEdu(arg);
	}

	@GetMapping("/get")
	public TcdsEmpEdu getEduInfo(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.getEduInfo(arg);
	}

	@PostMapping("/create")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object addEdu(@RequestBody @Valid TcdsEmpEdu arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int inserted = service.addEdu(arg);
		if (inserted > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@PutMapping("/update")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object updateEdu(@RequestBody @Valid TcdsEmpEdu arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int updated = service.updateEdu(arg);
		if (updated > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

	@DeleteMapping("/delete")
	@Transactional(rollbackFor = { SQLException.class, Exception.class })
	public Object deleteEdu(@RequestBody @Valid TcdsEmpEdu arg) throws SQLException {
		Map<Object, Object> result = new HashMap<>();
		int delete = service.deleteEdu(arg);
		if (delete > 0) {
			result.put("INFO", "SUCCESS");
		} else {
			result.put("INFO", "FAILURE");
		}
		return result;
	}

}
