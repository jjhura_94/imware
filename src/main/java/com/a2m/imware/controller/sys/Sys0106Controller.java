package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.service.sys.Sys0106Service;

@RestController
@RequestMapping(value = "/api/sys/sys0106")
public class Sys0106Controller {

	@Autowired
	private Sys0106Service service;
	
	@GetMapping("/search")
	public List search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchTsstRole(arg);
	}
	
}
