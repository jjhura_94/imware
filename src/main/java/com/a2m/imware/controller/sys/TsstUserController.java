package com.a2m.imware.controller.sys;

import com.a2m.imware.config.CommonTccoFileConfig;
import com.a2m.imware.model.request.TsstUserRequest;
import com.a2m.imware.model.response.TsstUserResponse;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.sys.TsstUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RestController
@RequestMapping(value = "/api/tsst-users")
public class TsstUserController {

    @Autowired
    private TsstUserService service;
    @Autowired
    private CommonTccoFileConfig commonTccoFileConfig;

    @Autowired
    private TccoFileService tccoFileService;

    @GetMapping("/emp-nos/{empNo}")
    public TsstUserResponse findFirstTsstUserByEmpNo(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.findFirstTsstUserByEmpNo(empNo);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity create(@RequestBody @Valid TsstUserRequest tsstUserRequest) throws Exception {
        BCryptPasswordEncoder b = new BCryptPasswordEncoder();
        tsstUserRequest.setPwd(b.encode("25f9e794323b453885f5181f1b624d0b"));//default: 123456789
        int inserted = service.insert(tsstUserRequest);
        return ResponseEntity.ok(inserted);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public Object update(@RequestBody TsstUserRequest tsstUserRequest) throws SQLException, Exception {

        tsstUserRequest.setPwd("");
        return service.update(tsstUserRequest);
    }

    @GetMapping("/count-all-by-user-id-and-user-uid-not")
    public ResponseEntity countAllByUserIdAndUserUidNot(@RequestParam String userId, @RequestParam String userUid) {
        Long count = service.countAllByUserIdAndUserUidNot(userId, userUid);
        return ResponseEntity.ok(count);
    }

    @GetMapping("/count-all-by-user-id-and-pwd")
    public Long countAllByUserIdAndPwd(@RequestParam String userId, @RequestParam String pwd) {
        TsstUserResponse user = service.getByUserId(userId);
        BCryptPasswordEncoder b = new BCryptPasswordEncoder();
        if (b.matches(pwd, user.getPwd())) {
            return 1L;
        }
        return -1L;
    }

}
