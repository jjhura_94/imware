package com.a2m.imware.controller.sys.sys0105;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.config.CommonTccoFileConfig;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.request.TcdsEmpMstRequest;
import com.a2m.imware.model.response.TcdsEmpMstResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.CommonDeptService;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.a2m.imware.util.CommonFileUtil;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.util.ImageUtil;
import com.google.common.base.Strings;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/sys/sys0105")
@SuppressWarnings({"rawtypes", "unchecked"})
@Validated
public class Sys0105Controller {

    @Autowired
    private Sys0105Service service;
    @Autowired
    private CommonTccoFileConfig commonTccoFileConfig;

    @Autowired
    private TccoFileService tccoFileService;

    @Autowired
    private Environment env;
    @Autowired
    private CommonDeptService commonDeptService;
    
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/search")
    public Object search( HttpServletRequest request, @RequestParam Map<Object, Object> arg) throws SQLException, ImwareException {
        if (arg.get("deptCode") != null && !Strings.isNullOrEmpty(arg.get("deptCode").toString())) {
            List<String> userIds = commonDeptService.getUidOfUser(request, arg.get("deptCode"));
            arg.put("USER_IDS", String.join(", ", userIds));
        }
        return service.search(arg);
    }

    @GetMapping("/{empNo}")
    public ResponseEntity get(@PathVariable("empNo") Long empNo) throws ImwareException, SQLException {
        TcdsEmpMstResponse tcdsEmpMstResponse = service.get(empNo);
        return ResponseEntity.ok(tcdsEmpMstResponse);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public long create(@RequestBody @Valid TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException {
       service.insert(tcdsEmpMstRequest);
        return tcdsEmpMstRequest.getEmpNo();
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity update(HttpServletRequest request, @RequestBody @Valid TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException, ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if(user != null && !user.isEmpty()){
            tcdsEmpMstRequest.setUserId(user.get("USER_UID").toString());
        }
        int updated = service.update(tcdsEmpMstRequest);
        
        return ResponseEntity.ok(updated);
    }

    @DeleteMapping("/emp-no/{empNo}")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public void deleteAllByEmpNos(@PathVariable("empNo") Long empNo) throws SQLException {
        service.delete(empNo);
    }

    @GetMapping("/searchForVacation")
    public Object searchForVacation() throws SQLException {
        return service.searchForVacation();
    }

    @RequestMapping(value = "/uploadAvatar", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Object uploadAvatar(@RequestParam("file") MultipartFile multipartFile, @RequestParam String userId, @RequestParam String atchFleSeq, @RequestParam int position) throws Exception {
        TccoFile tccoFile = setDefaultValues(userId, atchFleSeq, multipartFile);
        String dir = commonTccoFileConfig.getPathDefaultUploaddir() + "/";
        try {
            CommonFileUtil.save(dir.concat(tccoFile.getNewFleNm()), multipartFile);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        TccoFile output = tccoFileService.saveTccoFile(tccoFile);
        //update user: position = 1: avatar , 2 = signature
//        doUpload3(multipartFile);
        service.updateImageOrSiganture(userId, output.getAtchFleSeq(), position);
        return output;
    }

    public TccoFile setDefaultValues(String userId, String atchFleSeq, MultipartFile multipartFile) throws Exception {
        TccoFile oldTccoFile = StringUtils.isEmpty(atchFleSeq) ? null : tccoFileService.findBySequence(atchFleSeq);

        TccoFile tccoFile = new TccoFile();

        if (oldTccoFile == null) {
            tccoFile.setAtchFleSeq(UUID.randomUUID().toString());
            tccoFile.setCreatedDateStr(DateUtil.convertDateToStringDB(new Date()));
            tccoFile.setCreatedBy(userId);
        } else {
            tccoFile.setUpdatedDateStr(DateUtil.convertDateToStringDB(new Date()));
            tccoFile.setUpdatedBy(userId);
            tccoFile.setAtchFleSeq(atchFleSeq);
        }
        Long fleSz = multipartFile.getSize();
        String fleNm = multipartFile.getOriginalFilename();
        String newFleNm = CommonFileUtil.replaceFileName(tccoFile.getAtchFleSeq(), fleNm);
        String fileTp = CommonFileUtil.getExt(fleNm).replace(".", "");
        tccoFile.setFleSz(fleSz.toString());
        tccoFile.setFleNm(fleNm);
        tccoFile.setNewFleNm(newFleNm);

        tccoFile.setFleTp(fileTp);
        tccoFile.setFlePath(newFleNm);
        return tccoFile;
    }

    private String doUpload3(MultipartFile uploadfile) throws Exception {
        String fileName = null;
        String newFileName = null;
        File uploadRootDir = null;
        final String rootPath = env.getProperty("wschat.file.uploadDir");

        if (uploadfile instanceof MultipartFile) {
            MultipartFile multipartFile = (MultipartFile) uploadfile;

            uploadRootDir = new File(rootPath);
            uploadRootDir.setExecutable(true, true);
            uploadRootDir.setReadable(true);
            uploadRootDir.setWritable(true, true);
            // Create directory if it not exists.
            if (!uploadRootDir.exists()) {
                uploadRootDir.mkdirs();
            }

            fileName = multipartFile.getOriginalFilename();

            if (fileName != null && fileName.length() > 0) {
                try {
                    // Create file in Server.
                    int ext_pos = fileName.lastIndexOf(".");
                    String ext = fileName.substring(ext_pos).toLowerCase();
                    newFileName = "PROF_" + (new Date()).getTime() + ext;

                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + newFileName);

                    // Ouput stream file in Server.
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(multipartFile.getBytes());
                    stream.close();
                    BufferedImage in = ImageIO.read(serverFile);
                    String[] a = {"PNG", "JPG", "BMP"};
                    String[] b = {"MP4", "MOV", "WMV"};

                    if (Arrays.asList(a).contains(ext.substring(1).toUpperCase())) {
                        File uploadRootDirTN = null;
                        uploadRootDirTN = new File(rootPath + "/thumb");
                        uploadRootDirTN.setExecutable(true, true);
                        uploadRootDirTN.setReadable(true);
                        uploadRootDirTN.setWritable(true, true);
                        // Create directory if it not exists.
                        if (!uploadRootDirTN.exists()) {
                            uploadRootDirTN.mkdirs();
                        }
                        ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in,
                                uploadRootDirTN.getAbsolutePath() + File.separator + newFileName);
                    } else if (Arrays.asList(b).contains(ext.substring(1).toUpperCase())) {
                        /*File uploadRootDirTN = null;
						uploadRootDirTN = new File(rootPath + "/thumb");
						uploadRootDirTN.setExecutable(true, true);
						uploadRootDirTN.setReadable(true);
						uploadRootDirTN.setWritable(true, true);
						// Create directory if it not exists.
						if (!uploadRootDirTN.exists()) {
							uploadRootDirTN.mkdirs();
						}
						OpenCVFrameConverter.ToIplImage converterToIplImage = new OpenCVFrameConverter.ToIplImage();

						FFmpegFrameGrabber g = new FFmpegFrameGrabber(serverFile);
						g.setFormat(ext.substring(1).toUpperCase() == "" ? "mp4" : ext.substring(1).toUpperCase());
						g.start();

						for (int i = 0; i < 60; i++) {
							if (g.grab().image != null) {
								IplImage image = converterToIplImage.convert(g.grab());
								String thumbPath = rootPath + "/thumb/" + serverFile.getName().split("[.]")[0] + ".png";
								cvSaveImage(thumbPath, image);
							}
						}

						g.stop();
						g.close();

						BufferedImage in2 = ImageIO
								.read(new File(rootPath + "/thumb/" + serverFile.getName().split("[.]")[0] + ".png"));
						ImageUtil.resizeFromBufferedImageByMaxEdgeSize(in2,
								rootPath + "/thumb/" + serverFile.getName().split("[.]")[0] + ".png");*/
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
        }

        return newFileName;
    }
}
