package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.common.menu.MenuPermission;
import com.a2m.imware.common.menu.MenuPermissionType;
import com.a2m.imware.model.common.RequestDataWrapper;
import com.a2m.imware.model.common.ResponseData;
import com.a2m.imware.model.sys.TcdsDeptStruct;
import com.a2m.imware.model.sys.TcdsStructMgt;
import com.a2m.imware.service.sys.Sys0108Service;

@RestController
@RequestMapping(value = "/api/sys/sys0108")
public class Sys0108Controller {

	public static String MENU_URL = "sys/sys0108";
	private static String onlyOneStructCanActive = "message.error.onlyOneStructActiveAtTheSameTime";

	@Autowired
	private Sys0108Service serviceSys0108;

	@GetMapping("/getStructList")
	@MenuPermission(permissions = MenuPermissionType.READ)
	public List<TcdsStructMgt> getStructList(@RequestParam Map<Object, Object> arg) throws SQLException {
		return serviceSys0108.searchTcdsStructMgt(arg);
	}

	@GetMapping("/getDeptStructList")
	@MenuPermission(permissions = MenuPermissionType.READ)
	public List<TcdsDeptStruct> getDeptStructList(@RequestParam Map<Object, Object> arg) throws SQLException {
		return serviceSys0108.searchTcdsDeptStruct(arg);
	}

	@PostMapping("/updateStruct")
	@MenuPermission(permissions = { MenuPermissionType.MODIFY })
	@TransactionalWithRollback
	public ResponseData<String> updateStruct(@Valid @RequestBody TcdsStructMgt struct) throws SQLException {
		if (ImwareStringUtils.YES.equals(struct.getUseYn())) {
			long count = serviceSys0108.countActiveStruct(struct.getStructId());
			if (count > 0) {
				return ResponseData.error(onlyOneStructCanActive);
			}
		}
		serviceSys0108.updateTcdsStructMgt(struct);
		return ResponseData.success();
	}

	@PostMapping("/createStruct")
	@MenuPermission(permissions = { MenuPermissionType.WRITE })
	@TransactionalWithRollback
	public ResponseData<String> createStruct(@Valid @RequestBody TcdsStructMgt struct) throws SQLException {
		if (ImwareStringUtils.YES.equals(struct.getUseYn())) {
			long count = serviceSys0108.countActiveStruct(null);
			if (count > 0) {
				return ResponseData.error(onlyOneStructCanActive);
			}
		}
		serviceSys0108.insertTcdsStructMgt(struct);
		return ResponseData.success();
	}

	@PostMapping("/deleteStruct")
	@MenuPermission(permissions = { MenuPermissionType.DELETE })
	@TransactionalWithRollback
	public ResponseData<String> deleteStruct(@RequestBody Map<String, String> arg) throws SQLException {
		TcdsStructMgt struct = this.serviceSys0108.getTcdsStructMgt(arg.get("id"));
		if (ImwareStringUtils.YES.equals(struct.getUseYn())) {
			return ResponseData.error("message.error.organizationInUse");
		}

		Map<Object, Object> params = new HashMap<>();
		params.put("structId", arg.get("id"));
		List<TcdsDeptStruct> deptStructList = serviceSys0108.getTcdsDeptStruct(params);

		serviceSys0108.deleteTcdsStructMgt(arg.get("id"));
		if (deptStructList != null && deptStructList.size() > 0) {
			for (TcdsDeptStruct deptStruct : deptStructList) {
				serviceSys0108.checkAndDeleteTcdsDept(deptStruct.getDeptCode());
			}
		}
		return ResponseData.success();
	}

	@PostMapping("/cloneStruct")
	@MenuPermission(permissions = { MenuPermissionType.WRITE })
	@TransactionalWithRollback
	public ResponseData<String> cloneStruct(@Valid @RequestBody TcdsStructMgt struct) throws SQLException {
		if (ImwareStringUtils.YES.equals(struct.getUseYn())) {
			long count = serviceSys0108.countActiveStruct(null);
			if (count > 0) {
				return ResponseData.error(onlyOneStructCanActive);
			}
		}
		serviceSys0108.cloneStruct(struct);
		return ResponseData.success();
	}

	@PostMapping("/updateDept")
	@MenuPermission(permissions = { MenuPermissionType.MODIFY })
	@TransactionalWithRollback
	public ResponseData<String> updateDept(@Valid @RequestBody TcdsDeptStruct dept) throws SQLException {
		System.out.println(dept);
		serviceSys0108.updateTcdsDeptStruct(dept);
		return ResponseData.success();
	}

	@PostMapping("/deleteDept")
	@MenuPermission(permissions = { MenuPermissionType.DELETE })
	@TransactionalWithRollback
	public ResponseData<String> deleteDept(@RequestBody RequestDataWrapper<String> requestData) throws SQLException {
		String structId = requestData.getAdditionalParams().get("structId");
		List<String> deptIdList = requestData.getIdListForDelete();
		if (deptIdList != null) {
			for (String deptCode : deptIdList) {
				Map<Object, Object> params = new HashMap<>();
				params.put("deptCode", deptCode);
				params.put("structId", structId);
				serviceSys0108.deleteTcdsDeptStruct(params);

				serviceSys0108.checkAndDeleteTcdsDept(deptCode);
			}
		}
		return ResponseData.success();
	}

	@PostMapping("/createDept")
	@MenuPermission(permissions = { MenuPermissionType.WRITE })
	@TransactionalWithRollback
	public ResponseData<String> createDept(@Valid @RequestBody TcdsDeptStruct dept) throws SQLException {
		serviceSys0108.insertTcdsDeptStruct(dept);
		return ResponseData.success();
	}

	@GetMapping("/search")
	public List getDeptStruct(@RequestParam Map<Object, Object> arg) throws SQLException {
		return serviceSys0108.getDeptStruct(arg);
	}

	@GetMapping("/getTcdsOneDeptStructName")
//	@MenuPermission(permissions = MenuPermissionType.READ)
	public TcdsDeptStruct getTcdsOneDeptStructName(@RequestParam Map<Object, Object> arg) throws SQLException {
		return serviceSys0108.getTcdsOneDeptStructName(arg);
	}

	@GetMapping("/get2")
	public Map<Object, Object> getTcdsOneDeptStructName2(@RequestParam Map<Object, Object> arg) throws SQLException {
		return serviceSys0108.getTcdsOneDeptStructName2(arg);
	}

}
