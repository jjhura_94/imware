package com.a2m.imware.controller.sys.sys0105;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.config.CommonTccoFileConfig;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.request.TcdsEmpAwardRequest;
import com.a2m.imware.model.response.TcdsEmpAwardResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys010503Service;
import com.a2m.imware.util.CommonFileUtil;
import com.a2m.imware.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/sys/sys010503")
public class Sys010503Controller {
    @Autowired
    private Sys010503Service service;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private CommonTccoFileConfig commonTccoFileConfig;

    @Autowired
    private TccoFileService tccoFileService;

    
    
    @GetMapping("/{empNo}")
    public List getAllAward(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.getAllAwardByEmpNo(empNo);
    }
    
    @GetMapping("/getById")
    public TcdsEmpAwardResponse getById(@RequestParam Long id) throws SQLException {
        return service.getById(id);
    }

    @DeleteMapping("")
    public ResponseEntity deleteAllByWardIds(@RequestParam("awardIds") List<Long> awardIds) throws SQLException {
        service.deleteAllByAwardIds(awardIds);
        return ResponseEntity.ok(true);
    }
    
    @DeleteMapping("/deleteAward")
    public ResponseEntity deleteAward(@RequestParam Long id) throws SQLException {
        List<Long> awardIds = new ArrayList();
        awardIds.add(id);
        service.deleteAllByAwardIds(awardIds);
        return ResponseEntity.ok(true);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class,Exception.class})
    public Long create(HttpServletRequest request,@RequestBody @Valid TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException, ImwareException, Exception {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            tcdsEmpAwardRequest.setCreatedBy(user.get("USER_UID").toString());
            tcdsEmpAwardRequest.setEmpNo((Long) user.get("EMP_NO"));
        }
        return service.insert(tcdsEmpAwardRequest);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class,Exception.class})
    public ResponseEntity update(@RequestBody @Valid TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException, Exception {
        int updated = service.update(tcdsEmpAwardRequest);;
        return ResponseEntity.ok(updated);
    }
    
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public Object uploadAvatar(HttpServletRequest request, @RequestParam("file") MultipartFile multipartFile, @RequestParam long awardId, @RequestParam String atchFleSeq) throws Exception {
         String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        TccoFile tccoFile = setDefaultValues(userId, atchFleSeq, multipartFile);
        String dir = commonTccoFileConfig.getPathDefaultUploaddir() + "/";
        try {
            CommonFileUtil.save(dir.concat(tccoFile.getNewFleNm()), multipartFile);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        TccoFile output = tccoFileService.saveTccoFile(tccoFile);
        //update user: position = 1: avatar , 2 = signature
//        doUpload3(multipartFile);
       
        service.updateAttachment(awardId, output.getAtchFleSeq());
        return output;
    }

    public TccoFile setDefaultValues(String userId, String atchFleSeq, MultipartFile multipartFile) throws Exception {
        TccoFile oldTccoFile = StringUtils.isEmpty(atchFleSeq) ? null : tccoFileService.findBySequence(atchFleSeq);

        TccoFile tccoFile = new TccoFile();

        if (oldTccoFile == null) {
            tccoFile.setAtchFleSeq(UUID.randomUUID().toString());
            tccoFile.setCreatedDateStr(DateUtil.convertDateToStringDB(new Date()));
            tccoFile.setCreatedBy(userId);
        } else {
            tccoFile.setUpdatedDateStr(DateUtil.convertDateToStringDB(new Date()));
            tccoFile.setUpdatedBy(userId);
            tccoFile.setAtchFleSeq(atchFleSeq);
        }
        Long fleSz = multipartFile.getSize();
        String fleNm = multipartFile.getOriginalFilename();
        String newFleNm = CommonFileUtil.replaceFileName(tccoFile.getAtchFleSeq(), fleNm);
        String fileTp = CommonFileUtil.getExt(fleNm).replace(".", "");
        tccoFile.setFleSz(fleSz.toString());
        tccoFile.setFleNm(fleNm);
        tccoFile.setNewFleNm(newFleNm);

        tccoFile.setFleTp(fileTp);
        tccoFile.setFlePath(newFleNm);
        return tccoFile;
    }
}
