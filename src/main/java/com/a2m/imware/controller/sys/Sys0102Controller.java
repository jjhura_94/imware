package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.a2m.imware.service.sys.Sys0102Service;

@RestController
@RequestMapping(value = "/api/sys/sys0102")
public class Sys0102Controller {
	@Autowired
	private Sys0102Service service;
	
	@GetMapping("/find")
	public List getUserInfo(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.getUserInfo(arg);
	}
	
	@GetMapping("/search")
	public List getAllUser(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.searchUser(arg);
	}
	
	@PostMapping("/create")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Map addUser(@RequestBody @Valid Map arg) throws SQLException {
		Map result = new HashMap();
		int inserted = service.addUser(arg);
		result.put("value", inserted);
		return result;
	}
	
	@PutMapping("/update")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Map updateUser(@RequestBody @Valid Map arg) throws SQLException {
		Map result = new HashMap();
		int updated = service.updateUser(arg);
		result.put("value", updated);
		return result;
	}
	
		
}
