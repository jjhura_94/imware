package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.common.menu.MenuPermission;
import com.a2m.imware.common.menu.MenuPermissionType;
import com.a2m.imware.model.common.ResponseData;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.sys.TcdsDutyMgt;
import com.a2m.imware.service.sys.Sys0109Service;

@RestController
@RequestMapping(value = "/api/sys/sys0109")
public class Sys0109Controller {
	public static String MENU_URL = "sys/sys0109";
	
	public static String dutyIsInUse = "message.error.dutyIsInUse";

	@Autowired
	private Sys0109Service service0109;

	@GetMapping("/getDutyList")
	@MenuPermission(permissions = MenuPermissionType.READ)
	public Object getDutyList(@RequestParam Map<Object, Object> arg) throws SQLException {
		PageResponse pageResponse = new PageResponse();
		pageResponse.setDatas(service0109.searchTcdsDutyMgt(arg));
		pageResponse.setCount(service0109.countTcdsDutyMgt(arg));
		return pageResponse;
	}
	
	@PostMapping("/updateDuty")
	@MenuPermission(permissions = { MenuPermissionType.MODIFY })
	@TransactionalWithRollback
	public ResponseData<?> updateDuty(@Valid @RequestBody TcdsDutyMgt duty) throws SQLException {

		// Validate USE_YN from Y to N
		if(ImwareStringUtils.NO.equals(duty.getUseYn())) {
			TcdsDutyMgt oldDuty = service0109.getTcdsDutyMgt(duty.getDutyCode());
			if(ImwareStringUtils.YES.equals(oldDuty.getUseYn())) {
				Map<Object, Object> params = new HashMap<>();
				params.put("dutyCode", duty.getDutyCode());
				params.put("useYn", ImwareStringUtils.YES);
				long countEmp = service0109.countEmpByDutyCode(params);
				if(countEmp > 0) {
					return ResponseData.error(dutyIsInUse);
				}
			}
		}
		
		this.service0109.updateTcdsDutyMgt(duty);
		return ResponseData.success();
	}
	
	@PostMapping("/createDuty")
	@MenuPermission(permissions = { MenuPermissionType.WRITE })
	@TransactionalWithRollback
	public ResponseData<String> createDuty(@Valid @RequestBody TcdsDutyMgt duty) throws SQLException {
		this.service0109.insertTcdsDutyMgt(duty);
		return ResponseData.success();
	}
	
	@PostMapping("/deleteDuty")
	@MenuPermission(permissions = { MenuPermissionType.WRITE })
	@TransactionalWithRollback
	public ResponseData<String> deleteDuty(@RequestBody TcdsDutyMgt duty) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("dutyCode", duty.getDutyCode());
		long countEmp = service0109.countEmpByDutyCode(params);
		if(countEmp > 0) {
			return ResponseData.error(dutyIsInUse);
		}
		this.service0109.deleteTcdsDutyMgt(duty.getDutyCode());
		return ResponseData.success();
	}
}
