package com.a2m.imware.controller.sys.sys0105;

import com.a2m.imware.model.request.TcdsEmpCareerRequest;
import com.a2m.imware.model.response.TcdsEmpCareerResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys010502Service;
import com.a2m.imware.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/sys/sys010502")
public class Sys010502Controller {

    @Autowired
    private Sys010502Service service;
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/{empNo}")
    public List getAllCareer(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.getAllCareerByEmpNo(empNo);
    }

    @GetMapping("/getById")
    public TcdsEmpCareerResponse getById(@RequestParam Long id) throws SQLException {
        return service.getById(id);
    }

    @DeleteMapping("/deleteCarrer")
    public ResponseEntity deleteCarrer(@RequestParam Long id) throws SQLException {
        List<Long> carrerIds = new ArrayList();
        carrerIds.add(id);
        service.deleteAllByCareerIds(carrerIds);
        return ResponseEntity.ok(true);
    }

    @DeleteMapping("")
    public ResponseEntity deleteAllByCareerIds(@RequestParam("careerIds") List<Long> careerIds) throws SQLException {
        service.deleteAllByCareerIds(careerIds);
        return ResponseEntity.ok(true);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity create(HttpServletRequest request, @RequestBody @Valid TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException, ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            tcdsEmpCareerRequest.setCreatedBy(user.get("USER_UID").toString());
            tcdsEmpCareerRequest.setEmpNo((Long) user.get("EMP_NO"));
        }
        int inserted = service.insert(tcdsEmpCareerRequest);
        return ResponseEntity.ok(inserted);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity update(@RequestBody @Valid TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException {
        int updated = service.update(tcdsEmpCareerRequest);;
        return ResponseEntity.ok(updated);
    }
}
