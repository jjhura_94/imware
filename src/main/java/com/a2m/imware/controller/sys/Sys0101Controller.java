package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.common.ApvConstantUtils.State;
import com.a2m.imware.model.request.SaveMenuRequest;
import com.a2m.imware.model.request.TsstMenuRequest;
import com.a2m.imware.model.request.TsstRoleRequest;
import com.a2m.imware.service.sys.Sys0101Service;
@RestController
@RequestMapping(value = "/api/sys/sys0101")
@Validated
public class Sys0101Controller {
	@Autowired
	private Sys0101Service service;
	
	@GetMapping("/getMenuByUser")
	public List getMenuByUser(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.getMenuByUser(arg);
	}
	
	@GetMapping("/getAllMenu")
	public List getAllMenu() throws SQLException {
		return service.getAllMenu();
	}
	
	@GetMapping("/search")
	public List search(@RequestParam Map<Object, Object> arg) throws SQLException {
		return service.search(arg);
	}
	
	@PostMapping("/create")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Object addUser(@RequestBody @Valid TsstMenuRequest arg) throws SQLException {
		Map<String,Integer> result = new HashMap<String,Integer>();
		int inserted = service.addMenu(arg);
		result.put("value", inserted);
		return result;
	}
	
	@PutMapping("/update")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Object updateMenu(@RequestBody @Valid TsstMenuRequest arg) throws SQLException {
		Map<String,Integer> result =  new HashMap<String,Integer>();
		int inserted = service.updateMenu(arg);
		result.put("value", inserted);
		return result;
	}
	
	@PutMapping("/update/active")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Map active(@RequestBody @Valid Map arg) throws SQLException {
		arg.put("USE_YN", "Y");
		Map result = new HashMap();
		int updated = service.activeOrBlockMenu(arg);
		result.put("value", updated);
		return result;
	}
	
	@PutMapping("/update/block")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Map block(@RequestBody @Valid Map arg) throws SQLException {
		Map<String,Integer> result = new HashMap<String,Integer>();
		arg.put("USE_YN", "N");
		int updated = service.activeOrBlockMenu(arg);
		result.put("value", updated);
		return result;
	}
	
	@DeleteMapping("/delete/{menuId}")
	@Transactional(rollbackFor = {SQLException.class,Exception.class})
	public Object delete(@PathVariable("menuId") String menuId) throws SQLException {
		Map<String,Integer> result = new HashMap<String,Integer>();
		int inserted = service.deleteMenu(menuId);
		result.put("value", inserted);
		return result;
	}
	
	@GetMapping("/test")
	public Object test() throws SQLException {
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/save")
	@Transactional(rollbackFor = { Exception.class })
	public Object save(@RequestBody @Valid SaveMenuRequest arg) throws Exception {
		List<TsstMenuRequest> grid = arg.getTsstMenu();
		Date currentDate = new Date();
		for (TsstMenuRequest menuRequest : grid) {
			if(StringUtils.isNotEmpty(menuRequest.getMenuId())) {
				if(State.UPDATE.getValue().equals(menuRequest.getState())) {
					menuRequest.setUpdatedBy(arg.getSessUserId());
					menuRequest.setUpdatedDate(currentDate);
					service.updateMenu(menuRequest);
				}
				else if(State.DELETE.getValue().equals(menuRequest.getState())){
					this.service.deleteMenu(menuRequest.getMenuId());
				}
			}
			else {
				if(State.CREATE.getValue().equals(menuRequest.getState())) {
					menuRequest.setCreatedBy(arg.getSessUserId());
					menuRequest.setCreatedDate(currentDate);
					service.addMenu(menuRequest);
				}
				
			}
		}
		if(StringUtils.isNotEmpty(arg.getIdsDel())) {
			String escapedXml = StringEscapeUtils.escapeXml(arg.getIdsDel());
			if(!escapedXml.equals(arg.getIdsDel())) {
				throw new Exception("data invalid");
			}
//			this.service.deleteTsstRoles(com.a2m.imware.common.StringUtils.getForInQuery(arg.getIdsDel(), ","));
		}
		Map<String, String> result = new HashMap<String, String>();
		result.put("RESULT", "Y");
		return result;

	}
}
