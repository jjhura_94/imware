package com.a2m.imware.controller.sys.sys0105;

import com.a2m.imware.model.request.TcdsEmpEquipRequest;
import com.a2m.imware.model.response.TcdsEmpEquipResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys010505Service;
import com.a2m.imware.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/sys/sys010505")
public class Sys010505Controller {

    @Autowired
    private Sys010505Service service;
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/{empNo}")
    public List getAllEquipment(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.getAllEquipmentByEmpNo(empNo);
    }

    @DeleteMapping("")
    public ResponseEntity deleteAllByEquipmentIds(@RequestParam("equipIds") List<Long> equipIds) throws SQLException {
        service.deleteAllByEquipmentIds(equipIds);
        return ResponseEntity.ok(true);
    }

    @GetMapping("/getById")
    public TcdsEmpEquipResponse getById(@RequestParam Long id) throws SQLException {
        return service.getById(id);
    }

    @DeleteMapping("/deleteEquip")
    public ResponseEntity deleteEquip(@RequestParam Long id) throws SQLException {
        List<Long> equipIds = new ArrayList();
        equipIds.add(id);
        service.deleteAllByEquipmentIds(equipIds);
        return ResponseEntity.ok(true);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity create(HttpServletRequest request,@RequestBody @Valid TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException, ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            tcdsEmpEquipRequest.setCreatedBy(user.get("USER_UID").toString());
            tcdsEmpEquipRequest.setEmpNo((Long) user.get("EMP_NO"));
        }
        int inserted = service.insert(tcdsEmpEquipRequest);
        return ResponseEntity.ok(inserted);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity update(@RequestBody @Valid TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException {
        int updated = service.update(tcdsEmpEquipRequest);;
        return ResponseEntity.ok(updated);
    }
}
