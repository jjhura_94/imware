package com.a2m.imware.controller.sys.sys0105;

import com.a2m.imware.model.request.TcdsEmpSwRequest;
import com.a2m.imware.model.response.TcdsEmpSwResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.service.sys.sys0105.Sys010506Service;
import com.a2m.imware.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/sys/sys010506")
public class Sys010506Controller {

    @Autowired
    private Sys010506Service service;
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/{empNo}")
    public List getAllSw(@PathVariable("empNo") Long empNo) throws SQLException {
        return service.getAllSwByEmpNo(empNo);
    }

    @GetMapping("/getById")
    public TcdsEmpSwResponse getById(@RequestParam Long id) throws SQLException {
        return service.getById(id);
    }

    @DeleteMapping("")
    public ResponseEntity deleteAllBySwIds(@RequestParam("swIds") List<Long> swIds) throws SQLException {
        service.deleteAllBySwIds(swIds);
        return ResponseEntity.ok(true);
    }

    @DeleteMapping("/deleteSw")
    public ResponseEntity deleteSw(@RequestParam Long id) throws SQLException {
        List<Long> swIds = new ArrayList();
        swIds.add(id);
        service.deleteAllBySwIds(swIds);
        return ResponseEntity.ok(true);
    }

    @PostMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity create(HttpServletRequest request, @RequestBody @Valid TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException, ImwareException {
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            tcdsEmpSwRequest.setCreatedBy(user.get("USER_UID").toString());
            tcdsEmpSwRequest.setEmpNo((Long) user.get("EMP_NO"));
        }
        int inserted = service.insert(tcdsEmpSwRequest);
        return ResponseEntity.ok(inserted);
    }

    @PutMapping("")
    @Transactional(rollbackFor = {SQLException.class, Exception.class})
    public ResponseEntity update(@RequestBody @Valid TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException {
        int updated = service.update(tcdsEmpSwRequest);;
        return ResponseEntity.ok(updated);
    }
}
