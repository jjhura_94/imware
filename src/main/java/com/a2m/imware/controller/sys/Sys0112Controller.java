package com.a2m.imware.controller.sys;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.common.TransactionalWithRollback;
import com.a2m.imware.common.menu.MenuPermission;
import com.a2m.imware.common.menu.MenuPermissionType;
import com.a2m.imware.model.common.ResponseData;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.sys.TndmFormMgt;
import com.a2m.imware.service.sys.Sys0112Service;

@RestController
@RequestMapping(value = "/api/sys/sys0112")
@Validated
public class Sys0112Controller {
	public static String MENU_URL = "sys/sys0112";

	@Autowired
	private Sys0112Service service0112;

	@MenuPermission(permissions = MenuPermissionType.READ)
	@GetMapping("/getFormList")
	public Object getFormList(@RequestParam Map<Object, Object> arg) throws SQLException {

		PageResponse res = new PageResponse();
		res.setDatas(service0112.searchTndmFormMgt(arg));
		res.setCount(service0112.getCount(arg));
		return res;
	}

	@PostMapping("/deleteForm")
	@TransactionalWithRollback
	@MenuPermission(permissions = MenuPermissionType.DELETE)
	public Object deleteForm(@RequestBody TndmFormMgt arg) throws SQLException {
		this.service0112.deleteTndmFormMgt(arg.getId());
		return ResponseData.success();
	}
	
	@PostMapping("/updateForm")
	@TransactionalWithRollback
	@MenuPermission(permissions = MenuPermissionType.MODIFY)
	public Object updateForm(@RequestBody @Valid TndmFormMgt form) throws SQLException {
		
		if(ImwareStringUtils.YES.equals(form.getUseYn())) {
			int count = service0112.countActiveFormByType(form.getFormType(), form.getId());
			if(count > 0) {
				return ResponseData.error("message.error.onlyOneFormWithSameTypeActive");
			}
		}
		
		form.setUpdatedDate(new Date());
		this.service0112.updateTndmFormMgt(form);
		return ResponseData.success();
	}
	
	@PostMapping("/createForm")
	@TransactionalWithRollback
	@MenuPermission(permissions = MenuPermissionType.WRITE)
	public Object createForm(@RequestBody @Valid TndmFormMgt form) throws SQLException {
		if(ImwareStringUtils.YES.equals(form.getUseYn())) {
			int count = service0112.countActiveFormByType(form.getFormType(), null);
			if(count > 0) {
				return ResponseData.error("message.error.onlyOneFormWithSameTypeActive");
			}
		}
		form.setCreatedDate(new Date());
		this.service0112.insertTndmFormMgt(form);
		return ResponseData.success();
	}
}
