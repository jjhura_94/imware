package com.a2m.imware.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.Com0101DAO;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.TndmNotice;
import com.a2m.imware.model.TndmNoticeComment;
import com.a2m.imware.service.Com0101Service;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.util.AjaxResult;
import com.google.common.base.Strings;
import java.util.Arrays;

@Service
public class Com0101ServiceImpl implements Com0101Service {

    @Autowired
    private Com0101DAO dao;

    @Autowired
    private TccoFileService tccoFileService;

    @Override
    public List<TndmNotice> fetch(Map<Object, Object> filter) throws Exception {
        if (filter.get("USER_IDS") != null && !Strings.isNullOrEmpty(filter.get("USER_IDS").toString())) {
            List<String> userIds = new ArrayList<String>(Arrays.asList(filter.get("USER_IDS").toString().split(", ")));
            filter.put("USER_IDS", userIds);
        }
        List<Map<Object, Object>> result = dao.fetch(filter);
        if (result == null) {
            return new ArrayList<>(0);
        }

        return result.stream().map(obj -> {
            TndmNotice notice = new TndmNotice().fromMap(obj);
            try {
                notice.setAttachment(this.findAttachment(notice.getId()));
            } catch (Exception e) {
                // ignored
            }
            return notice;
        }).collect(Collectors.toList());
    }

    @Override
    public TndmNotice findById(Long id) throws Exception {
        Map<Object, Object> result = dao.findById(id);
        if (result == null) {
            return null;
        }

        TndmNotice notice = new TndmNotice().fromMap(result);
        if (notice != null) {
            try {
                notice.setAttachment(this.findAttachment(notice.getId()));
            } catch (Exception e) {
                // ignored
            }
        }

        return notice;
    }

    @Override
    public boolean insert(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return false;
        }
        int affectedRows = dao.insert(data);
        final boolean isSuccess = affectedRows > 0;
        if (isSuccess) {
            attachmentHandler(data);
        }

        return isSuccess;
    }

    @Override
    public void update(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return;
        }
        if (dao.update(data) > 0) {
            attachmentHandler(data);
        }
    }

    @Override
    public List<TccoFile> findAttachment(Long noticeId) throws Exception {
        if (noticeId == null || noticeId <= 0) {
            return new ArrayList<>(0);
        }
        List<Map<Object, Object>> result = dao.findAttachment(noticeId);
        if (result == null) {
            result = new ArrayList<>(0);
        }

        return result.stream().map(map -> {
            TccoFile file = new TccoFile().fromMap(map);
            return file;
        }).collect(Collectors.toList());
    }

    @Override
    public long count(Map<Object, Object> filter) throws Exception {
        if (filter.get("USER_IDS") != null && !Strings.isNullOrEmpty(filter.get("USER_IDS").toString())) {
            List<String> userIds = new ArrayList<String>(Arrays.asList(filter.get("USER_IDS").toString().split(", ")));
            filter.put("USER_IDS" , userIds);
            
        }
        return dao.count(filter);
    }

    @Override
    public long countNew() throws Exception {
        return dao.countNew();
    }

    @Override
    public boolean insertAttachment(Map<Object, Object> data) throws Exception {
        int affectedRows = dao.insertAttachment(data);
        return affectedRows > 0;
    }

    @SuppressWarnings("unchecked")
    private void attachmentHandler(Map<Object, Object> data) throws Exception {
        List<Map<Object, Object>> filesToAdd = data.get("filesToAdd") == null ? null
                : (List<Map<Object, Object>>) data.get("filesToAdd");
        List<Map<Object, Object>> filesToDelete = data.get("filesToDelete") == null ? null
                : (List<Map<Object, Object>>) data.get("filesToDelete");
        Long id = data.get("ID") == null ? null : (Long) data.get("ID");

        if (filesToAdd != null && id != null && id > 0) {
            filesToAdd.forEach(map -> {
                if (map == null || map.isEmpty() || !map.containsKey("ATCH_FLE_SEQ") || map.get("ATCH_FLE_SEQ") == null) {
                    return;
                }

                map.put("POST_ID", id);
                try {
                    this.insertAttachment(map);
                } catch (Exception e) {
                    // ignored
                }
            });
        }

        if (filesToDelete != null) {
            List<String> fileSequences = new ArrayList<>();
            filesToDelete.forEach(map -> {
                if (map == null || map.isEmpty() || !map.containsKey("ATCH_FLE_SEQ") || map.get("ATCH_FLE_SEQ") == null) {
                    return;
                }

                fileSequences.add(String.valueOf(map.get("ATCH_FLE_SEQ")));
            });

            if (!fileSequences.isEmpty()) {
                tccoFileService.delete(fileSequences);
            }
        }
    }

    @Override
    public List<TndmNoticeComment> findCommentLevel1(Long noticeId) throws Exception {
        if (noticeId == null || noticeId <= 0) {
            return new ArrayList<>(0);
        }
        List<Map<Object, Object>> result = dao.findCommentLevel1(noticeId);
        if (result == null) {
            return new ArrayList<>(0);
        }

        List<TndmNoticeComment> commentList = result.stream().map(map -> {
            TndmNoticeComment comment = new TndmNoticeComment().fromMap(map);
            if (comment != null && comment.getId() != null && comment.getId() > 0) {
                try {
                    comment.setChildren(this.findCommentLevel2(noticeId, comment.getId()));
                } catch (Exception e) {
                    // ignored
                }
            }

            return comment;
        }).collect(Collectors.toList());
        return commentList;
    }

    @Override
    public List<TndmNoticeComment> findCommentLevel2(Long noticeId, Long parentId) throws Exception {
        if (noticeId == null || noticeId <= 0 || parentId == null || parentId <= 0) {
            return new ArrayList<>(0);
        }

        Map<Object, Object> data = new HashMap<>();
        data.put("ID", parentId);
        data.put("NOTICE_ID", noticeId);
        List<Map<Object, Object>> childResult = dao.findCommentLevel2(data);
        if (childResult == null) {
            return new ArrayList<>(0);
        }

        return childResult.stream().map(child -> {
            TndmNoticeComment childComment = new TndmNoticeComment().fromMap(child);
            return childComment;
        }).collect(Collectors.toList());
    }

    @Override
    public boolean insertComment(Map<Object, Object> data) throws Exception {
        int affectedRows = dao.insertComment(data);
        return affectedRows > 0;
    }

    @Override
    public boolean updateComment(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return false;
        }
        return dao.updateComment(data) > 0;
    }

    @Override
    public Object deleteComment(Long id) {
        dao.deleteComment(id);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setMessage("Delete Success");
        ajaxResult.setStatus(true);
        return ajaxResult;
    }

}
