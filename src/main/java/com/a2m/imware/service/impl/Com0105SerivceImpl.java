/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.service.impl;

import com.a2m.imware.dao.Com0105DAO;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.TndmBoard;
import com.a2m.imware.model.TndmBoardComment;
import com.a2m.imware.model.TndmBusinessCard;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.util.Utils;
import com.a2m.imware.wschat.model.extra.ECBCMessage;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.google.common.base.Strings;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class Com0105SerivceImpl {

    @Autowired
    private Com0105DAO com0105DAO;
    @Autowired
    private TccoFileService tccoFileService;
    @Autowired
    private ExtraMessageUtil extraMessageUtil;

    /**
     * Search by condition
     *
     * @param params
     * @return
     * @throws Exception
     */
    public PageResponse search(Map<Object, Object> params) throws Exception {
        PageResponse res = new PageResponse();
        if (params.get("USER_IDS") != null && !Strings.isNullOrEmpty(params.get("USER_IDS").toString())) {
            List<String> userIds = new ArrayList<String>(Arrays.asList(params.get("USER_IDS").toString().split(", ")));
            params.put("USER_IDS", userIds);
        }
        if (!Strings.isNullOrEmpty(params.get("createTimeFrom").toString()) && !Strings.isNullOrEmpty(params.get("createTimeTo").toString())) {
            params.put("TIME_TYPE", 1);
        } else if (!Strings.isNullOrEmpty(params.get("createTimeFrom").toString()) && Strings.isNullOrEmpty(params.get("createTimeTo").toString())) {
            params.put("TIME_TYPE", 2);

        } else if (Strings.isNullOrEmpty(params.get("createTimeFrom").toString()) && !Strings.isNullOrEmpty(params.get("createTimeTo").toString())) {
            params.put("TIME_TYPE", 3);

        }
        if (!Strings.isNullOrEmpty(params.get("createTimeTo").toString())) {
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            Date d = f.parse(params.get("createTimeTo").toString());
            long milliseconds = d.getTime() + 86400000L;
            d = new Date(milliseconds);
            params.put("createTimeTo", f.format(d));
        }
        List<Map<Object, Object>> result = com0105DAO.search(params);
        if (result == null) {
            return res;
        }
        List<TndmBusinessCard> businessCards = result.stream().map(obj -> {
            TndmBusinessCard businessCard = new TndmBusinessCard().fromMap(obj);
            try {
                businessCard.setAttachment(this.findAttachment(businessCard.getId()));
            } catch (Exception e) {
            }
            getStatusName(businessCard);
            return businessCard;
        }).collect(toList());
        res.setDatas(businessCards);
        res.setCount(com0105DAO.count(params));
        return res;
    }

    /**
     * Find By ID
     */
    public Object findById(Long id) throws Exception {
        if (id == null || id <= 0) {
            return null;
        }
        Map<Object, Object> result = com0105DAO.findById(id);
        if (result == null) {
            return null;
        }
        TndmBusinessCard businessCard = new TndmBusinessCard().fromMap(result);
        if (businessCard != null) {
            try {
                businessCard.setAttachment(this.findAttachment(businessCard.getId()));
            } catch (Exception e) {
                // ignored
            }
        }
        return businessCard;
    }

    /**
     * Insert or update Data
     *
     * @param params
     * @return
     * @throws Exception
     */
    public Object save(TndmBusinessCard businessCard) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (businessCard == null) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            return ajaxResult;
        }

        if (!validateBusinessCard(businessCard)) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            return ajaxResult;
        }
        try {
            final boolean needUpdate = businessCard.getId() != null && businessCard.getId() > 0;
            boolean isSuccess = false;
            Map<Object, Object> dataToSave = businessCard.toMap();

            if (needUpdate) {
                dataToSave.put("UPDATED_BY", businessCard.getCreatedBy());
                businessCard.setUpdatedDate(new Date(System.currentTimeMillis()));
                com0105DAO.update(dataToSave);
                isSuccess = true;

                if (businessCard.getStatus().equals("12-03")) {
                    this.sendMessage(businessCard, true);
                }
            } else {
                dataToSave.put("STATUS", "12-01");//New
                com0105DAO.insert(dataToSave);
                isSuccess = true;
                this.sendMessage(businessCard, false);

            }
            if (isSuccess) {
                attachmentHandler(dataToSave);
            }
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            } else {
                TndmBusinessCard card = new TndmBusinessCard().fromMap(dataToSave);
                getStatusName(card);
                card.setCreatedDate(businessCard.getCreatedDate());
                card.setUpdatedDate(businessCard.getUpdatedDate());
                ajaxResult.setResponseData(card);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(AjaxResult.Code.INTERNAL_ERROR);
        }

        return ajaxResult.toMap();
    }

    /**
     * Validate Data
     *
     * @param businessCard
     * @return
     */
    public boolean validateBusinessCard(TndmBusinessCard businessCard) {
        return !(Strings.isNullOrEmpty(businessCard.getNameEn()) || Strings.isNullOrEmpty(businessCard.getNameKr())
                || Strings.isNullOrEmpty(businessCard.getDeptEn()) || Strings.isNullOrEmpty(businessCard.getDeptKr())
                || Strings.isNullOrEmpty(businessCard.getDutyEn()) || Strings.isNullOrEmpty(businessCard.getDutyKr())
                || Strings.isNullOrEmpty(businessCard.getPhone()) || Strings.isNullOrEmpty(businessCard.getEmail()));
    }

    public void getStatusName(TndmBusinessCard card) {
        switch (card.getStatus()) {
            case "12-01":
                card.setStatusName("com.0105.status.request");
                break;

            case "12-02":
                card.setStatusName("com.0105.status.confirm");
                break;

            case "12-03":
                card.setStatusName("com.0105.status.finish");
                break;
            default:
                card.setStatusName("Unknown");
                break;

        }
    }

    public List<TccoFile> findAttachment(Long noticeId) throws Exception {
        if (noticeId == null || noticeId <= 0) {
            return new ArrayList<>(0);
        }
        List<Map<Object, Object>> result = com0105DAO.findAttachment(noticeId);
        if (result == null) {
            result = new ArrayList<>(0);
        }

        return result.stream().map(map -> {
            TccoFile file = new TccoFile().fromMap(map);
            return file;
        }).collect(Collectors.toList());
    }

    public boolean insertAttachment(Map<Object, Object> data) throws Exception {
        int affectedRows = com0105DAO.insertAttachment(data);
        return affectedRows > 0;
    }

    @SuppressWarnings("unchecked")
    private void attachmentHandler(Map<Object, Object> data) throws Exception {
        List<Map<Object, Object>> filesToAdd = data.get("filesToAdd") == null ? null
                : (List<Map<Object, Object>>) data.get("filesToAdd");
        List<Map<Object, Object>> filesToDelete = data.get("filesToDelete") == null ? null
                : (List<Map<Object, Object>>) data.get("filesToDelete");
        Long id = data.get("ID") == null ? null : (Long) data.get("ID");

        if (filesToAdd != null && id != null && id > 0) {
            filesToAdd.forEach(map -> {
                if (map == null || map.isEmpty() || !map.containsKey("ATCH_FLE_SEQ") || map.get("ATCH_FLE_SEQ") == null) {
                    return;
                }

                map.put("POST_ID", id);
                try {
                    this.insertAttachment(map);
                } catch (Exception e) {
                    // ignored
                }
            });
        }

        if (filesToDelete != null) {
            List<String> fileSequences = new ArrayList<>();
            filesToDelete.forEach(map -> {
                if (map == null || map.isEmpty() || !map.containsKey("ATCH_FLE_SEQ") || map.get("ATCH_FLE_SEQ") == null) {
                    return;
                }

                fileSequences.add(String.valueOf(map.get("ATCH_FLE_SEQ")));
            });

            if (!fileSequences.isEmpty()) {
                tccoFileService.delete(fileSequences);
            }
        }
    }

    public List<TndmBoardComment> findCommentLevel1(Long boardId) throws Exception {
        if (boardId == null || boardId <= 0) {
            return new ArrayList<>(0);
        }
        List<Map<Object, Object>> result = com0105DAO.findCommentLevel1(boardId);
        if (result == null) {
            return new ArrayList<>(0);
        }

        List<TndmBoardComment> commentList = result.stream().map(map -> {
            TndmBoardComment comment = new TndmBoardComment().fromMap(map);
            if (comment != null && comment.getId() != null && comment.getId() > 0) {
                try {
                    comment.setChildren(this.findCommentLevel2(boardId, comment.getId()));
                } catch (Exception e) {
                    // ignored
                }
            }

            return comment;
        }).collect(Collectors.toList());
        return commentList;
    }

    public List<TndmBoardComment> findCommentLevel2(Long boardId, Long parentId) throws Exception {
        if (boardId == null || boardId <= 0 || parentId == null || parentId <= 0) {
            return new ArrayList<>(0);
        }

        Map<Object, Object> data = new HashMap<>();
        data.put("ID", parentId);
        data.put("BOARD_ID", boardId);
        List<Map<Object, Object>> childResult = com0105DAO.findCommentLevel2(data);
        if (childResult == null) {
            return new ArrayList<>(0);
        }

        return childResult.stream().map(child -> {
            TndmBoardComment childComment = new TndmBoardComment().fromMap(child);
            return childComment;
        }).collect(Collectors.toList());
    }

    public boolean insertComment(Map<Object, Object> data) throws Exception {
        int affectedRows = com0105DAO.insertComment(data);
        return affectedRows > 0;
    }

    public boolean updateComment(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return false;
        }
        return com0105DAO.updateComment(data) > 0;
    }

    public Object deleteComment(Long id) {
        com0105DAO.deleteComment(id);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setMessage("Delete Success");
        ajaxResult.setStatus(true);
        return ajaxResult;
    }

    public void sendMessage(TndmBusinessCard businessCard, boolean isComplete) throws ImwareException {
        if (isComplete) {

            ECBCMessage message = ECBCMessage.build()
                    .businessCard().completed().permalink("").userUID(businessCard.getCreatedBy()).build();
            extraMessageUtil.businessCard(message);
        } else {
            ECBCMessage message = ECBCMessage.build()
                    .businessCard().requested().userUID(businessCard.getCreatedBy()).build();
            extraMessageUtil.businessCard(message);

        }

    }
}
