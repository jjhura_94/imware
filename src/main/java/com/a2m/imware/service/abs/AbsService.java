/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.service.abs;

import com.a2m.imware.dao.abs.AbsDAO;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class AbsService {

    @Autowired
    private AbsDAO absDAO;

    public Map<Object, Object> findById(String userId) throws SQLException {
        return absDAO.findById(userId);
    }

    public void updateOrCreate(String userId, String rptUserId, Date from, Date to) throws SQLException {
        Map<Object, Object> absent = findById(userId);
        Map<Object, Object> params = new HashMap();
        params.put("userId", userId);
        params.put("rptUserId", rptUserId);
        params.put("fromDate", from);
        params.put("toDate", to);
        if (absent != null && !absent.isEmpty()) {
            absDAO.updateByUserId(params);
        } else {
            absDAO.insert(params);
        }
    }
}
