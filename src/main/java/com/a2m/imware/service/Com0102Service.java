/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.service;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.TndmBoard;
import com.a2m.imware.model.TndmBoardComment;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public interface Com0102Service {

    List<TndmBoard> fetch(Map<Object, Object> filter) throws Exception;

    TndmBoard findById(Long id) throws Exception;

    boolean insert(Map<Object, Object> data) throws Exception;

    void update(Map<Object, Object> data) throws Exception;

    List<TccoFile> findAttachment(Long noticeId) throws Exception;

    long count(Map<Object, Object> filter) throws Exception;

    boolean insertAttachment(Map<Object, Object> data) throws Exception;

    List<TndmBoardComment> findCommentLevel1(Long boardId) throws Exception;

    List<TndmBoardComment> findCommentLevel2(Long boardId, Long parentId) throws Exception;

    boolean insertComment(Map<Object, Object> data) throws Exception;

    boolean updateComment(Map<Object, Object> data) throws Exception;

    Object deleteComment(Long id);
    
    long countNew(Map<Object, Object> filter) throws Exception;

}
