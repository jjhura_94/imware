package com.a2m.imware.service.cms;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.cms.Cms0102DAO;
import com.a2m.imware.dao.cms.Cms0103DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.response.TslgLoginStateResponse;

@Service
public class Cms0103Service {

	@Autowired
	private Cms0103DAO cms0103Dao;

	@Autowired
	private Cms0102DAO cms0102DAO;

	public PageResponse searchTslgLoginState(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		String loginDate = "";
		String logoutDate = "";
		boolean checkHead = false;
		List<TslgLoginStateResponse> rs = new ArrayList<TslgLoginStateResponse>(0);

		Map<Object, Object> data = new HashMap<>();
		String userUidString = "'" + (String) arg.get("USER_UID") + "'";
//		String userUidString = (String) arg.get("USER_UID");
		data.put("USER_UID", (String) arg.get("USER_UID"));

		// Get DEPT_CODE
		List<Map<Object, Object>> deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO
				.getAllDeptCodeByUserUid2(data);

		String deptCodeString = "";
		String deptHeadString = "";

		if (deptCodeListMap.size() > 0) {
			for (int i = 0; i < deptCodeListMap.size(); i++) {
				if (i == 0) {
					deptCodeString = "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE") + "'";
				} else {
					deptCodeString = deptCodeString + "," + "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE")
							+ "'";
				}
			}

			while (!"".equals(deptCodeString)) {
				// GET USER_UID in DEPT_CODE
				data = new HashMap<>();
				data.put("DEPT_CODE", deptCodeString);
				data.put("USER_UID", deptHeadString);
				List<Map<Object, Object>> userUidList = (List<Map<Object, Object>>) cms0102DAO
						.getAllUserUidByDeptCode(data);
				for (int i = 0; i < userUidList.size(); i++) {
					userUidString = userUidString + "," + "'" + (String) userUidList.get(i).get("USER_UID") + "'";
//					userUidString = userUidString + "," + (String) userUidList.get(i).get("USER_UID");
				}

				// GET DEPT_CODE in DEPT_HEAD tìm phòng dưới cấp với list deptcode ở trên
				data = new HashMap<>();
				data.put("UP_DEPT_CODE", deptCodeString);
				List<Map<Object, Object>> deptCodeList = (List<Map<Object, Object>>) cms0102DAO
						.getAllDeptCodeByUpDeptCode(data);
				deptCodeString = "";
				for (int i = 0; i < deptCodeList.size(); i++) {
					if (i == 0) {
						deptCodeString = "'" + (String) deptCodeList.get(i).get("DEPT_CODE") + "'";
					} else {
						deptCodeString = deptCodeString + "," + "'" + (String) deptCodeList.get(i).get("DEPT_CODE")
								+ "'";
					}
				}

				if (!"".equals(deptCodeString)) {
					// GET DEPT_HEAD WITH DEPT_CODE
					data = new HashMap<>();
					data.put("DEPT_CODE", deptCodeString);
					deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO.getAllDeptHeadByDeptCode(data);
					deptHeadString = "";
					for (int i = 0; i < deptCodeListMap.size(); i++) {
						userUidString = userUidString + "," + "'" + (String) deptCodeListMap.get(i).get("USER_UID")
								+ "'";
						if (i == 0) {
							deptHeadString = "'" + (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						} else {
							deptHeadString = deptHeadString + "," + "'"
									+ (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						}
					}
				}
			}
			checkHead = true;
		} else {
			userUidString = "";
			checkHead = false;
		}

//		if (!"".equals(userUidString)) {
		arg.put("USER_UID_LIST", userUidString);
//		}

		if (!arg.isEmpty() && arg.get("LOGIN_DATE_STRING") != null && arg.get("LOGIN_DATE_STRING") != ""
				&& arg.get("LOGOUT_DATE_STRING") != null && arg.get("LOGOUT_DATE_STRING") != "") {

			loginDate = getDateString((String) arg.get("LOGIN_DATE_STRING"), 1);
			logoutDate = getDateString((String) arg.get("LOGOUT_DATE_STRING"), 2);
		}

		arg.put("LOGIN_DATE", loginDate);
		arg.put("LOGOUT_DATE", logoutDate);

		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) cms0103Dao.searchTslgLoginState(arg);
		for (Map<Object, Object> userMap : usersMap) {
			if (userMap != null && !userMap.isEmpty()) {
				TslgLoginStateResponse user = new TslgLoginStateResponse();
				try {
					user.fromMap(userMap);
					rs.add(user);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(cms0103Dao.count(arg));
		res.setCheckHead(checkHead);
		return res;
	}

	public TslgLoginStateResponse getTslgLoginState(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> userMap = (Map<Object, Object>) cms0103Dao.getTslgLoginState(arg);
		TslgLoginStateResponse user = new TslgLoginStateResponse();
		try {
			user.fromMap(userMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public Boolean searchTslgLoginState2(Map<Object, Object> arg) throws SQLException {
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.DATE, -1);
		String loginDate = dateFormat.format(cal.getTime()) + " 00:00:00";
		String logoutDate = dateFormat.format(cal.getTime()) + " 23:59:59";

		arg.put("LOGIN_DATE", loginDate);
		arg.put("LOGOUT_DATE", logoutDate);

		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) cms0103Dao.searchTslgLoginState2(arg);
		for (Map<Object, Object> map : usersMap) {
			if (map.get("LOGOUT_DATE") == null || map.get("LOGOUT_DATE").equals("")) {
				return true;
			}
		}
		return false;
	}

	public int insertLogin(TslgLoginStateResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setLoginDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return cms0103Dao.insertTslgLoginState(data);
	}

	public int insertLogout(TslgLoginStateResponse arg) throws SQLException {
		arg.setLogoutDateString(DateUtil.convertDateToStringDB(arg.getLogoutDate()));
		Map<Object, Object> data = arg.toMap();
		return cms0103Dao.insertTslgLoginState(data);
	}

	public int updateSessInfo(TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> mapGet = new HashMap<>();
		mapGet.put("USER_UID", arg.getUserUid());
		mapGet.put("DATE_DAY", DateUtil.convertDateToStringDB3(arg.getDateDay()));
		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) cms0103Dao.searchTslgLoginState3(mapGet);
		if (usersMap.size() > 1) {
			return 0;
		}
		TslgLoginStateResponse update = new TslgLoginStateResponse();
		update.setLoginNo((Long) usersMap.get(0).get("LOGIN_NO"));
		update.setLogoutDateString(DateUtil.convertDateToStringDB(arg.getLogoutDate()));
		Map<Object, Object> mapUpdate = update.toMap();
		return cms0103Dao.updateSessInfo(mapUpdate);
	}

	public int deleteTslgLoginState(TslgLoginStateResponse arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return cms0103Dao.deleteTslgLoginState(data);
	}

	private String getDateString(String date, int type) {
		String dateString = date;
		if (dateString.length() == 7) {
			int year = Integer.parseInt(dateString.substring(0, 3));
			int month = Integer.parseInt(dateString.substring(5, 7));
			if (type == 1) {
				dateString = dateString + "-01 00:00:00";
			} else {
				switch (month) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					dateString = dateString + "-31 23:59:59";
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					dateString = dateString + "-30 23:59:59";
					break;
				case 2:
					if (year % 4 == 0) {
						dateString = dateString + "-29 23:59:59";
					} else {
						dateString = dateString + "-28 23:59:59";
					}
					break;
				default:
					break;
				}
			}
		} else if (dateString.length() == 10) {
			if (type == 1) {
				dateString = dateString + " 00:00:00";
			} else {
				dateString = dateString + " 23:59:59";
			}
		}
		return dateString;
	}
}
