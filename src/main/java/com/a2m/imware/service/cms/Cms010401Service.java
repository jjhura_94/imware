package com.a2m.imware.service.cms;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.cms.Cms010401DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.response.TcdsRoomBookingResponse;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.ScheduleMessage;
import com.a2m.imware.wschat.module.message.ChatMsgDAO;

@Service
public class Cms010401Service {

	@Autowired
	private Cms010401DAO dao;

	@Autowired
	private ChatMsgDAO chatMsgDAO;

	@Autowired
	private ExtraMessageUtil extraMsgUtil;

	public PageResponse searchTcdsRoomBooking(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		String dateTime = "";
		String startTime = "";
		String endTime = "";
		List<TcdsRoomBookingResponse> rs = new ArrayList<TcdsRoomBookingResponse>(0);

		if (arg.get("DATE_TIME_STRING") != null && arg.get("DATE_TIME_STRING") != "") {
			dateTime = (String) arg.get("DATE_TIME_STRING");

			Map<Object, Object> dateMap = getDateString(dateTime);
			startTime = (String) dateMap.get("START_TIME");
			endTime = (String) dateMap.get("END_TIME");
		}

		arg.put("START_TIME", startTime);
		arg.put("END_TIME", endTime);

		List<Map<Object, Object>> edusMap = (List<Map<Object, Object>>) dao.searchTcdsRoomBooking(arg);
		for (Map<Object, Object> roomMap : edusMap) {
			if (roomMap != null && !roomMap.isEmpty()) {
				TcdsRoomBookingResponse room = new TcdsRoomBookingResponse();
				try {
					room.fromMap(roomMap);
					rs.add(room);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(dao.count(arg));
		return res;
	}

	public TcdsRoomBookingResponse getTcdsRoomBooking(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> roomMap = (Map<Object, Object>) dao.getTcdsRoomBooking(arg);
		TcdsRoomBookingResponse room = new TcdsRoomBookingResponse();
		try {
			room.fromMap(roomMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return room;
	}

	public int insertTcdsRoomBooking(TcdsRoomBookingResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartTimeString(DateUtil.convertDateToStringDB(arg.getStartTime()));
		arg.setEndTimeString(DateUtil.convertDateToStringDB(arg.getEndTime()));
		arg.setCreateDateString(DateUtil.convertDateToStringDB(currentDate));
		arg.setCreatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.insertTcdsRoomBooking(data);
	}

	public int updateTcdsRoomBooking(TcdsRoomBookingResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartTimeString(DateUtil.convertDateToStringDB(arg.getStartTime()));
		arg.setEndTimeString(DateUtil.convertDateToStringDB(arg.getEndTime()));
		arg.setUpdatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.updateTcdsRoomBooking(data);
	}

	public int deleteTcdsRoomBooking(TcdsRoomBookingResponse arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return dao.deleteTcdsRoomBooking(data);
	}

	public void scheduleMessageBookingRoom() throws SQLException {
		Map<Object, Object> arg = new HashMap<Object, Object>();
		Date now = new Date();
		arg.put("START_TIME", DateUtil.convertDateToStringDB(now));
		List<Map<Object, Object>> roomsMap = (List<Map<Object, Object>>) dao.searchTcdsRoomBooking(arg);
		if (roomsMap == null || roomsMap.isEmpty()) {
			return;
		}

		List<Map<Object, Object>> edusMapInTime = new ArrayList<Map<Object, Object>>();
		for (Map<Object, Object> roomMap : roomsMap) {
			if (roomMap != null && !roomMap.isEmpty()) {
				Date dateRbk = (Date) roomMap.get("START_TIME");
				long diffInMillies = Math.abs(dateRbk.getTime() - now.getTime());
				if (diffInMillies <= 1800000) {
					edusMapInTime.add(roomMap);
				}
			}
		}

		for (Map<Object, Object> roomMap : roomsMap) {
			if (roomMap != null && !roomMap.isEmpty()) {
				arg = new HashMap<Object, Object>();
				arg.put("USER_ID", roomMap.get("USER_ID"));
				arg.put("MEETING_ROOM_NAME", roomMap.get("ROOM_NAME"));
				List<Map<Object, Object>> mgssMap = (List<Map<Object, Object>>) chatMsgDAO.getListMsgRoomBooking(arg);
				Date dateRbk = (Date) roomMap.get("START_TIME");
				Boolean check = false;
				long diffInMilliesNow = Math.abs(dateRbk.getTime() - now.getTime());
				if (mgssMap != null && !mgssMap.isEmpty()) {
					for (Map<Object, Object> mgs : mgssMap) {
						Date dateMgs = (Date) mgs.get("SEND_DATE");
						long diffInMillies = Math.abs(dateRbk.getTime() - dateMgs.getTime());
						if (diffInMillies <= 1800000) {
							check = true;
						}
					}
				}
				if (!check && diffInMilliesNow <= 1800000) {
					extraMsgUtil.schedule(
							ScheduleMessage.builder()
							.meetingRoom()
							.roomName((String) roomMap.get("ROOM_NAME"))
							.userUID((String) roomMap.get("CREATED_BY"))
							.build());

				}
			}
		}

	}

	private Map<Object, Object> getDateString(String date) {
		Map<Object, Object> data = new HashMap<>();
		String dateString = date;
		String startTime = "";
		String endTime = "";
		if (dateString.length() == 7) {
			int year = Integer.parseInt(dateString.substring(0, 3));
			int month = Integer.parseInt(dateString.substring(5, 7));
			startTime = dateString + "-01 00:00:00";
			switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				endTime = dateString + "-31 23:59:59";
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				endTime = dateString + "-30 23:59:59";
				break;
			case 2:
				if (year % 4 == 0) {
					endTime = dateString + "-29 23:59:59";
				} else {
					endTime = dateString + "-28 23:59:59";
				}
				break;
			default:
				break;
			}

		} else if (dateString.length() == 10) {
			startTime = dateString + " 00:00:00";
			endTime = dateString + " 23:59:59";
		}
		data.put("START_TIME", startTime);
		data.put("END_TIME", endTime);
		return data;
	}
}
