package com.a2m.imware.service.cms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.cms.Cms0102DAO;
import com.a2m.imware.dao.cms.Cms0103DAO;
import com.a2m.imware.model.response.TslgLoginStateResponse;

@Service
public class Cms0103ExcelService {

	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_DAY = 1;
	public static final int COLUMN_INDEX_DEPARTMENT = 2;
	public static final int COLUMN_INDEX_NAME = 3;
	public static final int COLUMN_INDEX_START_TIME = 4;
	public static final int COLUMN_INDEX_END_TIME = 5;
	public static final int COLUMN_INDEX_NOTE = 6;

	@Autowired
	private Cms0103DAO cms0103Dao;

	@Autowired
	private Cms0102DAO cms0102DAO;

	public List<TslgLoginStateResponse> searchTslgLoginState(Map<Object, Object> arg) throws SQLException {
		String loginDate = "";
		String logoutDate = "";
		List<TslgLoginStateResponse> rs = new ArrayList<TslgLoginStateResponse>(0);

		Map<Object, Object> data = new HashMap<>();
		String userUidString = "'" + (String) arg.get("USER_UID") + "'";
//		String userUidString = (String) arg.get("USER_UID");
		data.put("USER_UID", (String) arg.get("USER_UID"));

		// Get DEPT_CODE
		List<Map<Object, Object>> deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO
				.getAllDeptCodeByUserUid2(data);

		String deptCodeString = "";
		String deptHeadString = "";

		if (deptCodeListMap.size() > 0) {
			for (int i = 0; i < deptCodeListMap.size(); i++) {
				if (i == 0) {
					deptCodeString = "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE") + "'";
				} else {
					deptCodeString = deptCodeString + "," + "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE")
							+ "'";
				}
			}

			while (!"".equals(deptCodeString)) {
				// GET USER_UID in DEPT_CODE
				data = new HashMap<>();
				data.put("DEPT_CODE", deptCodeString);
				data.put("USER_UID", deptHeadString);
				List<Map<Object, Object>> userUidList = (List<Map<Object, Object>>) cms0102DAO
						.getAllUserUidByDeptCode(data);
				for (int i = 0; i < userUidList.size(); i++) {
					userUidString = userUidString + "," + "'" + (String) userUidList.get(i).get("USER_UID") + "'";
//					userUidString = userUidString + "," + (String) userUidList.get(i).get("USER_UID");
				}

				// GET DEPT_CODE in DEPT_HEAD tìm phòng dưới cấp với list deptcode ở trên
				data = new HashMap<>();
				data.put("UP_DEPT_CODE", deptCodeString);
				List<Map<Object, Object>> deptCodeList = (List<Map<Object, Object>>) cms0102DAO
						.getAllDeptCodeByUpDeptCode(data);
				deptCodeString = "";
				for (int i = 0; i < deptCodeList.size(); i++) {
					if (i == 0) {
						deptCodeString = "'" + (String) deptCodeList.get(i).get("DEPT_CODE") + "'";
					} else {
						deptCodeString = deptCodeString + "," + "'" + (String) deptCodeList.get(i).get("DEPT_CODE")
								+ "'";
					}
				}

				if (!"".equals(deptCodeString)) {
					// GET DEPT_HEAD WITH DEPT_CODE
					data = new HashMap<>();
					data.put("DEPT_CODE", deptCodeString);
					deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO.getAllDeptHeadByDeptCode(data);
					deptHeadString = "";
					for (int i = 0; i < deptCodeListMap.size(); i++) {
						userUidString = userUidString + "," + "'" + (String) deptCodeListMap.get(i).get("USER_UID")
								+ "'";
						if (i == 0) {
							deptHeadString = "'" + (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						} else {
							deptHeadString = deptHeadString + "," + "'"
									+ (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						}
					}
				}
			}
		} else {
			userUidString = "";
		}

		arg.put("USER_UID_LIST", userUidString);

		if (!arg.isEmpty() && arg.get("LOGIN_DATE_STRING") != null && arg.get("LOGIN_DATE_STRING") != ""
				&& arg.get("LOGOUT_DATE_STRING") != null && arg.get("LOGOUT_DATE_STRING") != "") {

			loginDate = getDateString((String) arg.get("LOGIN_DATE_STRING"), 1);
			logoutDate = getDateString((String) arg.get("LOGOUT_DATE_STRING"), 2);
		}

		arg.put("LOGIN_DATE", loginDate);
		arg.put("LOGOUT_DATE", logoutDate);

		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) cms0103Dao.searchTslgLoginState(arg);
		for (Map<Object, Object> userMap : usersMap) {
			if (userMap != null && !userMap.isEmpty()) {
				TslgLoginStateResponse user = new TslgLoginStateResponse();
				try {
					user.fromMap(userMap);
					rs.add(user);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return rs;
	}

	@SuppressWarnings("resource")
	public ByteArrayOutputStream writeExcel(List<TslgLoginStateResponse> tslgLoginStateResponse)
			throws IOException, InvalidFormatException {
		Workbook workbook = new XSSFWorkbook();

		// Create sheet
		Sheet sheet = workbook.createSheet("Equipment"); // Create sheet with sheet name

		int rowIndex = 0;

		// Write header
		writeHeader(sheet, rowIndex);

		for (int i = 0; i < tslgLoginStateResponse.size(); i++) {
			rowIndex++;
			Row row = sheet.createRow(rowIndex);
			writeBook(tslgLoginStateResponse.get(i), row);
		}

		int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
		autosizeColumn(sheet, numberOfColumn);

		// Create file excel
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		workbook.write(os);
		return os;
	}

	private static void autosizeColumn(Sheet sheet, int lastColumn) {
		for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
			sheet.autoSizeColumn(columnIndex);
			sheet.setColumnWidth(columnIndex, sheet.getColumnWidth(columnIndex) * 20 / 10);
		}
	}

	private static void writeBook(TslgLoginStateResponse tslgLoginStateResponse, Row row) {
		Cell cell = row.createCell(COLUMN_INDEX_ID);
		cell.setCellValue(row.getRowNum());

		cell = row.createCell(COLUMN_INDEX_DAY);
		if (tslgLoginStateResponse.getDateDay() == null) {
			cell.setCellValue("");
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
			String strDate = formatter.format(tslgLoginStateResponse.getDateDay());
			cell.setCellValue(strDate);
		}

		cell = row.createCell(COLUMN_INDEX_DEPARTMENT);
		cell.setCellValue(tslgLoginStateResponse.getDeptName());

		cell = row.createCell(COLUMN_INDEX_NAME);
		cell.setCellValue(tslgLoginStateResponse.getNameKr());

		cell = row.createCell(COLUMN_INDEX_START_TIME);
		if (tslgLoginStateResponse.getLoginDate() == null) {
			cell.setCellValue("");
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			String strDate = formatter.format(tslgLoginStateResponse.getLoginDate());
			cell.setCellValue(strDate);
		}

		cell = row.createCell(COLUMN_INDEX_END_TIME);
		if (tslgLoginStateResponse.getLogoutDate() == null) {
			cell.setCellValue("");
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			String strDate = formatter.format(tslgLoginStateResponse.getLogoutDate());
			cell.setCellValue(strDate);
		}

		cell = row.createCell(COLUMN_INDEX_NOTE);
		cell.setCellValue("");
	}

	private static void writeHeader(Sheet sheet, int rowIndex) {
		// create CellStyle
		CellStyle cellStyle = createStyleForHeader(sheet);

		// Create row
		Row row = sheet.createRow(rowIndex);
		// Create cells
		Cell cell = row.createCell(COLUMN_INDEX_ID);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("번호");

		cell = row.createCell(COLUMN_INDEX_DAY);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("일");

		cell = row.createCell(COLUMN_INDEX_DEPARTMENT);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("소속부서");

		cell = row.createCell(COLUMN_INDEX_NAME);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("성명");

		cell = row.createCell(COLUMN_INDEX_START_TIME);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("출근시간");

		cell = row.createCell(COLUMN_INDEX_END_TIME);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("퇴근시간");

		cell = row.createCell(COLUMN_INDEX_NOTE);
		cell.setCellStyle(cellStyle);
		cell.setCellValue("비고");
	}

	private static CellStyle createStyleForHeader(Sheet sheet) {
		// Create font
		Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(true);
		font.setFontHeightInPoints((short) 16); // font size
		font.setColor(IndexedColors.BLACK.getIndex()); // text color

		// Create CellStyle
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		cellStyle.setFont(font);
		cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);

		return cellStyle;
	}

	private String getDateString(String date, int type) {
		String dateString = date;
		if (dateString.length() == 7) {
			int year = Integer.parseInt(dateString.substring(0, 3));
			int month = Integer.parseInt(dateString.substring(5, 7));
			if (type == 1) {
				dateString = dateString + "-01 00:00:00";
			} else {
				switch (month) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					dateString = dateString + "-31 23:59:59";
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					dateString = dateString + "-30 23:59:59";
					break;
				case 2:
					if (year % 4 == 0) {
						dateString = dateString + "-29 23:59:59";
					} else {
						dateString = dateString + "-28 23:59:59";
					}
					break;
				default:
					break;
				}
			}
		} else if (dateString.length() == 10) {
			if (type == 1) {
				dateString = dateString + " 00:00:00";
			} else {
				dateString = dateString + " 23:59:59";
			}
		}
		return dateString;
	}
}
