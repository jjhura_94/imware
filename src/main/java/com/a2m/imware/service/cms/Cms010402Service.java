package com.a2m.imware.service.cms;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.cms.Cms010402DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.response.TcdsEquipBookingResponse;

@Service
public class Cms010402Service {

	@Autowired
	private Cms010402DAO dao;

	public PageResponse searchTcdsEquipBooking(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		String startTime = "";
		String endTime = "";
		List<TcdsEquipBookingResponse> rs = new ArrayList<TcdsEquipBookingResponse>(0);

		if (!arg.isEmpty() && arg.get("START_TIME_STRING") != null && arg.get("START_TIME_STRING") != ""
				&& arg.get("END_TIME_STRING") != null && arg.get("END_TIME_STRING") != "") {

			startTime = getDateString((String) arg.get("START_TIME_STRING"), 1);
			endTime = getDateString((String) arg.get("END_TIME_STRING"), 2);
		}

		arg.put("START_TIME", startTime);
		arg.put("END_TIME", endTime);

		List<Map<Object, Object>> edusMap = (List<Map<Object, Object>>) dao.searchTcdsEquipBooking(arg);
		for (Map<Object, Object> EquipMap : edusMap) {
			if (EquipMap != null && !EquipMap.isEmpty()) {
				TcdsEquipBookingResponse Equip = new TcdsEquipBookingResponse();
				try {
					Equip.fromMap(EquipMap);
					rs.add(Equip);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(dao.count(arg));
		return res;
	}

	public TcdsEquipBookingResponse getTcdsEquipBooking(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> EquipMap = (Map<Object, Object>) dao.getTcdsEquipBooking(arg);
		TcdsEquipBookingResponse Equip = new TcdsEquipBookingResponse();
		try {
			Equip.fromMap(EquipMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Equip;
	}

	public int insertTcdsEquipBooking(TcdsEquipBookingResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartTimeString(DateUtil.convertDateToStringDB(arg.getStartTime()));
		arg.setEndTimeString(DateUtil.convertDateToStringDB(arg.getEndTime()));
		arg.setCreateDateString(DateUtil.convertDateToStringDB(currentDate));
		arg.setCreatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.insertTcdsEquipBooking(data);
	}

	public int updateTcdsEquipBooking(TcdsEquipBookingResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartTimeString(DateUtil.convertDateToStringDB(arg.getStartTime()));
		arg.setEndTimeString(DateUtil.convertDateToStringDB(arg.getEndTime()));
		arg.setUpdatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.updateTcdsEquipBooking(data);
	}

	public int deleteTcdsEquipBooking(TcdsEquipBookingResponse arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return dao.deleteTcdsEquipBooking(data);
	}

	private String getDateString(String date, int type) {
		String dateString = date;
		if (dateString.length() == 7) {
			int year = Integer.parseInt(dateString.substring(0, 3));
			int month = Integer.parseInt(dateString.substring(5, 7));
			if (type == 1) {
				dateString = dateString + "-01 00:00:00";
			} else {
				switch (month) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					dateString = dateString + "-31 23:59:59";
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					dateString = dateString + "-30 23:59:59";
					break;
				case 2:
					if (year % 4 == 0) {
						dateString = dateString + "-29 23:59:59";
					} else {
						dateString = dateString + "-28 23:59:59";
					}
					break;
				default:
					break;
				}
			}
		} else if (dateString.length() == 10) {
			if (type == 1) {
				dateString = dateString + " 00:00:00";
			} else {
				dateString = dateString + " 23:59:59";
			}
		}
		return dateString;
	}
}
