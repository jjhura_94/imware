/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.service.cms;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.cms.Cms0105DAO;
import com.a2m.imware.model.TcdsEvent;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.common.UserInfoService;
import com.a2m.imware.util.AjaxResult;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.ScheduleMessage;
import com.a2m.imware.wschat.model.extra.TaskRequestMessage;
import com.a2m.imware.wschat.module.message.ChatMsgDAO;
import com.google.common.base.Strings;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service

public class Cms0105Service {

    @Autowired
    private Cms0105DAO dao;

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private ExtraMessageUtil extraMessageUtil;

    public PageResponse search(Map<Object, Object> params) throws SQLException, ImwareException {
        PageResponse res = new PageResponse();
        if ((params.get("dateSearchFrom") != null && params.get("dateSearchTo") != null) && !Strings.isNullOrEmpty(params.get("dateSearchFrom").toString()) && !Strings.isNullOrEmpty(params.get("dateSearchTo").toString())) {
            params.put("TIME_TYPE", 1);
        } else if ((params.get("dateSearchFrom") != null && params.get("dateSearchTo") != null) && !Strings.isNullOrEmpty(params.get("dateSearchFrom").toString()) && Strings.isNullOrEmpty(params.get("dateSearchTo").toString())) {
            params.put("TIME_TYPE", 2);
        } else if ((params.get("dateSearchFrom") != null && params.get("dateSearchTo") != null) && Strings.isNullOrEmpty(params.get("dateSearchFrom").toString()) && !Strings.isNullOrEmpty(params.get("dateSearchTo").toString())) {
            params.put("TIME_TYPE", 3);
        }

        if (params.get("startMonth") != null && params.get("endMonth") != null) {
            long start = Long.parseLong(params.get("startMonth").toString());
            long end = Long.parseLong(params.get("endMonth").toString());
            if (start > 0) {
                params.put("monthStart", new SimpleDateFormat("yyyy/MM/dd").format(new Date(start)));
            }
            if (end > 0) {
                params.put("monthEnd", new SimpleDateFormat("yyyy/MM/dd").format(new Date(end)));
            }

        }
        List<Map<Object, Object>> result = dao.search(params);
        if (result == null) {
            return res;
        }
        List<TcdsEvent> tcdsEvents = result.stream().map(obj -> {
            TcdsEvent tcdsEvent = new TcdsEvent().fromMap(obj);
            getEventName(tcdsEvent);
            return tcdsEvent;
        }).collect(toList());

//        if (tcdsEvents != null && !tcdsEvents.isEmpty()) {
//            List<TcdsEvent> eventsRemove = tcdsEvents.stream().filter(event -> !params.get("currentId").toString().equals(event.getUserId()) && !"18-03".equals(event.getShareType())).collect(toList());
//            if (eventsRemove != null && !eventsRemove.isEmpty()) {
//                tcdsEvents.removeAll(eventsRemove);
//            }
//        }
        if (params.get("userIds") != null) {
            List<String> empNo = userInfoService.getEmpNo((ArrayList) params.get("userIds"));
            if (empNo != null && !empNo.isEmpty()) {
                params.put("empNo", empNo);
            }

        }

        List<Map<Object, Object>> vacationEvent = dao.getDateVacation(params);
        if (vacationEvent != null && !vacationEvent.isEmpty()) {
            List<TcdsEvent> events = vacationEvent.stream().map(obj -> {
                TcdsEvent tcdsEvent = new TcdsEvent().fromMap(obj);
                tcdsEvent.setEventType("20-05");
                getEventName(tcdsEvent);
                return tcdsEvent;
            }).collect(toList());
            tcdsEvents.addAll(events);
        }

        res.setDatas(tcdsEvents);
        res.setCount(dao.count(params));
        return res;
    }

    public void setColor(TcdsEvent tcdsEvent) {

    }

    /**
     * Find By ID
     */
    public Object findById(Long id) throws Exception {
        if (id == null || id <= 0) {
            return null;
        }
        Map<Object, Object> result = dao.findById(id);
        if (result == null) {
            return null;
        }
        TcdsEvent tcdsEvent = new TcdsEvent().fromMap(result);
        tcdsEvent.setFromDateString(DateUtil.convertDateToStringDB(tcdsEvent.getFromDate()));
        tcdsEvent.setToDateString(DateUtil.convertDateToStringDB(tcdsEvent.getToDate()));
        return tcdsEvent;
    }

    public Object save(TcdsEvent tcdsEvent) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (tcdsEvent == null) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("You send nothing to save!");
            ajaxResult.setResponseData(AjaxResult.Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        if (!validateTcdsEvent(tcdsEvent)) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Missing some arguments!");
            ajaxResult.setResponseData(AjaxResult.Code.MISSING_ARGUMENTS);
            return ajaxResult;
        }
        if (tcdsEvent.getFromDate() != null && tcdsEvent.getToDate() != null) {
            if (tcdsEvent.getFromDate().getTime() > tcdsEvent.getToDate().getTime()) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("Time to must more time from!");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
                return ajaxResult;
            }
        }
        if (tcdsEvent.getAlarmLong() > tcdsEvent.getToLong()) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Time to must more time alarm!");
            ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            return ajaxResult;
        }

        try {
            final boolean needUpdate = tcdsEvent.getId() != null && tcdsEvent.getId() > 0;
            boolean isSuccess = false;
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date dateFrom = new Date(tcdsEvent.getFromLong());
            String dateTextfrom = dateFormatGmt.format(dateFrom);
            Date dateTo = new Date(tcdsEvent.getToLong());
            String dateTextTo = dateFormatGmt.format(dateTo);
            tcdsEvent.setFromDateString(dateTextfrom);
            tcdsEvent.setToDateString(dateTextTo);
            Date alarmDate = new Date(tcdsEvent.getAlarmLong());
            String dateTextAlarm = dateFormatGmt.format(alarmDate);
            tcdsEvent.setAlarmDateString(dateTextAlarm);
            Map<Object, Object> dataToSave = tcdsEvent.toMap();
            if (needUpdate) {
                dao.update(dataToSave);
                isSuccess = true;
            } else {
                dao.insert(dataToSave);
                isSuccess = true;
            }
            ajaxResult.setStatus(isSuccess);
            if (!isSuccess) {
                ajaxResult.setMessage("Cannot save data!\nPlease try again later.");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            } else {
                TcdsEvent event = new TcdsEvent().fromMap(dataToSave);
                ajaxResult.setResponseData(event);
            }

        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("Internal server error: " + e.getLocalizedMessage());
            ajaxResult.setResponseData(AjaxResult.Code.INTERNAL_ERROR);
        }
        return ajaxResult.toMap();
    }

    public boolean validateTcdsEvent(TcdsEvent tcdsEvent) {
        return !(Strings.isNullOrEmpty(tcdsEvent.getEventType())
                || Strings.isNullOrEmpty(tcdsEvent.getContent())
                || tcdsEvent.getFromLong() <= 0
                || tcdsEvent.getToLong() <= 0 );
    }

    public void getEventName(TcdsEvent tcdsEvent) {
        if (Strings.isNullOrEmpty(tcdsEvent.getEventType())) {
            tcdsEvent.setEventName("Unknown");
            return;
        }
        switch (tcdsEvent.getEventType()) {
            case "20-01":
                tcdsEvent.setEventName("cms.0105.add.type.1");
                break;
            case "20-02":
                tcdsEvent.setEventName("cms.0105.add.type.2");
                break;
            case "20-03":
                tcdsEvent.setEventName("cms.0105.add.type.3");
                break;
            case "20-04":
                tcdsEvent.setEventName("cms.0105.add.type.4");
                break;
            case "20-05":
                tcdsEvent.setEventName("cms.0105.add.type.5");
                break;
        }
    }

    public void getListSendMessage() throws SQLException {
        try {
            List<Map<Object, Object>> result = dao.getListSendMessage();
            if (result != null && !result.isEmpty()) {
                List<TcdsEvent> tcdsEvents = result.stream().map(obj -> {
                    TcdsEvent tcdsEvent = new TcdsEvent().fromMap(obj);
                    return tcdsEvent;
                }).collect(toList());
                for (TcdsEvent tcdsEvent : tcdsEvents) {
                    switch (tcdsEvent.getEventType()) {
                        case "20-02":
                            extraMessageUtil.schedule(ScheduleMessage.builder().businessTrip()
                                    .tripName(tcdsEvent.getContent()).userUID(tcdsEvent.getUserId()).build());
                            break;
                        case "20-03":
                            extraMessageUtil.schedule(ScheduleMessage.builder().meetingRoom()
                                    .roomName(tcdsEvent.getContent()).userUID(tcdsEvent.getUserId()).build());
                            break;
                        case "20-04":
                            extraMessageUtil.schedule(ScheduleMessage.builder().event()
                                    .eventName(tcdsEvent.getContent()).userUID(tcdsEvent.getUserId()).build());
                            break;
                    }
                }
            }
        } catch (Exception e) {
        }

    }
}
