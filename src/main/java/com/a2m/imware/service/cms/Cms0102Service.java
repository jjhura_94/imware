package com.a2m.imware.service.cms;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.ApvConstantUtils;
import com.a2m.imware.common.ApvConstantUtils.DocStatus;
import com.a2m.imware.common.ApvConstantUtils.DrftStatus;
import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.apv.Apv0101DAO;
import com.a2m.imware.dao.cms.Cms0102DAO;
import com.a2m.imware.dao.sys.Sys0105DAO;
import com.a2m.imware.model.apv.TndmApvlHist;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.request.TndmDocMstRequest;
import com.a2m.imware.model.request.TndmDrftRequest;
import com.a2m.imware.model.request.TsstUserRequest;
import com.a2m.imware.model.response.TcdsEmpVacationApprovalResponse;
import com.a2m.imware.model.response.TcdsEmpVacationApvlResponse;
import com.a2m.imware.model.response.TcdsEmpVacationInfoResponse;
import com.a2m.imware.model.response.TcdsEmpVacationRequestResponse;
import com.a2m.imware.service.common.ComSeqService;
import com.a2m.imware.wschat.model.extra.ExtraMessageUtil;
import com.a2m.imware.wschat.model.extra.VacationConfirmationMessage;

@Service
public class Cms0102Service {

	private static final String HALF_DAY_VACATION = "14-00";

	private static final String ANNUAL_VACATION = "14-01";

	private static final String NORMAL_VACATION = "14-05";

//	private static final String GENERAL_SICK_LEAVE_VACATION = "14-02-01";
//
//	private static final String WORK_SICK_LEAVE_VACATION = "14-02-02";
//
//	private static final String ASSIGNMENT_VACATION = "14-03";
//
//	private static final String CONGRA_AND_CONDOL_VACATION = "14-04-01";
//
//	private static final String MENSTRUAL_VACATION = "14-04-02";
//
//	private static final String METERNITY_VACATION = "14-04-03";
//
//	private static final String REWARD_VACATION = "14-04-04";
//
//	private static final String SENIORITY_VACATION = "14-04-05";
//
//	private static final String DISASTER_VACATION = "14-04-06";

	private static final String NOT_YET_APPROVED_APVL_STATUS = "15-01";

	private static final String APPROVED_APVL_STATUS = "15-02";

	private static final String NOT_APPROVED_APVL_STATUS = "15-03";

	private static final String NEW_REQUEST_STATUS = "16-01";

	private static final String PROCESSING_REQUEST_STATUS = "16-02";

	private static final String APPROVED_REQUEST_STATUS = "16-03";

	private static final String REJECT_REQUEST_STATUS = "16-04";

	private static final Integer HOURS_OF_DAY = 8;

	@Autowired
	private Cms0102DAO cms0102DAO;

	@Autowired
	private Sys0105DAO sys0105DAO;

	@Autowired
	private Apv0101DAO apv0101DAO;

	@Autowired
	private ExtraMessageUtil extraMsgUtil;

	@Autowired
	private ComSeqService comSeqService;

	/**
	 * Get list request for user
	 * 
	 * @param arg
	 * @return
	 * @throws SQLException
	 */
	public PageResponse searchTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		List<TcdsEmpVacationRequestResponse> rs = new ArrayList<TcdsEmpVacationRequestResponse>(0);
//		String empNoList = (String) arg.get("EMP_NO");

		Map<Object, Object> data = new HashMap<>();
		String userUidString = (String) arg.get("USER_UID");
		data.put("USER_UID", userUidString);

//		// Get DEPT_CODE
//		List<Map<Object, Object>> deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO
//				.getAllDeptCodeByUserUid2(data);
//
//		String deptCodeString = "";
//		String deptHeadString = "";
//		if (deptCodeListMap.size() > 0) {
//			for (int i = 0; i < deptCodeListMap.size(); i++) {
//				if (i == 0) {
//					deptCodeString = "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE") + "'";
//				} else {
//					deptCodeString = deptCodeString + "," + "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE")
//							+ "'";
//				}
//			}
//
//			while (deptCodeString != "") {
//				// GET USER_UID in DEPT_CODE
//				data = new HashMap<>();
//				data.put("DEPT_CODE", deptCodeString);
//				data.put("USER_UID", deptHeadString);
//				List<Map<Object, Object>> userUidList = (List<Map<Object, Object>>) cms0102DAO
//						.getAllUserUidByDeptCode(data);
//				userUidString = "";
//				for (int i = 0; i < userUidList.size(); i++) {
//					empNoList = empNoList + "," + (Long) userUidList.get(i).get("EMP_NO");
//					if (i == 0) {
//						userUidString = "'" + (String) userUidList.get(i).get("USER_UID") + "'";
//					} else {
//						userUidString = userUidString + "," + "'" + (String) userUidList.get(i).get("USER_UID") + "'";
//					}
//				}
//
//				// GET DEPT_CODE in DEPT_HEAD
//				data = new HashMap<>();
//				data.put("UP_DEPT_CODE", deptCodeString);
//				List<Map<Object, Object>> deptCodeList = (List<Map<Object, Object>>) cms0102DAO
//						.getAllDeptCodeByUpDeptCode(data);
//				deptCodeString = "";
//				for (int i = 0; i < deptCodeList.size(); i++) {
//					if (i == 0) {
//						deptCodeString = "'" + (String) deptCodeList.get(i).get("DEPT_CODE") + "'";
//					} else {
//						deptCodeString = deptCodeString + "," + "'" + (String) deptCodeList.get(i).get("DEPT_CODE")
//								+ "'";
//					}
//				}
//
//				if (deptCodeString != "") {
//					// GET DEPT_HEAD WITH DEPT_CODE
//					data = new HashMap<>();
//					data.put("DEPT_CODE", deptCodeString);
//					deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO.getAllDeptHeadByDeptCode(data);
//					deptHeadString = "";
//					for (int i = 0; i < deptCodeListMap.size(); i++) {
//						empNoList = empNoList + "," + (Long) deptCodeListMap.get(i).get("EMP_NO");
//						if (i == 0) {
//							deptHeadString = "'" + (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
//						} else {
//							deptHeadString = deptHeadString + "," + "'"
//									+ (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
//						}
//					}
//				}
//			}
//		}
//		if (empNoList != "") {
////			userUidString = userUidString + "," + (String) arg.get("EMP_NO");
//			arg.put("EMP_NO_LIST", empNoList);
//		}

		if (!arg.isEmpty() && arg.get("BASE_YEAR") != null
				&& (arg.get("BASE_YEAR").equals("") || arg.get("BASE_YEAR").equals("0"))) {
			arg.put("BASE_YEAR", "");
		} else if (arg.get("BASE_YEAR") != null) {
			arg.put("BASE_YEAR", Integer.parseInt((String) arg.get("BASE_YEAR")));
		}
		arg.put("EMP_NO", Long.parseLong((String) arg.get("EMP_NO")));
		List<Map<Object, Object>> requestsMap = (List<Map<Object, Object>>) cms0102DAO
				.searchTcdsEmpVacationRequest(arg);
		for (Map<Object, Object> requestMap : requestsMap) {
			if (requestMap != null && !requestMap.isEmpty()) {
				TcdsEmpVacationRequestResponse request = new TcdsEmpVacationRequestResponse();
				try {
					request.fromMap(requestMap);
					request.setFromDateString(DateUtil.formatter.format(request.getFromDate()));
					request.setToDateString(DateUtil.formatter.format(request.getToDate()));
					rs.add(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(cms0102DAO.countVacationRequest(arg));
		return res;
	}

	public PageResponse searchTcdsEmpVacationRequest2(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		List<TcdsEmpVacationRequestResponse> rs = new ArrayList<TcdsEmpVacationRequestResponse>(0);
		String empNoList = (String) arg.get("EMP_NO");

		Map<Object, Object> data = new HashMap<>();
		String userUidString = (String) arg.get("USER_UID");
		data.put("USER_UID", userUidString);

		// Get DEPT_CODE
		List<Map<Object, Object>> deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO
				.getAllDeptCodeByUserUid2(data);

		String deptCodeString = "";
		String deptHeadString = "";
		if (deptCodeListMap.size() > 0) {
			for (int i = 0; i < deptCodeListMap.size(); i++) {
				if (i == 0) {
					deptCodeString = "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE") + "'";
				} else {
					deptCodeString = deptCodeString + "," + "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE")
							+ "'";
				}
			}

			while (deptCodeString != "") {
				// GET USER_UID in DEPT_CODE
				data = new HashMap<>();
				data.put("DEPT_CODE", deptCodeString);
				data.put("USER_UID", deptHeadString);
				List<Map<Object, Object>> userUidList = (List<Map<Object, Object>>) cms0102DAO
						.getAllUserUidByDeptCode(data);
				userUidString = "";
				for (int i = 0; i < userUidList.size(); i++) {
					empNoList = empNoList + "," + (Long) userUidList.get(i).get("EMP_NO");
					if (i == 0) {
						userUidString = "'" + (String) userUidList.get(i).get("USER_UID") + "'";
					} else {
						userUidString = userUidString + "," + "'" + (String) userUidList.get(i).get("USER_UID") + "'";
					}
				}

				// GET DEPT_CODE in DEPT_HEAD
				data = new HashMap<>();
				data.put("UP_DEPT_CODE", deptCodeString);
				List<Map<Object, Object>> deptCodeList = (List<Map<Object, Object>>) cms0102DAO
						.getAllDeptCodeByUpDeptCode(data);
				deptCodeString = "";
				for (int i = 0; i < deptCodeList.size(); i++) {
					if (i == 0) {
						deptCodeString = "'" + (String) deptCodeList.get(i).get("DEPT_CODE") + "'";
					} else {
						deptCodeString = deptCodeString + "," + "'" + (String) deptCodeList.get(i).get("DEPT_CODE")
								+ "'";
					}
				}

				if (deptCodeString != "") {
					// GET DEPT_HEAD WITH DEPT_CODE
					data = new HashMap<>();
					data.put("DEPT_CODE", deptCodeString);
					deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO.getAllDeptHeadByDeptCode(data);
					deptHeadString = "";
					for (int i = 0; i < deptCodeListMap.size(); i++) {
						empNoList = empNoList + "," + (Long) deptCodeListMap.get(i).get("EMP_NO");
						if (i == 0) {
							deptHeadString = "'" + (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						} else {
							deptHeadString = deptHeadString + "," + "'"
									+ (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						}
					}
				}
			}
		}
		if (empNoList != "") {
			arg.put("EMP_NO_LIST", empNoList);
		}

		List<Map<Object, Object>> requestsMap = (List<Map<Object, Object>>) cms0102DAO
				.searchTcdsEmpVacationRequest2(arg);
		for (Map<Object, Object> requestMap : requestsMap) {
			if (requestMap != null && !requestMap.isEmpty()) {
				TcdsEmpVacationRequestResponse request = new TcdsEmpVacationRequestResponse();
				try {
					request.fromMap(requestMap);
					rs.add(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		return res;
	}

	/**
	 * Get list request for user usage history
	 * 
	 * @param arg
	 * @return
	 * @throws SQLException
	 */
	public PageResponse searchVRForUsageHistory(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		List<TcdsEmpVacationApprovalResponse> rs = new ArrayList<TcdsEmpVacationApprovalResponse>(0);

		if (!arg.isEmpty() && arg.get("FROM_DATE_STRING") != null && arg.get("FROM_DATE_STRING") != "") {
			arg.put("FROM_DATE", arg.get("FROM_DATE_STRING") + " 08:00:00");
		}

		if (!arg.isEmpty() && arg.get("TO_DATE_STRING") != null && arg.get("TO_DATE_STRING") != "") {
			arg.put("TO_DATE", arg.get("TO_DATE_STRING") + " 18:00:00");
		}

		List<Map<Object, Object>> requestsMap = (List<Map<Object, Object>>) cms0102DAO.searchVRForUsageHistory(arg);
		for (Map<Object, Object> requestMap : requestsMap) {
			if (requestMap != null && !requestMap.isEmpty()) {
				TcdsEmpVacationApprovalResponse request = new TcdsEmpVacationApprovalResponse();
				try {
					request.fromMap(requestMap);
					if (!request.getNoteApvl().equals("1")) {
						Map<Object, Object> data1 = new HashMap<>();
						data1.put("VACATION_RQST_ID", request.getIdRqst());
						List<Map<Object, Object>> listMap = (List<Map<Object, Object>>) cms0102DAO
								.searchVAForApproval(data1);
						for (int i = 0; i < listMap.size(); i++) {
							if (i != 0) {
								if (request.getNoteApvl().equals(listMap.get(i).get("NOTE"))) {
									if (request.getStatusApvl().equals(NOT_YET_APPROVED_APVL_STATUS)) {
										if (listMap.get(i - 1).get("STATUS").equals(APPROVED_APVL_STATUS)) {
											request.setCheckApprovalAndReject(true);
										} else {
											request.setCheckApprovalAndReject(false);
										}
									} else {
										request.setCheckApprovalAndReject(false);
									}
								}
							} else {
								if (request.getNoteApvl().equals(listMap.get(i).get("NOTE"))) {
									if (request.getStatusApvl().equals(NOT_YET_APPROVED_APVL_STATUS)) {
										request.setCheckApprovalAndReject(true);
									} else {
										request.setCheckApprovalAndReject(false);
									}
								}
							}
						}
					} else if (request.getStatusApvl().equals(NOT_YET_APPROVED_APVL_STATUS)) {
						request.setCheckApprovalAndReject(true);
					} else {
						request.setCheckApprovalAndReject(false);
					}

					request.setFromDateRqstString(DateUtil.formatter.format(request.getFromDateRqst()));
					request.setToDateRqstString(DateUtil.formatter.format(request.getToDateRqst()));
					rs.add(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(cms0102DAO.countVRForUsageHistory(arg));
		return res;
	}

	public List<Map<Object, Object>> searchTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException {
		return cms0102DAO.searchTcdsEmpVacationInfo(arg);
	}

	public List<Map<Object, Object>> searchListDept(Map<Object, Object> arg) throws SQLException {
		List<Map<Object, Object>> result = new ArrayList<>();
		Map<Object, Object> data = new HashMap<>();
		String userUidString = "'" + (String) arg.get("USER_UID") + "'";
		data.put("USER_UID", (String) arg.get("USER_UID"));

		// Get DEPT_CODE
		List<Map<Object, Object>> deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO
				.getAllDeptCodeByUserUid2(data);

		String deptCodeString = "";
		String deptHeadString = "";

		if (deptCodeListMap.size() > 0) {
			for (int i = 0; i < deptCodeListMap.size(); i++) {
				result.add(deptCodeListMap.get(i));
				if (i == 0) {
					deptCodeString = "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE") + "'";
				} else {
					deptCodeString = deptCodeString + "," + "'" + (String) deptCodeListMap.get(i).get("DEPT_CODE")
							+ "'";
				}
			}

			while (!"".equals(deptCodeString)) {
				// GET USER_UID in DEPT_CODE
				data = new HashMap<>();
				data.put("DEPT_CODE", deptCodeString);
				data.put("USER_UID", deptHeadString);
				List<Map<Object, Object>> userUidList = (List<Map<Object, Object>>) cms0102DAO
						.getAllUserUidByDeptCode(data);
				for (int i = 0; i < userUidList.size(); i++) {
					userUidString = userUidString + "," + "'" + (String) userUidList.get(i).get("USER_UID") + "'";
				}

				// GET DEPT_CODE in DEPT_HEAD tìm phòng dưới cấp với list deptcode ở trên
				data = new HashMap<>();
				data.put("UP_DEPT_CODE", deptCodeString);
				List<Map<Object, Object>> deptCodeList = (List<Map<Object, Object>>) cms0102DAO
						.getAllDeptCodeByUpDeptCode(data);
				deptCodeString = "";
				for (int i = 0; i < deptCodeList.size(); i++) {
					result.add(deptCodeList.get(i));
					if (i == 0) {
						deptCodeString = "'" + (String) deptCodeList.get(i).get("DEPT_CODE") + "'";
					} else {
						deptCodeString = deptCodeString + "," + "'" + (String) deptCodeList.get(i).get("DEPT_CODE")
								+ "'";
					}
				}

				if (!"".equals(deptCodeString)) {
					// GET DEPT_HEAD WITH DEPT_CODE
					data = new HashMap<>();
					data.put("DEPT_CODE", deptCodeString);
					deptCodeListMap = (List<Map<Object, Object>>) cms0102DAO.getAllDeptHeadByDeptCode(data);
					deptHeadString = "";
					for (int i = 0; i < deptCodeListMap.size(); i++) {
						userUidString = userUidString + "," + "'" + (String) deptCodeListMap.get(i).get("USER_UID")
								+ "'";
						if (i == 0) {
							deptHeadString = "'" + (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						} else {
							deptHeadString = deptHeadString + "," + "'"
									+ (String) deptCodeListMap.get(i).get("DEPT_HEAD") + "'";
						}
					}
				}
			}
		}

		return result;
	}

	public String insertVacation(TcdsEmpVacationRequestResponse arg) throws SQLException {
		String returnVal = "N";
		Date currentDate = new Date();
		arg.setStatus(NEW_REQUEST_STATUS);
//		arg.setFromDateString(DateUtil.convertDateToStringDB(arg.getFromDate()));
//		arg.setToDateString(DateUtil.convertDateToStringDB(arg.getToDate()));
		arg.setCreatedDateString(DateUtil.convertDateToStringDB(currentDate));

		if (arg.getVacationType().equals("14-02")) {
			arg.setVacationType(arg.getVacationType2());
		}
		Map<Object, Object> data = arg.toMap();

		int result = cms0102DAO.insertTcdsEmpVacationRequest(data);

		if (result < 1) {
			returnVal = "N";
			return returnVal;
		}

		Map<Object, Object> vacationRequest = cms0102DAO.getTheLastVR(data);

		// init doc master save to db
		TndmDocMstRequest docMst = new TndmDocMstRequest();
		docMst.setDocNo(comSeqService.getSeq("SEQ_DOC_ID"));
		docMst.setCreatedBy(arg.getSessUserId());
		docMst.setDocDate(currentDate);
		docMst.setCreatedDate(currentDate);
		docMst.setDocDeptCode(arg.getDeptCode());
		docMst.setDocDeptName(arg.getDeptName());
		docMst.setDocUserUid(arg.getSessUserId());
		docMst.setDocStatus(DocStatus.SUBMITTED.getValue());
		docMst.setSubmittedDate(currentDate);
		docMst.setVacationId((Long) vacationRequest.get("ID"));
		apv0101DAO.insertTndmDocMst(docMst);

		TndmDrftRequest drft = new TndmDrftRequest();
		drft.setDocNo(docMst.getDocNo());
		drft.setDrftDocNo(comSeqService.getSeq("SEQ_DRFT_ID"));
		drft.setCreatedDate(currentDate);
		drft.setCreatedBy(arg.getSessUserId());
		drft.setDrftTitle("휴가-" + arg.getNameKr());
		drft.setDrftContent("휴가-" + arg.getNameKr() + "-" + arg.getDeptName() + "-"
				+ vacationRequest.get("VACATION_TYPE_STRING") + "-" + arg.getReason());
		drft.setDrftType("06-06");
		drft.setRecentYn(true);
		drft.setDrftStatus(DrftStatus.SUBMITTED.getValue());
		apv0101DAO.insertTndmDrft(drft);

		TndmApvlHist apvlHist = new TndmApvlHist();
		apvlHist.setDrftDocNo(drft.getDrftDocNo());
		apvlHist.setSancUserUid(arg.getSessUserId());
		apvlHist.setSancYn(ApvConstantUtils.Y);
		apvlHist.setSancDate(currentDate);
		apvlHist.setMethod(DocStatus.SUBMITTED.getValue());
		apv0101DAO.insertTndmApvlHist(apvlHist);

		TndmDrftApvlLine item = new TndmDrftApvlLine();
		item.setDocNo(drft.getDocNo());
		item.setDrftDocNo(drft.getDrftDocNo());
		item.setSancDate(currentDate);
		item.setSancOrdNo(1);
		item.setSancYn("Y");
		item.setSancType(ApvConstantUtils.SUBMIT);
		item.setSancDutyCode((String) vacationRequest.get("DUTY_CODE"));
		item.setSancDutyName((String) vacationRequest.get("DUTY_NAME"));
		item.setSancDeptCode(arg.getDeptCode());
		item.setSancDeptName(arg.getDeptName());
		item.setSancUserUid(arg.getSessUserId());
		apv0101DAO.insertTndmDrftApvlLine(item);

		Map<Object, Object> getMap = new HashMap<>();
		getMap.put("USER_UID", arg.getSessUserId());

		if (arg.getVacationType().equals(NORMAL_VACATION) || arg.getVacationType().equals(ANNUAL_VACATION)
				|| arg.getVacationType().equals(HALF_DAY_VACATION)) {
			if (arg.getTimeLeave() != null) {
				if (arg.getTimeLeave() <= HOURS_OF_DAY) {
					String deptCode = cms0102DAO.getDeptCodeByUserUid(getMap);
					getMap = new HashMap<>();
					getMap.put("DEPT_CODE", deptCode);

					String deptHead = cms0102DAO.getDeptHead(getMap);
					if (deptHead.equals(arg.getSessUserId())) {
						getMap = new HashMap<>();
						getMap.put("USER_UID", deptHead);
						List<Map<Object, Object>> deapheadsMap = (List<Map<Object, Object>>) cms0102DAO
								.getListDeptHead(getMap);
						String deptHeapTemp = "";
						if (deapheadsMap != null && deapheadsMap.size() > 1) {
							for (int i = 0; i < deapheadsMap.size(); i++) {
								if (deptHead.equals((String) deapheadsMap.get(i).get("DEPT_HEAD"))) {
									int j = i + 1;
									if (j < deapheadsMap.size()) {
										deptHeapTemp = (String) deapheadsMap.get(j).get("DEPT_HEAD");
									}
								}
							}
						} else {
							deptHead = "";
						}
						deptHead = deptHeapTemp;

						if (!"".equals(deptHead)) {
							Map<Object, Object> getMap1 = new HashMap<>();
							getMap1.put("USER_UID", deptHead);

							Map<Object, Object> map1 = cms0102DAO.getAllInfoByUserUid(getMap1);

							item = new TndmDrftApvlLine();
							item.setDocNo(drft.getDocNo());
							item.setDrftDocNo(drft.getDrftDocNo());
							item.setSancDate(currentDate);
							item.setSancOrdNo(2);
							item.setSancYn("N");
							item.setSancType(ApvConstantUtils.APPROVE);
							item.setSancDutyCode((String) map1.get("DUTY_CODE"));
							item.setSancDutyName((String) map1.get("DUTY_NAME"));
							item.setSancDeptCode((String) map1.get("DEPT_CODE"));
							item.setSancDeptName((String) map1.get("DEPT_NM"));
							item.setSancUserUid(deptHead);
							apv0101DAO.insertTndmDrftApvlLine(item);

							TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
							newApvl.setVacationRqstId((Long) vacationRequest.get("ID"));
							newApvl.setUserUid(deptHead);
							newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
							newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
							newApvl.setNote("1");

							Map<Object, Object> dataInsertApvl = newApvl.toMap();
							cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
						}
					} else {
						Map<Object, Object> getMap1 = new HashMap<>();
						getMap1.put("USER_UID", deptHead);

						Map<Object, Object> map1 = cms0102DAO.getAllInfoByUserUid(getMap1);
						item = new TndmDrftApvlLine();
						item.setDocNo(drft.getDocNo());
						item.setDrftDocNo(drft.getDrftDocNo());
						item.setSancDate(currentDate);
						item.setSancOrdNo(2);
						item.setSancYn("N");
						item.setSancType(ApvConstantUtils.APPROVE);
						item.setSancDutyCode((String) map1.get("DUTY_CODE"));
						item.setSancDutyName((String) map1.get("DUTY_NAME"));
						item.setSancDeptCode((String) map1.get("DEPT_CODE"));
						item.setSancDeptName((String) map1.get("DEPT_NM"));
						item.setSancUserUid(deptHead);
						apv0101DAO.insertTndmDrftApvlLine(item);

						TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
						newApvl.setVacationRqstId((Long) vacationRequest.get("ID"));
						newApvl.setUserUid(deptHead);
						newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
						newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
						newApvl.setNote("1");

						Map<Object, Object> dataInsertApvl = newApvl.toMap();
						cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
					}

					returnVal = "Y";
				} else {
					insertDeptHeadList(getMap, (Long) vacationRequest.get("ID"), currentDate, drft);
					returnVal = "Y";
				}
			}
		} else {
			if (arg.getTimeLeave() != null) {
				if (arg.getTimeLeave() <= HOURS_OF_DAY) {
					String deptCode = cms0102DAO.getDeptCodeByUserUid(getMap);
					getMap = new HashMap<>();
					getMap.put("DEPT_CODE", deptCode);

					String deptHead = cms0102DAO.getDeptHead(getMap);
					if (deptHead.equals(arg.getSessUserId())) {
						getMap = new HashMap<>();
						getMap.put("USER_UID", deptHead);
						List<Map<Object, Object>> deapheadsMap = (List<Map<Object, Object>>) cms0102DAO
								.getListDeptHead(getMap);
						String deptHeapTemp = "";
						if (deapheadsMap != null && deapheadsMap.size() > 1) {
							for (int i = 0; i < deapheadsMap.size(); i++) {
								if (deptHead.equals((String) deapheadsMap.get(i).get("DEPT_HEAD"))) {
									int j = i + 1;
									if (j < deapheadsMap.size()) {
										deptHeapTemp = (String) deapheadsMap.get(j).get("DEPT_HEAD");
									}
								}
							}
						} else {
							deptHead = "";
						}
						deptHead = deptHeapTemp;

						if (!"".equals(deptHead)) {
							Map<Object, Object> getMap1 = new HashMap<>();
							getMap1.put("USER_UID", deptHead);

							Map<Object, Object> map1 = cms0102DAO.getAllInfoByUserUid(getMap1);

							item = new TndmDrftApvlLine();
							item.setDocNo(drft.getDocNo());
							item.setDrftDocNo(drft.getDrftDocNo());
							item.setSancDate(currentDate);
							item.setSancOrdNo(2);
							item.setSancYn("N");
							item.setSancType(ApvConstantUtils.APPROVE);
							item.setSancDutyCode((String) map1.get("DUTY_CODE"));
							item.setSancDutyName((String) map1.get("DUTY_NAME"));
							item.setSancDeptCode((String) map1.get("DEPT_CODE"));
							item.setSancDeptName((String) map1.get("DEPT_NM"));
							item.setSancUserUid(deptHead);
							apv0101DAO.insertTndmDrftApvlLine(item);

							TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
							newApvl.setVacationRqstId((Long) vacationRequest.get("ID"));
							newApvl.setUserUid(deptHead);
							newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
							newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
							newApvl.setNote("1");

							Map<Object, Object> dataInsertApvl = newApvl.toMap();
							cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
						}

					} else {
						Map<Object, Object> getMap1 = new HashMap<>();
						getMap1.put("USER_UID", deptHead);

						Map<Object, Object> map1 = cms0102DAO.getAllInfoByUserUid(getMap1);
						item = new TndmDrftApvlLine();
						item.setDocNo(drft.getDocNo());
						item.setDrftDocNo(drft.getDrftDocNo());
						item.setSancDate(currentDate);
						item.setSancOrdNo(2);
						item.setSancYn("N");
						item.setSancType(ApvConstantUtils.APPROVE);
						item.setSancDutyCode((String) map1.get("DUTY_CODE"));
						item.setSancDutyName((String) map1.get("DUTY_NAME"));
						item.setSancDeptCode((String) map1.get("DEPT_CODE"));
						item.setSancDeptName((String) map1.get("DEPT_NM"));
						item.setSancUserUid(deptHead);
						apv0101DAO.insertTndmDrftApvlLine(item);

						TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
						newApvl.setVacationRqstId((Long) vacationRequest.get("ID"));
						newApvl.setUserUid(deptHead);
						newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
						newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
						newApvl.setNote("1");

						Map<Object, Object> dataInsertApvl = newApvl.toMap();
						cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
					}

					returnVal = "Y";
				} else {
					insertDeptHeadList(getMap, (Long) vacationRequest.get("ID"), currentDate, drft);
					returnVal = "Y";
				}
			}
		}

		return returnVal;
	}

	public String updateVacation(TcdsEmpVacationApprovalResponse approvalRequest) throws SQLException {
		String returnVal = "N";
		Date currentDate = new Date();

		Map<Object, Object> data = new HashMap<>();
		data.put("EMP_NO", approvalRequest.getEmpNoRqst());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		int yearNow = calendar.get(Calendar.YEAR);
		data.put("BASE_YEAR", (Integer) yearNow);

		Map<Object, Object> infoMap = cms0102DAO.getTcdsEmpVacationInfo(data); // *2
		TcdsEmpVacationInfoResponse info = new TcdsEmpVacationInfoResponse();
		info.fromMap(infoMap);

		TcdsEmpVacationRequestResponse request = new TcdsEmpVacationRequestResponse();

		String status = approvalRequest.getStatusApvl();

		// search list apvl with update status of vacation request
		data = new HashMap<>();
		data.put("ID", approvalRequest.getIdApvl());
		data.put("VACATION_RQST_ID", approvalRequest.getIdRqst());
		data.put("USER_UID", approvalRequest.getUserUidApvl());
		List<Map<Object, Object>> vacationApvlsMap = (List<Map<Object, Object>>) cms0102DAO.searchVAForApproval2(data);
		// APPROVED
		if (status.equals(APPROVED_APVL_STATUS)) {
			Map<Object, Object> updateApvlMap = new HashMap<>();
			TcdsEmpVacationApvlResponse updateApvl = new TcdsEmpVacationApvlResponse();
			updateApvl.setId(approvalRequest.getIdApvl());
			updateApvl.setStatus(status);
			updateApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
			updateApvlMap = updateApvl.toMap();
			cms0102DAO.updateTcdsEmpVacationApvl(updateApvlMap);

			data = new HashMap<>();
			if (vacationApvlsMap.size() >= 1) {
				// Update status V_Request
				request.setId(approvalRequest.getIdRqst());
				request.setStatus(PROCESSING_REQUEST_STATUS);
				request.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
				data = request.toMap();
				cms0102DAO.updateTcdsEmpVacationRequest(data);
			} else {
				info.setUsedDays(info.getUsedDays() + approvalRequest.getTimeLeaveRqst());
				infoMap = new HashMap<>();
				infoMap = info.toMap();
				cms0102DAO.updateTcdsEmpVacationInfo(infoMap);
				// update notice
				extraMsgUtil.vacationConfirmation(VacationConfirmationMessage.builder().accepted(true)
						.userUID(approvalRequest.getUserUidRqst()).build());

				// Update status V_Request
				request.setId(approvalRequest.getIdRqst());
				request.setStatus(APPROVED_REQUEST_STATUS);
				request.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
				data = request.toMap();
				cms0102DAO.updateTcdsEmpVacationRequest(data);
			}
			returnVal = "Y";
		}
		// NOT_APPROVED
		else if (status.equals(NOT_APPROVED_APVL_STATUS)) {
			Map<Object, Object> updateApvlMap = new HashMap<>();
			TcdsEmpVacationApvlResponse updateApvl = new TcdsEmpVacationApvlResponse();
			updateApvl.setId(approvalRequest.getIdApvl());
			updateApvl.setStatus(status);
			updateApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
			updateApvlMap = updateApvl.toMap();
			cms0102DAO.updateTcdsEmpVacationApvl(updateApvlMap);

			if (vacationApvlsMap.size() >= 1) {
				for (Map<Object, Object> apvlMap : vacationApvlsMap) {
					if (apvlMap != null && !apvlMap.isEmpty()) {
						TcdsEmpVacationApvlResponse apvl = new TcdsEmpVacationApvlResponse();
						try {
							apvl.fromMap(apvlMap);
							if (apvl.getStatus().equals(NOT_YET_APPROVED_APVL_STATUS)) {
								apvl.setStatus(NOT_APPROVED_APVL_STATUS);
								apvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));

								data = new HashMap<>();
								data = apvl.toMap();
								cms0102DAO.updateTcdsEmpVacationApvl(data);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			}
			// Update status V_Request
			request.setId(approvalRequest.getIdRqst());
			request.setStatus(REJECT_REQUEST_STATUS);
			request.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
			data = request.toMap();
			cms0102DAO.updateTcdsEmpVacationRequest(data);

			// update notice
			extraMsgUtil.vacationConfirmation(VacationConfirmationMessage.builder().accepted(false)
					.userUID(approvalRequest.getUserUidRqst()).build());

			returnVal = "Y";
		} else if (status.equals(NOT_YET_APPROVED_APVL_STATUS)) {
			Map<Object, Object> updateApvlMap = new HashMap<>();
			TcdsEmpVacationApvlResponse updateApvl = new TcdsEmpVacationApvlResponse();
			updateApvl.setId(approvalRequest.getIdApvl());
			updateApvl.setStatus(status);
			updateApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
			updateApvlMap = updateApvl.toMap();
			cms0102DAO.updateTcdsEmpVacationApvl(updateApvlMap);
			returnVal = "Y";
		}

		return returnVal;
	}

	public int updateTcdsEmpVacationRequest(TcdsEmpVacationRequestResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setFromDateString(DateUtil.convertDateToStringDB(arg.getFromDate()));
		arg.setToDateString(DateUtil.convertDateToStringDB(arg.getFromDate()));
		arg.setCreatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return cms0102DAO.updateTcdsEmpVacationRequest(data);
	}

	public int countDeptHead(Map<Object, Object> arg) throws SQLException {
		return cms0102DAO.countDeptHead(arg);
	}

	public int deleteTcdsEmpVacation(TcdsEmpVacationRequestResponse arg) throws SQLException {
		int valRe = 0;
		Map<Object, Object> data = new HashMap<>();
		data.put("VACATION_RQST_ID", arg.getId());
		data.put("USER_ID", arg.getUserUid());
		Map<Object, Object> draftRequest = apv0101DAO.getTndmDrftCMS0102(data);
		if (draftRequest != null) {

			data = new HashMap<>();
			data.put("drftDocNo", draftRequest.get("drftDocNo"));
			data.put("draftDocNo", draftRequest.get("drftDocNo"));
			apv0101DAO.deleteTndmDrftApvlLine((String) draftRequest.get("drftDocNo"));
			apv0101DAO.deleteTndmApvlHist(data);
			apv0101DAO.deleteTndmDrft((String) draftRequest.get("drftDocNo"));

			data.put("VACATION_RQST_ID", arg.getId());
			cms0102DAO.deleteTcdsEmpVacationApvl(data);

			data = new HashMap<>();
			data = arg.toMap();
			cms0102DAO.deleteTcdsEmpVacationRequest(data);

			valRe = apv0101DAO.deleteTndmDocMst((String) draftRequest.get("docNo"));
		}
		return valRe;
	}

	public int deleteTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException {
		return cms0102DAO.deleteTcdsEmpVacationRequest(arg);
	}

	public int deleteTcdsEmpVacationApvl(Map<Object, Object> arg) throws SQLException {
		return cms0102DAO.deleteTcdsEmpVacationApvl(arg);
	}

	public TcdsEmpVacationInfoResponse getTcdsEmpVacationInfo(Map<Object, Object> arg) throws SQLException {
		if (arg.get("BASE_YEAR") == null || Long.parseLong((String) arg.get("BASE_YEAR")) == 0) {
			Date toDay = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(toDay);
			int yearNow = calendar.get(Calendar.YEAR);
			arg.put("BASE_YEAR", yearNow);
		} else {
			arg.put("BASE_YEAR", Integer.parseInt((String) arg.get("BASE_YEAR")));
		}
		arg.put("EMP_NO", Long.parseLong((String) arg.get("EMP_NO")));
		Map<Object, Object> vIMap = (Map<Object, Object>) cms0102DAO.getTcdsEmpVacationInfo(arg);
		TcdsEmpVacationInfoResponse vI = new TcdsEmpVacationInfoResponse();
		try {
			vI.fromMap(vIMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vI;
	}

	public void insertScheduleVacationDays() throws Exception {
		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) sys0105DAO.searchForVacation(new HashMap<>());
		if (usersMap.size() < 1) {
			return;
		}
		Date toDay = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(toDay);
		int yearNow = calendar.get(Calendar.YEAR);
		// Insert all vacation with 2020 date now (26/08/2020)
		insertOrUpdateScheduleInfoList(usersMap, toDay, yearNow);

		// Insert all vacation with 2021 date now (01/01/2021)
		String dateNext = "01/01" + "/" + (yearNow + 1);
		Date dateWithNextYear = new SimpleDateFormat("dd/MM/yyyy").parse(dateNext);
		calendar.setTime(dateWithNextYear);
		int yearNext = calendar.get(Calendar.YEAR);
		insertOrUpdateScheduleInfoList(usersMap, dateWithNextYear, yearNext);
	}

	/**
	 * Update Schedule vacation
	 * 
	 * @throws SQLException
	 */
	public void updateScheduleVacationDays() throws Exception {
		Map<Object, Object> map = new HashMap<>();
		map.put("UPDATE_STATUS", "Y");
		List<Map<Object, Object>> usersMap = (List<Map<Object, Object>>) sys0105DAO.searchForVacation(map);
		if (usersMap.size() < 1) {
			return;
		}

		Date toDay = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(toDay);
		int yearNow = calendar.get(Calendar.YEAR);
		insertOrUpdateScheduleInfoList(usersMap, toDay, yearNow);
	}

	public void insertVacationInfoWithNewEmp(TsstUserRequest tsstUserRequest) throws Exception {
		List<Map<Object, Object>> usersMap = new ArrayList<>();
		Map<Object, Object> user = sys0105DAO.getForVacation(tsstUserRequest.getEmpNo());
		if (user != null && !user.isEmpty()) {
			usersMap.add(user);
		}

		if (usersMap.size() < 1) {
			return;
		}

		Date toDay = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(toDay);
		int yearNow = calendar.get(Calendar.YEAR);
		// Insert all vacation with 2020 date now (26/08/2020)
		insertOrUpdateScheduleInfoList(usersMap, toDay, yearNow);

		// Insert all vacation with 2021 date now (01/01/2021)
		String dateNext = "01/01" + "/" + (yearNow + 1);
		Date dateWithNextYear = new SimpleDateFormat("dd/MM/yyyy").parse(dateNext);
		calendar.setTime(dateWithNextYear);
		int yearNext = calendar.get(Calendar.YEAR);
		insertOrUpdateScheduleInfoList(usersMap, dateWithNextYear, yearNext);
	}

	private void insertOrUpdateScheduleInfoList(List<Map<Object, Object>> usersMap, Date dateEnd, int year)
			throws SQLException {
		ZoneId defaultZoneId = ZoneId.systemDefault();
		Map<Object, Object> datatest = new HashMap<Object, Object>();
		datatest.put("BASE_YEAR", (Integer) year);
		for (Map<Object, Object> user : usersMap) {
			datatest.put("EMP_NO", (Long) user.get("EMP_NO"));
			Map<Object, Object> infoGet = cms0102DAO.getTcdsEmpVacationInfo(datatest);

			// calculate
			Date startDate = (Date) user.get("START_DATE");
			Instant instantStart = startDate.toInstant();
			LocalDate localDateStart = instantStart.atZone(defaultZoneId).toLocalDate();

			Instant instantEnd = dateEnd.toInstant();
			LocalDate localDateEnd = instantEnd.atZone(defaultZoneId).toLocalDate().plusDays(1);

			Period diffBetTwoDate = Period.between(localDateStart, localDateEnd);
			int yearBTD = diffBetTwoDate.getYears();
			int monthBTD = diffBetTwoDate.getMonths();
			TcdsEmpVacationInfoResponse info = new TcdsEmpVacationInfoResponse();
			info.setEmpNo((Long) user.get("EMP_NO"));
			info.setBaseYear(year);
			info.setYearOfEntry(yearBTD);
			info.setUsedDays(0);// the first
			// add vacation info annual days
			int annualDays = 0;
			if (yearBTD < 1) { // Less than a year
				if (monthBTD > 4) {
					annualDays = monthBTD; // Employees who work less than 1 year can get 1 day of leave per month.
				} else {
					annualDays = 4; // config
				}
			} else { // Over a year
				switch (yearBTD) {
				case 1:
				case 2:
					annualDays = 15;
					break;
				case 3:
				case 4:
					annualDays = 16;
					break;
				case 5:
				case 6:
					annualDays = 17;
					break;
				case 7:
				case 8:
					annualDays = 18;
					break;
				case 9:
				case 10:
					annualDays = 19;
					break;
				case 11:
				case 12:
					annualDays = 20;
					break;
				case 13:
				case 14:
					annualDays = 21;
					break;
				default:
					break;
				}
				if (yearBTD >= 15) {
					annualDays = 25;
				}
			}
			annualDays = annualDays * 8;
			info.setAnnualDays(annualDays);

			// add vacation info awarded days
			int awardedDays = 0;
			if (yearBTD >= 5 && yearBTD <= 15) { // 5 - 15 years
				awardedDays = 5;
			} else if (yearBTD >= 15) { // Over 15 years
				awardedDays = 10;
			}

			awardedDays = awardedDays * 8;
			info.setAwardedDays(awardedDays);
			if (infoGet == null || infoGet.isEmpty()) {
				Map<Object, Object> mapInsert = info.toMap();
				cms0102DAO.insertTcdsEmpVacationInfo(mapInsert);
			} else {
				info.setId((Long) infoGet.get("ID"));
				Map<Object, Object> mapUpdate = info.toMap();
				cms0102DAO.updateTcdsEmpVacationInfo(mapUpdate);
			}
		}
	}

	private void insertDeptHeadList(Map<Object, Object> getMap, Long id, Date currentDate, TndmDrftRequest drft)
			throws SQLException {
		String userUid = (String) getMap.get("USER_UID");
		int temp = 2;

		List<Map<Object, Object>> deapheadsMap = (List<Map<Object, Object>>) cms0102DAO.getListDeptHead(getMap);
		if (deapheadsMap.size() == 0) {
			return;
		}

		if (deapheadsMap.size() == 1) {
			for (int i = 0; i < deapheadsMap.size(); i++) {
				if (deapheadsMap.get(i).get("DEPT_HEAD") != null && !deapheadsMap.get(i).get("DEPT_HEAD").equals("")
						&& !userUid.equals(deapheadsMap.get(i).get("DEPT_HEAD"))) {
					TndmDrftApvlLine item = new TndmDrftApvlLine();
					item.setDocNo(drft.getDocNo());
					item.setDrftDocNo(drft.getDrftDocNo());
					item.setSancDate(currentDate);
					item.setSancOrdNo(2);
					item.setSancYn("N");
					item.setSancType(ApvConstantUtils.APPROVE);
					item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
					item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
					item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
					item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
					item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
					apv0101DAO.insertTndmDrftApvlLine(item);

					TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
					newApvl.setVacationRqstId(id);
					newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
					newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
					newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
					newApvl.setNote(i + 1 + "");

					Map<Object, Object> dataInsertApvl = newApvl.toMap();
					cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
				}
			}
		} else if (deapheadsMap.size() > 1) {
			if (deapheadsMap.size() == 2) {
				for (int i = 0; i < deapheadsMap.size(); i++) {
					if (i == 0) {
						if (deapheadsMap.get(i).get("DEPT_HEAD") != null
								&& !deapheadsMap.get(i).get("DEPT_HEAD").equals("")) {
							if (!userUid.equals(deapheadsMap.get(i).get("DEPT_HEAD"))) {
								TndmDrftApvlLine item = new TndmDrftApvlLine();
								item.setDocNo(drft.getDocNo());
								item.setDrftDocNo(drft.getDrftDocNo());
								item.setSancDate(currentDate);
								item.setSancOrdNo(temp);
								item.setSancYn("N");
								item.setSancType(ApvConstantUtils.REVIEW);
								item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
								item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
								item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
								item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
								item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
								apv0101DAO.insertTndmDrftApvlLine(item);

								TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
								newApvl.setVacationRqstId(id);
								newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
								newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
								newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
								newApvl.setNote(temp + "");
								Map<Object, Object> dataInsertApvl = newApvl.toMap();
								cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
								temp++;
							} else {
								continue;
							}
						}
					} else if (i == (deapheadsMap.size() - 1)) {
						TndmDrftApvlLine item = new TndmDrftApvlLine();
						item.setDocNo(drft.getDocNo());
						item.setDrftDocNo(drft.getDrftDocNo());
						item.setSancDate(currentDate);
						item.setSancOrdNo(temp);
						item.setSancYn("N");
						item.setSancType(ApvConstantUtils.APPROVE);
						item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
						item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
						item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
						item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
						item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						apv0101DAO.insertTndmDrftApvlLine(item);

						TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
						newApvl.setVacationRqstId(id);
						newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
						newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
						newApvl.setNote(temp + "");
						Map<Object, Object> dataInsertApvl = newApvl.toMap();
						cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
						temp++;
					}
				}
			} else {
				for (int i = 0; i < deapheadsMap.size(); i++) {
					if (i == 0) {
						if (deapheadsMap.get(i).get("DEPT_HEAD") != null
								&& !deapheadsMap.get(i).get("DEPT_HEAD").equals("")) {
							if (!userUid.equals(deapheadsMap.get(i).get("DEPT_HEAD"))) {
								TndmDrftApvlLine item = new TndmDrftApvlLine();
								item.setDocNo(drft.getDocNo());
								item.setDrftDocNo(drft.getDrftDocNo());
								item.setSancDate(currentDate);
								item.setSancOrdNo(temp);
								item.setSancYn("N");
								item.setSancType(ApvConstantUtils.REVIEW);
								item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
								item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
								item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
								item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
								item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
								apv0101DAO.insertTndmDrftApvlLine(item);

								TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
								newApvl.setVacationRqstId(id);
								newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
								newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
								newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
								newApvl.setNote(temp + "");
								Map<Object, Object> dataInsertApvl = newApvl.toMap();
								cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
								temp++;
							} else {
								continue;
							}
						}
//					} else if (i == 1 || i == (deapheadsMap.size() - 1)) {
//						if (temp == 1) {
//							if (i == 1) {
//								TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
//								newApvl.setVacationRqstId(id);
//								newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
//								newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
//								newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
//								newApvl.setNote(temp + "");
//								Map<Object, Object> dataInsertApvl = newApvl.toMap();
//								cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
//								temp++;
//							}
//						} else if (temp == 2) {
//							if (i == (deapheadsMap.size() - 1)) {
//								TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
//								newApvl.setVacationRqstId(id);
//								newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
//								newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
//								newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
//								newApvl.setNote(temp + "");
//								Map<Object, Object> dataInsertApvl = newApvl.toMap();
//								cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
//								temp++;
//							}
//						}
//					}
					} else if (i == (deapheadsMap.size() - 1)) {
						TndmDrftApvlLine item = new TndmDrftApvlLine();
						item.setDocNo(drft.getDocNo());
						item.setDrftDocNo(drft.getDrftDocNo());
						item.setSancDate(currentDate);
						item.setSancOrdNo(temp);
						item.setSancYn("N");
						item.setSancType(ApvConstantUtils.APPROVE);
						item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
						item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
						item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
						item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
						item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						apv0101DAO.insertTndmDrftApvlLine(item);

						TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
						newApvl.setVacationRqstId(id);
						newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
						newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
						newApvl.setNote(temp + "");
						Map<Object, Object> dataInsertApvl = newApvl.toMap();
						cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
						temp++;
					} else {
						TndmDrftApvlLine item = new TndmDrftApvlLine();
						item.setDocNo(drft.getDocNo());
						item.setDrftDocNo(drft.getDrftDocNo());
						item.setSancDate(currentDate);
						item.setSancOrdNo(temp);
						item.setSancYn("N");
						item.setSancType(ApvConstantUtils.REVIEW);
						item.setSancDutyCode((String) deapheadsMap.get(i).get("DUTY_CODE"));
						item.setSancDutyName((String) deapheadsMap.get(i).get("DUTY_NAME"));
						item.setSancDeptCode((String) deapheadsMap.get(i).get("DEPT_CODE"));
						item.setSancDeptName((String) deapheadsMap.get(i).get("DEPT_NM"));
						item.setSancUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						apv0101DAO.insertTndmDrftApvlLine(item);

						TcdsEmpVacationApvlResponse newApvl = new TcdsEmpVacationApvlResponse();
						newApvl.setVacationRqstId(id);
						newApvl.setUserUid((String) deapheadsMap.get(i).get("DEPT_HEAD"));
						newApvl.setStatus(NOT_YET_APPROVED_APVL_STATUS);
						newApvl.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
						newApvl.setNote(temp + "");
						Map<Object, Object> dataInsertApvl = newApvl.toMap();
						cms0102DAO.insertTcdsEmpVacationApvl(dataInsertApvl);
						temp++;
					}
				}
			}

		}
	}

	/**
	 * 
	 * @param arg
	 * @return
	 * @throws SQLException
	 */
	public TcdsEmpVacationRequestResponse getTcdsEmpVacationRequest(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> vacationRequestMap = (Map<Object, Object>) cms0102DAO.getTcdsEmpVacationRequest(arg);
		TcdsEmpVacationRequestResponse vacationRequest = new TcdsEmpVacationRequestResponse();
		try {
			vacationRequest.fromMap(vacationRequestMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vacationRequest;
	}

	/**
	 * 
	 * @param arg
	 * @return
	 * @throws SQLException
	 */
	public TcdsEmpVacationApprovalResponse getVRForUsageHistory(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> vacationApprovalMap = (Map<Object, Object>) cms0102DAO.getVRForUsageHistory(arg);
		TcdsEmpVacationApprovalResponse vacationApproval = new TcdsEmpVacationApprovalResponse();
		try {
			vacationApproval.fromMap(vacationApprovalMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vacationApproval;
	}
}
