package com.a2m.imware.service.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.search.SearchDAO;

@Service
public class SearchService {
	@Autowired
	private SearchDAO searchDAO;
	
	public List<Map<Object, Object>> getListElectronicPaymentWithConditions(Map<Object, Object> args) throws Exception{
		if(args.get("LIMIT")!=null) {
			try {
				args.replace("LIMIT", Integer.parseInt(args.get("LIMIT").toString()));
			}catch(Exception e){
				return null;
			}
		}
		if(args.get("OFFSET")!=null) {
			try {
				args.replace("OFFSET", Integer.parseInt(args.get("OFFSET").toString()));
			}catch(Exception e){
				return null;
			}
		}
		return searchDAO.getListElectronicPaymentWithConditions(args);
	}
	
	public int getCountElectronicPaymentWithConditions(Map<Object, Object> args) throws Exception{
		return searchDAO.getCountElectronicPaymentWithConditions(args);
	}
	
	public List<Map<Object, Object>> getListBusinessManagementWithConditions(Map<Object, Object> args) throws Exception{
		if(args.get("LIMIT")!=null) {
			try {
				args.replace("LIMIT", Integer.parseInt(args.get("LIMIT").toString()));
			}catch(Exception e){
				return null;
			}
		}
		if(args.get("OFFSET")!=null) {
			try {
				args.replace("OFFSET", Integer.parseInt(args.get("OFFSET").toString()));
			}catch(Exception e){
				return null;
			}
		}
		return searchDAO.getListBusinessManagementWithConditions(args);
	}
	
	public int getCountBusinessManagementWithConditions(Map<Object, Object> args) throws Exception{
		return searchDAO.getCountBusinessManagementWithConditions(args);
	}
	
	public List<Map<Object, Object>> getListCommunitiesWithConditions(Map<Object, Object> args) throws Exception{
		if(args.get("LIMIT")!=null) {
			try {
				args.replace("LIMIT", Integer.parseInt(args.get("LIMIT").toString()));
			}catch(Exception e){
				return null;
			}
		}
		if(args.get("OFFSET")!=null) {
			try {
				args.replace("OFFSET", Integer.parseInt(args.get("OFFSET").toString()));
			}catch(Exception e){
				return null;
			}
		}
		return searchDAO.getListCommunitiesWithConditions(args);
	}
	
	public int getCountCommunitiesWithConditions(Map<Object, Object> args) throws Exception{
		return searchDAO.getCountCommunitiesWithConditions(args);
	}
	
	public List<Map<Object, Object>> getListMyPageWithConditions(Map<Object, Object> args) throws Exception{
		if(args.get("LIMIT")!=null) {
			try {
				args.replace("LIMIT", Integer.parseInt(args.get("LIMIT").toString()));
			}catch(Exception e){
				return null;
			}
		}
		if(args.get("OFFSET")!=null) {
			try {
				args.replace("OFFSET", Integer.parseInt(args.get("OFFSET").toString()));
			}catch(Exception e){
				return null;
			}
		}
		return searchDAO.getListMyPageWithConditions(args);
	}
	
	public int getCountMyPageWithConditions(Map<Object, Object> args) throws Exception{
		return searchDAO.getCountMyPageWithConditions(args);
	}
}
