package com.a2m.imware.service.apv;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.a2m.imware.dao.apv.Apv0107DAO;
import com.a2m.imware.model.apv.TndmDocReceived;
import com.a2m.imware.model.apv.TndmDocReceivedAttach;
import com.a2m.imware.model.request.PageResponse;

@Service
public class Apv0107Service {
    
	@Autowired
	private Apv0107DAO dao;

    public Object searchTndmDocReceived(Map arg) throws SQLException{
    	PageResponse res = new PageResponse();
		res.setDatas(dao.searchTndmDocReceived(arg));
		res.setCount(dao.countTndmDocReceived(arg));
		return res;
	}
    
	public TndmDocReceived getTndmDocReceived(Long id) throws SQLException{
		return dao.getTndmDocReceived(id);
	}

	public int deleteTndmDocReceived(String id) throws SQLException{
		return dao.deleteTndmDocReceived(id);
	}

	public int insertTndmDocReceived(TndmDocReceived arg) throws SQLException{
		return dao.insertTndmDocReceived(arg);
	}

	public int updateTndmDocReceived(TndmDocReceived arg) throws SQLException{
		return dao.updateTndmDocReceived(arg);
	}
	
	public List searchTndmDocReceivedAttach(Map arg) throws SQLException{
		return dao.searchTndmDocReceivedAttach(arg);
	}

	public Map getTndmDocReceivedAttach(String id) throws SQLException{
		return dao.getTndmDocReceivedAttach(id);
	}

	public int deleteTndmDocReceivedAttach(String id) throws SQLException{
		return dao.deleteTndmDocReceivedAttach(id);
	}

	public int insertTndmDocReceivedAttach(TndmDocReceivedAttach arg) throws SQLException{
		return dao.insertTndmDocReceivedAttach(arg);
	}

	public int updateTndmDocReceivedAttach(TndmDocReceivedAttach arg) throws SQLException{
		return dao.updateTndmDocReceivedAttach(arg);
	}
}

