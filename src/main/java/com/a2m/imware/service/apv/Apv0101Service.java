package com.a2m.imware.service.apv;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.a2m.imware.common.ApvConstantUtils;
import com.a2m.imware.common.ApvConstantUtils.FormType;
import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.dao.apv.Apv0101DAO;
import com.a2m.imware.dao.common.TccoFileDAO;
import com.a2m.imware.model.apv.TndmApvlHist;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.apv.TndmDrftBusTrip;
import com.a2m.imware.model.apv.TndmDrftBusTripDetail;
import com.a2m.imware.model.apv.TndmDrftReference;
import com.a2m.imware.model.apv.TndmDrftReferer;
import com.a2m.imware.model.request.DrftRequest;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.request.TndmDocMstRequest;
import com.a2m.imware.model.request.TndmDrftAttachRequest;
import com.a2m.imware.model.request.TndmDrftMeetingMinuteRequest;
import com.a2m.imware.model.request.TndmDrftRequest;
import com.a2m.imware.model.request.TndmDrftSpendingRequest;
import com.a2m.imware.model.response.TcdsEmpMstResponse;
import com.a2m.imware.model.response.TndmDrftAttachResponse;
import com.a2m.imware.model.response.TndmDrftResponse;
import com.a2m.imware.service.common.ComSeqService;
import com.a2m.imware.service.sys.Sys0108Service;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.a2m.imware.util.ContentInfoAfterRenderingPDF;

@Service
public class Apv0101Service {

	private static String headerTemplate = "<p style=\"line-height:3pt;text-align:center;word-break:keep-all;text-autospace:none;\"><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:0pt;font-size:12pt;\">“21C</span><span style=\"font-size:12pt;font-family:'Arial Unicode MS';\">를 선도하는 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:0pt;font-size:12pt;\">e-Biz </span><span style=\"font-size:12pt;font-family:'Arial Unicode MS';\">전문기업 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:0pt;font-size:12pt;\">AtwoM”</span></p><p style=\"line-height:200%;text-align:center;word-break:keep-all;text-autospace:none;\"><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.5pt;font-size:24pt;\">(</span><span style=\"letter-spacing:-0.5pt;font-size:24pt;font-family:'Arial Unicode MS';\">주</span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.5pt;font-size:24pt;\">) </span><span style=\"letter-spacing:-0.5pt;font-size:24pt;font-family:'Arial Unicode MS';\">에 이 투 엠</span></p><br/><p style=\"line-height:3pt;\"><span style=\"letter-spacing:-0.5pt;font-size:12pt;font-family:'Arial Unicode MS';\">수신자 #DOC_TYPE</span></p><p style=\"line-height:3pt;\"><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.5pt;font-size:12pt;\">(</span><span style=\"letter-spacing:-0.5pt;font-size:12pt;font-family:'Arial Unicode MS';\">경유</span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.5pt;font-size:12pt;\">) </span></p><p style=\"line-height:3pt;ttext-autospace:none;\"><span style=\"letter-spacing:-0.5pt;font-size:12pt;font-family:'Arial Unicode MS';\">제 목 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.5pt;font-size:12pt;\">: #TITLE</span></p><hr />";
	private static final String IMG_STAMP = "<p><span style=\"font-family:Batang;font-size:small;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPsAAAA4CAYAAAA7Do68AAAgAElEQVR4Ae2dB5xcV3no/7fO3Km7O9uLVtKqWM1Fko17xTG2MaYlhOLkPWowYAIJL/QSAw7wKKGTF0KvBuJusLGFe5MtWUKWbPW2RVunz9z6ft/ZHVu4ENkSeTZvjz2anXvvOfec75zvfP07WhRFEbNlFgL/n0EgCAJ838e2bTRNOyKjb7QZi8WOSHtHuhFtFtmPNEhn23shQEBonHwE0Y8Usjfa1HX9WYNANgqpd6T68nQdePa9erpWZq/NQuAFBgFBqiONXI02DxUUYRQRhBHyXavXEYT/UxbzT9n4bNuzEPj/CQKu6yrKbFnWoQ27HjK5b5CY45DsaEEzjow48Uwvn0X2Z4LM7PVZCDwHCDwrFVgUkXSSWE4cTf47QrqDZ+r2rMz+TJCZvf6ChkBDfpZB1Ot1TNPkkCnucxx5A9EPFWmFfW9ox300Ig1Erha+4E9B42cp+3Oc2NlqLwwICAKK1l0+f2pkPxQkb2wIAj19hpILwutiFItmkPxPROFnKfsLY83O9vI5QKBBNRGtu/zQNELtCWqqqOfjD03f1w6yRB98S71eAy0Shnv6TigNNBC08fDM7+mfT2j8FS4HAfL2aqWCYVmYtq02oND3cUsVanWXdCaNEbcU1kufVTuaPk3pD5PczyL7c1hEs1VeABCYQRQfCMIQ/ADdtHCjgLrvYRkGZt1DN00CP8AwTcIoxIw0jAhC1ydwQ7xiCS0M8T2PMPCxc004zWkiNAr1MtUDkyQ9jeLO/URRSM11STY1Ua9WyRzVT833aDIcpoaHsXSTTHsbXuBj5pKEmQROaBGEHjXNx9PBJiLm1ij7dYI1D9N05ovQmnIo3Z1xeHCfZeMPD36ztZ+nEBCqG4WgF2qMb9hCwkkrhC4PjuA4CbxKhWK1gp1KEYaRuqfrGlpvM/ViGfZOobemMR2bysQkum0ru7zhxHHjltIDaI4jTANWOkVu5TI0XSOIArS4hWkY+AYkXB+95NK2dAFu0sLI17GyCeoxg6hQZfzBLYRmRPOS+eTv/z3JlQspb9pCzQyZvPK3WNlmUmedqDaXwyTszCL783Sxznbr8CAQgkLi2u4RYk05wq6MYuOdnhRmzMLWdBzdINQ1dCICAgx0tFAjI2qyJRqh7yl2Ol5uEQEbAh/P9cnvHUbMbK0D80i19zC+diNUPfIjIzjSRtVTLLq5uIed27fSZqZoX3YU5uJOdl93G33nn0Y8l6Zu6yQMi5Ef3kjqdTHcNRupaxreI9uxu3N0VDXMA3nF+ks/D5OwzyL74S2p2drPVwgIsovDCkmbVFsLlcExwvEC9bBIqPtYhoVvpcgePUBt225CJ6K+fZD0SccytXeS0oYdxG2PyNSoPbSNsFIjeeJy6GojXo/RkkkTM0z8QpWg4mF3tdDR30ko/5ka8n60OnMzC7AKLgEhptDnyQL1B9ZT787ijebRtw2hDe6nPlzAXjaH6rathGN54svnMTY3iZbx0PRQbU6HC+tZyn64EJyt/7yEwLTyLaQwPq7YcaunGaspiX4ggrSJkWshVndwowij5BIOTmBsGSKx6jiSC/rpmNtLaNQIp4oMP7yLSr5MPBEnedwibCOhjGNKgTZZoTI4hJ2I4Vt1ItPADXwyuRxm2gbLwIwb4DQRajqxRAIci2hOjnhvjrA5wcTuXcSnCuSNGokFOazJIu6+UWLjlSMK21lkP6LgnG3s+QKBaVNWSDRVxg899B0jVB7eTjQ+QayrmYIEwfTMJXPSUuoHJvGLU0ztH6S5UkVLp6hLnQPjjN+zgcRR82l55XmM3LGW8Pr7cXrbMXIZrOYW9ExM/V2rVkiaKaJKHSYmGN+5i6hYw58qYMTjOKceQzw3l/rkFKV1eeK6RrVUgOFJmibqxGMOiYRFJp2gXF7HYz+5gwEjge4Fiv0+XHld5mUW2Z8vq3O2H0cUAnqI0q7Hqj4mAcW160l1dVG7YAWlRAzWbmVy6x4yqxZiGAbJtmbK87ogEWdq2x52XLOG7lSC9IIerBOXE6ayNHW1oA0ewB89wPj4IPGlRxHr7KTzRcfijk8qM1lUKmBYAfnxcYLAIplqIRsYJO0sZccm94ozKGx7lMjSMLqaFTvffs5Kkk29NDkOrl7lwM+vouvlZ2JOlpFxCJLOIvsRXR6zjf1ZQUDTMAwLM5FQmmzamogfPUCwYQcVt0I4UUFvb8WLW1itOYLKJLFEkigIaJnTS+pVLyWgzFQ8whiZILOvjOa5hHGNoDlBSzyGYcXRpioM/uw3JOb3gKajBx5aENHc1AaJNKn2LrzxPPkHN5Fa3IZX87Cm6kTlCaK6h1asMBVtJ9/6KOHeUVpfcw7ZY04keewC2LSZIFDS/xGZmlnKfkTAONvI8w0C0w4pGjg2IRZRPAtNTdTKNVInLYL9JbSqSXnNBrz120iesZzITlOPAoLduxn/3o1opx1HorOdqOIycs8DZM47nkw8i+8kqG/fjje6GXv1QmKtDrmzj2P4P+8gPlqmnrUJJV7+0T0kmsdJvPRU3KmqMu8ZfV3o6QS2bhCU6ph2HK9WQa+X2bZzDym3TLKnGT2oE1WrkEgeMdDOIvsRA+VsQ88rCIjrWQSu7xHzfDAsQsPErGnYk3WCTBwjncZqaSIR6kSWjR530KoubrlI4pRlJM9ehWZYUA+o7BxC7+3ASCbQjQit36W4dweRFmLETPRknLbXvRhCjRbdELsf7sZNVB/cRUw3qOMTjyJKO4bJP7YbNx1DJySTTBNGAW5YpO/0VWhtzXgb92AlDOr5PHrSmHGjO3zoziL74cNwtoXnKwR0jXgiiRaE6F5I5AfEF81h8Nb7iaUzZFJtRH1tJLrbqeQnqE4VSFkGnq0TGBqmbqLrJpEJNSvCNnUCS1d+7LploT6xGLHmLJ5lYgQaeC6BXkeLmdTSFtVsnJSIFK1Z5bLrj+VJZ1uI+puJAp+YC36ljF6IKB0YJtnTitaUIbQMsEyqpdK0y+wRkNtnkf35ulBn+3VYEIh0cEXcrbo4nk5BZG0JMFk+wNz5fUQTZXTDQrN0Ko/sUhQ6WY0wXYvAaKHq+Bi6gSaE1dXwIkh5PiXBwRCspjh6tY3QSeO2tZHN19l3/wbKazeQCetE7RlINNNy3NGQixO2JQltk6azjmXigU3o+yYIszFqlknTQD++0UcQhcSbMvg9/RhRxMTgkLIIiNgu/TjUTDMSbPN0QTmzyH5YS2q28vMdAm6tRszzSFoJdv/sJrJWQvnE1ywdTw9J+QaOEUM/dzmxTJJa0sJJOegpi0jcZ0UNHtOYc/ZqYukE1kygi96WId2cJDItUssXooU63ccvJ1w2QKRH1BIWMc0kZtrKSy+1YgF1A2zbIL16KfFaiG9GBHVX+e5H6SSOB8HOMSb37icxvwuvWiesuyif3EMEtCB6GIZPm4VnFtkPEYizj70AIRBJhJmYr1ycZXOYO7cdwzII4wYJA8zAIIhZ6JGmglDizRlqjoXlgx238AMf3YsIXJ8oZuKWK+hF0PxQ6QK0tIONRa1WppYfh+1DFNZvxEi4hD3N2M3zqU+E6FNVmtvbSJ61gqhUYvDq3xJ6BpKEytA0rEyarnNOYPL2Bwj2jxF2tqqouExbG/nBEUQ6kE3mUOxvguzVapVEIvEU6j6L7C/ANTzb5UOEgKaRTGeIGwYx3aK+/wCuXkPz61QqZWrlkM5TV6rcb4WHHmUPNeaftBIt7rBj/SaaUhn0qQp61aMwOka8KUuspQnN8wlFDFjWRTjXJL9xB6bnYqeSGEsWqzDVcG4bvmHT7BtYFQ+tOUVFIlcPjBDok3S/7CJirk0Y0ygnLaq2gW36uCmXVF83dlOGSkuSpGFT0yF2iMguefVSqdTTAmgW2Z8WLLMX/1wgIHJwrVQmXyxQ37MPv56nvnYLmdOPJTewBM3QGdq3l66+HjrbsmhOXLxxMG0TbW4b1D1SVpxEuEDZvP0oxK1UMZsdnLZm6oCTTZPIxgm0kJb+NpC4dcmMo1mYgUboVpVNPQh8ipu34WwdZSy2Dm3cJa2ZFIpFcm++gEq9zvjIEON7b2VR4XjinRaRLWmpj4RLzawH3Z/Lmp4dx5MhoIGpgZGwSbomTUvmUI+nKG7YxEhLErM5g++FSgnnlHzqaY/S4DBNyT6suE3b3F7ivkmtWiOIPEJ/JvNr4KNpBtpYmXxhF8kXLUJvybLn6puU773dmsYfGiYWi2N09ECsicD30c2IREcWliyCrhasVUuoPbqT8d/ejemYmFGAEcvSunwFnXMGqO+bJKjUCA5MkpCAnsMNeZt1l33yCpn9/ecCAcnnpungSxRauUr1sUFGr7mD9tOWkbz4xQqJR+5aS0cuiZ6vEe/sojoyhD9ZwuxugZhJvVhFS9iYqQSl3SMq0cXU3iHSR/URHttH/bG9yqxmd+bou+TlYEqwrEt+4yMUiwX6TloKmq0UZpquYyIJM3S8qo+3b4jRm+/DzKRoOms1WiKBbScIjQg7nsQ3CpiWWAL86ZRVygB3eBR+lo3/c1nds+N4CgTEr1ycW0SjHRQgde4q6sf2Eg0Og+uiO3GMeAxT5W73SeWaMTxJHCWKvSrxQMeQJFRBSCyXUUqzVnQC3UQzDHSRo8OIqOYy/tAW7KpPYccu6tt3kOntZmrqAax4kkqxRFqi4F60BDeuq6Qa2p4JEu29JF59jmo3KEQqKk5YfdmktDBQ1yuVKrbrojD/KSN8dhdmkf3ZwWv26RcIBHQ3JDZZxh+T+HULvS0LbgV91wTl6x6m+cxV6KccTTybpNKWJPBciuUSdktMBca0tOaojRcJp2qUHtiBHmjUcxauxLwft4hETysOpnLYGbruTtLpLIlTlxGb10V0wWnEe9oloB5PkmJU6uy97X4WBwPE4w5FK0lmoI/Ju65Gv/423M4cVm4enqOheRAZVTx/ksBoohZ6NHkhmmxch2pof4Y5mkX2ZwDM7OUXOAQMHbIJnL4OhaiuyN5aiFnzyCwZQF/WQ0K84VzQyh5hMiSRSOLX6yr9VEiE3uSgmQbVrEV9zyi5o1egLZtPFDeVC60oziRvXZiwcY6dj+9Y1PYcwB0ap9rejF2WtsCvubQUXbzRAnpzmkRTM7Rnyb35Qsrbd6CPjGNle/C3D6H3T6e/qlcqZGMdirqLSHIkyiyyHwkozrbx/IOAHhFYGoEl/vG+OnXFMjTsrhz777mFxHVFkt1tBP2t6KkYQa2OIYkky3VCP6QymScYK1IeHqd1YIBkrpXy3Y8ytm832VVLaFuxSCntNN0glcpgYmGUQ/Ztfoy2044hNqcHw7YITANHtOkjk9jtTRDXlSKOSgkrCMjkWon3zeXAtnHKv99BdtFxSAIqSX9lxBywLAJXdP4SEHN4WrpZZH/+LdPZHh0JCCh5OsSJO5TyE1gtWfS6h9HsMPeNF+COFagUi2DoRG6gkDCyTeVDL+mio2pA2JsjPdBDoBkYdQen41g6JuYTDOfZfc1tYGkkVw1QKRdpihnUrQAnmcAaKhGN7yQ/OalywzstLVTcKvHMMhXJtv0HP1KbTyzdRdxpJUxWMLwqLS8/DX3lArRqSO/pZ+BaHsRTmHZApAdqEzgc0Mwi++FAb7bu8w4C4ioqXmTi167rBqZloVXrOD059FpENFrGG54gPDBFzI/Q0h5Tk0UyZywjLNbwLB1JOuVNFdC27scPIyrlGmaxjhcEBLkE6Tkd9J20GkM0+bpJ1xkr0TNJHC1iwd9ciCfuqpat2hEAictts7jZ6uBFDrk3vYFEMoUZalgSA6+bOJ5PwYgwgwgjplOd8pBst3Gh7JL6+giw8rPI/rxbrrMdOhwINM5IF0WYLwlhq3WSpkPg+dSLRcJiFa9ewW5LK4eaqMmheWUPZtLBSKTUiaqCnS1z+wkGQjRbWOfpYyEkL7xwAhJkIkgptndfl5TVEaXdQ5TxiOdaYM8BlT8+ptsYiTjOQBeVwVEsx0F3YnjbxogdlSU/cgAnHicWjzF57yNKv5Dfvgf7xKMYv+sBWs8/gUjXlZ3+CFjeZtNSHc7CeiHXFeon5emio17I42qc6SbKa2F8K4Ui0dg47allxFIOkaGhS1J5cVQRrxtBVi/C9wKFyKErLLOucsBLuipdTmOZOY4pkjTREiYfRmrzGN26i+ySeXiFImwdJtmcJVGsUNswhL6wlXqLgWcGJHUoPbaHeK6Z1MJ+tPES5Uf34pRd9jzyIEHCoLOjCz2Qsyw8RlOiYxDln0coG4thH5EpmaXsRwSML6xGBNHl7DOhgvF4/IXV+f+itw3EbDwWTybx7RKlvWMUxsboOGYxWtVn7MHN5E5Zrii3O5bnwNgBWjo6qB4Yo3mgj6n9wypyLDJ0dEPHcRyq5SqJTJrRwSGSjoNZkvNmNKw5HQTZNOHYFJV8nqmjmtEdndiuMdI9HSp/faKjHXX4k62ROqoPbySvDpfILOilZU4nZkuayI+Ib4qIr9tHveTjxmycTEadNNPYnBvjei7fs8j+XKD2Z1JH5Ns/1yIOLyp4JOkwGlboqviUf7eBwbFxso5D4e4HGR8bJbOgTyWIaJ3bpxxoRDsuMnRKzku3TSUKKNNXBPGkjVcSk1gMt+5iJOOKA9AOFBj90c1kz1hJOC9HSyZFlC/j7S9Rum8zua5WtIyDlRB0i1T8fG3PKPVlfaSbs+y7+g6Criz9rzqL7DmroFSluqeAHSUIs21EJNHV2a6HN1uzyH548HtB1lYyp2mqY4xfkAN4Fp0WljxjOsS7W2i5+GTckSE8PSB95nFoZlJFl2V62wmbkiqoRfK157IJdaKLV6gysXmHOinG7GiiVimTTKXU0U6aYZLMNU8fBhEEZI9dSJhLMPnYHvqOXUI5X8VozlAZn8SVuHUd5ABHR/ZXL6LtlGPR57RAvkLLOauV5541USW/YbtktsI4UEIr1RX3JVF2R6I8K2QXViIMJSCgcVTltEvPk1kn6ZiE2snz059QHbyl5B1V+wk5qFFXnpcy/Y4GxZFvkS3lM6MomcnCIc836go72ninfAvFknvykXvysW37iefDQClipD8SyyxFWWqUbBYp90jTFG3uEy5LMu4omhm7qjHdvgRFyFlh8k6RFxt9aowjklNDZ+RjVW3mH13kv5k+NuqofszATP4++P1y3JD8bnwabcl7G+NtXGu0K6y6lEa/pudimoWXa9OZGRq1UEkSJRZa7sViEm0lRcbmTydQEHXyTL/F6USGJbB163XSqdQfjF1dd93Hxy7yr7QpfSgUCspLTUIxG2Ofhleg5GE1DQo20zDyPI9araZCNxswmX5+ep5VL2dg3LhfKpaIxR3sTApXUjTXawxddRttYUStI0nUnMEsFjGLHhP5CbLHLcZJJSmOTxF2d6oF4SdMMr0djG/fQ+fAEmy3TiI0CMZL1Kol6nJWnOYQxWy03hbCTBzr0TpBoUosMij4NVokoUXcwo7FMB2LugXWsj5CyYqzP8/ott04rU1YKlDHomnpIkI7ZKxSQ7M0LNNUZ9PJQZLTK3V6Vp7Lv4eM7LKgHnjgAdatv48wEiO/hhbFaWvrZNOmTZx22mns3buXyclJ3va2tykZp16vc+211/LYls2EtTymoeNHBvXIVDKjLAA5M/v1r389CxYsUBNfLpf5+te/rt7lB4EyoUwv5umNQCbzxS9+MW984xtV3YmJCT7+8Y9z4YUXcvbZZysYfPOb3ySdTvNXf/VXrFmzhuuvv54rrriCpqYm/CDkupvv5LY77gbfJemWlRKmbppEqTTLly9n7R1ruOKTnyCZnM7sKQtLxvLlL3+VnTt3EI/F1eZxySWvZ+myZVx99TXceeedXH755SppgDx/4MABvv+DH7JzcELZaRUCiqVUKGrkko1r/O3f/i1z585VC78xeZVKhZ/97Gd0d3dz7rnnqnuy0D/ykY+wevVqXvGKV6h3y/Pyni1btnDllVci9QSW8nnNa16j2v3qV7+qnr300ksVAgsC3nfffXzvu9/ho+9/Dy2iORYkUf9HTBQqXP7pz3HyySfzhje8QW0sewb3cfkX/zeCPKawxoFkW7GViepFp56Mic5NV13LV7/yFVpbW9UwBFY///nP+f73v682A5m/E088kQ9+8INqPDJf/f39vOtd71L9knEUi0W1Vu64827K5RItLc28/OKLOfXUU/nlL3/JDTfcwFe+8hWy2axaJ9KmzO2vfvUr9Q6Br9x797vfrd7xvr9/L29+21s5eslyhsaG2XLzo/zwqu8oWTtITG/8gj6yBk9cfixrvvZZPnLFp+lr7cCzQN87STiex2xvouuUFUShTyolqaZDiKVxyCjvOUM2vbipwl8TkUbYlsVMxjFbsjjz2pH0WKILDBImlf1j2HPb1WmwRlMKvaOFznkd0xF1XkBQcZlcu4HUqsXoTQkyrsa4FuAU6vjNSQ5XTXfIyC6zOD4+ziObdrH5kUfVpK5ctYJstoVHHnmEgYEBduzYwdjYmLon/whiigIo05yj7DZRLBW55eabWbpwPqeccopamLJwcrmcqiOTPjw8rJBTrnV1zCGZmA4RLNcmEWIk1EHeJZMrzwuyP/zww+qgPUEwKbIwhELdddddDA4OqsUplFGK1Gtu7mDBghWUSxP84sp/4+gVK1h1zMkk081q4UxMTqrnVAXhujyPes1jyeKVdLbPm3aBDHxamjuo1z0FF+mH9EeKLMS7776bq666igtf8T9o7+zDtHR8P1Chj5ZWJ2HWHl+4jffItyD2bbfdppDj4PaGhobUmGRxSpF7u3bt4sYbb1SLXuoJRZYNbc6cOWrMIyMjT0lkIJvxQ+vWcell/4BwL9EMFyR9C9EZm5hk5cqVavwyf5mmFl76l5dQqlRZe/vvWL/mFoVQiVwzXX29/G7NrapfAlcpgrSf+cxnWLt2Lfl8Xm2Scv3BBx9UG5Ygo1yX/jaKzM0vfvELvv/jn3Hy+a9kZX8fa35zI5/57Gdpa2ujVCohREDgKkU4lnXr1in4yntkc6m7Lp0dHYpQXHDBBZTKQn1rKuFkbl4fyzoX8Vd6wGNrH+bu++9VbZ133nl0dHTQlEwzee8a6pJIIqhjhpIx1sZe2IthGypDjWbaioORzdT1PEVMGpyTbhk4Pa1EyTjJsBNxdfUlHVbMQTzsdOHuNJ14wlEpqpLpNGHdw31kD5P3/J5kRwcT1SlcLSC1Y5z06iWU8nlaALdWpzw+RmpO03QiugbQnsP3ISO7TLwA56QTT+HjH/s0iaTDhz70PkqVItdcd52yN7qESuPY6IcsvvPPP1+l3ykGPnfecx83/vYWQjPOWeedT2drDlNOp5xZwFIviKAW6IwVKoTGMHZsDN8XMcBSLKRpVBWCN5Bd6sgi2LpnmAnvXuFFGStWac210NrVx3ixiuvWVUSRPCuc+Qmr57Nq5VwEqX9765Ucu3olr3/taxDW+sYbf4Mm9tSDiiDVxy//OPlykVq9phBNNp3rb/kFr3/dJWrsvvaHSf5kIaTTSV520cnMXzAwvRFE8n5xhtTVSR8yboHrwUUWrix+QdoGAsn9BuI3nhUq/bvf3cZD6zeSyXXiTYyjiYNGLMnNt91JV/88fN3Gf1L0hGxcuhVj4IRzaWltV++P2XIEsUtxYj933HojoSlJjqcFp+akw8UnLEOisZor42xfe7+i/NnWnBKF7ow05SXW6J+MR6j22OQEB/KTVCseccNSXJIQi1tvvRXZhBYvXtwYCpPFMlf/+mZOOeMM3nPZmxSn9eLTT+Cdb3s7963bQHCQaCOVBLb3P7CW/aMTNLV3MTE+SaLJJtfVyfBEnnyljquZ1AmoFYu4U3m6j55P16tewVELl3L72odUAsfzXnEx8+fNZce27VQjnwc3rOeoth5W9ncRahGBbWDYJnEtpTLMythkrTW+G/OimwamEyfQDZVpFgmrDQIlEmhRRLKlBU/FxQc4mnjameh+hDmnjVzvGUReQG/ZpVwpUKj4itLHm7O4YreP61giRj0+I4+D7Vn/ccjILi3L4jRMDd100Q0Tw9QVmy3ChBwk7xngSXb+g4os2Gq5wk2/u43vfv/7tLW3s37zNj5xxed5z6VvZunCaSr9eBUBhmbh+xqWX8WJQV2ydUooIRGGr7Nv/z414bLLKqQHXvbKV3L+xRer65d/7BPs3r2HnYOjjEyVVICDHKonRVg3x7CJjIi6YRPzwYkMkoah2nIMsaZOI7uMROhVV1c373nvP1EJNG749U2KSr357W+jrbWFvr5Orrn2KoXwT0ZO8eKSdyX0aQZM+v+45CXs3Yy318H1hDI2KGJjYUm/BZkO/shcvOpVr2Tpsav4+re/z86hUWTPbEs18+ZL38bRK5Zxwy134COM5hOlpaVFXfn9nb8mLueLz/RBNo8aAa4eMXfxIgVrcSJRiRq0xLTcLoctzCQ0lFzo0j9xLjm4SO6zE044gf+87nYw5tA3N0l5YrvisARJBOFHR0dVFTWeaX0VZQ9KFZeMzLEWEcZi6PEURiKj8sjJs40i8/7aN1xCpquff/s/36YknnCBx5L2Hi59x9+RTDp4ukktkuQUpmKP4x7ka2Wu++2tDE0U0bSQz//rF/nH9/0jldCjVijxra98lTOOOZ6lL1pFyrbUeWyyOSNnqR+UsVXmSMYiHIfoHay4M31Mk6h0ZP2bOpqhkexsmeaQNJ3xveMk0mlSoUZ1PE91aIJY0SUq13CLJVwRw3JZ7EQCMxWneUEfJA20oAJuWTQnjeE/5+9nhezyFhm0UJ7JqUl+9OMfMz42zuDQkEIL07SnkX+mO49t3conP/kphg+Mo8U7OPW0c7nkklcptvvHP/kZ73zXZbRmU7z4nHN405vepDaTIAx42UUXsX3HDggcDD2JmZ1W9lWrdXTqOPGEWjTCgomYELNt7r31NxRG9il02r1jJ339c5g3r18lDSxL7u2DFl9axbMAABX5SURBVIvqXgRFYQ8rFdVWIAt3hsOYnJzi7nvuZclRR9Hd3UU6neLYY49RJ35u2LiRrY+lOX7l0eRyzQIR5X8tyCeLv1HkfUJFZdPRZzYStXXMIK0sIpFxD04MKHX27dtHvlDgmmuv5eRTTmFg/kCjSbZt2468f+GChUp+LBSKfO5zn0N3UrzlLW+kWCxx002/5corf8XRK5bKboKmCUc0XSQ2Op3J8rrXvZ6RAyOYpsXdd9+F63qcfvppGI5Da08XCSfJ+PgE7bmc2kCktvRtaGiYUqnMVD5PR2enalSuy6exYcnf99xzDzW3yKeu+CDHHbOEfdu285EPfUjB46yzz+G3N9+sxLFGnaamDOe/5Dx++IMf8J73/gMLFy5gw6ZHlXLqxBNWc8sN1/wBBySIJgj3H//xXVasWMZJJ75J6S6uu/4Grr76Wv7mktcq3YhsCvKsJHV84O67+NL3/g8TkxU+8YmPUqmU+PZ3vsWHP/xRTj75JMV5fPADH2BRdz+JZAJ3qoxbDDAcW7Uh4xKuK+EklPgldne12Zmm+g6qNUqFIrF0klgqgStHRUWRGoNhGvQNDKhNV5JHJub3kmqdPvM9iluK1Rdk10fzGBLumohjkaYWVtWSsWPxGXeemYl8jl/PGtlL9TqDhQlGRg7QPXQA34+ohjZRaBP4suAbMiXEUlnOeMnLaG1roXdON50dbcTMiNNOGGDFonewf7jI7v3j9HZ3EaKRn5jgy//6ZTbvHlQa7iDwlAZcEEmALcATOnXd9RvJ5Vp49atfrRBGFFDrNz9GOVC6Yv7yoos5/yXn0pTL8ps7bmHduvU4wiIcVIQV3LxlKyMFl/WP7qTq+aQMg1qtzuB4nk994au8+Y3/g79+xUUYYgqRNEe+z/DOx6hNDOMV88ojSmh1XPKI+094o0lfBZEr1Sr//JkvoNsxxeoK0ovAb2kRCTPisssuU7oL6ZZot8u+x32/f5i5y5ZyYHiK//jJL/nw/3qvYsTlrPGb7riXR/eN8M8f/RBHpZIMDQ2ya9c23vLOd/DqV74K1/VV/7c8solYGBCnjiE5x2fGvWXHLj7+qc+pnGeCBOId5gaW6t+dD23C9000tuA4d/Phf3wzrc3Nj4tYogO54977GSm53PfwIwwsXKTsIzLWg4sgsMj8P/rVz/nkFR+ks6Ob0qRJsZ5h6dIFbN0/Sik0CY3p+ZC+OabBG//6lXS0ZPj1fWu567FdDMybz3svvpB+sXdLIgfhKg8S90T3IJv1qaeezPlnn8npq49h3d2/wy+MYwceFq7Kwx4Jt5k0uXXNzdjxGP/8sb9n+TFHK05x0cKl/PQnV7F3t+iDTKUUbevvxTYt9NYmJWKq5BS6pkTRtKSaEmhKcglJZjmjwBVuTUslyCZj6kQYLdKJaaJ5l2c1FeIqiTR8icITbtgPyY+OE3Mj3LEpDEl6MXSAcP+oEqHcXIpENk3LX6zGarLQY+KuO41XB8P62f59yMiukC0MGRkdYWRslJrncva5L2Zu/zwe3bZTydNRJH68M+wmEbf87g5++JOfK8AYM7K5Wy0Ri2rKJ7iuxSm7AV0dHcz72Afo6mjjPe99D7uGx5TCRQBqWbZwRUqjKX0QD8fOpiS9PT1qrKLgk88Da9dRqvsYmo7lBtz865vw8SmEUxx33HEqCOFg4Mgufdvtt2E7CbZs3cG996/lnFNPUt5KvXP6+ZfP/gsdbcKGzdQKQ8ZHR9m6ZTOT46NsWL+e/r4+NTbR0Ips1iiycMQ68eUvf0XpD+p+yLXX38D+/YNc+va3kBQ5UI9YumTJQRQRRsfHWbfxYS55/d8yPDTOf3znB5x75um8aOXR6rmLX34xb/ibN9DalFGv6u3tZdnypfz4pz9k2/bHCH2LO+68l9e+5tUoeM/kYWtQ0EVHLeZTn/4kwwdGVXu2ZWHKSSa6ju/5imMTKtLd1UVPS/pxiiYUbPPmzWzduk0p7K669kZecs6Zqh+Nthtjl99Lly5l8eKjKLk1Fi5YREf7PE468QR6ejqVYu7Tn/4cdlzCTaaLHK+cSSV59cUXcsHFFyhuSbYdoWciXkgOt4OLvKOvr4+jFi/iS1/8Cmt+fSNjg3uwdI3zzj0HSQ5FFCgEFpm77Lm85e/erjTiw7v3ctNNv1YKP9etcsyqbqVwK5YHyWSyakPwdQ2zIE4ztnKskai4cKIETQ6BbRLO2L1lw5Qi38KRSl+n9TEhk2NjpNpyaj2OPboLveLhdLeS6G5lcvMu/NEiQTqGbZsEpRodS8RO30boekTzOrCb0uqUmdqe/eiGTWrp0oNB8Jz+PmRkl9aFfb/t9tvp6ellaqrA7bffyby/macmR9lvRMEW+NNsHXD6qSdy1JIZRUwo8lqRL3/ha5yw+iRe/vILCC0Tn4h4PEZnZwe2LDzD5Ovf+jblcmVa/lEmPjHTywkdOqYe8abXvZrFixapnV4W4rJlyzjp5JMZHptUwE1qJrKQK16VezfczfZtO1R8cCMMyQ9DNv5+E+sf3sA7L30b11xzHd/45rdYNK9fcRiisGpvy+HEbUUV5R1ymuZ1N9zIxFSBnr45/OCHP2Lp8uXKxOX6AUJ5G1RO9gfLtjhOWH91KInP+vXrcKsVTjr+OFKOsGlCcaf9FaSeKBF//JMfkUmlOWbFCpYsDrjh+hu58he/5KgF80AX/+kOck1ZJC5bOAThHj720Y9y8x238z3Rh7T28uEPvZ/jVx1DoVginy+S7Eo+vqHELJM9u/fwr1/5GkE47Scg8yrblK5peF4Jw/J479+/l/4zz1GUX/qWLxT58U9/yqLFiznrL87n81/4Itdcez2v/+tXq1CuJyO8cE3DQ6MEhs6evYPs2rePhzbcg1A8r67x+99vYn5ft1qw0n6lVGJifBxNj6jWJpUsPD5RZGKyqsQ5MfnKO4SLUltqFCkx5oMf/F/cdPOtfP/f/43zzjqd17/hdXR39/Doo48qcUMohHjDzemfR13T+NY3v8mdt9xGIpshl+vArcSZmMwzOraXOfOalfmyOlUizCbI33IfXacfh9WWISzX8bYPoy+fozzqvEpV5Z+PfNEJ6Iptl3HYlk2tWMYIoCnTrMYrDjRaPSC3dIAgaVORyLZaSHU4T6ilsOohhV37qQ2OYroRVjJFZesudVRV+4tX44SGOmNeOLTDJe6HjOyy4AXov/n1Gt5+2WWMjY3z79/+Lpl0M5VyBc/3p3dSY7pJmZyF/X0smNP7+C4kWtjmdJ3eOS0cc+wyRIspRZCjsWBaWlt53RvfihdMO3JouqYQ13DLKoj/pz/+sZL5L3rpS6frahrbt2/n+uuuI9mUU1ReKKtQZJmAXHMvZ55xmpLzZaHIQlxzzwN87otf4aRTT+c1F51He8rmXz7/r3z6S9+gp6dbKXucKKCRxPfA6Cg//9XV/OqG3/LGt7+Lnp4uLr/8Ct5/+ee47F2XUo5MPOMJH/NSuaz6JGKHFEHkkR1bqE6MsuVhESnEJCMBDiZz+/vV7zW33MpdN93CO9/1TnqamtWiPveU1Vx59Q1s2TNEXbMVN+AQYmAoFlbGXap79PctoCnTpeKdRa7+3BfuYO0Da5Ui7DWvmqv6IP/EDZ0zTziWeZ/+GDWkJWFA9ZlNM+DA6BBf+OKXGNw3AWIzjkJ279vP57/+bXYPT3HFP3+Egd4udm56iB/8+KdosQQFL8K0nnBYEpgLjKNQlKwmjt2qTlZR6lVl5SgpCtggDqLX+MbXvs2aW35H1XWpGbqab1MOb7AsWpqbmahrJBMONc0gDcoa89DDG9W7RV+D5TBeC/n5dTfz4EPr2L1rN9RrZCVXfKXK/ocfodiX5Zprr+GVF7yct176d8p0lp/yFSe6YeN6Lv/k5WzZvIsFFy5S6Z/9eb1UQxe/WsV2LKU5n9w9qA6IMCyL9KI51IZG0Ioe/vIEfrGqAm/EjyIUeX3KpRbT0IU7ELP17kGlX7LntWEv6UPPZVVKq6jukkXDzqax2puJicwe1yndeK86LUaeC1JxRRT/2+zs4rTxn1ddxVlnns4Jx69UpoWH1q3nZz+/UrFmIk8FfqBs5w3EPRiJZbFN/57WQssPoSZPLtlknJedfqyyZT9+T4KUJIKpWuO2NWtUO4/fm7H/yyKbO7ef5maxTsrmMY3scr1QLDM+MUGys10pdq659gZOOeVk/ucbXqsQ7S/OPRfLSfGlb36bHTt30eI8sQlJW/feey833Xwzb3rT/+T8885RgREf+cgH+Po3/p3rrruRzs7OxxWTgt879o/y4c//m3JEEccgMcNUazWl+f6nK76m7NvSwUTC4TMfeR9LF8xl69atXHTRRZx5xplq05R+X3jBBRyYKpFKTyf9F2jJddl49+zZwxe+8AUmyy6BEVNhkkJZHt6wUZmT/uG97+bnv/jVU+ZDHE9WSHDFQc6A01sSDLWmaG5umqaeMwB+5JHNSjH1gfe/j4H5/cQ1jXdfdhlO00+4/4EHmTd3WpRpsLRSTcQqJxGRnxxmeMTDlkNUxSPOsCmFhrrf29unNhmp94o3vJbTXyrnoYNnozi8hCCUHVPi3ne+9yNuu/0u5RAl7Y8eOMAvf/WfjBZqSvm5ZOkSYjFbjfWvX/OXSvn3zS9/VXGJsvG3d3djtiZoyjZxy5pr2T/+GN1dvcTsZrVx7Nq5n/b2NhYvXgBeQGjpZFaIP0WgPD91kbObHVIpkyBfVfngpR8yr7geVD2Gb32QnlNXoTsGRmBT2T9MuVYkt2iAUqGM09dG8Y6NZLqbsEYKlB5+jOSKeXiTBdzNe9Bamhl6YCPZZQM4Zx9HTAJ4fI/S1CTNnZ3KZDszJU/5kvUgONfAu6c8MHPhkCm7UKN3vvOdtHZ0Tsflahof/cD72Lt3P9/5znfp7WinUsjTnk3+gSLl4BeLdlTkzJ6enmfsmKCZJAF4MkZ7mkZo6Mzr76O7JaPkpEbbxx9/PMcfv5GJYpVSeWgaWUQzrpRhUCtWlAFfgJHJZPjIh/5JiQ7C1opWXGzmQv2PWbWKe++9n0fWP/gH7Ysn26lnnElMEg6IIwpw4urjWPHlz6NpEbfeehthbQmWcsmF+QNz+No3/rdSMooCU/rRMKcbkhtN4qx9SbKg0Z1VSYt461vfquAmi7NRenp7ef/73kvN91k4fy6trdPOR4IgYqf+1re+hTTvK08wU21CsoFK/2RT2Ld7N/Pm9EzvfI1GG5vu05hykjGblSuWsWzRghnwa5x91hmcetbZxGxTKaxENyEwvOwdb8MLQiXKpQ1xFnpCASomvXe/45/YtGUXstm5hnAJEYQeVlhT47jwL855fIEu6G5jQVerYtOVAVyFp0Kg9JkRSxYOELdM0k5M8SILFy7kS5//LJ74FcwscoFlY83kpwo8esapzOmfgxm3sTpztOYcvvGNb3L1T6/n949uZ/3+UQJ/gpgdZ96Co3ndJRczMDCPqFzFt2LUdMlO42PJ4Y66Tt2URJAm1bxHcdd+2nLLlNOOVSqjVWskhd3WNSr7xvC27KMyPEru9FXYLlTHJonHFqOLQs7zKG/bTTqRRp/TRjS/De3EBTiuT/emveS37MTgOCLx0gx09EQTrh9iN3bkg+bx2f6pRbIqDqMIyzY1NaUCBCrlstpdZTE8XREFhjwr5iYxmf1XO9GT2xDzmHhSiTwui6tRX3a2ihviiVxz0HBkYeqBiBfil20pv4Ant3nwbwGF6CXkI5rWg6nVwc89+W9R9snYZFx/iiKIUsjnFUWUcR+slX6m98mkVisVZV6z7SfMb8/0vFwXOMpYpP2GD8Mfe17gJeOWerJJHSq8/libT3dP5kOKcAyHUqRPIh7I8yKoSEBLPfBwC0VCOX85OS1GTXsP6iTEUuPXiaWSeKNFokwMKx4jlAw10oaIm6KLi8ArVSnf8XucYwaolspEe8dIrV7K2HV30vyKk1TOOdyAuLiEWzpGuU5N97ENi8Gb7iXzkuMx900RbNiFMbdd2Bj8QhFrdIK9e3fSccpKsquPoXr/Y9R9F8u0CGxIr1yEfZA14lDg8ORnniAjT75ziL9lkhs+0ZJ1448VWUQN19g/9twz3RNlSCYtUtsfFllkKVHTH4To6gnJv6tGKAyizNa09vQPaz/xSzYPQaaDqdQTd5/5r2f7/DO39PR3hHqJR92zKcLyP9vNR+AoHNyhFoHXwZzIodZ7ts8dKpI32pV1Jp8GHfOLFXUwYml4ikxvlnhr8nGxS9ZMWPYY27wXfdlcAt8l9A00V1MKVdP3qJWqxE2LWrnM2Lbd5DIZrGyS+sSU2ujCep3yzv3kii7lHXsJd40ShToixYeD48QW9RE7/Why3d3imYYz0D3NlRgWpTHhMAIq+8r0HX8GyWPmTXM8XVlsOeRi3yRm3HmcsDXG+Fy+DxvZn8tL/yR1JID5yTyK0gk0hvjHEf1P0qfZRv+fQECQXDhO0TPF7BjlfUPYiSQp22H85nWkl8xFSwlnKdjkY8ccDPXboLJ7BGNRD1YiQSyboaZFOKFGmC+T7mgj1daKbmgEupiBDRHxVV1jQRfBZInauu1kXnkaVlOGyNSxi1X237seR7iCQgk9jCgOjhAlDKK+FuLzWjBdn/Doxei2rY5vNr0Qq6tFuZ5brhgS5VSYwwdlAxMOv6X/1y00hOKn9OPwnRGe0uTshec1BATZRRwR8UKyzGSyOcbLeWIr5tHV0qJs4kY2jlerKwWfnAwT0yLlO0Kxhln30Ss+7tYhYj1ZggNFJrbtUGmoE01ZCvtHKExOEtufp3VgHmWJFq77aLZFKMdHRQYlW8cRJzDbomlOD6ZjE/XmMMSFtw714QmcVJp6tUpteJSEVicIPerFOpRt6gmT5FH9KjR3anKcOQNdhw3zPx9kP2xQzDbw5wIBJdbNHFssiO+KS7NpEds9xeR9m9G7mvEdQ2nTLSdBKptBL1eUbTs+5WFVAmoTQxQ3Pka2cyVGV5buztUEcV3J78m+DrolHmTnKCPrN9HhSvyGQWgbJPv7KN14P3ZTinJcp97bTmTpKv2VPdCOZhmU2x0oF6nuHsL3XKyyi2eKA5GBqafRkwZmOo5mift5lbS4ZT/VcPWsp+uwFXTP+o2zFWYh8N8IAaHukw9sQY/HsHdPKpfV1BnHUI+Lr9s0DgmLLI4wYgQqbRzCiMUhZeIPj5Fc3o+X1Ik1tP1anekzmiDwTCrlOslMhmKpjCn+E6ID8FwVlxAT51oJpNE0dSqNnEgjylat5il3XYldEP+DermCIQFFk1Xym7apTqU7Oinmp2hevoD4sj4iUzvs7LCzyP7fuPBmX/XfDwGh7FXJ95YvY8Zj1I2AdF/XtJ+B8sfQ0KMAPZBD1qZjFJR47Mu1CoEhFpoIuygOvBq+X8KtF3DF2cbMUipWiRmW4gYC11WOMmGpgmbbGPM7KOUL2Pk6jm7CaB7NDRkZH1EuwPG2FvRkXDkPUfVJt7VSK+ZV1JxuWiQGeogvmUMkfh/6f6Ve/q9h+38B9x36YpabgZUAAAAASUVORK5CYII=\" style=\"display:block;margin-left:auto;margin-right:auto;\" /></span></p>";
	private static final String INTERNAL_RCV = "내부결재";
	private static String footerTempate = "<p style=\"line-height:3pt;tab-stops:left blank 30.0pt left blank 60.0pt left blank 90.0pt left blank 120.0pt left blank 150.0pt left blank 180.0pt left blank 210.0pt left blank 240.0pt left blank 270.0pt left blank 300.0pt left blank 330.0pt left blank 360.0pt left blank 390.0pt left blank 420.0pt left blank 450.0pt left blank 480.0pt left blank 510.0pt left blank 540.0pt left blank 570.0pt left blank 600.0pt left blank 630.0pt left blank 660.0pt left blank 690.0pt left blank 720.0pt left blank 750.0pt left blank 780.0pt left blank 810.0pt left blank 840.0pt left blank 870.0pt left blank 900.0pt left blank 930.0pt left blank 960.0pt left blank 990.0pt left blank 1020.0pt left blank 1050.0pt left blank 1080.0pt left blank 1110.0pt left blank 1140.0pt left blank 1170.0pt;text-autospace:none;\"><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">시행 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">#TAG_DOC (#FINISH_APPROVING_DATE)                     </span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">접수 #RECIPIENTS</span><span style=\"font-family:'Arial Unicode MS';font-size:16px;letter-spacing:-0.533333px;\">                     </span><span style=\"font-family:'Arial Unicode MS';\"> </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">(#SUBMIT_DATE)</span></p><p style=\"line-height:3pt;tab-stops:left blank 30.0pt left blank 60.0pt left blank 90.0pt left blank 120.0pt left blank 150.0pt left blank 180.0pt left blank 210.0pt left blank 240.0pt left blank 270.0pt left blank 300.0pt left blank 330.0pt left blank 360.0pt left blank 390.0pt left blank 420.0pt left blank 450.0pt left blank 480.0pt left blank 510.0pt left blank 540.0pt left blank 570.0pt left blank 600.0pt left blank 630.0pt left blank 660.0pt left blank 690.0pt left blank 720.0pt left blank 750.0pt left blank 780.0pt left blank 810.0pt left blank 840.0pt left blank 870.0pt left blank 900.0pt left blank 930.0pt left blank 960.0pt left blank 990.0pt left blank 1020.0pt left blank 1050.0pt left blank 1080.0pt left blank 1110.0pt left blank 1140.0pt left blank 1170.0pt;text-autospace:none;\"><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">우 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">305-150 </span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">대전광역시 유성구 반석로</span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">13, </span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">반석동웰빙타운</span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">8</span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">층 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">/ www.a2m.co.kr</span></p><p style=\"line-height:3pt;word-break:keep-all;\"><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">전화 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">(042)864-4238, 4239 / </span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">전송 </span><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">(042)862-6091 / </span><a href=\"mailto:#MAIL_TO\"><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;color:#0000ff;\">#SUBMITED_ACCOUNT</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;color:#0000ff;\">#SUBMITED_MAIL_TAIL</span></span></a><span lang=\"EN-US\" style=\"font-family:'Arial Unicode MS';letter-spacing:-0.4pt;font-size:12pt;\">/</span><span style=\"letter-spacing:-0.4pt;font-size:12pt;font-family:'Arial Unicode MS';\">공개</span></p>";

	@Autowired
	private Apv0101DAO dao;

	@Autowired
	private Sys0105Service sys0105Service;

	@Autowired
	private Sys0108Service sys0108Service;

	@Autowired
	private TccoFileDAO tccoFileDao;

	@Autowired
	private ComSeqService comSeqService;

	public PageResponse searchTndmDrft(Map arg) throws SQLException {
		PageResponse res = new PageResponse();
		res.setDatas(dao.searchTndmDrft(arg));
		res.setCount(dao.count(arg));
		return res;
	}

	public TndmDrftResponse getTndmDrft(String id) throws SQLException {
		return dao.getTndmDrft(id);
	}

	public TndmDrftResponse getTndmDrftResponseById(String id) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("drftDocNo", id);
		List<TndmDrftResponse> TndmDrftResponseList = dao.searchTndmDrft(params);
		if (TndmDrftResponseList != null && TndmDrftResponseList.size() > 0) {
			return TndmDrftResponseList.get(0);
		} else
			return null;
	}

	public int deleteTndmDrft(String id) throws SQLException {
		return dao.deleteTndmDrft(id);
	}

	public int insertTndmDrft(TndmDrftRequest arg) throws SQLException {
		return dao.insertTndmDrft(arg);
	}

	public int updateTndmDrft(TndmDrftRequest arg) throws SQLException {
		return dao.updateTndmDrft(arg);
	}

	public int insertTndmDocMst(TndmDocMstRequest arg) throws SQLException {
		return dao.insertTndmDocMst(arg);
	}

	public int updateTndmDocMst(TndmDocMstRequest arg) throws SQLException {
		return dao.updateTndmDocMst(arg);
	}

	public TndmDocMstRequest getTndmDocMst(String arg) throws SQLException {
		return dao.getTndmDocMst(arg);
	}

	public int insertTndmDrftMeetingMinute(TndmDrftMeetingMinuteRequest arg) throws SQLException {
		return dao.insertTndmDrftMeetingMinute(arg);
	}

	public int updateTndmDrftMeetingMinute(TndmDrftMeetingMinuteRequest arg) throws SQLException {
		return dao.updateTndmDrftMeetingMinute(arg);
	}

	public int insertTndmDrftBusTrip(TndmDrftBusTrip arg) throws SQLException {
		return dao.insertTndmDrftBusTrip(arg);
	}

	public int updateTndmDrftBusTrip(TndmDrftBusTrip arg) throws SQLException {
		return dao.updateTndmDrftBusTrip(arg);
	}

	public List<TndmDrftBusTripDetail> getTndmDrftBusTripDetail(Long id) throws SQLException {
		return dao.getTndmDrftBusTripDetail(id);
	}

	public int deleteTndmDrftBusTrip(String id) throws SQLException {
		return dao.deleteTndmDrftBusTrip(id);
	}

	public int deleteTndmDrftSpending(String id) throws SQLException {
		return dao.deleteTndmDrftSpending(id);
	}

	public int insertTndmDrftSpending(TndmDrftSpendingRequest arg) throws SQLException {
		return dao.insertTndmDrftSpending(arg);
	}

	public int updateTndmDrftSpending(TndmDrftSpendingRequest arg) throws SQLException {
		return dao.updateTndmDrftSpending(arg);
	}

	public List<TndmDrftAttachResponse> searchTndmDrftAttach(Map arg) throws SQLException {
		return dao.searchTndmDrftAttach(arg);
	}

	public int deleteTndmDrftAttach(String drftDocNo) throws SQLException {
		return dao.deleteTndmDrftAttach(drftDocNo);
	}

	public int insertTndmDrftAttach(TndmDrftAttachRequest arg) throws SQLException {
		return dao.insertTndmDrftAttach(arg);
	}

	public int updateTndmDrftAttach(TndmDrftAttachRequest arg) throws SQLException {
		return dao.updateTndmDrftAttach(arg);
	}

	public int deleteTndmDrftAttachBySeq(String atchFleSeq) throws SQLException {
		return dao.deleteTndmDrftAttachBySeq(atchFleSeq);
	}

	public DrftRequest getDrftRequest(String arg) throws SQLException {
		return dao.getDrftRequest(arg);
	}

	public List<TndmDrftApvlLine> searchTndmDrftApvlLine(Map arg) throws SQLException {
		return dao.searchTndmDrftApvlLine(arg);
	}

	public TndmDrftApvlLine getTndmDrftApvlLine(Long id) throws SQLException {
		return dao.getTndmDrftApvlLine(id);
	}

	public int deleteTndmDrftApvlLine(String drftDocNo) throws SQLException {
		return dao.deleteTndmDrftApvlLine(drftDocNo);
	}

	public int insertTndmDrftApvlLine(TndmDrftApvlLine arg) throws SQLException {
		return dao.insertTndmDrftApvlLine(arg);
	}

	public int updateTndmDrftApvlLine(TndmDrftApvlLine arg) throws SQLException {
		return dao.updateTndmDrftApvlLine(arg);
	}

	public List<TndmDrftReference> searchTndmDrftReference(Map arg) throws SQLException {
		return dao.searchTndmDrftReference(arg);
	}

	public int deleteTndmDrftReference(String drftDocNo) throws SQLException {
		return dao.deleteTndmDrftReference(drftDocNo);
	}

	public int insertTndmDrftReference(TndmDrftReference arg) throws SQLException {
		return dao.insertTndmDrftReference(arg);
	}

	public int updateTndmDrftReference(TndmDrftReference arg) throws SQLException {
		return dao.updateTndmDrftReference(arg);
	}

	public List<TndmDrftReferer> searchTndmDrftReferer(Map arg) throws SQLException {
		return dao.searchTndmDrftReferer(arg);
	}

	public int deleteTndmDrftReferer(String drftDocNo) throws SQLException {
		return dao.deleteTndmDrftReferer(drftDocNo);
	}

	public int insertTndmDrftReferer(TndmDrftReferer arg) throws SQLException {
		return dao.insertTndmDrftReferer(arg);
	}

	public int updateTndmDrftReferer(TndmDrftReferer arg) throws SQLException {
		return dao.updateTndmDrftReferer(arg);
	}

	public Object searchTndmDrftWaitingApv(Map arg) throws SQLException {
		PageResponse res = new PageResponse();
		res.setDatas(dao.searchTndmDrftWaitingApv(arg));
		res.setCount(dao.countTndmDrftWaitingApv(arg));
		return res;
	}

	public List<TndmApvlHist> searchTndmApvlHist(Map<Object, Object> arg) throws SQLException {
		return this.dao.searchTndmApvlHist(arg);
	}

	public int insertTndmApvlHist(TndmApvlHist arg) throws SQLException {
		return this.dao.insertTndmApvlHist(arg);
	}

	public String getMaxTagDoc(Map<Object, Object> arg) throws SQLException {
		return this.dao.getMaxTagDoc(arg);
	}

	// Approve document

	public void approveDocument(DrftRequest tndmDraft, List<TndmDrftApvlLine> apvlLineList,
			TndmDrftApvlLine currentApvlLine, int approvalCase, String comment, String delegateUid,
			Map<Object, Object> fileInfo, TndmDocMstRequest tempDocMst) throws Exception {
		String currentUserUid = currentApvlLine.getSancUserUid();
		String draftDocNo = currentApvlLine.getDrftDocNo();
		String docNo = currentApvlLine.getDocNo();
		String docStatus = ApvConstantUtils.DocStatus.APPROVING.getValue();
		String draftStatus = ApvConstantUtils.DocStatus.APPROVING.getValue();
		int currentSancOrderNo = currentApvlLine.getSancOrdNo();

		Map<Object, Object> params = new HashMap<>();
		params.put("currentUserUid", currentUserUid);
		params.put("draftDocNo", draftDocNo);
		params.put("docNo", docNo);
		params.put("currentSancOrderNo", currentSancOrderNo);

		if (this.isDelegateCase(approvalCase)) {
			params.put("approvalType", ApvConstantUtils.PROXY_APPROVE);
			this.dao.changeApprovalType(params);
		}

		if (this.isNotProxyAndDelegateCase(approvalCase)) {
			params.put("newUserUid", delegateUid);
			params.put("proxyType", ApvConstantUtils.PROXY_APPROVE);
			this.dao.updateSancOrderNo(params);
			this.dao.insertDelegateApproval(params);
		}

		if (this.isEndApprovalCase(approvalCase)) {
			docStatus = ApvConstantUtils.DocStatus.APPROVED.getValue();
			draftStatus = ApvConstantUtils.DocStatus.APPROVED.getValue();
		}

		/*
		 * - updateSancYnType: 1, 2, 3 - 1: update SanYn of current line - 2: update
		 * SanYn of current line and next line - 3: update SanYn of line with
		 * SANC_ORD_NO >= SANC_ORD_NO of current line
		 */
		params.put("sancYn", ImwareStringUtils.YES);
		if (this.isNormalCase(approvalCase)) {
			params.put("updateSancYnType", 1);
			this.dao.updateSancYnInDrftApvlLine(params);
		} else if (this.isNormalProxyOrNormalDelegateCase(approvalCase)) {
			params.put("updateSancYnType", 2);
			this.dao.updateSancYnInDrftApvlLine(params);
		} else if (this.isEndApprovalCase(approvalCase)) {
			params.put("updateSancYnType", 3);
			this.dao.updateSancYnInDrftApvlLine(params);
		}

		params.put("draftStatus", draftStatus);
		this.dao.updateDraftStatus(params);

		if (this.isEndApprovalCase(approvalCase)) {
			// Letter case, insert file
			if (fileInfo != null && tempDocMst != null) {
				tccoFileDao.insert(fileInfo);
				params.put("atchFileSeq", fileInfo.get("ATCH_FLE_SEQ"));
				params.put("tagDoc", tempDocMst.getTagDoc());
				params.put("finishedDate", tempDocMst.getFinishedDate());
			} else {
				// normal case
				params.put("finishedDate", new Date());
			}

		}
		params.put("docStatus", docStatus);
		this.dao.updateDocMstForApproval(params);

		// save approve history
		params.put("comment", comment);
		params.put("method", draftStatus);
		this.dao.insertApvlHist(params);

	}

	public void rejectDocument(List<TndmDrftApvlLine> apvlLineList, TndmDrftApvlLine currentApvlLine, int approvalCase,
			String comment) throws Exception {
		String currentUserUid = currentApvlLine.getSancUserUid();
		String draftDocNo = currentApvlLine.getDrftDocNo();
		String docNo = currentApvlLine.getDocNo();
		int currentSancOrderNo = currentApvlLine.getSancOrdNo();
		String draftStatus, docStatus;

		Map<Object, Object> params = new HashMap<>();
		params.put("currentUserUid", currentUserUid);
		params.put("draftDocNo", draftDocNo);
		params.put("docNo", docNo);
		params.put("currentSancOrderNo", currentSancOrderNo);

//		if (this.isEndApprovalCase(approvalCase)) {
		// user login is latest approver
//			params.put("updateSancYnType", 3);
		params.put("sancYn", ImwareStringUtils.YES);
		this.dao.updateSancYnInDrftApvlLine(params);

		draftStatus = ApvConstantUtils.DocStatus.REJECT.getValue();
		params.put("draftStatus", draftStatus);
		this.dao.updateDraftStatus(params);

		docStatus = ApvConstantUtils.DocStatus.REJECT.getValue();
		params.put("docStatus", docStatus);
		params.put("finishedDate", new Date());
		this.dao.updateDocMstForApproval(params);
//		}
//		else {
//			// user login is reviewer
//			draftStatus = ApvConstantUtils.DocStatus.RETURN.getValue();
//			params.put("draftStatus", draftStatus);
//			this.dao.updateDraftStatus(params);
//			
//			docStatus = ApvConstantUtils.DocStatus.RETURN.getValue();
//			params.put("docStatus", docStatus);
//			this.dao.updateDocMstForApproval(params);
//
//			// If approver return the document, set all line before current line: SancYn = N
//			params.put("updateSancYnType", 4);
//			params.put("sancYn", ImwareStringUtils.NO);
//			this.dao.updateSancYnInDrftApvlLine(params);
//		}

		// save approve history
		params.put("comment", comment);
		params.put("method", draftStatus);
		this.dao.insertApvlHist(params);

	}

	public String genTagDoc(DrftRequest tndmDraft) throws SQLException {
		TcdsEmpMstResponse drafter = sys0105Service.getEmpMstByUserUid(tndmDraft.getCreatedBy());
		String deptName = drafter.getDeptName() == null ? "" : drafter.getDeptName();
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		String currentYear = String.valueOf(calendar.get(Calendar.YEAR));
		String deptTagDocPrefix = "a2m " + deptName + "-" + currentYear + "-";

		Map<Object, Object> param = new HashMap<>();
		param.put("deptCode", drafter.getDeptCode());
		param.put("tagDocFrefix", deptTagDocPrefix);
		String maxTagDoc = this.getMaxTagDoc(param);

		int tagDocNo = 1;
		if (maxTagDoc != null) {
			try {
				tagDocNo = Integer.parseInt(maxTagDoc.replace(deptTagDocPrefix, ""));
				tagDocNo = tagDocNo + 2;
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}

		DecimalFormat df = new DecimalFormat("000");
		return deptTagDocPrefix + df.format(tagDocNo);
	}

	public List<TndmDrftApvlLine> getDraftApvlLineList(Map<Object, Object> arg) throws SQLException {
		return dao.getDraftApvlLineList(arg);
	}

	public List<Map<Object, Object>> getAbsentById(Map<Object, Object> arg) throws SQLException {
		return dao.getAbsentById(arg);
	}

	public int changeApprovalType(Map<Object, Object> arg) throws SQLException {
		return dao.changeApprovalType(arg);
	}

	public int getApproveCase(List<TndmDrftApvlLine> apvlLineList, TndmDrftApvlLine currentApvlLine,
			TndmDrftApvlLine nextApvlLine, String delegateUid) throws SQLException {
		String currentUserUid = currentApvlLine.getSancUserUid();

		if (ApvConstantUtils.ARBITRARY_APPROVE.equals(currentApvlLine.getSancType())) {
			return ApvConstantUtils.TYPE_ARBITRARY;
		}

		TndmDrftApvlLine lastDraftApvlLine = apvlLineList.get(apvlLineList.size() - 1);
		if (currentUserUid.equals(lastDraftApvlLine.getSancUserUid())) {
			// final approver in normal case
			return ApvConstantUtils.CUR_LINE_IS_LAST_APVL_LINE;
		}

		// Next approver is absent
		if (delegateUid != null) {
			if (ApvConstantUtils.PROXY_APPROVE.equals(currentApvlLine.getSancType())) {
				if (ApvConstantUtils.ARBITRARY_APPROVE.equals(nextApvlLine.getSancType())) {
					return ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_ARBITRARY;
				}
				if (ApvConstantUtils.APPROVE.equals(nextApvlLine.getSancType())) {
					return ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_APPROVE;
				}
				return ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_NORMAL;
			}

			if (currentApvlLine.getSancUserUid().equals(delegateUid)) {
				if (ApvConstantUtils.ARBITRARY_APPROVE.equals(nextApvlLine.getSancType())) {
					return ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_ARBITRARY;
				}

				if (ApvConstantUtils.APPROVE.equals(nextApvlLine.getSancType())) {
					return ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_APPROVE;
				}
				return ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_NORMAL;
			}

			if (!currentApvlLine.getSancUserUid().equals(delegateUid)) {
				return ApvConstantUtils.CUR_USER_IS_NOT_PROXY_AND_DELEGATE;
			}
		}

		return ApvConstantUtils.NORMAL_APRROVAL_CASE;
	}

	public boolean isEndApprovalCase(int approvalCase) {
		// if user logged in is latest approver
		return approvalCase == ApvConstantUtils.TYPE_ARBITRARY
				|| approvalCase == ApvConstantUtils.CUR_LINE_IS_LAST_APVL_LINE
				|| approvalCase == ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_ARBITRARY
				|| approvalCase == ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_APPROVE
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_ARBITRARY
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_APPROVE;
	}

	// Next approver is absent
	public boolean isDelegateCase(int approvalCase) {
		return approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_NORMAL
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_ARBITRARY
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_APPROVE;
	}

	// Next approver is absent
	public boolean isNotProxyAndDelegateCase(int approvalCase) {
		return approvalCase == ApvConstantUtils.CUR_USER_IS_NOT_PROXY_AND_DELEGATE;
	}

	public boolean isNormalCase(int approvalCase) {
		return approvalCase == ApvConstantUtils.NORMAL_APRROVAL_CASE
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_NOT_PROXY_AND_DELEGATE;
	}

	public boolean isNormalProxyOrNormalDelegateCase(int approvalCase) {
		return approvalCase == ApvConstantUtils.TYPE_PROXY_AND_NEXT_APPROVER_IS_NORMAL
				|| approvalCase == ApvConstantUtils.CUR_USER_IS_DELEGATE_AND_NEXT_APPROVER_IS_NORMAL;
	}

	public MutablePair<TndmDrftApvlLine, TndmDrftApvlLine> getCurrentAndNextApvlLine(
			List<TndmDrftApvlLine> apvlLineList, String currentUserUid) {
		// Left for current ApvlLine, right for next apvlLine
		MutablePair<TndmDrftApvlLine, TndmDrftApvlLine> apvlLines = new MutablePair<>();
		for (int i = 0; i < apvlLineList.size(); i++) {
			TndmDrftApvlLine apvlLine = apvlLineList.get(i);
			if (currentUserUid.equals(apvlLine.getSancUserUid())) {
				apvlLines.setLeft(apvlLine);
				if (i < apvlLineList.size() - 1) {
					apvlLines.setRight(apvlLineList.get(i + 1));
				}
				break;
			}
		}
		return apvlLines;
	}

	public String getDelegateUserUid(TndmDrftApvlLine nextApvlLine) throws SQLException {
		String delegateUserUid = null;
		if (nextApvlLine != null) {
			Map<Object, Object> params = new HashMap<>();
			params.put("absentUserId", nextApvlLine.getSancUserUid());
			List<Map<Object, Object>> absentUser = this.getAbsentById(params);
			if (absentUser != null && absentUser.size() > 0) {
				delegateUserUid = (String) absentUser.get(0).get("RPT_USER_UID");
			}
		}
		return delegateUserUid;
	}

	// Export pdf
	public static int getElementHeight(Element element, ITextRenderer renderer) throws Exception {

		ContentInfoAfterRenderingPDF contentInfoAfterRenderingPDF = caculateParagraphDimension(
				new StringBuilder(element.toString()), renderer);
		int heightOfContent = contentInfoAfterRenderingPDF.getHeight();
		return heightOfContent;
		// return contentPdf;
	}

	public static ContentInfoAfterRenderingPDF caculateParagraphDimension(StringBuilder contentPdf,
			ITextRenderer renderer) throws IOException {
		String openHtml = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body>";
		String closeHtml = "</body></html>";
		StringBuilder newContentPdf = contentPdf;

		if (!contentPdf.toString().startsWith("<html>")) {
			newContentPdf = new StringBuilder(openHtml).append(contentPdf).append(closeHtml);
		}

		org.jsoup.nodes.Document document = Jsoup
				.parse(newContentPdf.toString().replace("<o:p>", "").replace("</o:p>", ""), "", Parser.xmlParser());

		document.outputSettings().escapeMode(org.jsoup.nodes.Entities.EscapeMode.xhtml);
		// String data = document.toString();
		renderer.setDocumentFromString(document.toString());
		renderer.layout();
		int height = renderer.getRootBox().getBoxDimensions().getHeight();
		int numberOfPage = renderer.getRootBox().getLayer().getPages().size();
		ContentInfoAfterRenderingPDF contentInfoAfterRenderingPDF = new ContentInfoAfterRenderingPDF(height,
				numberOfPage);
		// System.out.println("height of content = " + height);
		renderer.finishPDF();
		return contentInfoAfterRenderingPDF;
	}

	public static int getA4PageHeight(ITextRenderer renderer) throws IOException {
		// int heightA4Page = caculateHeightOfA4(new StringBuilder("<div style=\"height:
		// 297mm;\"></div>"), renderer);
		// return heightA4Page;
		return 19200;
	}

	public static void addBlankToElement(Element element, ITextRenderer renderer, int minHeight) throws Exception {
		String pTag1px = "<p style=\"height: 1px;\"></p>";

		int heightofParagraphTag = 0;
		heightofParagraphTag = caculateParagraphDimension(
				new StringBuilder("<div></div>").append(pTag1px).append(pTag1px), renderer).getHeight()
				- caculateParagraphDimension(new StringBuilder("<div></div>").append(pTag1px), renderer).getHeight();

		// ContentInfoAfterRenderingPDF contentInfoAfterRenderingPDF =
		// caculateParagraphDimension(new StringBuilder(element.toString()), renderer);
		// int heightOfContent = contentInfoAfterRenderingPDF.getHeight();
		int heightOfContent = getElementHeight(element, renderer);
		int addHeight = -1;
		if (heightOfContent < minHeight) {
			addHeight = (minHeight - heightOfContent) / heightofParagraphTag;
		}

		for (int i = 0; i < addHeight; i++) {
			element.append(pTag1px);
		}
		// return contentPdf;
	}

	public static String addHeaderAndFooter(String content, String baseURL) throws Exception {
		String header = headerTemplate;
		String footer = footerTempate;
		footer = footer.replace("(#SUBMIT_DATE)", "");

		String mailTo = null;
		if (!ImwareStringUtils.isEmpty(mailTo)) {
			footer = footer.replace("#MAIL_TO", mailTo);
			String[] fullAccount = mailTo.split("@");
			if (fullAccount.length >= 2) {
				footer = footer.replace("#SUBMITED_ACCOUNT", fullAccount[0]);
				footer = footer.replace("#SUBMITED_MAIL_TAIL", "@" + fullAccount[1]);
			} else {
				footer = footer.replace("#SUBMITED_ACCOUNT", "");
				footer = footer.replace("#SUBMITED_MAIL_TAIL", "");
			}
		} else {
			footer = footer.replace("#MAIL_TO", "");
			footer = footer.replace("#SUBMITED_ACCOUNT", "");
			footer = footer.replace("#SUBMITED_MAIL_TAIL", "");
		}

		StringBuilder result = new StringBuilder();
		result.append(header).append(content).append(footer);
		return result.toString();
	}

	public static String convertMoneyToHangul(String money) {
		String[] han1 = { "", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구" };
		String[] han2 = { "", "십", "백", "천" };
		String[] han3 = { "", "만", "억", "조", "경" };

		StringBuffer result = new StringBuffer();
		int len = money.length();
		for (int i = len - 1; i >= 0; i--) {
			result.append(han1[Integer.parseInt(money.substring(len - i - 1, len - i))]);
			if (Integer.parseInt(money.substring(len - i - 1, len - i)) > 0)
				result.append(han2[i % 4]);
			if (i % 4 == 0)
				result.append(han3[i / 4]);
		}

		return result.toString();
	}

	public List searchTndmFormMgt(Map arg) throws SQLException {
		return dao.searchTndmFormMgt(arg);
	}

	public void deleteDrftDoc(Map arg) throws SQLException {
		dao.deleteDrftDoc(arg);
	}

	public int deleteTndmApvlHist(Map<Object, Object> arg) throws SQLException {
		return dao.deleteTndmApvlHist(arg);
	}

	public int updateDraftStatus(Map<Object, Object> arg) throws SQLException {
		return dao.updateDraftStatus(arg);
	}

	public int updateDocMstForApproval(Map<Object, Object> arg) throws SQLException {
		return dao.updateDocMstForApproval(arg);
	}

	public void cloneForeignOriginLetter(DrftRequest oldTndmDraft, Map<Object, Object> sendingOutFile)
			throws Exception {
		Date currentDate = new Date();

		TndmDocMstRequest newDocMst = this.getTndmDocMst(oldTndmDraft.getDocNo());
		newDocMst.setDocNo(comSeqService.getSeq("SEQ_DOC_ID"));
		newDocMst.setCreatedDate(currentDate);
		newDocMst.setExportFileSeq((String) sendingOutFile.get("ATCH_FLE_SEQ"));
		newDocMst.setOriginDocNo(oldTndmDraft.getDocNo());
		this.insertTndmDocMst(newDocMst);

		tccoFileDao.insert(sendingOutFile);

		TndmDrftRequest newDraft = new TndmDrftRequest();
		newDraft.setDocNo(newDocMst.getDocNo());
		newDraft.setDrftDocNo(comSeqService.getSeq("SEQ_DRFT_ID"));
		newDraft.setCreatedDate(currentDate);
		newDraft.setRecentYn(true);
		newDraft.setForeignOrgin(true);
		newDraft.setDrftStatus(ApvConstantUtils.DocStatus.APPROVED.getValue());
		newDraft.setDrftType(FormType.SENDING_LETTER.getValue());

		newDraft.setCreatedBy(oldTndmDraft.getCreatedBy());
		newDraft.setDrftTitle(oldTndmDraft.getDrftTitle());
		newDraft.setDrftContent(oldTndmDraft.getDrftContent());
		newDraft.setSendTo(oldTndmDraft.getSendTo());

		this.insertTndmDrft(newDraft);

		Map<Object, Object> arg = new HashMap<>();
		arg.put("drftDocNo", oldTndmDraft.getDrftDocNo());
		arg.put("draftDocNo", oldTndmDraft.getDrftDocNo());

		List<TndmDrftReference> tndmDrftRequestList = this.searchTndmDrftReference(arg);
		if (tndmDrftRequestList != null) {
			for (TndmDrftReference item : tndmDrftRequestList) {
				TndmDrftReference reference = new TndmDrftReference();
				reference.setDocNo(item.getDocNo());
				reference.setDrftDocNo(newDraft.getDrftDocNo());
				this.insertTndmDrftReference(reference);
			}
		}

		List<TndmDrftReferer> tndmDrftRefererList = this.searchTndmDrftReferer(arg);
		if (tndmDrftRefererList != null) {
			for (TndmDrftReferer item : tndmDrftRefererList) {
				TndmDrftReferer referer = new TndmDrftReferer();
				referer.setUserUid(item.getUserUid());
				referer.setDrftDocNo(newDraft.getDrftDocNo());
				this.insertTndmDrftReferer(referer);
			}
		}

		List<TndmDrftAttachResponse> oldDraftAttaches = this.searchTndmDrftAttach(arg);
		if (oldDraftAttaches != null) {
			for (TndmDrftAttachResponse oldAttachment : oldDraftAttaches) {
				TndmDrftAttachRequest attachRequest = new TndmDrftAttachRequest();
				attachRequest.setDrftAttachOrdNo(oldAttachment.getDrftAttachOrdNo());
				attachRequest.setDrftDocNo(newDraft.getDrftDocNo());
				attachRequest.setAtchFleSeq(oldAttachment.getAtchFleSeq());
				this.insertTndmDrftAttach(attachRequest);
			}
		}

		List<TndmDrftApvlLine> apvlLineList = this.getDraftApvlLineList(arg);
		if (apvlLineList != null) {
			for (TndmDrftApvlLine apvlLine : apvlLineList) {
				apvlLine.setIdApvlLine(null);
				apvlLine.setDrftDocNo(newDraft.getDrftDocNo());
				this.insertTndmDrftApvlLine(apvlLine);
			}
		}

		List<TndmApvlHist> apvlHistList = this.searchTndmApvlHist(arg);
		if (apvlHistList != null) {
			for (TndmApvlHist apvlHist : apvlHistList) {
				apvlHist.setId(null);
				apvlHist.setDrftDocNo(newDraft.getDrftDocNo());
				this.insertTndmApvlHist(apvlHist);
			}
		}
	}

	/**
	 * TienNQ
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public Map<Object, Object> getTndmDrftCMS0102(Map<Object, Object> arg) throws SQLException {
		arg.put("VACATION_RQST_ID", Long.parseLong((String) arg.get("VACATION_RQST_ID")));
		return dao.getTndmDrftCMS0102(arg);
	}

}
