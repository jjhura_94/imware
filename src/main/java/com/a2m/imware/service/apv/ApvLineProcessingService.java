package com.a2m.imware.service.apv;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.apv.ApvLineProcessingDAO;
import com.a2m.imware.model.apv.TndmApvlLineFavorite;
import com.a2m.imware.model.apv.TndmApvlLineFavoriteDetail;

@Service
public class ApvLineProcessingService {
	
	
	@Autowired
	ApvLineProcessingDAO apvLineDao;
	

	public List<TndmApvlLineFavorite> searchTndmApvlLineFavorite(Map<Object, Object> arg) throws SQLException{
		return apvLineDao.searchTndmApvlLineFavorite(arg);
	}
	
	public int deleteTndmApvlLineFavorite(Long id) throws SQLException{
		return apvLineDao.deleteTndmApvlLineFavorite(id);
	}
	
	public int insertTndmApvlLineFavorite(TndmApvlLineFavorite arg) throws SQLException{
		return apvLineDao.insertTndmApvlLineFavorite(arg);
	};

	public int updateTndmApvlLineFavorite(TndmApvlLineFavorite arg) throws SQLException{
		return apvLineDao.updateTndmApvlLineFavorite(arg);
	}
	
	public List<TndmApvlLineFavoriteDetail> searchTndmApvlLineFavoriteDetail(Map<Object, Object> arg) throws SQLException{
		return apvLineDao.searchTndmApvlLineFavoriteDetail(arg);
	}
	
	public int deleteTndmApvlLineFavoriteDetail(Long id) throws SQLException{
		return apvLineDao.deleteTndmApvlLineFavoriteDetail(id);
	}
	
	public int insertTndmApvlLineFavoriteDetail(TndmApvlLineFavoriteDetail arg) throws SQLException{
		return apvLineDao.insertTndmApvlLineFavoriteDetail(arg);
	}

	public int updateTndmApvlLineFavoriteDetail(TndmApvlLineFavoriteDetail arg) throws SQLException{
		return apvLineDao.updateTndmApvlLineFavoriteDetail(arg);
	}
	
}
