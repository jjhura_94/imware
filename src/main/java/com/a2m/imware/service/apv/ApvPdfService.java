package com.a2m.imware.service.apv;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.apv.TndmDrftApvlLine;
import com.a2m.imware.model.response.TcdsEmpMstResponse;
import com.a2m.imware.service.common.TccoFileService;
import com.a2m.imware.service.sys.sys0105.Sys0105Service;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.IOException;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.font.FontProvider;

@Service
public class ApvPdfService {
	
    @Value("${dir.upload.pdf}")
    private String pdfUploadDir;
    
    @Value("${dir.font}")
    private String configFontDir;
    
    @Autowired
    private TccoFileService tccoFileService;

    @Autowired
    Environment environment;
    
    @Autowired
    private Sys0105Service sys0105Service;
    
    public Map<Object, Object> genPdf(String html) throws IOException, java.io.IOException {
		Validate.notBlank(html, "Html content cannot be blank!!!");
		
        if (!html.startsWith("<html>")) {
        	html = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
        			+ "<style>\r\n" + 
        			"    @page {\r\n" + 
        			"        margin-top: 10mm; margin-left:20mm; margin-right:20mm; margin-bottom:10mm;" + 
        			"    }\r\n" + 
        			"</style></head><body>"
                    + html
                    + "</body></html>";
        }
		html = this.updateFonts(html);
		System.out.println(html);
		
        String atchFileSeq = java.util.UUID.randomUUID().toString();
        String fileNamePhysical = atchFileSeq.replace("-", "");
        String path = pdfUploadDir + "/" + fileNamePhysical + ".pdf";
        
        if (!(new File(pdfUploadDir)).isDirectory()) {
            new File(pdfUploadDir).mkdirs();
        }
        
		

		ConverterProperties converterProperties = new ConverterProperties();
	    FontProvider fontProvider = new DefaultFontProvider();
	    File fontsDir = new File(configFontDir);
        if (!fontsDir.exists()) {
            // For development.
            fontsDir = new File(getClass().getResource("/fonts").getFile());
        }
        File[] fontFiles = fontsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String _name = name.toLowerCase();
                return _name.endsWith(".ttf") || _name.endsWith(".otf");
            }
        });

        if (fontFiles != null) {
            for (int i = 0, len = fontFiles.length; i < len; i++) {
            	fontProvider.addFont(FontProgramFactory.createFont(fontFiles[i].getAbsolutePath()));
            }
        }
	    converterProperties.setFontProvider(fontProvider);

	    
	    PdfDocument pdf = new PdfDocument(new PdfWriter(path));
		HtmlConverter.convertToPdf(html, pdf, converterProperties);
        
		
        Map<Object, Object> fileInfo = new HashMap<>();
        fileInfo.put("fileNamePhysical", fileNamePhysical);
        fileInfo.put("FLE_TP", "pdf");
        fileInfo.put("ATCH_FLE_SEQ", atchFileSeq);
        File f = new File(path);
        String FLE_NM = fileNamePhysical + ".pdf";
        String NEW_FLE_NM = fileNamePhysical + ".pdf";
        fileInfo.put("FLE_PATH", NEW_FLE_NM);
        fileInfo.put("FLE_NM", FLE_NM);
        fileInfo.put("NEW_FLE_NM", NEW_FLE_NM);
        fileInfo.put("FLE_SZ", f.length());
        
		return fileInfo;
	}
    
    public String updateFonts(String text) {

        String patternString = "font-family:[&;a-zA-Z0-9ㄱ-ㅎ가-힣 ',_-]*;";
        String[] ACCEPT_FONTS = new String[]{"font-family: Batang;", "font-family: 'Arial Unicode MS';",
            "font-family: NanumBarunGothic;", "font-family: 'Malgun Gothic';"};
        List<String> fonts = Arrays.asList(ACCEPT_FONTS);
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);
        String ret = "";
        int start = 0;
        // int end = 0;
        while (matcher.find()) {
            ret += text.substring(start, matcher.start());
            start = matcher.end();
            if (fonts.contains(matcher.group())) {
                ret += matcher.group();
            } else {
                ret += "font-family: Batang;";
            }

        }
        if (start < text.length()) {
            ret += text.substring(start);
        }
        ret = ret.replaceAll("font-family:[ ]*\"", "font-family: Batang;\""); // for empty font

        return ret;

    }
    
    public String getBizReportSignature(List<TndmDrftApvlLine> draftApvlLineList) throws Exception {
        if (draftApvlLineList == null || draftApvlLineList.size() == 0) {
            return ImwareStringUtils.EMPTY;
        }

        String block1 = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size: 11pt;border-collapse: collapse;border: none;margin-left: auto;margin-right: -0.5px;\">\r\n" + 
        		"   <tbody>\r\n" + 
        		"      <tr>";
        StringBuilder block2 = new StringBuilder();
        String block3 = "</tr><tr>";
        StringBuilder block4 = new StringBuilder();
        String block5 = "</tr></tbody></table>";

        String tdOpen = "<td style=\"text-align:center; border: 1px solid black;  padding: 5px;\">";
        String tdClose = "</td>";
        String imgOpen = "<div style=\"width:80px; height:60px;\"><img style=\"object-fit: contain;width:80px; height:60px;\" src=\"";
        String imgClose = "\"></div>";
        
        String serverUrl = environment.getProperty("server.host") + ":" + environment.getProperty("server.port");

        if (draftApvlLineList != null) {
            for (TndmDrftApvlLine apvlLine : draftApvlLineList) {
				TcdsEmpMstResponse emp = sys0105Service.getEmpMstByUserUid(apvlLine.getSancUserUid());
				TccoFile imgFile = tccoFileService.findBySequence(emp.getSignaturePath());
				String signaturePath = imgOpen + imgClose;
				if (ImwareStringUtils.YES.equals(apvlLine.getSancYn()) && imgFile != null) {
					signaturePath = imgOpen + serverUrl + "/resources/" + imgFile.getFlePath() + imgClose;
				}

                String dutyName = emp.getDutyName();
                block2.append(tdOpen).append(dutyName).append(tdClose);
                block4.append(tdOpen).append(signaturePath).append(tdClose);
            }
        }

        String signature = new StringBuilder().append(block1).append(block2).append(block3).append(block4)
                .append(block5).toString();
        return signature;
    }

}
