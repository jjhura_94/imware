package com.a2m.imware.service;

import java.util.List;
import java.util.Map;

import com.a2m.imware.model.TccoFile;
import com.a2m.imware.model.TndmNotice;
import com.a2m.imware.model.TndmNoticeComment;

public interface Com0101Service {

	List<TndmNotice> fetch(Map<Object, Object> filter) throws Exception;
	
	TndmNotice findById(Long id) throws Exception;
	
	boolean insert(Map<Object, Object> data) throws Exception;
	
	void update(Map<Object, Object> data) throws Exception;
	
	List<TccoFile> findAttachment(Long noticeId) throws Exception;
	
	long count(Map<Object, Object> filter) throws Exception;
	
	boolean insertAttachment(Map<Object, Object> data) throws Exception;
	
	List<TndmNoticeComment> findCommentLevel1(Long noticeId) throws Exception;
	
	List<TndmNoticeComment> findCommentLevel2(Long noticeId, Long parentId) throws Exception;
	
	boolean insertComment(Map<Object, Object> data) throws Exception;
	
	boolean updateComment(Map<Object, Object> data) throws Exception;
        
        Object deleteComment(Long id);
        
        long countNew() throws Exception;

	
}
