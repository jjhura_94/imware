package com.a2m.imware.service.common;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.ComSeq;

@Service
public class ComSeqService {
	@Autowired
	private ComSeq dao;
	
	public String getSeq(String seqName) throws SQLException{
		dao.setSeq(seqName);
		return dao.getSeq();
	}
}
