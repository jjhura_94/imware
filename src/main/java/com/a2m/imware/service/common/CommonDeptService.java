/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a2m.imware.service.common;

import com.a2m.imware.dao.common.CommonDeptDAO;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.util.Utils;
import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class CommonDeptService {

    @Autowired
    private CommonDeptDAO commonDeptDAO;
    @Autowired
    private UserInfoService userInfoService;

    public List<String> getChildrenOfUser(String deptCode, String userId) {
        //if deptCode null -> get deptcode from userId;
        List<String> userIds = new ArrayList();
        List<String> deptStructs = new ArrayList();
        recursiveGetDeptCode(deptCode, deptStructs);
        if (!deptStructs.isEmpty()) {
            if (deptStructs.size() == 1) {
                userIds.add(userId);
            } else {
                userIds = commonDeptDAO.getUserByDeptCodes(deptStructs);
            }
        }
        return userIds;
    }

    public List<String> getUidOfUser(String deptCode, String userId) {
        //if deptCode null -> get deptcode from userId;
        List<String> userIds = new ArrayList();
        List<String> deptStructs = new ArrayList();
        recursiveGetDeptCode(deptCode, deptStructs);
        if (!deptStructs.isEmpty()) {
            userIds = commonDeptDAO.getUserByDeptCodes(deptStructs);
        }
        return userIds;
    }

    public void recursiveGetDeptCode(String deptCode, List<String> deptStructs) {
        Map<Object, Object> dept_struct = commonDeptDAO.getDeptStructByDeptCode(deptCode);
        if (dept_struct != null && !dept_struct.isEmpty()) {
            String struct_id = dept_struct.get("STRUCT_ID").toString();
            String struct_deptCode = dept_struct.get("DEPT_CODE").toString();
            deptStructs.add(struct_deptCode);
            List<Map<Object, Object>> deptChild = commonDeptDAO.getChirldrentDepartment(struct_deptCode, struct_id);
            if (deptChild != null && !deptChild.isEmpty()) {
                for (Map<Object, Object> map : deptChild) {
                    recursiveGetDeptCode(map.get("DEPT_CODE").toString(), deptStructs);
                }
            }

        }
    }

    public Map<String, String> getByDeptCodes(List<String> deptCodes) {
        return commonDeptDAO.getByDeptCodes(deptCodes);

    }

    public List<String> getUidOfUser(HttpServletRequest request, Object depCode) throws ImwareException {
        String deptCode = "";
        String token = Utils.getTokenFromRequest(request);
        String userId = userInfoService.getUserIdFromToken(token);
        if (depCode == null || Strings.isNullOrEmpty(depCode.toString())) {
            Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
            deptCode = user.get("DEPT_CODE").toString();
        }else{
            deptCode = depCode.toString();
        }
        List<String> userIds = this.getUidOfUser(deptCode, userId);
        if (userIds.isEmpty()) {
            userIds.add("99998888");
        }
        return userIds;
    }

}
