package com.a2m.imware.service.common;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.UserInfoDAO;
import com.a2m.imware.model.util.ImwareException;
import java.util.List;

@Service
public class UserInfoService {

    @Autowired
    private UserInfoDAO dao;

    /*
	 * @Cacheable( value = "userInfoCache", key = "#token")
     */
    public Map<Object, Object> getUserInfo(String userId, String token) throws ImwareException {
        Map<Object, Object> rs = dao.getUserInfo(userId);
        return rs;
    }

    public Map<Object, Object> getUserById(Long empNo) throws ImwareException {
        Map<Object, Object> rs = dao.getUserById(empNo);
        return rs;
    }

    @Cacheable(value = "userIdWithTokenCache", key = "#token")
    public String getUserIdFromToken(String token) throws ImwareException {
        return dao.getUserIdFromToken(token);
    }

    public void saveLoginInfo(Map<Object, Object> loginInfo) throws ImwareException {
        dao.saveLoginInfo(loginInfo);
    }

    public void saveLogoutInfo(Map<Object, Object> logoutInfo) throws ImwareException {
        dao.saveLogoutInfo(logoutInfo);
    }

    public List<String> getUserIds() throws ImwareException {
        return dao.getUserIds();
    }
    public List<String> getEmpNo(List<String> userIds) throws ImwareException {
        return dao.getEmpNo(userIds);
    }

    /**
     * Update IS_FIRST_LOGIN to true.
     *
     * @param userUid
     * @throws Exception
     * @author HungDM
     */
    public void updateFirstLogin(String userUid) throws Exception {
        dao.updateFirstLogin(userUid);
    }
}
