package com.a2m.imware.service.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.a2m.imware.dao.common.TndmCommentDAO;
import com.a2m.imware.model.common.TndmComment;


@Service
public class TndmCommentService {
    
	@Autowired
	private TndmCommentDAO dao;

    public List searchTndmComment(Map arg) throws SQLException{
		return dao.searchTndmComment(arg);
	}

	public Map getTndmComment(Long id) throws SQLException{
		return dao.getTndmComment(id);
	}

	public int deleteTndmComment(Long id) throws SQLException{
		return dao.deleteTndmComment(id);
	}

	public int insertTndmComment(TndmComment arg) throws SQLException{
		return dao.insertTndmComment(arg);
	}

	public int updateTndmComment(TndmComment arg) throws SQLException{
		return dao.updateTndmComment(arg);
	}


    
}
