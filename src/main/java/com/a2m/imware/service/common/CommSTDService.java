package com.a2m.imware.service.common;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.CommSTDDAO;
import com.a2m.imware.model.util.ImwareException;

@Service
public class CommSTDService {

	@Autowired
	private CommSTDDAO dao;

	public List<Map<Object, Object>> getListComm(Map<Object, Object> agr) throws ImwareException {
		return dao.getListComm(agr);
	}

}
