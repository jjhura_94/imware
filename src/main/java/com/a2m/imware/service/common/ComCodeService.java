package com.a2m.imware.service.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.ComCodeDAO;

@Service
public class ComCodeService {
    
	@Autowired
	private ComCodeDAO dao;

    public List searchTccoStd(Map arg) throws SQLException{
		return dao.searchTccoStd(arg);
	}

	public Map getTccoStd(String id) throws SQLException{
		return dao.getTccoStd(id);
	}

	public int deleteTccoStd(String id) throws SQLException{
		return dao.deleteTccoStd(id);
	}

	public int insertTccoStd(Map arg) throws SQLException{
		return dao.insertTccoStd(arg);
	}

	public int updateTccoStd(Map arg) throws SQLException{
		return dao.updateTccoStd(arg);
	}


    public List getTccoStds(String upCommCd) {
		return dao.getTccoStds(upCommCd);
    }
}
