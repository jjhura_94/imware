package com.a2m.imware.service.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.ReferrerDAO;
import com.a2m.imware.model.apv.TndmReferrerFavorite;
import com.a2m.imware.model.apv.TndmReferrerFavoriteDetail;

@Service
public class ReferrerService {
    
	@Autowired
	private ReferrerDAO dao;
	
	public List<TndmReferrerFavorite> searchTndmReferrerFavorite(Map arg) throws SQLException {
		return dao.searchTndmReferrerFavorite(arg);
	}

	public Map getTndmReferrerFavorite(Long id) throws SQLException{
		return dao.getTndmReferrerFavorite(id);
	}

	public int deleteTndmReferrerFavorite(Long id) throws SQLException{
		return dao.deleteTndmReferrerFavorite(id);
	}
 
	public int insertTndmReferrerFavorite(TndmReferrerFavorite arg) throws SQLException{
		return dao.insertTndmReferrerFavorite(arg);
	}

	public int updateTndmReferrerFavorite(TndmReferrerFavorite arg) throws SQLException{
		return dao.updateTndmReferrerFavorite(arg);
	}
    
    
    
	public List searchTndmReferrerFavoriteDetail(Map arg) throws SQLException{
		return dao.searchTndmReferrerFavoriteDetail(arg);
	}
	public Map getTndmReferrerFavoriteDetail(Long id) throws SQLException{
		return dao.getTndmReferrerFavoriteDetail(id);
	}

	public int deleteTndmReferrerFavoriteDetail(Long id) throws SQLException{
		return dao.deleteTndmReferrerFavoriteDetail(id);
	}
 
	public int insertTndmReferrerFavoriteDetail(TndmReferrerFavoriteDetail arg) throws SQLException{
		return dao.insertTndmReferrerFavoriteDetail(arg);
	}

	public int updateTndmReferrerFavoriteDetail(TndmReferrerFavoriteDetail arg) throws SQLException{
		return dao.updateTndmReferrerFavoriteDetail(arg);
	}
    
}
