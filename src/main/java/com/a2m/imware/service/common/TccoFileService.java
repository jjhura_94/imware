package com.a2m.imware.service.common;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.TccoFileDAO;
import com.a2m.imware.model.TccoFile;
import com.a2m.imware.util.Utils;

@Service
public class TccoFileService {

    @Autowired
    private TccoFileDAO tccoFileDao;

    /**
     * Find a file by given file sequence.
     *
     * @param fileSequence
     * @return null if file does not exist in DB, else return a TccoFile
     * instance.
     * @throws Exception
     * @author HungDM
     */
    public TccoFile findBySequence(String fileSequence) throws Exception {
        if (Utils.isNullOrEmpty(fileSequence)) {
            return null;
        }
        Map<Object, Object> result = tccoFileDao.findBySequence(fileSequence);
        if (result == null) {
            return null;
        }

        return new TccoFile().fromMap(result);
    }

    /**
     * Find list of file by given file sequences.
     *
     * @param fileSequences
     * @return null if find nothing, else return a list of TccoFile instance.
     * @throws Exception
     * @author HungDM
     */
    public List<TccoFile> findBySequences(List<String> fileSequences) throws Exception {
        if (fileSequences == null || fileSequences.isEmpty()) {
            return null;
        }
        List<Map<Object, Object>> result = tccoFileDao.findBySequences(fileSequences);
        if (result == null) {
            return null;
        }

        // TODO: check and remove files does not exist on disk
        return result.stream().map(data -> {
            TccoFile file = new TccoFile().fromMap(data);
            return file;
        }).collect(Collectors.toList());
    }

    /**
     * Insert a new row into TCCO_FILE table by given data.
     *
     * @param data is a map keep data for column names in DB.
     * @return true if insert success, else return false.
     * @throws Exception
     * @author HungDM
     */
    public boolean insert(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return false;
        }
        int affected = tccoFileDao.insert(data);

        return affected > 0;
    }

    /**
     * Delete file from DB and disk by given file sequences.
     *
     * @param fileSequences
     * @return true if delete success, else return false.
     * @throws Exception
     * @author HungDM
     */
    public boolean delete(List<String> fileSequences) throws Exception {
        if (fileSequences == null || fileSequences.isEmpty()) {
            return false;
        }

        List<TccoFile> files = findBySequences(fileSequences);
        int affected = tccoFileDao.delete(fileSequences);
        if (affected > 0 && files != null && !files.isEmpty()) { // success => delete files from disk
            Utils.deleteFilesFromDisk(
                    files.stream().map(tccoFile -> tccoFile.getNewFleNm()).collect(Collectors.toList()));
        }

        return affected > 0;
    }

    // For testing...
    public List<Map<Object, Object>> findTcdsEmpAttaches(String refId) {
        return tccoFileDao.findTcdsEmpAttaches(refId);
    }

    public boolean insertToTcdsEmpAttach(Map<Object, Object> data) throws Exception {
        if (data == null || data.isEmpty()) {
            return false;
        }
        int affected = tccoFileDao.insertToTcdsEmpAttach(data);

        return affected > 0;
    }

    public boolean deleteFromTcdsEmpAttach(List<String> attachIdList) throws Exception {
        if (attachIdList == null || attachIdList.isEmpty()) {
            return false;
        }
        int affected = tccoFileDao.deleteFromTcdsEmpAttach(attachIdList);

        return affected > 0;
    }

    public int insertTccoFile(TccoFile tccoFile) {
        try {
            return tccoFileDao.insertTccoFile(tccoFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int updateTccoFile(TccoFile tccoFile) {
        try {
            return tccoFileDao.updateTccoFile(tccoFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public TccoFile saveTccoFile(TccoFile tccoFile) {
        try {
            TccoFile old = findBySequence(tccoFile.getAtchFleSeq());

            int recordExcute;

            if (old == null) {
                recordExcute = insertTccoFile(tccoFile);
            } else {
                recordExcute = updateTccoFile(tccoFile);
            }

            if (recordExcute == 0) {
                return null;
            }
            return findBySequence(tccoFile.getAtchFleSeq());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
