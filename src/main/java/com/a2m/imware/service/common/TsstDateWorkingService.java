package com.a2m.imware.service.common;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.common.TsstDateWorkingDAO;

@Service
public class TsstDateWorkingService {

	@Autowired
	private TsstDateWorkingDAO dao;

	public List<Map<Object, Object>> searchTsstDateWorking(Map<Object, Object> arg) throws SQLException {
		return dao.searchTsstDateWorking(arg);
	}

	public Map<Object, Object> getTsstDateWorking(Map<Object, Object> arg) throws SQLException {
		return dao.getTsstDateWorking(arg);

	}
}
