package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.sys.Sys0101DAO;
import com.a2m.imware.model.request.TsstMenuRequest;
import com.a2m.imware.model.request.TsstRoleMenuRequest;


@Service
public class Sys0101Service {
	
	private DecimalFormat myFormatter = new DecimalFormat("00");

	@Autowired
	private Sys0101DAO dao;
	
	public List getMenuByUser(Map arg) throws SQLException {
		return dao.getMenuByUser(arg);
	}
	
	public List getAllMenu() throws SQLException {
		return dao.getAllMenu();
	}
	
	public List search(Map arg) throws SQLException {
		return dao.searchTsstMenu(arg);
	}
	
	public int addMenu(TsstMenuRequest arg) throws SQLException{
		String menuId = getMenuId(arg);
		arg.setMenuId(menuId);
		arg.setCreatedBy(arg.getSessUserId());
		Integer maxOrdNo = getMaxOrdNo();
		maxOrdNo = maxOrdNo == null ? 1 : maxOrdNo+1;
		arg.setOrdNo(maxOrdNo);
		if(!StringUtils.isNotEmpty(arg.getUpMenuId())) {
			arg.setLev(1);
		}
		else {
			arg.setLev(2);
		}
		return dao.insertTsstMenu(arg);
	}
	
	public int updateMenu(TsstMenuRequest arg) throws SQLException{
		return dao.updateTsstMenu(arg);
	}
	
	public String getMenuId(TsstMenuRequest arg) throws SQLException{
		String upMenuId = arg.getUpMenuId();
		String menuId = "";
		if(StringUtils.isNotEmpty(upMenuId)) {
			String maxMenuId = getMaxMenuId(upMenuId);
			if(!StringUtils.isNotEmpty(maxMenuId)){
//				if cannot find upMenu then throw SQLException
				throw new SQLException();
			}
			else if(maxMenuId.length() == 6) {
				menuId = upMenuId + "_01";
			}
			else {
				menuId = upMenuId + "_" +  myFormatter.format(Integer.parseInt(maxMenuId.substring(8, 9))+1);
			}
		}
		else {
			String maxMenuId = getMaxMenuId(null);
			if(StringUtils.isNotEmpty(maxMenuId)){
				menuId = "MNU" + "_" +  myFormatter.format(Integer.parseInt(maxMenuId.substring(5, 6))+1);
			}
			else {
				menuId = "MNU_01";
			}
		}
		return menuId;
	}
	
	public String getMaxMenuId(String menuId) throws SQLException{
		return dao.getMaxMenuId(menuId);
	};
	
	public Integer getMaxOrdNo() throws SQLException{
		return dao.getMaxOrdNo();
	};
	
	public int activeOrBlockMenu(Map arg) throws SQLException {
		return dao.activeOrBlockMenu(arg);
	}
	
	public int deleteMenu(String meuId) throws SQLException {
		dao.deleteTsstRoleMenu(meuId);
		return dao.deleteTsstMenu(meuId);
	}
	
}
