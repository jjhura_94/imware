package com.a2m.imware.service.sys;

import com.a2m.imware.dao.sys.TsstUserRoleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

@Service
public class TsstUserRoleService {
    @Autowired
    private TsstUserRoleDAO dao;

    @Transactional(rollbackFor = Exception.class)
    public void deleteAllByUserUid(String userUid) throws SQLException {
        dao.deleteAllByUserUid(userUid);
    }
}
