package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010506DAO;
import com.a2m.imware.model.request.TcdsEmpSwRequest;
import com.a2m.imware.model.response.TcdsEmpSwResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010506Service {

    @Autowired
    private Sys010506DAO dao;

    public List getAllSwByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllSwByEmpNo(empNo);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }

    public void deleteAllBySwIds(List<Long> SW_IDS) {
        dao.deleteAllBySwIds(SW_IDS);
    }

    public int insert(TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpSwRequest.getSwType()) || Strings.isNullOrEmpty(tcdsEmpSwRequest.getProductName())
                || tcdsEmpSwRequest.getStartDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpSwRequest);
        return dao.insert(tcdsEmpSwRequest);
    }

    public int update(TcdsEmpSwRequest tcdsEmpSwRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpSwRequest.getSwType()) || Strings.isNullOrEmpty(tcdsEmpSwRequest.getProductName())
                || tcdsEmpSwRequest.getStartDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpSwRequest);
        return dao.update(tcdsEmpSwRequest);
    }

    public void convertDate(TcdsEmpSwRequest empSwRequest) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(empSwRequest.getStartDateLong());
        String dateTextfrom = dateFormatGmt.format(dateFrom);
        empSwRequest.setStartDateStr(dateTextfrom);
        if (empSwRequest.getExpiredDateLong() > 0) {
            Date dateTo = new Date(empSwRequest.getExpiredDateLong());
            String dateTextTo = dateFormatGmt.format(dateTo);
            empSwRequest.setExpiredDateStr(dateTextTo);
        }
    }

    public TcdsEmpSwResponse getById(Long id) throws SQLException {
        return dao.getById(id);
    }
}
