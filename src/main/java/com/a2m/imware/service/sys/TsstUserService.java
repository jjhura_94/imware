package com.a2m.imware.service.sys;

import com.a2m.imware.dao.sys.TsstUserDAO;
import com.a2m.imware.model.request.TsstUserRequest;
import com.a2m.imware.model.response.TsstUserResponse;
import com.a2m.imware.service.cms.Cms0102Service;
import com.a2m.imware.service.common.ComSeqService;
import com.a2m.imware.util.AjaxResult;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service
public class TsstUserService {
    
    @Autowired
    private TsstUserDAO dao;
    
    @Autowired
    private TsstUserRoleService tsstUserRoleService;
    
    @Autowired
    private ComSeqService comSeqService;
    
    @Autowired
    private Cms0102Service cms0102Service;
    
    public TsstUserResponse findFirstTsstUserByEmpNo(Long empNo) {
        return dao.findFirstTsstUserByEmpNo(empNo);
    }
    
    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return;
        }
        
        List<TsstUserResponse> tsstUserResponses = dao.findAllTsstUserByEmpNo(empNo);
        if (tsstUserResponses == null) {
            dao.deleteAllByEmpNo(empNo);
            return;
        }
        
        for (TsstUserResponse tsstUserResponse : tsstUserResponses) {
            tsstUserRoleService.deleteAllByUserUid(tsstUserResponse.getUserUid());
        }
        dao.deleteAllByEmpNo(empNo);
    }
    
    public int insert(TsstUserRequest tsstUserRequest) throws Exception {
        if (tsstUserRequest == null) {
            return 0;
        }
        tsstUserRequest.setUserUid(comSeqService.getSeq("SEQ_DOC_ID"));
        tsstUserRequest.setUseYn("Y");
        cms0102Service.insertVacationInfoWithNewEmp(tsstUserRequest);
        return dao.insert(tsstUserRequest);
    }
    
    public Object update(TsstUserRequest tsstUserRequest) throws Exception {
        AjaxResult ajaxResult = new AjaxResult();
        if (tsstUserRequest == null) {
            return 0;
        }

        //Validate data
        TsstUserResponse user = getByUserId(tsstUserRequest.getUserId());
        if (user == null) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("User not exist!");
            ajaxResult.setResponseData(AjaxResult.Code.FAILED);
            return ajaxResult;
        }
        BCryptPasswordEncoder b = new BCryptPasswordEncoder();
        if (tsstUserRequest.isChangePwd()) {
            if (Strings.isNullOrEmpty(tsstUserRequest.getOldPwdUpdate())) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("Old password not empty");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
                return ajaxResult;
            }
            if (!b.matches(tsstUserRequest.getOldPwdUpdate(), user.getPwd())) {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("Old password not match!");
                ajaxResult.setResponseData(AjaxResult.Code.FAILED);
                return ajaxResult;
            }
            tsstUserRequest.setPwd(b.encode(tsstUserRequest.getNewPwdUpdate()));
        }
        dao.update(tsstUserRequest);
        ajaxResult.setResponseData(tsstUserRequest);
        ajaxResult.setStatus(true);
        return ajaxResult;
    }
    
    public Long countAllByUserIdAndUserUidNot(String userId, String userUidNot) {
        return dao.countAllByUserIdAndUserUidNot(userId, userUidNot);
    }
    
    public Long countAllByUserIdAndPwd(String userId, String pwd) {
        return dao.countAllByUserIdAndPwd(userId, pwd);
    }
    
    public TsstUserResponse getByUserId(String userId) {
        return dao.getByUserId(userId);
    }
    
    public void setResponseError() {
        
    }
}
