package com.a2m.imware.service.sys.sys0105;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.a2m.imware.model.response.TcdsEmpMstResponse;
import com.a2m.imware.service.sys.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.a2m.imware.dao.sys.Sys0105DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.request.TcdsEmpMstRequest;
import com.a2m.imware.model.util.ImwareException;
import com.a2m.imware.service.abs.AbsService;
import com.a2m.imware.service.common.UserInfoService;
import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

@Service
public class Sys0105Service {

    @Autowired
    private Sys0105DAO dao;

    @Autowired
    private Sys010503Service sys010503Service;

    @Autowired
    private Sys010501Service sys010501Service;

    @Autowired
    private Sys010502Service sys010502Service;

    @Autowired
    private Sys010504Service sys010504Service;

    @Autowired
    private Sys010505Service sys010505Service;

    @Autowired
    private Sys010506Service sys010506Service;

    @Autowired
    private TsstUserService tsstUserService;
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private AbsService absService;

    public PageResponse search(Map arg) throws SQLException {
        PageResponse res = new PageResponse();
        if (arg.get("USER_IDS") != null && !Strings.isNullOrEmpty(arg.get("USER_IDS").toString())) {
            List<String> userIds = new ArrayList<String>(Arrays.asList(arg.get("USER_IDS").toString().split(", ")));
            arg.put("USER_IDS", userIds);
        }
        res.setDatas(dao.search(arg));
        res.setCount(dao.count(arg));
        return res;
    }

    public TcdsEmpMstResponse getEmpMstByUserUid(String userUid) throws SQLException {
        HashMap<Object, Object> params = new HashMap<>();
        params.put("userUid", userUid);
        List<?> empList = this.dao.search(params);
        if (empList != null && empList.size() > 0) {
            return (TcdsEmpMstResponse) empList.get(0);
        }

        return null;
    }

    public int insert(List<TcdsEmpMstRequest> tcdsEmpMstRequests) throws SQLException {
        if (tcdsEmpMstRequests == null || tcdsEmpMstRequests.size() == 0) {
            return 0;
        }
        return dao.insert(tcdsEmpMstRequests);
    }

    public int update(List<TcdsEmpMstRequest> tcdsEmpMstRequests) throws SQLException {
        if (tcdsEmpMstRequests == null || tcdsEmpMstRequests.size() == 0) {
            return 0;
        }
        return dao.update(tcdsEmpMstRequests);
    }

    public TcdsEmpMstResponse get(Long empNo) throws ImwareException, SQLException {
        TcdsEmpMstResponse tcdsEmpMstResponse = dao.get(empNo);
        if (tcdsEmpMstResponse != null && (!Strings.isNullOrEmpty(tcdsEmpMstResponse.getAbsentYn()) && tcdsEmpMstResponse.getAbsentYn().equals("Y"))) {
            Map<Object, Object> user = userInfoService.getUserById(tcdsEmpMstResponse.getEmpNo());
            if (user != null && !user.isEmpty()) {
                Map<Object, Object> absent = absService.findById(user.get("USER_UID").toString());
                if (absent != null && !absent.isEmpty()) {
                    tcdsEmpMstResponse.setAbsFromDate((Date) absent.get("ABS_FROM_DATE"));
                    tcdsEmpMstResponse.setAbsToDate((Date) absent.get("ABS_TO_DATE"));
                    tcdsEmpMstResponse.setAbsentUserId(absent.get("RPT_USER_UID").toString());
                }
            }
        }
        return tcdsEmpMstResponse;
    }

    @Transactional(rollbackFor = Exception.class)
    public int deleteAllByEmpNos(List<Long> empNos) throws SQLException {
        if (empNos == null) {
            return 0;
        }
        for (Long empNo : empNos) {
            sys010503Service.deleteAllByEmpNo(empNo);

            sys010501Service.deleteAllByEmpNo(empNo);

            sys010502Service.deleteAllByEmpNo(empNo);

            sys010504Service.deleteAllByEmpNo(empNo);

            sys010505Service.deleteAllByEmpNo(empNo);

            sys010506Service.deleteAllByEmpNo(empNo);

            tsstUserService.deleteAllByEmpNo(empNo);
        }

        return dao.deleteAllByEmpNos(empNos);
    }

    public long insert(TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException {
        tcdsEmpMstRequest.setStatus("Y");
        dao.insert(tcdsEmpMstRequest);
        return tcdsEmpMstRequest.getEmpNo();
    }

    public int update(TcdsEmpMstRequest tcdsEmpMstRequest) throws SQLException {
        if ((!Strings.isNullOrEmpty(tcdsEmpMstRequest.getAbsentYn()) && tcdsEmpMstRequest.getAbsentYn().equals("Y")) && !Strings.isNullOrEmpty(tcdsEmpMstRequest.getAbsentUserId())
                && tcdsEmpMstRequest.getAbsFromDate() != null
                && tcdsEmpMstRequest.getAbsToDate() != null) {
            absService.updateOrCreate(tcdsEmpMstRequest.getUserId(), tcdsEmpMstRequest.getAbsentUserId(),
                    tcdsEmpMstRequest.getAbsFromDate(), tcdsEmpMstRequest.getAbsToDate());
        }
        if (tcdsEmpMstRequest.getEndDate() != null) {
            tcdsEmpMstRequest.setStatus("N");
        } else {
            tcdsEmpMstRequest.setStatus("Y");
        }
        return dao.update(tcdsEmpMstRequest);
    }

    public void delete(Long empNo) throws SQLException {
        if (empNo == null) {
            return;
        }

        sys010503Service.deleteAllByEmpNo(empNo);

        sys010501Service.deleteAllByEmpNo(empNo);

        sys010502Service.deleteAllByEmpNo(empNo);

        sys010504Service.deleteAllByEmpNo(empNo);

        sys010505Service.deleteAllByEmpNo(empNo);

        sys010506Service.deleteAllByEmpNo(empNo);

        tsstUserService.deleteAllByEmpNo(empNo);

        dao.delete(empNo);
    }

    public List<Map<Object, Object>> searchForVacation() throws SQLException {
        return dao.searchForVacation(new HashMap<>());
    }

    public void updateImageOrSiganture(String userId, String image, int position) throws SQLException, ImwareException {
        TcdsEmpMstRequest empMstRequest = new TcdsEmpMstRequest();
        Map<Object, Object> user = userInfoService.getUserInfo(userId, null);
        if (user != null && !user.isEmpty()) {
            empMstRequest.setEmpNo(Long.parseLong(user.get("EMP_NO").toString()));
            if (position == 1) {
                empMstRequest.setImgPath(image);

            } else {
                empMstRequest.setSignaturePath(image);
            }
            update(empMstRequest);
        }
    }

}
