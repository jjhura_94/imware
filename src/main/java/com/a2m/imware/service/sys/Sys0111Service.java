package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.sys.Sys0111DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.response.TcdsEquipMstResponse;

@Service
public class Sys0111Service {

	@Autowired
	private Sys0111DAO dao;

	public PageResponse search(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		List<Map<Object, Object>> edusMap = (List<Map<Object, Object>>) dao.searchTcdsEquipmentMgt(arg);
		List<TcdsEquipMstResponse> rs = new ArrayList<TcdsEquipMstResponse>(0);
		for (Map<Object, Object> eduMap : edusMap) {
			if (eduMap != null && !eduMap.isEmpty()) {
				TcdsEquipMstResponse edu = new TcdsEquipMstResponse();
				try {
					edu.fromMap(eduMap);
					rs.add(edu);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		res.setDatas(rs);
		res.setCount(dao.count(arg));
		return res;
	}

	public Map<Object, Object> get(Map<Object, Object> arg) throws SQLException {
		return dao.getTcdsEquipmentMgt(arg);
	}

	public int create(TcdsEquipMstResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setCreateDateString(DateUtil.convertDateToStringDB(currentDate));
		arg.setCreatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.insertTcdsEquipmentMgt(data);
	}

	public int update(TcdsEquipMstResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setUpdatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.updateTcdsEquipmentMgt(data);
	}

	public int delete(TcdsEquipMstResponse arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return dao.deleteTcdsEquipmentMgt(data);
	}
}
