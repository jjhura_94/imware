package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010501DAO;
import com.a2m.imware.model.request.TcdsEmpEduRequest;
import com.a2m.imware.model.response.TcdsEmpEduResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010501Service {

    @Autowired
    private Sys010501DAO dao;
    
    public List getAllEduByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllEduByEmpNo(empNo);
    }
    
    public TcdsEmpEduResponse getEduById(Long id) throws SQLException {
        return dao.getEduById(id);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }
    
    public void deleteAllByEduIds(List<Long> eduIds) {
        dao.deleteAllByEduIds(eduIds);
    }
    
    public int insert(TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException {
        if(!validateEduData(tcdsEmpEduRequest)){
            return -1;
        }
        convertDate(tcdsEmpEduRequest);
        return dao.insert(tcdsEmpEduRequest);
    }
    
    public int update(TcdsEmpEduRequest tcdsEmpEduRequest) throws SQLException {
        if(!validateEduData(tcdsEmpEduRequest)){
            return -1;//false
        }
        convertDate(tcdsEmpEduRequest);
        return dao.update(tcdsEmpEduRequest);
    }
    
    public void convertDate(TcdsEmpEduRequest tcdsEmpEduRequest){
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(tcdsEmpEduRequest.getStartDateLong());
        String dateTextfrom = dateFormatGmt.format(dateFrom);
        Date dateTo = new Date(tcdsEmpEduRequest.getEndDateLong());
        String dateTextTo = dateFormatGmt.format(dateTo);
        tcdsEmpEduRequest.setStartDateStr(dateTextfrom);
        tcdsEmpEduRequest.setEndDateStr(dateTextTo);
    }
    
    public boolean validateEduData(TcdsEmpEduRequest input){
        return !(Strings.isNullOrEmpty(input.getEduLevel()) || input.getStartDateLong() <=0 || input.getEndDateLong()<=0 
                || Strings.isNullOrEmpty(input.getSchoolName()) || Strings.isNullOrEmpty(input.getMajorName())
                || Strings.isNullOrEmpty(input.getGraduationStatus()) || Strings.isNullOrEmpty(input.getDegree()));
    }
}
