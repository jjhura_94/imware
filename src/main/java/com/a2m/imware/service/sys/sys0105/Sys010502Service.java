package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010502DAO;
import com.a2m.imware.model.request.TcdsEmpCareerRequest;
import com.a2m.imware.model.response.TcdsEmpCareerResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010502Service {

    @Autowired
    private Sys010502DAO dao;

    public List getAllCareerByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllCareerByEmpNo(empNo);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }

    public void deleteAllByCareerIds(List<Long> careerIds) {
        dao.deleteAllByCareerIds(careerIds);
    }

    public int insert(TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException {
        if(Strings.isNullOrEmpty(tcdsEmpCareerRequest.getBusinessName()) || Strings.isNullOrEmpty(tcdsEmpCareerRequest.getPositionTitle())
                || Strings.isNullOrEmpty(tcdsEmpCareerRequest.getClient())
                || tcdsEmpCareerRequest.getStartDateLong() <=0 || tcdsEmpCareerRequest.getEndDateLong() <= 0){
            return -1;
        }
        this.convertDate(tcdsEmpCareerRequest);
        return dao.insert(tcdsEmpCareerRequest);
    }

    public int update(TcdsEmpCareerRequest tcdsEmpCareerRequest) throws SQLException {
        if(Strings.isNullOrEmpty(tcdsEmpCareerRequest.getBusinessName()) || Strings.isNullOrEmpty(tcdsEmpCareerRequest.getPositionTitle())
                || Strings.isNullOrEmpty(tcdsEmpCareerRequest.getClient())
                || tcdsEmpCareerRequest.getStartDateLong() <=0 || tcdsEmpCareerRequest.getEndDateLong() <= 0){
            return -1;
        }
        this.convertDate(tcdsEmpCareerRequest);
        return dao.update(tcdsEmpCareerRequest);
    }

    public void convertDate(TcdsEmpCareerRequest tcdsEmpCareerRequest) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(tcdsEmpCareerRequest.getStartDateLong());
        String dateTextfrom = dateFormatGmt.format(dateFrom);
        Date dateTo = new Date(tcdsEmpCareerRequest.getEndDateLong());
        String dateTextTo = dateFormatGmt.format(dateTo);
        tcdsEmpCareerRequest.setStartDateStr(dateTextfrom);
        tcdsEmpCareerRequest.setEndDateStr(dateTextTo);
    }

    public TcdsEmpCareerResponse getById(Long id) throws SQLException {
        return dao.getById(id);
    }
}
