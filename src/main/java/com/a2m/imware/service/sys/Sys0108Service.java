package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.dao.sys.Sys0108DAO;
import com.a2m.imware.model.sys.TcdsDeptStruct;
import com.a2m.imware.model.sys.TcdsStructMgt;

@Service
public class Sys0108Service {
	@Autowired
	private Sys0108DAO dao;

	public List<TcdsStructMgt> searchTcdsStructMgt(Map<Object, Object> arg) throws SQLException {
		return dao.searchTcdsStructMgt(arg);
	}

	public TcdsStructMgt getTcdsStructMgt(String id) throws SQLException {
		return dao.getTcdsStructMgt(id);
	}

	public TcdsStructMgt getActiveStructMgt() throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("useYn", ImwareStringUtils.YES);
		// Expect only one active STRUCT
		List<TcdsStructMgt> activeStruct = dao.searchTcdsStructMgt(params);
		if (activeStruct != null && activeStruct.size() > 0) {
			return activeStruct.get(0);
		} else
			return null;
	}

	public List<TcdsDeptStruct> getActiveDeptStruct() throws SQLException {
		TcdsStructMgt activeStructMgt = this.getActiveStructMgt();
		if (activeStructMgt != null) {
			Map<Object, Object> params = new HashMap<>();
			params.put("structId", activeStructMgt.getStructId());
			return this.searchTcdsDeptStruct(params);

		} else {
			return new ArrayList<TcdsDeptStruct>();
		}
	}

	public Long countActiveStruct(String excludeStructId) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("useYn", ImwareStringUtils.YES);
		params.put("excludeStructId", excludeStructId);
		return this.dao.countStructMgt(params);
	}

	public int deleteTcdsStructMgt(String id) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("structId", id);
		this.deleteTcdsDeptStruct(params);
		return dao.deleteTcdsStructMgt(id);
	}

	public int insertTcdsStructMgt(TcdsStructMgt struct) throws SQLException {
		struct.setStructId(this.getStructMaxId());
		struct.setCreatedDate(new Date());
		return dao.insertTcdsStructMgt(struct);
	}

	public int updateTcdsStructMgt(TcdsStructMgt arg) throws SQLException {
		return dao.updateTcdsStructMgt(arg);
	}

	public String getStructMaxId() throws SQLException {
		DecimalFormat myFormatter = new DecimalFormat("0000");
		String maxId = dao.getStructMaxId();
		return ImwareStringUtils.genNextId(maxId, myFormatter, ImwareStringUtils.STRUCT_ID_PREFIX);
	}

	public void cloneStruct(TcdsStructMgt struct) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("structId", struct.getStructId());
		List<TcdsDeptStruct> tcdsDeptStructList = this.searchTcdsDeptStruct(params);

		struct.setCreatedDate(new Date());
		struct.setStructId(this.getStructMaxId());
		dao.insertTcdsStructMgt(struct);

		if (tcdsDeptStructList != null) {
			for (TcdsDeptStruct tcdsDeptStruct : tcdsDeptStructList) {
				tcdsDeptStruct.setStructId(struct.getStructId());
				this.insertTcdsDeptStruct(tcdsDeptStruct);
			}
		}
	}

	public List<TcdsDeptStruct> searchTcdsDeptStruct(Map<Object, Object> arg) throws SQLException {
		return dao.searchTcdsDeptStruct(arg);
	}

	public Long countTcdsDeptStruct(Map<Object, Object> arg) throws SQLException {
		return dao.countTcdsDeptStruct(arg);
	}

	public List<TcdsDeptStruct> getTcdsDeptStruct(Map<Object, Object> id) throws SQLException {
		return dao.getTcdsDeptStruct(id);
	}

	public TcdsDeptStruct getTcdsOneDeptStructName(Map<Object, Object> arg) throws SQLException {
		return dao.getTcdsOneDeptStructName(arg);
	}

	public int deleteTcdsDeptStruct(Map<Object, Object> arg) throws SQLException {
		return dao.deleteTcdsDeptStruct(arg);
	}

	public int insertTcdsDeptStruct(TcdsDeptStruct deptStruct) throws SQLException {
		if (ImwareStringUtils.isEmpty(deptStruct.getDeptCode())) {
			String deptCode = this.getDeptMaxId();
			deptStruct.setDeptCode(deptCode);
			dao.insertTcdsDept(deptCode);
		}
		return dao.insertTcdsDeptStruct(deptStruct);
	}

	public int updateTcdsDeptStruct(TcdsDeptStruct dept) throws SQLException {
		return dao.updateTcdsDeptStruct(dept);
	}

	public void checkAndDeleteTcdsDept(String deptCode) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("deptCode", deptCode);
		Long deptCount = this.countTcdsDeptStruct(params);
		if (deptCount == 0) {
			Long empCount = dao.countEmpByDeptCode(params);
			if (empCount == 0) {
				dao.deleteTcdsDept(params);
			}
		}

	}

	public int insertTcdsDept(String id) throws SQLException {
		return dao.insertTcdsDept(id);
	}

	public String getDeptMaxId() throws SQLException {
		DecimalFormat myFormatter = new DecimalFormat("0000");
		String maxId = dao.getDeptMaxId();
		return ImwareStringUtils.genNextId(maxId, myFormatter, ImwareStringUtils.DEPT_ID_PREFIX);
	}

	public List getDeptStruct(Map arg) throws SQLException {
		TcdsStructMgt activeStructMgt = this.getActiveStructMgt();
		if (activeStructMgt != null) {
			arg.put("STRUCT_ID", activeStructMgt.getStructId());

		}
		return dao.getDeptStruct(arg);
	}

	public Map<Object, Object> getTcdsOneDeptStructName2(Map<Object, Object> arg) throws SQLException {
		return dao.getTcdsOneDeptStructName2(arg);
	}
}
