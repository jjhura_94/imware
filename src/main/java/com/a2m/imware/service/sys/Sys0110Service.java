package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.sys.Sys0110DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.response.TcdsRoomMstResponse;

@Service
public class Sys0110Service {

	@Autowired
	private Sys0110DAO dao;

	public PageResponse search(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		List<Map<Object, Object>> edusMap = (List<Map<Object, Object>>) dao.searchTcdsMeetingRoomMgt(arg);
		List<TcdsRoomMstResponse> rs = new ArrayList<TcdsRoomMstResponse>(0);
		for (Map<Object, Object> eduMap : edusMap) {
			if (eduMap != null && !eduMap.isEmpty()) {
				TcdsRoomMstResponse edu = new TcdsRoomMstResponse();
				try {
					edu.fromMap(eduMap);
					rs.add(edu);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		res.setDatas(rs);
		res.setCount(dao.count(arg));
		return res;
	}

	public Map<Object, Object> get(Map<Object, Object> arg) throws SQLException {
		return dao.getTcdsMeetingRoomMgt(arg);
	}

	@SuppressWarnings("unchecked")
	public Object save(Map<Object, Object> arg) {
		String returnVal = "N";
		String userUid = (String) arg.get("SESS_USER_ID");
		List<Map<Object, Object>> grid = (List<Map<Object, Object>>) arg.get("grid");
		Date date = new Date();
		try {
			for (Map<Object, Object> map : grid) {
				String crud = (String) map.get("CRUD");
				if ("C".equals(crud)) {
					map.put("CREATED_BY", userUid);
					map.put("CREATED_DATE", DateUtil.convertDateToStringDB(date));
					map.put("UPDATED_BY", userUid);
					dao.insertTcdsMeetingRoomMgt(map);
				} else if ("U".equals(crud)) {
					map.put("UPDATED_BY", userUid);
					map.put("UPDATED_DATE",DateUtil.convertDateToStringDB(date));
					dao.updateTcdsMeetingRoomMgt(map);
				} else if ("D".equals(crud)) {
					dao.deleteTcdsMeetingRoomMgt(map);
				}
			}
			returnVal = "Y";
		} catch (SQLException e) {
			e.printStackTrace();
			return returnVal;
		}
		return returnVal;
	}
	
	public int create(TcdsRoomMstResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setCreateDateString(DateUtil.convertDateToStringDB(currentDate));
		arg.setCreatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.insertTcdsMeetingRoomMgt(data);
	}

	public int update(TcdsRoomMstResponse arg) throws SQLException {
		Date currentDate = new Date();
		arg.setUpdatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.updateTcdsMeetingRoomMgt(data);
	}

	public int delete(TcdsRoomMstResponse arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return dao.deleteTcdsMeetingRoomMgt(data);
	}
}
