package com.a2m.imware.service.sys;


import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.sys.Sys0103DAO;
import com.a2m.imware.model.TsstUserRole;
import com.a2m.imware.model.request.TsstRoleMenuRequest;
import com.a2m.imware.model.request.TsstRoleRequest;
import com.a2m.imware.model.response.TsstRoleMenuResponse;

import java.util.List;

@Service
public class Sys0103Service {
    
	@Autowired
	private Sys0103DAO dao;
	
	private DecimalFormat myFormatter = new DecimalFormat("000");

    public List searchTsstRole(Map arg) throws SQLException{
		return dao.searchTsstRole(arg);
	}

	public Map getTsstRole(String id) throws SQLException{
		return dao.getTsstRole(id);
	}

	public int deleteTsstRole(String id) throws SQLException{
		return dao.deleteTsstRole(id);
	}

	public int insertTsstRole(TsstRoleRequest arg) throws SQLException{
		arg.setRoleId(getMaxRoleId());
		return dao.insertTsstRole(arg);
	}

	public int updateTsstRole(TsstRoleRequest arg) throws SQLException{
		return dao.updateTsstRole(arg);
	}

	public String getMaxRoleId() throws SQLException{
		String maxRoleId = dao.getMaxRoleId();
		maxRoleId = maxRoleId.replaceAll("R", "");
		String newRoleId = "R" + myFormatter.format((Integer.parseInt(maxRoleId)+1));
		return newRoleId;
	}

	public int deleteTsstRoles(String ids) throws SQLException{
		return dao.deleteTsstRoles(ids);
	}
	
	public List searchTsstUserRole(Map arg) throws SQLException{
		return dao.searchTsstUserRole(arg);
	}
	
	public int updateTsstUserRole(TsstUserRole userRole) throws SQLException{
		return dao.updateTsstUserRole(userRole);
	}
	
	public int insertTsstUserRole(TsstUserRole userRole) throws SQLException{
		return dao.insertTsstUserRole(userRole);
	}
	
	public int deleteTsstUserRole(TsstUserRole userRole) throws SQLException{
		return dao.deleteTsstUserRole(userRole);
	}
	
	public List searchTsstRoleMenu(Map arg) throws SQLException{
		return dao.searchTsstRoleMenu(arg);
	}
    
	public TsstRoleMenuResponse getTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException{
		return dao.getTsstRoleMenu(arg);
	}
	
	public int updateTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException{
		return dao.updateTsstRoleMenu(arg);
	}
    
	public int insertTsstRoleMenu(TsstRoleMenuRequest arg) throws SQLException{
		return dao.insertTsstRoleMenu(arg);
	}
}
