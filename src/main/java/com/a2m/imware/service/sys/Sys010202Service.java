package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.DateUtil;
import com.a2m.imware.dao.sys.Sys010202DAO;
import com.a2m.imware.model.request.PageResponse;
import com.a2m.imware.model.sys.TcdsEmpEdu;

@Service
public class Sys010202Service {

	@Autowired
	private Sys010202DAO dao;

	public PageResponse getAllEdu(Map<Object, Object> arg) throws SQLException {
		PageResponse res = new PageResponse();
		
		List<Map<Object, Object>> edusMap = (List<Map<Object, Object>>) dao.getAllEdu(arg);
		List<TcdsEmpEdu> rs = new ArrayList<TcdsEmpEdu>(0);
		for (Map<Object, Object> eduMap : edusMap) {
			if (eduMap != null && !eduMap.isEmpty()) {
				TcdsEmpEdu edu = new TcdsEmpEdu();
				try {
					edu.fromMap(eduMap);
					rs.add(edu);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		res.setDatas(rs);
		res.setCount(dao.count(arg));
		return res;
	}

	public TcdsEmpEdu getEduInfo(Map<Object, Object> arg) throws SQLException {
		Map<Object, Object> eduMap = (Map<Object, Object>) dao.getEduInfo(arg);
		TcdsEmpEdu edu = new TcdsEmpEdu();
		try {
			edu.fromMap(eduMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return edu;
	}

	public int addEdu(TcdsEmpEdu arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartDateString(DateUtil.convertDateToStringDB(arg.getStartDate()));
		arg.setEndDateString(DateUtil.convertDateToStringDB(arg.getEndDate()));
		arg.setCreateDateString(DateUtil.convertDateToStringDB(currentDate));
		arg.setCreatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.addEdu(data);
	}

	public int updateEdu(TcdsEmpEdu arg) throws SQLException {
		Date currentDate = new Date();
		arg.setStartDateString(DateUtil.convertDateToStringDB(arg.getStartDate()));
		arg.setEndDateString(DateUtil.convertDateToStringDB(arg.getEndDate()));
		arg.setUpdatedBy(arg.getSessUserId());
		arg.setUpdatedDateString(DateUtil.convertDateToStringDB(currentDate));
		Map<Object, Object> data = arg.toMap();
		return dao.updateEdu(data);
	}

	public int deleteEdu(TcdsEmpEdu arg) throws SQLException {
		Map<Object, Object> data = arg.toMap();
		return dao.deleteEdu(data);
	}
}
