package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010503DAO;
import com.a2m.imware.model.request.TcdsEmpAwardRequest;
import com.a2m.imware.model.request.TcdsEmpCareerRequest;
import com.a2m.imware.model.response.TcdsEmpAwardResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010503Service {

    @Autowired
    private Sys010503DAO dao;

    public List getAllAwardByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllAwardByEmpNo(empNo);
    }

    public void deleteAllByAwardIds(List<Long> awardIds) {
        dao.deleteAllByAwardIds(awardIds);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }

    public long insert(TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException, Exception {
        if (Strings.isNullOrEmpty(tcdsEmpAwardRequest.getAwardName()) || Strings.isNullOrEmpty(tcdsEmpAwardRequest.getAwardOrg())
                || tcdsEmpAwardRequest.getAwardDateLong() <=0 ) {
            return -1;
        }
        this.convertDate(tcdsEmpAwardRequest);
        dao.insert(tcdsEmpAwardRequest);
        return tcdsEmpAwardRequest.getAwardId();
    }

    public int update(TcdsEmpAwardRequest tcdsEmpAwardRequest) throws SQLException, Exception {
        if (Strings.isNullOrEmpty(tcdsEmpAwardRequest.getAwardName()) || Strings.isNullOrEmpty(tcdsEmpAwardRequest.getAwardOrg())
                || tcdsEmpAwardRequest.getAwardDateLong() <=0 ) {
            return -1;
        }
        this.convertDate(tcdsEmpAwardRequest);
        return dao.update(tcdsEmpAwardRequest);
    }

    public void convertDate(TcdsEmpAwardRequest tcdsEmpAwardRequest) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(tcdsEmpAwardRequest.getAwardDateLong());
        String dateTextfrom = dateFormatGmt.format(dateFrom);
        tcdsEmpAwardRequest.setAwardDateStr(dateTextfrom);
    }

    public TcdsEmpAwardResponse getById(Long id) throws SQLException {
        return dao.getById(id);
    }

    public void updateAttachment(long awardId, String atchFleSeq) throws SQLException {
        TcdsEmpAwardRequest tcdsEmpAwardRequest = new TcdsEmpAwardRequest();
        tcdsEmpAwardRequest.setFileSeq(atchFleSeq);
        tcdsEmpAwardRequest.setAwardId(awardId);
        dao.update(tcdsEmpAwardRequest);
    }
}
