package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.sys.Sys0102DAO;

@Service
public class Sys0102Service {

	@Autowired
	private Sys0102DAO dao;
	
	private BCrypt bcrypt = new BCrypt();
	
	public List getUserInfo(Map arg) throws SQLException {
		return dao.getUserInfo(arg);
	}
	
	public List searchUser(Map arg) throws SQLException {
		return dao.searchUser(arg);
	}
	
	public int addUser(Map arg) throws SQLException {
		String password = new Random().ints(10, 33, 122).mapToObj(i -> String.valueOf((char)i)).collect(Collectors.joining());
		String genPw = bcrypt.hashpw(password, BCrypt.gensalt(10));
		arg.put("USER_UID", new Date().getTime()+"");
		arg.put("PWD", genPw);
		arg.put("USE_YN", "Y");
		return dao.addUser(arg);
	}
	
	public int updateUser(Map arg) throws SQLException {
		return dao.updateUser(arg);
	}
	
	public int activeOrBlockUser(Map arg) throws SQLException {
		return dao.activeOrBlockUser(arg);
	}
}
	
