package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010505DAO;
import com.a2m.imware.model.request.TcdsEmpEquipRequest;
import com.a2m.imware.model.response.TcdsEmpEquipResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010505Service {

    @Autowired
    private Sys010505DAO dao;

    public List getAllEquipmentByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllEquipmentByEmpNo(empNo);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }

    public void deleteAllByEquipmentIds(List<Long> equipIds) {
        dao.deleteAllByEquipmentIds(equipIds);
    }

    public int insert(TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpEquipRequest.getIpAddress()) || tcdsEmpEquipRequest.getPurchaseDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpEquipRequest);
        return dao.insert(tcdsEmpEquipRequest);
    }

    public int update(TcdsEmpEquipRequest tcdsEmpEquipRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpEquipRequest.getIpAddress()) || tcdsEmpEquipRequest.getPurchaseDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpEquipRequest);
        return dao.update(tcdsEmpEquipRequest);
    }

    public void convertDate(TcdsEmpEquipRequest empEquipRequest) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(empEquipRequest.getPurchaseDateLong());
        String purchaseDate = dateFormatGmt.format(dateFrom);
        empEquipRequest.setPurchaseDateStr(purchaseDate);
    }

    public TcdsEmpEquipResponse getById(Long id) throws SQLException {
        return dao.getById(id);
    }
}
