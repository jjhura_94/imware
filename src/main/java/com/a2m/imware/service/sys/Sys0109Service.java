package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.dao.sys.Sys0109DAO;
import com.a2m.imware.model.sys.TcdsDutyMgt;

@Service
public class Sys0109Service {
	@Autowired
	private Sys0109DAO dao;

    public List<Object> searchTcdsDutyMgt(Map<Object, Object> arg) throws SQLException{
		return dao.searchTcdsDutyMgt(arg);
	}
    
    public Integer countTcdsDutyMgt(Map<Object, Object> arg) throws SQLException{
    	return dao.countTcdsDutyMgt(arg);
    }

	public TcdsDutyMgt getTcdsDutyMgt(String id) throws SQLException{
		return dao.getTcdsDutyMgt(id);
	}

	public int deleteTcdsDutyMgt(String id) throws SQLException{
		return dao.deleteTcdsDutyMgt(id);
	}

	public int insertTcdsDutyMgt(TcdsDutyMgt duty) throws SQLException{
		duty.setCreatedDate(new Date());
		duty.setDutyCode(getMaxId());
		return dao.insertTcdsDutyMgt(duty);
	}

	public int updateTcdsDutyMgt(TcdsDutyMgt duty) throws SQLException{
		duty.setUpdatedDate(new Date());
		return dao.updateTcdsDutyMgt(duty);
	}
	
	public String getMaxId() throws SQLException{
		DecimalFormat myFormatter = new DecimalFormat("000");
		String maxId = dao.getMaxId();
		return ImwareStringUtils.genNextId(maxId, myFormatter, ImwareStringUtils.DUTY_ID_PREFIX);
	}
	
    public Long countEmpByDutyCode(Map<Object, Object> arg) throws SQLException {
    	return this.dao.countEmpByDutyCode(arg);
    }
}
