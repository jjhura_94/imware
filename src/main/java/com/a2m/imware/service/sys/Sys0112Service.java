package com.a2m.imware.service.sys;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.common.ImwareStringUtils;
import com.a2m.imware.dao.sys.Sys0112DAO;
import com.a2m.imware.model.sys.TndmFormMgt;

@Service
public class Sys0112Service {
	
	@Autowired
	private Sys0112DAO dao;
	
	public List<TndmFormMgt> searchTndmFormMgt(Map<Object, Object> arg) throws SQLException {
		return this.dao.searchTndmFormMgt(arg);
	}
	
    public int getCount(Map<Object, Object> arg) throws SQLException {
    	return this.dao.getCount(arg);
    }
    
    public int countActiveFormByType(String formType, Long excludeId) throws SQLException {
    	Map<Object, Object> params = new HashMap<>();
    	params.put("useYn", ImwareStringUtils.YES);
    	params.put("formType", formType);
    	params.put("excludeId", excludeId);
    	return this.dao.getCount(params);
    }
    
    public TndmFormMgt getTndmFormMgt(Long id) throws SQLException {
    	return this.dao.getTndmFormMgt(id);
    }
    
    public TndmFormMgt getActiveTndmFormMgtBy(String formType) throws SQLException {
    	if(ImwareStringUtils.isNotEmpty(formType)) {
        	Map<Object, Object> params = new HashMap<>();
        	params.put("useYn", ImwareStringUtils.YES);
        	params.put("formType", formType);
        	List<TndmFormMgt> formMgtList = this.dao.searchTndmFormMgt(params);
        	if(formMgtList != null && formMgtList.size() > 0) {
        		return formMgtList.get(0);
        	}
    	}
    	
    	return null;
    }

    public int deleteTndmFormMgt(Long id) throws SQLException {
    	return this.dao.deleteTndmFormMgt(id);
    }
 
    public int insertTndmFormMgt(TndmFormMgt arg) throws SQLException {
    	return this.dao.insertTndmFormMgt(arg);
    }

    public int updateTndmFormMgt(TndmFormMgt form) throws SQLException {
    	return this.dao.updateTndmFormMgt(form);
    }
}
