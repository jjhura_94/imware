package com.a2m.imware.service.sys.sys0105;

import com.a2m.imware.dao.sys.Sys010504DAO;
import com.a2m.imware.model.request.TcdsEmpAwardRequest;
import com.a2m.imware.model.request.TcdsEmpCertRequest;
import com.a2m.imware.model.response.TcdsEmpAwardResponse;
import com.a2m.imware.model.response.TcdsEmpCertResponse;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Sys010504Service {

    @Autowired
    private Sys010504DAO dao;

    public List getAllCertificateByEmpNo(Long empNo) throws SQLException {
        if (empNo == null) {
            return new ArrayList();
        }
        return dao.getAllCertificateByEmpNo(empNo);
    }

    public void deleteAllByEmpNo(Long empNo) throws SQLException {
        dao.deleteAllByEmpNo(empNo);
    }

    public void deleteAllByCertificateIds(List<Long> certIds) {
        dao.deleteAllByCertificateIds(certIds);
    }

    public long insert(TcdsEmpCertRequest tcdsEmpCertRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpCertRequest.getQualification()) || Strings.isNullOrEmpty(tcdsEmpCertRequest.getPublisher())
                || tcdsEmpCertRequest.getAcquisitionDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpCertRequest);
        dao.insert(tcdsEmpCertRequest);
        return tcdsEmpCertRequest.getCertId();
    }

    public int update(TcdsEmpCertRequest tcdsEmpCertRequest) throws SQLException {
        if (Strings.isNullOrEmpty(tcdsEmpCertRequest.getQualification()) || Strings.isNullOrEmpty(tcdsEmpCertRequest.getPublisher())
                || tcdsEmpCertRequest.getAcquisitionDateLong() <= 0) {
            return -1;
        }
        this.convertDate(tcdsEmpCertRequest);
        return dao.update(tcdsEmpCertRequest);
    }

    public void convertDate(TcdsEmpCertRequest tcdsEmpCertRequest) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateFrom = new Date(tcdsEmpCertRequest.getAcquisitionDateLong());
        String dateTextfrom = dateFormatGmt.format(dateFrom);
        tcdsEmpCertRequest.setAcquisitionDateStr(dateTextfrom);
    }

    public TcdsEmpCertResponse getById(Long id) throws SQLException {
        return dao.getById(id);
    }

    public void updateAttachment(long certId, String atchFleSeq) throws SQLException {
        TcdsEmpCertRequest empCertRequest = new TcdsEmpCertRequest();
        empCertRequest.setFileSeq(atchFleSeq);
        empCertRequest.setCertId(certId);
        dao.update(empCertRequest);
    }
}
