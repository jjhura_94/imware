package com.a2m.imware.service.sys;


import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.sys.Sys0106DAO;

import java.util.List;

@Service
public class Sys0106Service {
    
	@Autowired
	private Sys0106DAO dao;

    public List searchTsstRole(Map arg) throws SQLException{
		return dao.searchTsstRole(arg);
	}

	public Map getTsstRole(String id) throws SQLException{
		return dao.getTsstRole(id);
	}

	public int deleteTsstRole(String id) throws SQLException{
		return dao.deleteTsstRole(id);
	}

	public int insertTsstRole(Map arg) throws SQLException{
		return dao.insertTsstRole(arg);
	}

	public int updateTsstRole(Map arg) throws SQLException{
		return dao.updateTsstRole(arg);
	}


    
}
