package com.a2m.imware.service.task;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.a2m.imware.dao.task.Task0101DAO;
import com.a2m.imware.model.task.TndmTaskAttach;
import com.a2m.imware.model.task.TndmTaskChecklist;
import com.a2m.imware.model.task.TndmTaskComment;
import com.a2m.imware.model.task.TndmTaskCommentAttach;
import com.a2m.imware.model.task.TndmTaskMgt;
import com.a2m.imware.model.task.TndmTaskRecipient;
import com.a2m.imware.model.task.TndmTaskReferrer;
import com.google.common.base.Strings;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

@Service
public class Task0101Service {

	@Autowired
	private Task0101DAO dao;
	
    public List<TndmTaskMgt> searchTndmTaskMgt(Map<Object, Object> arg) throws SQLException, ParseException {
        
        if ((arg.get("createTimeFrom") != null && arg.get("createTimeTo") != null) && !Strings.isNullOrEmpty(arg.get("createTimeFrom").toString()) && !Strings.isNullOrEmpty(arg.get("createTimeTo").toString())) {
            arg.put("TIME_TYPE", 1);
        } else if ((arg.get("createTimeFrom") != null && arg.get("createTimeTo") != null) && !Strings.isNullOrEmpty(arg.get("createTimeFrom").toString()) && Strings.isNullOrEmpty(arg.get("createTimeTo").toString())) {
            arg.put("TIME_TYPE", 2);

        } else if ((arg.get("createTimeFrom") != null && arg.get("createTimeTo") != null) && Strings.isNullOrEmpty(arg.get("createTimeFrom").toString()) && !Strings.isNullOrEmpty(arg.get("createTimeTo").toString())) {
            arg.put("TIME_TYPE", 3);
        }
        if (arg.get("createTimeTo") != null && !Strings.isNullOrEmpty(arg.get("createTimeTo").toString())) {
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            Date d = f.parse(arg.get("createTimeTo").toString());
            long milliseconds = d.getTime() + 86400000L;
            d = new Date(milliseconds);
            arg.put("createTimeTo", f.format(d));
        }
        
        if (arg.get("USER_IDS") != null && !Strings.isNullOrEmpty(arg.get("USER_IDS").toString())) {
            List<String> userIds = new ArrayList<String>(Arrays.asList(arg.get("USER_IDS").toString().split(", ")));
            arg.put("USER_IDS", userIds);
        }
        List<TndmTaskMgt> taskMgts = this.dao.searchTndmTaskMgt(arg);
        for (TndmTaskMgt taskMgt : taskMgts) {
            taskMgt.setAttachmentCount(dao.countAttachment(taskMgt.getId()));
        }
    	return taskMgts;
    }
    
    
    public TndmTaskMgt getById(Long id) throws SQLException{
        TndmTaskMgt taskMgt = dao.getTndmTaskMgt(id);
        if(taskMgt != null){
            Map<Object, Object>  params = new HashMap();
            params.put("taskId" , taskMgt.getId());
            taskMgt.setAttachment(dao.searchTndmTaskAttach(params));
            taskMgt.setRecipients(this.searchTndmTaskRecipient(params));
            taskMgt.setReferrers(this.searchTndmTaskReferrer(params));
        }
        return taskMgt;
    }
    
    public Integer countTndmTaskMgt(Map<Object, Object> arg) throws SQLException {
    	return this.dao.countTndmTaskMgt(arg);
    }
    
    public int insertTndmTaskMgt(TndmTaskMgt task) throws SQLException {
    	task.setCreatedDate(new Date());
    	return this.dao.insertTndmTaskMgt(task);
    }
    
    public int deleteTndmTaskMgt(Long taskId) throws SQLException {
    	dao.deleteTndmTaskChecklist(taskId);
    	this.dao.deleteTndmTaskAttachByTaskId(taskId);
    	this.deleteTndmTaskCommentByTaskId(taskId);
    	this.dao.deleteTndmTaskRecipientByTaskId(taskId);
    	this.dao.deleteTndmTaskReferrerByTaskId(taskId);
    	return this.dao.deleteTndmTaskMgt(taskId);
    }
    

    public int updateTndmTaskMgt(TndmTaskMgt arg) throws SQLException {
    	arg.setUpdatedDate(new Date());
    	return this.dao.updateTndmTaskMgt(arg);
    }
    
    
    
    // Task referrer
    public int insertTndmTaskReferrer(TndmTaskReferrer arg) throws SQLException {
    	return this.dao.insertTndmTaskReferrer(arg);
    }
    
    public List<TndmTaskReferrer> searchTndmTaskReferrer(Map<Object, Object> arg) throws SQLException {
    	return this.dao.searchTndmTaskReferrer(arg);
    }


    public int deleteTndmTaskReferrer(Long id) throws SQLException {
    	return this.dao.deleteTndmTaskReferrer(id);
    }
 

    public int updateTndmTaskReferrer(TndmTaskReferrer arg) throws SQLException {
    	return this.updateTndmTaskReferrer(arg);
    }
    
    
    
    // Task recipient
    public int insertTndmTaskRecipient(TndmTaskRecipient arg) throws SQLException {
    	return this.dao.insertTndmTaskRecipient(arg);
    }
    public List<TndmTaskRecipient> searchTndmTaskRecipient(Map<Object, Object> arg) throws SQLException {
    	return this.dao.searchTndmTaskRecipient(arg);
    }

    public int deleteTndmTaskRecipient(Long id) throws SQLException {
    	return this.dao.deleteTndmTaskRecipient(id);
    }
 

    public int updateTndmTaskRecipient(TndmTaskRecipient arg) throws SQLException {
    	return this.dao.updateTndmTaskRecipient(arg);
    }
    
    
    
    
    // Task Attach
    public List<TndmTaskAttach> searchTndmTaskAttach(Map<Object, Object> arg) throws SQLException {
    	return this.dao.searchTndmTaskAttach(arg);
    }


    public int deleteTndmTaskAttach(String seq) throws SQLException {
    	return this.dao.deleteTndmTaskAttach(seq);
    }
 
    public int insertTndmTaskAttach(TndmTaskAttach arg) throws SQLException {
    	return this.dao.insertTndmTaskAttach(arg);
    }
    
    
    
    // Task Comment 
    public List<TndmTaskComment> searchTndmTaskComment(Map<Object, Object> arg) throws SQLException {
    	return this.dao.searchTndmTaskComment(arg);
    }

    public TndmTaskComment getTndmTaskComment(Long id) throws SQLException {
    	return this.dao.getTndmTaskComment(id);
    }

    public int deleteTndmTaskComment(Long id) throws SQLException {
    	return this.dao.deleteTndmTaskComment(id);
    }
    
    public int deleteTndmTaskComment(TndmTaskComment comment) throws SQLException {
    	if(comment.getUpCommentId() == null) {
    		this.dao.deleteTndmTaskCommentAttachByCommentId(comment.getCommentId());
    		this.dao.deleteTndmTaskCommentByUpComId(comment.getCommentId());
    	}

    	return this.deleteTndmTaskComment(comment.getCommentId());

    }
    
    public int deleteTndmTaskCommentByTaskId(Long taskId) throws SQLException {
		Map<Object, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("level", 1);
		List<TndmTaskComment> level1Comments = this.searchTndmTaskComment(params);
		
		for(TndmTaskComment level1Comment: level1Comments) {
			this.dao.deleteTndmTaskCommentAttachByCommentId(level1Comment.getCommentId());
		}
    	return this.dao.deleteTndmTaskCommentByTaskId(taskId);
    }
    
 
    public int insertTndmTaskComment(TndmTaskComment comment) throws SQLException {
    	comment.setCreatedDate(new Date());
    	return this.dao.insertTndmTaskComment(comment);
    }

    public int updateTndmTaskComment(TndmTaskComment comment) throws SQLException {
		comment.setIsEdited(true);
    	return this.dao.updateTndmTaskComment(comment);
    }
    
    
    // Comment attach
    public List<TndmTaskCommentAttach> searchTndmTaskCommentAttach(Map<Object, Object> arg) throws SQLException {
    	return this.dao.searchTndmTaskCommentAttach(arg);
    }

    public int insertTndmTaskCommentAttach(TndmTaskCommentAttach arg) throws SQLException {
    	return this.dao.insertTndmTaskCommentAttach(arg);
    }
    
    
    public int updateFavourite(List<Long> ids, boolean favourite){
        return dao.updateFavourite(ids, favourite);
    }
    
    
    public List searchTndmTaskChecklist(Map arg) throws SQLException{
		return dao.searchTndmTaskChecklist(arg);
	}

	public Map getTndmTaskChecklist(Long id) throws SQLException{
		return dao.getTndmTaskChecklist(id);
	}

	public int deleteTndmTaskChecklist(Long id) throws SQLException{
		return dao.deleteTndmTaskChecklist(id);
	}

	public int insertTndmTaskChecklist(TndmTaskChecklist arg) throws SQLException{
		return dao.insertTndmTaskChecklist(arg);
	}

	public int updateTndmTaskChecklist(TndmTaskChecklist arg) throws SQLException{
		return dao.updateTndmTaskChecklist(arg);
	}
      
	
	public TndmTaskMgt getTndmTaskMgt(Long taskId) throws SQLException {
    	return this.dao.getTndmTaskMgt(taskId);
    }
        
        public int countTaskNew(Map<Object, Object> arg) throws SQLException{
            return this.dao.countTaskNew(arg);
        }
}
